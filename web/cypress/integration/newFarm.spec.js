describe('Register new farm', function() {
  it('Register farm with 1 color', function() {
    const uuid = () => Cypress._.random(0, 1e6)
    const id = uuid()

    cy.visit('/login')
    cy.get('input[name=email]').type('test@test.com')
    cy.get('input[name=password]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
    cy.visit('/farm/registration')
    const farmName = `Gård-${id}`
    cy.get('input[name=farm]').type(farmName)
    cy.get('form').submit()
    cy.contains(`Gården ${farmName} ble lagt til!`, { timeout: 5000 })
  })

  it('Register farm with 2 colors', function() {
    const uuid = () => Cypress._.random(0, 1e6)
    const id = uuid()

    cy.visit('/login')
    cy.get('input[name=email]').type('test@test.com')
    cy.get('input[name=password]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
    cy.visit('/farm/registration')
    cy.get('[type="checkbox"]').check()
    const farmName = `Gård-${id}`
    cy.get('input[name=farm]').type(farmName)
    cy.get('form').submit()
    cy.contains(`Gården ${farmName} ble lagt til!`, { timeout: 5000 })
  })
})