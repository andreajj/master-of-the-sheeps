describe('register user successfully', function() {
  it('register', function() {
    const uuid = () => Cypress._.random(0, 1e6)
    const id = uuid()

    cy.visit('/register')
    cy.get('input[name=email]').type(`${id}@test.com`)
    cy.get('input[name=password]').type('Test@123')
    cy.get('input[name=passwordConfirmation]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
  })
})