describe('Farms', function() {
  it('Edit farm', function() {
    cy.visit('/login')
    cy.get('input[name=email]').type('farms@test.com')
    cy.get('input[name=password]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
    cy.visit('/farm/list')

    cy.get(`[aria-label="edit"]`).eq(0).click()
    cy.get('button:contains("Endre")').click()
    cy.get('input[name=farm]').type('Tested')
    cy.get('button:contains("Lagre")').click()
  })

  it('Delete farm', function() {
    cy.visit('/login')
    cy.get('input[name=email]').type('farms@test.com')
    cy.get('input[name=password]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
    cy.visit('/farm/list')

    cy.get(`[aria-label="delete"]`).eq(1).click()
    cy.get('button:contains("Ja")').click()
  })

  it('Check number of my farms', function() {
    cy.visit('/login')
    cy.get('input[name=email]').type('test@test.com')
    cy.get('input[name=password]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
    cy.visit('/farm/list')

    cy.contains('span', 'Test2Farger — test@test.com')
    cy.contains('span', 'TestEnFarge — test@test.com')
  })
})