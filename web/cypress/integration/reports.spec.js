describe('Reports', function() {
  it('Check number of reports', function() {
    cy.visit('/login')
    cy.get('input[name=email]').type('test@test.com')
    cy.get('input[name=password]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
    cy.visit('/reports')
    cy.url().should('eq', Cypress.config().baseUrl + '/reports')
    cy.contains('h2', 'Tidligere rapporter')
      .next()
      .children()
      .should('have.length', 2)
  })

  it('Check loading of a report', function() {
    cy.visit('/login')
    cy.get('input[name=email]').type('test@test.com')
    cy.get('input[name=password]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
    cy.visit('/reports')
    cy.url().should('eq', Cypress.config().baseUrl + '/reports')
    cy.contains('h2', 'Tidligere rapporter')
      .next()
      .children()
      .should('have.length', 2)
      .eq(1)
      .click()
    cy.contains('h2', 'Laster inn rapport... Vennligst ikke forlat siden.')
    cy.wait(10000)
    // NB: There's a problem with cypress iframe and pdf viewer so this will have to be commented out
    // until it is solved
    //
    // Issue: https://github.com/cypress-io/cypress/issues/20291
    /*cy.get('iframe')
      .its('body').then((body) => { 
        cy.wrap(body).should('contain', 'Rapport') 
      })*/
    cy.contains('button', 'Velg en annen rapport').click()
    cy.url().should('eq', Cypress.config().baseUrl + '/reports')
  })
})