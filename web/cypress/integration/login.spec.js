describe('login successfully', function() {
  it('login', function() {
    cy.visit('/login')
    cy.get('input[name=email]').type('test@test.com')
    cy.get('input[name=password]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
  })
})