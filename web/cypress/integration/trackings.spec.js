describe('Trackings', function() {
  it('Check number of trackings', function() {
    cy.visit('/login')
    cy.get('input[name=email]').type('test@test.com')
    cy.get('input[name=password]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
    cy.get('h6')
      .filter((_, elt) => { return elt.innerText.match(/^Rutebeskrivelse:\w*/) }) 
      .its('length').should('eq', 3)
  })

  it('Check loading of a tracking', function() {
    cy.visit('/login')
    cy.get('input[name=email]').type('test@test.com')
    cy.get('input[name=password]').type('Test@123')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
    cy.get('h6').contains('Rutebeskrivelse: "Elgeseter gt. til Sluppen"', { timeout: 10000 }).click()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings/cvqX0OlVqZwvb8ND9ufy')
    cy.contains('button', 'Velg en annen sporing', { timeout: 10000 }).click()
    cy.url().should('eq', Cypress.config().baseUrl + '/trackings')
  })
})