describe('Forgot password', function() {
  it('Reset password', function() {
    cy.visit('/forgot')
    cy.get('input[name=email]').type('test@test.com')
    cy.get('form').submit()
    cy.url().should('eq', Cypress.config().baseUrl + '/login')
    // It's impossible to test the actual password reset with a oobCode code as the emulator doesnt support it
  })
})