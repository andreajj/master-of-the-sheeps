#!/bin/bash

path=$( cd ../web && pwd )
echo $path
(cd $path && npm run start:server) &
(cd $path && npx wait-on http://0.0.0.0:3000 && npm run cy:run)
lsof -t -i:3000 | xargs kill -9