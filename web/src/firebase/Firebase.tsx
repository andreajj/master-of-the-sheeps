import { initializeApp } from 'firebase/app';
import { 
    getFirestore, 
    collection, 
    doc, 
    DocumentReference, 
    DocumentData, 
    getDoc, 
    getDocs, 
    setDoc, 
    query, 
    where, 
    QuerySnapshot, 
    documentId, 
    deleteDoc, 
    onSnapshot, 
    updateDoc, 
    deleteField, 
    FieldValue, 
    orderBy,
    GeoPoint,
    serverTimestamp,
    CollectionReference,
    writeBatch,
    connectFirestoreEmulator,
    initializeFirestore
} from 'firebase/firestore';
import {
    getAuth,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    reauthenticateWithCredential,
    updateEmail,
    sendPasswordResetEmail,
    updatePassword,
    getIdToken,
    EmailAuthProvider,
    UserCredential,
    AuthCredential,
    EmailAuthCredential,
    Unsubscribe,
    verifyPasswordResetCode,
    confirmPasswordReset,
    connectAuthEmulator
} from 'firebase/auth';
import { getStorage, ref, uploadBytes, getBlob, deleteObject, connectStorageEmulator } from "firebase/storage";
import 'firebase/firestore';
import firebase from "firebase/compat";
import FirestoreError = firebase.firestore.FirestoreError;
import { DeadSheep, Farm, Location, Other, Predator, SheepHerd, Tracking, Image, Part, ReportType, Report, OfflineMap } from '../types';
//import FieldPath = firebase.firestore.FieldPath;

interface User {
    uid: string;
    email: string | null;
    emailVerified: boolean;
    phoneNumber: string | null;
    photoURL: string | null;
    roles: Array<string>;
}

// Config data for firebase project
const config = {
    apiKey: 'AIzaSyDr68WfrhDZPfV-hUhqbgrx4f-oiNEMmG8',
    authDomain: 'sheep-tracker-master.firebaseapp.com',
    projectId: 'sheep-tracker-master',
    storageBucket: 'sheep-tracker-master.appspot.com',
    messagingSenderId: '198962729188',
    appId: '1:198962729188:web:fe1ca6fdcd223fa51e9cf1',
};

class Firebase {
    app;
    auth;
    db;
    storage;
    EmailAuthProvider;

    constructor() {
        // Initialize firebase app
        this.app = initializeApp(config);

        if (process.env.NODE_ENV !== "development") {
            this.auth = getAuth();
            this.db = getFirestore();
            this.storage = getStorage();
        } else {
            this.auth = getAuth();
            this.storage = getStorage();
            // Needed to make cypress work
            // source: https://stackoverflow.com/questions/59336720/cant-use-cypress-to-test-app-using-using-firestore-local-emulator
            this.db = initializeFirestore(this.app, { experimentalForceLongPolling: true });
        }

        // Connects the Firebase Firestore, Firebase Auth, and Firebase Storage
        // to the local emulator for testing under development
        // process.env.NODE_ENV can be "development", "none", "production"
        if(process.env.NODE_ENV === "development") {
            connectFirestoreEmulator(this.db, '0.0.0.0', 8080);
            connectAuthEmulator(this.auth, "http://0.0.0.0:9099");
            connectStorageEmulator(this.storage, "0.0.0.0", 9199);
        }

        this.EmailAuthProvider = EmailAuthProvider;
    }

    getApiKey: () => void = () => {
        return config.apiKey;
    };

    // Registration function with email and password
    doCreateUserWithEmailAndPassword: (email: string, password: string) => Promise<UserCredential> = (
        email,
        password,
    ) => createUserWithEmailAndPassword(this.auth, email, password);

    // Login function with email and password
    doSignInWithEmailAndPassword: (email: string, password: string) => Promise<UserCredential> = (email, password) =>
        signInWithEmailAndPassword(this.auth, email, password);

    // ReAuthenticate user with email and password
    doReauthenticateWithCredential: (credentials: AuthCredential) => Promise<UserCredential> = (credentials) => {
        const user = this.auth.currentUser;
        if (!user) {
            throw new Error('user was not logged in.');
        }
        return reauthenticateWithCredential(user, credentials);
    };

    // Create email credentials
    doCreateEmailAuthCredentials: (email: string, password: string) => EmailAuthCredential = (email, password) =>
        this.EmailAuthProvider.credential(email, password);

    // Signout function
    doSignOut: () => void = () => this.auth.signOut();

    // Email update function
    doUpdateEmail: (email: string) => Promise<void> = (email) => {
        const user = this.auth.currentUser;
        if (!user) {
            throw new Error('user was not logged in.');
        }
        return updateEmail(user, email);
    };

    // Reset function
    doPasswordReset: (email: string) => Promise<void> = (email) => sendPasswordResetEmail(this.auth, email);

    // Password update function with new password
    doPasswordUpdate: (password: string) => Promise<void> = (password) => {
        const user = this.auth.currentUser;
        if (!user) {
            throw new Error('user was not logged in.');
        }
        return updatePassword(user, password);
    };

    doVerifyPasswordResetCode: (code: string) => Promise<string> = (code) => verifyPasswordResetCode(this.auth, code);

    doConfirmPasswordReset: (code: string, newPassword: string) => Promise<void> = (code, newPassword) => confirmPasswordReset(this.auth, code, newPassword);

    // Get Id token function (forceRefresh => true or false)
    doGetIdToken: (forceRefresh: boolean) => Promise<string> = (forceRefresh) => {
        const user = this.auth.currentUser;
        if (!user) {
            throw new Error('user was not logged in.');
        }
        return getIdToken(user, forceRefresh);
    };

    // Auth listener. Fires function on auth change
    onAuthUserListener: (next: (user: User) => void, fallback: () => void) => Unsubscribe = (next, fallback) =>
        this.auth.onAuthStateChanged((authUser) => {
            if (authUser) {
                // Get user document
                // NB! TODO! Temporarly disabled as it's not needed and used reads
                /*getDoc(this.userRef(authUser.uid)).then((doc) => {
                    const dbUser = doc.data() || {};
                    // default empty roles
                    if (!dbUser.roles) {
                        dbUser.roles = [];
                    }
                    // merge auth and db user
                    const _authUser: User = {
                        uid: authUser.uid,
                        email: authUser.email,
                        emailVerified: authUser.emailVerified,
                        phoneNumber: authUser.phoneNumber,
                        photoURL: authUser.photoURL,
                        roles: dbUser.roles,
                    };

                    next(_authUser);
                });*/
                const _authUser: User = {
                    uid: authUser.uid,
                    email: authUser.email,
                    emailVerified: authUser.emailVerified,
                    phoneNumber: authUser.phoneNumber,
                    photoURL: authUser.photoURL,
                    roles: [],
                };

                next(_authUser);
            } else {
                fallback();
            }
        });

    doRegisterFarm: (farmName: string, color1: string, color2: string, twoColors: boolean) => Promise<void> = (farmName, color1, color2, twoColors) => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        } else if (!this.auth.currentUser.email) {
            throw Error("User does not have an email");
        }
        const collectionRef = collection(this.db, `farms`);
        const documentRef = doc(collectionRef, farmName);

        const newFarm: {
            color1: string,
            color2?: string,
            owner: {
                uuid: string,
                email: string,
            }
        } = {
            color1: color1,
            owner: {
                uuid: this.auth.currentUser.uid,
                email: this.auth.currentUser.email,
            }
        };

        if (twoColors) {
            newFarm.color2 = color2;
        }

        return setDoc(documentRef, newFarm);
    };

    doEditFarm: (farm: Farm, color1: string, color2: string, twoColors: boolean, newFarmName: string) => Promise<void> = (farm, color1, color2, twoColors, newFarmName) => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const collectionRef = collection(this.db, `farms`);
        const documentRef = doc(collectionRef, farm.name);
        const batch = writeBatch(this.db);
        const newDocumentRef = doc(collectionRef, newFarmName);

        const payload: {
            owner: {
                uuid: string,
                email: string
            }
            color1: string,
            color2?: string | FieldValue,
        } = {
            owner: {
                ...farm.owner,
            },
            color1: color1,
        };

        if (twoColors) {
            payload.color2 = color2;
        }

        if (farm.name === newFarmName) {
            if (!twoColors) {
                payload.color2 = deleteField();
            }
            
            batch.update(documentRef, payload);
        } else {
            batch.set(newDocumentRef, payload);
            batch.delete(documentRef);
        }
        return batch.commit();
    };

    doDeleteFarms: (farmNames: Array<string>) => Promise<void> = async (farmNames) => {
        for (const farmName of farmNames) {
            await this.doDeleteFarm(farmName);
        }
    };

    doDeleteFarm: (farmName: string) => Promise<void> = (farmName) => {
        const collectionRef = collection(this.db, `farms`);
        const documentRef = doc(collectionRef, farmName);

        return deleteDoc(documentRef);
    };

    doGetMyFarms: (onNext: (snapshot: QuerySnapshot<DocumentData>) => void, onError?: ((error: FirestoreError) => void) | undefined) => Unsubscribe = (onNext, onError) => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const collectionRef = collection(this.db, `farms`);

        const q = query(collectionRef, where("owner.uuid", "==", this.auth.currentUser.uid));

        return onSnapshot(q, onNext, onError);
    };

    doGetMyFarmsOnce: () => Promise<Farm[]> = async () => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const collectionRef = collection(this.db, `farms`);

        const q = query(collectionRef, where("owner.uuid", "==", this.auth.currentUser.uid));

        const farmDocuments = await getDocs(q);
        const farms = farmDocuments.docs.map((farmDocument) => {
            const data = farmDocument.data();
            const farm: Farm = {
                name: farmDocument.id,
                color1: data.color1,
                owner: {
                    uuid: data.owner.uuid,
                    email: data.owner.email,
                }
            };
            return farm;
        });
        return farms;
    };

    doSearchFarms: (name: string, onNext: (snapshot: QuerySnapshot<DocumentData>) => void, onError?: ((error: FirestoreError) => void) | undefined) => Unsubscribe = (name, onNext, onError) => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const collectionRef = collection(this.db, `farms`);

        const q = query(
            collectionRef,
            orderBy(documentId()),
            where(documentId(), '>=', name.toUpperCase()),
            where(documentId(), '<=', name.toLowerCase() + '\uf8ff'),
        );

        return onSnapshot(q, onNext, onError);
    };

    private getMyTrackings = async () => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const trackingsRef = collection(this.db, 'trackings');
        const q = query(
            trackingsRef,
            where("owner", "==", this.auth.currentUser.uid)
        );
        const trackingDocs = await getDocs(q);
        const trackings = trackingDocs.docs.map((trackingDoc) => {
            const data = trackingDoc.data();
            const tracking: Tracking = {
                id: trackingDoc.id,
                locations: data.locations,
                offlineMap: data.offlineMap,
                startTimestamp: data.startTimestamp,
                endTimestamp: data.endTimestamp,
                routeDescription: data.routeDescription,
                farms: data.farms,
                owner: data.owner,
                sheepHerds: [],
                predators: [],
                others: [],
                deadSheeps: [],
            };
            return tracking;
        });
        return trackings;
    };

    public getTrackings = async (from?: Date, to?: Date) => {
        const farms = await this.doGetMyFarmsOnce();
        const farmNames = [...farms.map((farm) => farm.name)];

        const trackingsRef = collection(this.db, 'trackings');

        //IMPORTANT: This the where array-contains-any query is limited to 10 values so split it into multiple queries if more than 10
        const partLength = 10;
        const numberOfParts = Math.ceil(farmNames.length / 10);
        const parts = new Array(numberOfParts);
        for (let i = 0; i < numberOfParts; i++) {
            parts[i] = (farmNames.splice(0, partLength));
        }
        
        const trackings: Tracking[] = [];

        for(const part of parts) {
            const queries = [orderBy('startTimestamp', "desc"), where('farms', 'array-contains-any', part)];
            if (from) {
                queries.push(where('startTimestamp', '>=', from));
            } else if (to) {
                queries.push(where('startTimestamp', '<=', to));
            }
            const q = query(
                trackingsRef,
                ...queries
            );
            const trackingDocs = await getDocs(q);
            const partOfTrackings = trackingDocs.docs.map((trackingDoc) => {
                const data = trackingDoc.data();
                const locations: Array<Location> = data.locations.map((geoPoint: GeoPoint) => [geoPoint.longitude, geoPoint.latitude]);
                const tracking: Tracking = {
                    id: trackingDoc.id,
                    locations: locations,
                    offlineMap: data.offlineMap,
                    startTimestamp: data.startTimestamp,
                    endTimestamp: data.endTimestamp,
                    routeDescription: data.routeDescription,
                    farms: data.farms,
                    owner: data.owner,
                    sheepHerds: [],
                    predators: [],
                    others: [],
                    deadSheeps: [],
                };
                return tracking;
            });

            trackings.push(...partOfTrackings);
        }
        
        return trackings;
    };

    getSingleTracking = async (id: string) => {
        const trackingsRef = collection(this.db, 'trackings');
        const trackingRef = doc(trackingsRef, id);
        const trackingDoc = await getDoc(trackingRef);

        if (trackingDoc.exists()) {
            const data = trackingDoc.data();
            const locations: Array<Location> = data.locations.map((geoPoint: GeoPoint) => [geoPoint.longitude, geoPoint.latitude]);
            const tracking: Tracking = {
                id: trackingDoc.id,
                locations: locations,
                offlineMap: data.offlineMap,
                startTimestamp: data.startTimestamp,
                endTimestamp: data.endTimestamp,
                routeDescription: data.routeDescription,
                farms: data.farms,
                owner: data.owner,
                sheepHerds: [],
                predators: [],
                others: [],
                deadSheeps: [],
            };
            return tracking;
        }
        throw new Error("Tracking does not exist");
    };

    private getPredators = async (trackingId: string) => {
        const trackingsRef = collection(this.db, 'trackings');
        const trackingRef = doc(trackingsRef, trackingId);
        const predatorsRef = collection(trackingRef, 'predators');
        const predators = await getDocs(predatorsRef);

        const newPredators: Array<Predator> = predators.docs.map((predator) => {
            const data = predator.data();

            const location: Location = [data.location.longitude, data.location.latitude];
            const upsertLocations: Array<Location> = data.upsertLocations.map((geoPoint: GeoPoint) => [geoPoint.longitude, geoPoint.latitude]);

            const newPredator: Predator = {
                type: data.type,
                description: data.description,
                location: location,
                upsertLocations: upsertLocations,
                id: predator.id
            };
            return newPredator;
        });
        return newPredators;
    };

    private getOthers = async (trackingId: string) => {
        const trackingsRef = collection(this.db, 'trackings');
        const trackingRef = doc(trackingsRef, trackingId);
        const othersRef = collection(trackingRef, 'others');
        const others = await getDocs(othersRef);

        const newOthers: Array<Other> = others.docs.map((other) => {
            const data = other.data();

            const location: Location = [data.location.longitude, data.location.latitude];
            const upsertLocations: Array<Location> = data.upsertLocations.map((geoPoint: GeoPoint) => [geoPoint.longitude, geoPoint.latitude]);

            const newOther: Other = {
                type: data.type,
                description: data.description,
                location: location,
                upsertLocations: upsertLocations,
                id: other.id,
            };

            return newOther;
        });
        
        return newOthers;
    };

    private getParts = async (ref: DocumentReference<DocumentData>) => {
        const partsRef = collection(ref, 'parts');
        const parts = await getDocs(partsRef);

        const newParts = parts.docs.map((part) => {
            const data = part.data();

            const newPart: Part = {
                data: data.data,
                id: part.id,
            };

            return newPart;
        });

        return newParts;
    };

    private getImages = async (ref: DocumentReference<DocumentData>) => {
        const imagesRef = collection(ref, 'images');
        const images = await getDocs(imagesRef);

        const newImages: Array<Image> = [];
        for (const image of images.docs) {
            const data = image.data();

            const parts = await this.getParts(image.ref);

            const newImage: Image = {
                parts: parts,
                numberOfParts: data.numberOfParts,
                size: data.size,
                id: image.id,
            };
            
            newImages.push(newImage);
        }

        return newImages;
    };

    private getDeadSheep = async (trackingId: string) => {
        const trackingsRef = collection(this.db, 'trackings');
        const trackingRef = doc(trackingsRef, trackingId);
        const deadhSheepsRef = collection(trackingRef, 'deadSheep');
        const deadSheeps = await getDocs(deadhSheepsRef);

        const newDeadSheeps: Array<DeadSheep> = [];
        for (const deadSheep of deadSheeps.docs) {
            const data = deadSheep.data();

            const location: Location = [data.location.longitude, data.location.latitude];
            const upsertLocations: Array<Location> = data.upsertLocations.map((geoPoint: GeoPoint) => [geoPoint.longitude, geoPoint.latitude]);
            const images = await this.getImages(deadSheep.ref);

            const newDeadSheep: DeadSheep = {
                description: data.description,
                images: images,
                location: location,
                upsertLocations: upsertLocations,
                id: deadSheep.id,
            };

            newDeadSheeps.push(newDeadSheep);
        }

        return newDeadSheeps;
    };

    private getSheepHerds = async (trackingId: string) => {
        const trackingsRef = collection(this.db, 'trackings');
        const trackingRef = doc(trackingsRef, trackingId);
        const sheepHerdsRef = collection(trackingRef, 'sheepHerds');
        const sheepHerds = await getDocs(sheepHerdsRef);

        const newSheepHerds: Array<SheepHerd> = [];

        for (const sheepHerd of sheepHerds.docs) {
            const data = sheepHerd.data();

            const location: Location = [data.location.longitude, data.location.latitude];
            const upsertLocations: Array<Location> = data.upsertLocations.map((geoPoint: GeoPoint) => [geoPoint.longitude, geoPoint.latitude]);
            const images = await this.getImages(sheepHerd.ref);

            const newSheepHerd: SheepHerd = {
                sheeps: data.sheeps,
                lambs: data.lambs,
                woundedAnimals: data.woundedAnimals,
                location: location,
                upsertLocations: upsertLocations,
                farms: data.farms,
                images: images,
                id: sheepHerd.id,
            };

            newSheepHerds.push(newSheepHerd);
        }

        return newSheepHerds;
    };

    private getTrackingsOnce: (trackings: Array<Tracking>) => Promise<Tracking[]> = async (trackings) => {
        const newTrackings: Array<Tracking> = [];
        for(const tracking of trackings) {
            const predators = await this.getPredators(tracking.id);
            const others = await this.getOthers(tracking.id);
            const deadSheeps = await this.getDeadSheep(tracking.id);
            const sheepHerds = await this.getSheepHerds(tracking.id);

            const newTracking: Tracking = {
                ...tracking,
                predators: predators,
                others: others,
                sheepHerds: sheepHerds,
                deadSheeps: deadSheeps,
            };

            newTrackings.push(newTracking);
        }

        return newTrackings;
    };

    doGetSingleTrackingOnce: (id: string) => Promise<Tracking> = async (id) => {
        const tracking = await this.getSingleTracking(id);
        if (!tracking) throw Error("Error getting the single tracking");

        const predators = await this.getPredators(tracking.id);
        const others = await this.getOthers(tracking.id);
        const deadSheeps = await this.getDeadSheep(tracking.id);
        const sheepHerds = await this.getSheepHerds(tracking.id);

        const newTracking: Tracking = {
            ...tracking,
            predators: predators,
            others: others,
            sheepHerds: sheepHerds,
            deadSheeps: deadSheeps,
        };

        return newTracking;
    };

    doGetMyTrackingsOnce: () => Promise<Tracking[]> = async () => {
        const trackings = await this.getMyTrackings();
        return this.getTrackingsOnce(trackings);
    };

    doGetTrackingsOnce: (from?: Date, to?: Date) => Promise<Tracking[]> = async (from, to) => {
        const trackings = await this.getTrackings(from, to);
        const sortedTrackings = trackings.sort((trackingA, trackingB) => trackingA.startTimestamp.toMillis() - trackingB.startTimestamp.toMillis());
        
        return this.getTrackingsOnce(sortedTrackings);
    };

    doSaveReport: (pdf: Blob, type: ReportType, seasonStart: Date, seasonEnd: Date, numberOfTrackings: number) => Promise<void> = async (pdf, type, seasonStart, seasonEnd, numberOfTrackings) => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const userRef = this.userRef(this.auth.currentUser.uid);
        const reportsRef = collection(userRef, 'reports');
        const reportRef = doc(reportsRef);

        const storageRef = ref(this.storage, `users/${this.auth.currentUser.uid}/reports/${reportRef.id}`);
        const pdfWithContentType = pdf.slice(0, pdf.size, "application/pdf");
        await uploadBytes(storageRef, pdfWithContentType);
        
        
        await setDoc(reportRef, {
            timestamp: serverTimestamp(),
            seasonStart: seasonStart,
            seasonEnd: seasonEnd,
            type: type,
            numberOfTrackings: numberOfTrackings,
            storageRef: storageRef.fullPath,
        });
    };

    doGetReports: () => Promise<Array<Report>> = async () => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const userRef = this.userRef(this.auth.currentUser.uid);
        const reportsRef = collection(userRef, 'reports');
        const queryRef = query(reportsRef, orderBy('timestamp', 'desc'));
        const reportDocs = await getDocs(queryRef);

        const reports = reportDocs.docs.map((doc) => {
            const data = doc.data();

            const report: Report = {
                timestamp: data.timestamp,
                seasonStart: data.seasonStart,
                seasonEnd: data.seasonEnd,
                type: data.type,
                numberOfTrackings: data.numberOfTrackings,
                storageRef: data.storageRef,
                id: doc.id,
            };

            return report;
        });

        return reports;
    };

    doGetReport: (id: string) => Promise<Report | undefined> = async (id) => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const userRef = this.userRef(this.auth.currentUser.uid);
        const reportsRef = collection(userRef, 'reports');
        const reportRef = doc(reportsRef, id);
        const reportDoc = await getDoc(reportRef);
        
        if (reportDoc.exists()) {
            const data = reportDoc.data();

            const report: Report = {
                timestamp: data.timestamp,
                seasonStart: data.seasonStart,
                seasonEnd: data.seasonEnd,
                type: data.type,
                numberOfTrackings: data.numberOfTrackings,
                storageRef: data.storageRef,
                id: reportDoc.id,
            };
            return report;
        } 
    };

    doDownloadReport: (storagePath: string) => Promise<Blob> = async (storagePath) => {
        const storageRef = ref(this.storage, storagePath);
        const blob = await getBlob(storageRef);
        return blob;
    };

    doDeleteReports: (reports: Array<Report>) => Promise<void> = async (reports) => {
        for (const report of reports) {
            await this.doDeleteReport(report.storageRef, report.id);
        }
    };

    doDeleteReport: (storagePath: string, reportId: string) => Promise<void> = async (storagePath, reportId) => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const storageRef = ref(this.storage, storagePath);
        const userRef = this.userRef(this.auth.currentUser.uid);
        const reportsRef = collection(userRef, 'reports');
        const reportRef = doc(reportsRef, reportId);

        await deleteDoc(reportRef);
        await deleteObject(storageRef);
    };

    doGetOfflineMaps: () => Promise<Array<OfflineMap>> = async () => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const userRef = this.userRef(this.auth.currentUser?.uid);
        const offlineMapsRef = collection(userRef, "offlineMaps");
        const offlineMapDocs = await getDocs(offlineMapsRef);
        const offlineMaps = offlineMapDocs.docs.map((offlineMapDoc) => {
            const data = offlineMapDoc.data();

            const newOfflineMap: OfflineMap = {
                name: data.name,
                styleUrl: data.styleUrl,
                minZoom: data.minZoom,
                maxZoom: data.maxZoom,
                bounds: {
                    neLng: data.bounds.neLng,
                    neLat: data.bounds.neLat,
                    swLng: data.bounds.swLng,
                    swLat: data.bounds.swLat,
                },
                id: offlineMapDoc.id,
            };
            return newOfflineMap;
        });
        return offlineMaps;
    };

    doDeleteOfflineMap: (offlineMapUuid: string) => Promise<void> = async (offlineMapUuid) => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const userRef = this.userRef(this.auth.currentUser?.uid);
        const offlineMapsRef = collection(userRef, "offlineMaps");
        const offlineMapRef = doc(offlineMapsRef, offlineMapUuid);

        return deleteDoc(offlineMapRef);
    };

    doDeleteOfflineMaps: (offlineMaps: Array<OfflineMap>) => Promise<void> = async (offlineMaps) => {
        for (const offlineMap of offlineMaps) {
            await this.doDeleteOfflineMap(offlineMap.id);
        }
    };

    doDeleteTracking: (tracking: Tracking) => Promise<void> = async (tracking) => {
        const batch = writeBatch(this.db);
        // refs
        const trackingsRef = collection(this.db, 'trackings');
        const trackingRef = doc(trackingsRef, tracking.id);
        const predatorsRef = collection(trackingRef, 'predators');
        const othersRef = collection(trackingRef, 'others');
        const deadhSheepsRef = collection(trackingRef, 'deadSheep');
        const sheepHerdsRef = collection(trackingRef, 'sheepHerds');

        // Delete sheep herds
        for (const sheepHerd of tracking.sheepHerds) {
            const sheepHerdref = doc(sheepHerdsRef, sheepHerd.id);
            batch.delete(sheepHerdref);
            // Delete images
            for (const image of sheepHerd.images) {
                const imagesRef = collection(sheepHerdref, 'images');
                const imageRef = doc(imagesRef, image.id);
                batch.delete(imageRef);
                // Delete parts
                for (const part of image.parts) {
                    const partsRef = collection(imagesRef, 'parts');
                    const partRef = doc(partsRef, part.id);
                    batch.delete(partRef);
                }
            }
        }

        // Delete others
        for (const other of tracking.others) {
            const otherRef = doc(othersRef, other.id);
            batch.delete(otherRef);
        }

        // Delete predators
        for (const predator of tracking.predators) {
            const predatorRef = doc(predatorsRef, predator.id);
            batch.delete(predatorRef);
        }

        // Delete dead sheeps
        for (const deadSheep of tracking.deadSheeps) {
            const deadSheepRef = doc(deadhSheepsRef, deadSheep.id);
            batch.delete(deadSheepRef);
            // Delete images
            for (const image of deadSheep.images) {
                const imagesRef = collection(deadSheepRef, 'images');
                const imageRef = doc(imagesRef, image.id);
                batch.delete(imageRef);
                // Delete parts
                for (const part of image.parts) {
                    const partsRef = collection(imagesRef, 'parts');
                    const partRef = doc(partsRef, part.id);
                    batch.delete(partRef);
                }
            }
        }

        // Delete tracking
        batch.delete(trackingRef);

        // Commit batch
        await batch.commit();
    };

    doDeleteTrackings: (trackings: Array<Tracking>) => Promise<void> = async (trackings) => {
        for (const tracking of trackings) {
            await this.doDeleteTracking(tracking);
        }
    };

    doDeleteAllData: () => Promise<void> = async () => {
        // Delete all trackings
        const trackings = await this.doGetMyTrackingsOnce();
        await this.doDeleteTrackings(trackings);

        // Delete all farms
        const farms = await this.doGetMyFarmsOnce();
        await this.doDeleteFarms(farms.map((farm) => farm.name));

        // Delete all reports
        const reports = await this.doGetReports();
        await this.doDeleteReports(reports);

        // Delete all offlineMaps
        const offlineMaps = await this.doGetOfflineMaps();
        await this.doDeleteOfflineMaps(offlineMaps);

        // Delete user doucment
        // User document arent used at this time
        /*if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const userRef = this.userRef(this.auth.currentUser.uid);
        await deleteDoc(userRef);*/
    };

    doDeleteAccount: () => Promise<void> = async () => {
        await this.doDeleteAllData();
        await this.auth.currentUser?.delete();
    };

    doGetMyData: () => Promise<Record<string, unknown>> = async () => {
        if (!this.auth.currentUser) {
            throw Error("User is not logged in.");
        }
        const trackings = await this.doGetMyTrackingsOnce();
        const farms = await this.doGetMyFarmsOnce();
        const reports = await this.doGetReports();
        const offlineMaps = await this.doGetOfflineMaps();
        const user = {
            uid: this.auth.currentUser.uid,
            email: this.auth.currentUser.email,
            emailVerified: this.auth.currentUser.emailVerified,
            displayName: this.auth.currentUser.displayName,
            phoneNumber: this.auth.currentUser.phoneNumber,
            photoURL: this.auth.currentUser.photoURL,
            providerData: this.auth.currentUser.providerData,
            // @ts-expect-error: it does exist
            createdAt: this.auth.currentUser.createdAt,
            // @ts-expect-error: it does exist
            lastLoginAt: this.auth.currentUser.lastLoginAt
        };

        const data = {
            trackings,
            farms,
            reports,
            offlineMaps,
            user: user,
        };
        return data;
    };

    // User document ref
    userRef: (uid: string) => DocumentReference<DocumentData> = (uid) => {
        const collectionRef = collection(this.db, `users`);
        return doc(collectionRef, uid);
    };
}

export default Firebase;
