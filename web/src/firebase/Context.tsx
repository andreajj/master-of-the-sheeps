import React, { useContext } from 'react';
import Firebase from './Firebase';

// Firebase context, can be used with useContext hook
const FirebaseContext = React.createContext<Firebase | undefined>(undefined);

export const useFirebase = () => {
    const firebaseContext = useContext(FirebaseContext);
    if (firebaseContext === undefined) {
        throw new Error('useFirebase must be used within a FirebaseContext.Provider');
    }
    return firebaseContext;
};

export default FirebaseContext;
