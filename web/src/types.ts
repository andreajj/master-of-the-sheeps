/*
 * @created 04/02/2022 - 23:08
 * @project web
 * @author andreasjj
 */
import { Timestamp } from 'firebase/firestore';

enum OtherType {
    Moose = "Elg",
    Viper = "Hoggorm",
    Dog = "Hund",
    Reindeer = "Reinsdyr",
    Other = "Annet"
}

enum PredatorType {
    Wolverine = "Jerv",
    Bear = "Bjørn",
    Wolf = "Ulv",
    Eagle = "Ørn",
    Other = "Annet"
}

enum Tie {
    None = "None",
    Red = "Red",
    Blue = "Blue",
    Yellow = "Yellow",
    Green = "Green",
}

type Location = [number, number];

interface Part {
    data: string,
    id: string,
}

interface Image {
    parts: Array<Part>,
    numberOfParts: number,
    size: number,
    id: string,
}

interface Farm {
    name: string,
    color1: string,
    color2?: string,
    owner: {
        uuid: string,
        email: string
    }
}

interface OfflineMap {
    name: string,
    styleUrl: string,
    minZoom?: number,
    maxZoom?: number,
    bounds: {
        neLng: number,
        neLat: number,
        swLng: number,
        swLat: number,
    },
    id: string,
}

interface Tracking {
    id: string,
    locations: Array<Location>, // may have caused errors somewhere, was previously array of strings
    offlineMap: OfflineMap,
    startTimestamp: Timestamp,
    endTimestamp: Timestamp,
    routeDescription?: string,
    farms: Array<string>,
    owner: string,
    sheepHerds: Array<SheepHerd>,
    predators: Array<Predator>,
    others: Array<Other>,
    deadSheeps: Array<DeadSheep>,
}

interface SheepHerd {
    sheeps: {
        [Tie.Blue]: number,
        [Tie.Green]: number,
        [Tie.None]: number,
        [Tie.Red]: number,
        [Tie.Yellow]: number,
    }
    lambs: number,
    woundedAnimals: number,
    location: Location,
    upsertLocations: Array<Location>
    farms: Array<Farm>,
    images: Array<Image>,
    id: string,
}

interface Predator {
    type: PredatorType,
    description?: string
    location: Location,
    upsertLocations: Array<Location>
    id: string,
  }
  
  interface Other {
    type: OtherType,
    description: string,
    location: Location,
    upsertLocations: Array<Location>
    id: string,
  }
  
  interface DeadSheep {
    description: string,
    images: Array<Image>
    location: Location,
    upsertLocations: Array<Location>
    id: string,
  }

  enum ReportType {
      sessongRapport = "Sessongrapport"
  }

  interface Report {
    timestamp: Timestamp
    seasonStart: Timestamp,
    seasonEnd: Timestamp,
    type: ReportType,
    numberOfTrackings: number,
    storageRef: string,
    id: string,
  }

export {
    Farm,
    OfflineMap,
    Tracking,
    Location,
    Predator,
    PredatorType,
    OtherType,
    Other,
    Tie,
    DeadSheep,
    SheepHerd,
    Image,
    Part,
    ReportType,
    Report,
};