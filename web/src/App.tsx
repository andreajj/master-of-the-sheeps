import React from 'react';
import { Router } from './router';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import FirebaseContext from './firebase/Context';
import Firebase from './firebase/Firebase';
import { initialState, StoreProvider } from './state/store';
import { CssBaseline } from '@mui/material';

export const App: React.FC = () => {
    return (
        <FirebaseContext.Provider value={new Firebase()}>
            <StoreProvider initState={initialState}>
                <Router />
                <ToastContainer
                    position="top-center"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <CssBaseline />
            </StoreProvider>
        </FirebaseContext.Provider>
    );
};

export default App;
