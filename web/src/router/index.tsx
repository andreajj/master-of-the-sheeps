import React, { useEffect, useState } from 'react';
import { Routes, Route, BrowserRouter, useNavigate, Outlet, Navigate } from 'react-router-dom';
import { LoadingSpinner } from '../components/LoadingSpinner';
import { Login } from '../components/pages/entry/login';
import { Logout } from '../components/pages/entry/logout';
import { Register } from '../components/pages/entry/register';
import { useFirebase } from '../firebase/Context';
import { loginUser, logoutUser } from '../state/reducers';
import { useStore } from '../state/store';
import Auth from './Auth';
import { FarmRegistration } from '../components/pages/farm/registration';
import { FarmList } from '../components/pages/farm/list';
import { ReportOverview } from '../components/pages/report/overview';
import { ReportPreview } from '../components/pages/report/preview';
import TrackingsView from '../components/pages/trackings/trackingsView';
import TrackingView from '../components/pages/trackings/trackingView';
import { Profile } from '../components/pages/profile';
import { ForgotPassword } from '../components/pages/entry/forgotPassword';
import { View } from '../components/pages/view';
import { ResetPassword } from '../components/pages/entry/resetPassword';

const RouterContainer: React.FC = ({ children }) => {
    const [isLoading, setIsLoading] = useState(true);
    const firebase = useFirebase();
    const [state, dispatch] = useStore();
    const loggedIn = !!state.user;

    useEffect(() => {
        if (loggedIn) {
            setIsLoading(true);
        }

        const listener = firebase.onAuthUserListener(
            (user) => {
                dispatch(loginUser(user));
                setIsLoading(false);
            },
            () => {
                dispatch(logoutUser());
                setIsLoading(false);
            },
        );

        return () => {
            listener();
        };
    }, [dispatch, firebase, loggedIn]);

    if (isLoading) {
        return <LoadingSpinner />;
    }

    return <>{children}</>;
};

export const Router: React.FC = () => {
    return (
        <BrowserRouter>
            <RouterContainer>
                <Routes>
                    <Route path="/view/:id" element={<View />} />
                    <Route element={<Auth loggedIn={false} />}>
                        <Route path="/login" element={<Login />} />
                        <Route path="/register" element={<Register />} />
                        <Route path="/forgot" element={<ForgotPassword />} />
                        <Route path="/reset" element={<ResetPassword />} />
                    </Route>
                    <Route element={<Auth loggedIn={true} />}>
                        <Route path="/" element={<Navigate to="/trackings" />} />
                        <Route path="/trackings" element={<Outlet />}>
                            <Route path="" element={<TrackingsView/>} />
                            <Route path=":id" element={<TrackingView/>} />
                        </Route>
                        <Route path="/reports" element={<Outlet />}>
                            <Route path="" element={<ReportOverview />} />
                            <Route path=":id" element={<ReportPreview />} />
                        </Route>
                        <Route path="/farm" element={<Outlet />}>
                            <Route path="registration" element={<FarmRegistration />} />
                            <Route path="list" element={<FarmList />} />
                        </Route>
                        <Route path="/profile" element={<Profile />} />
                        <Route path="/logout" element={<Logout />} />
                    </Route>
                </Routes>
            </RouterContainer>
        </BrowserRouter>
    );
};
