import { Box, Grid } from '@mui/material';
import React from 'react';
import Header from './Header';

export const Template: React.FC = ({ children }) => {
    return (
      <Box 
        sx={(theme) => ({ display: 'flex', flexDirection: 'column', minHeight: '100vh' })}
      >
        <Header />
        <Grid 
          flex={1} 
          sx={(theme) => ({
            display: 'flex', 
            padding: 2,
            maxHeight: `Calc(100vh - ${theme.mixins.toolbar.minHeight}px)`,
            [theme.breakpoints.up("sm")]: {
              // eslint-disable-next-line
              // @ts-expect-error
              maxHeight: `Calc(100vh - ${theme.mixins.toolbar['@media (min-width:600px)']['minHeight']}px)` 
            },
            /*[`${theme.breakpoints.up("xs")} and (orientation: landscape)`]: {
              // eslint-disable-next-line
              // @ts-expect-error
              maxHeight: `Calc(100vh - ${theme.mixins.toolbar['@media (min-width:0px) and (orientation: landscape)'].minHeight}px)` 
            },*/
          })}
        >
          {children}
        </Grid>
      </Box>
    );
};
