import {Box, Typography} from '@mui/material';
import React, {useState, useEffect, useRef} from 'react';
import { useParams } from 'react-router-dom';
import { useFirebase } from '../../../firebase';
import { Tracking } from '../../../types';
import Mapbox from '../trackings/Map';
import { Map } from 'mapbox-gl';

export const View: React.FC = () => {
  const firebase = useFirebase();
  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  const [tracking, setTracking] = useState<Tracking>();
  const mapRef = useRef<Map>(null);

  useEffect(() => {
    const init = async () => {
      setLoading(true);
       try {
           if(id) {
            const tracking = await firebase.getSingleTracking(id);
            setTracking(tracking);
           }
       } catch (error) {
           console.log(error);
       }
       setLoading(false);
   };
   init();
  }, [id]);

    return (
        <Box
          sx={{
            width: "100vw",
            height: "100vh",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {loading && (
            <Typography component="h1" variant="h1">
              Laster inn...
            </Typography>
          )}
          {!loading && !tracking && (
            <Typography component="h1" variant="h1">
              Bildet finnes ikke!
            </Typography>
          )}
          {!loading && tracking && (
            <Mapbox 
              selectedTracking={tracking} 
              showUserLocation={true} 
              showSheepHerds={false} 
              showPredators={false} 
              showOthers={false} 
              showDeadSheep={false}
              ref={mapRef}
              height='100vh'
              width='100vw'
            />
          )}
        </Box>
    );
};
