import React from 'react';
import { Box, Typography } from '@mui/material';

interface MapPlaceholderProps {
    text?: string
}

const MapPlaceholder: React.FC<MapPlaceholderProps> = ({text}) => {
    
    return (
           <Box sx={{ backgroundColor: 'lightgrey', height: '85vh', display: 'flex', justifyContent: 'center', alignItems: 'center', direction: 'column' }}>
                <Typography variant="h2">{text ? text : ''}</Typography>
            </Box>
       
    );
};

export default MapPlaceholder;
