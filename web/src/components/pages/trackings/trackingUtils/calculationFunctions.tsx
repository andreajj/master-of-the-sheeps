import { Tie } from "../../../../types";

export const getExpectedLambCountFromTie = (tie: Tie) => {
    switch (tie) {
        case Tie.None: {
            return 0;
        }
        case Tie.Blue: {
            return 1;
        }
        case Tie.Green: {
            return 3;
        }
        case Tie.Red: {
            return 0;
        }
        case Tie.Yellow: {
            return 2;
        }
    }
};
