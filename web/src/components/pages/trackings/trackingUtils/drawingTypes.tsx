export enum LayerTypes {
    UserLocationPoints = "userLocationPoints",
    UserLocationPointsStroke = "userLocationPointsStroke",
    UserLocationLines = "userLocationLines",
    SheepHerdObservationLines = "sheepHerdObservationLines",
    PredatorObservationLines = "predatorObservationLines",
    OtherObservationLines = "otherObservationLines",
    DeadSheepObservationLines = "deadSheepObservationLines"
}

export enum ClickableLayerTypes {
    SheepHerdPoints = "sheepHerdPoints",
    PredatorPoints = "predatorPoints",
    OtherPoints = "otherPoints",
    DeadSheepPoints = "deadSheepPoints",
}

export interface PointFeature {
    type: "Feature",
    properties: object
    geometry: {
        type: "Point",
        coordinates: [number, number]
    },
}

export type LineFeature = {
    type: "Feature";
    properties: object,
    geometry: {
      type: "LineString";
      coordinates: number[][];
    };
  };