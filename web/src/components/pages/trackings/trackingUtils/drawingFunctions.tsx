import { Tracking } from "../../../../types";
import {PointFeature, LayerTypes, LineFeature, ClickableLayerTypes} from "./drawingTypes";
import { getExpectedLambCountFromTie } from "./calculationFunctions";
import { Tie } from "../../../../types";
import { useFirebase } from '../../../../firebase';

const ORIGINAL_OBSERVATION_LINE_COLOR = "#000000";
const EDITED_OBSERVATION_LINE_COLOR = "#4b51fa";

export const createUserLocationPointsFeatureCollection = (tracking: Tracking) => {
    const locations = tracking.locations;
    if(locations.length===0) return;

    const listOfPoints: PointFeature[] = [];
    for (let i = 0; i < locations.length; i++) {
        const location = locations[i];
        const _point: PointFeature = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: location
            },
            properties: {}
        };
        listOfPoints.push(_point);
    }
    const geoJSON = {
        type: "FeatureCollection" as const,
        features: listOfPoints
    };
    return geoJSON;
};

export const createUserLocationLinesFeature = (tracking: Tracking) => {
    const locations = tracking.locations;
    if(locations.length < 2) return;

    const lineFeature: LineFeature = {
        type: "Feature",
        geometry: {
            type: "LineString",
            coordinates: locations
        },
        properties: {}
    };
    return lineFeature;
};

const createSheepherdObservationLinesFeatureCollection = (tracking: Tracking) => {
    const {sheepHerds} = tracking;
    if(!sheepHerds.length) return undefined;

    const sheepObservationLinesFeaturesList: LineFeature[] = [];

    // lines for when observation first created
    for (let i = 0; i < sheepHerds.length; i++) {
        const sheepHerd = sheepHerds[i];
        const line: LineFeature = {
            type: "Feature" as const,
            properties: {
                edit: false
            },
            geometry: {
                type: "LineString",
                coordinates: [sheepHerd.location, sheepHerd.upsertLocations[0]]
            },
        };
        sheepObservationLinesFeaturesList.push(line);
    }

    // lines for any subsequent edits on observation
    for (let i = 0; i < sheepHerds.length; i++) { 
        const sheepHerd = sheepHerds[i];
        for (let j = 0; j < sheepHerd.upsertLocations.length; j++) {
            const currentEditLocation = sheepHerd.upsertLocations[j];
            if(j===0) {
                continue;
            } else {
                const editedLine: LineFeature = {
                    type: "Feature" as const,
                    properties: {
                        edit: true
                    },
                    geometry: {
                        type: "LineString",
                        coordinates: [sheepHerd.location, currentEditLocation]
                    },
                };
                sheepObservationLinesFeaturesList.push(editedLine);
            }
        }
    }

    const sheepHerdObservationLinesFeatureCollection: GeoJSON.FeatureCollection = {
        type: "FeatureCollection",
        features: sheepObservationLinesFeaturesList,
    };
    return sheepHerdObservationLinesFeatureCollection;
};

const createPredatorObservationLinesFeatureCollection = (tracking: Tracking) => {
    const {predators} = tracking;
    if(!predators.length) return undefined;

    const predatorObservationLinesFeaturesList: LineFeature[] = [];

    // lines for when observation first created
    for (let i = 0; i < predators.length; i++) {
        const predator = predators[i];
        const line: LineFeature = {
            type: "Feature" as const,
            properties: {
                edit: false
            },
            geometry: {
                type: "LineString",
                coordinates: [predator.location, predator.upsertLocations[0]]
            },
        };
        predatorObservationLinesFeaturesList.push(line);
    };

    // lines for any subsequent edits on observation
    for (let i = 0; i < predators.length; i++) { 
        const predator = predators[i];
        for (let j = 0; j < predator.upsertLocations.length; j++) {
            const currentEditLocation = predator.upsertLocations[j];
            if(j===0) {
                continue;
            } else {
                const editedLine: LineFeature = {
                    type: "Feature" as const,
                    properties: {
                        edit: true
                    },
                    geometry: {
                        type: "LineString",
                        coordinates: [predator.location, currentEditLocation]
                    },
                };
                predatorObservationLinesFeaturesList.push(editedLine);
            }
        }
    }


    const predatorObservationLinesFeatureCollection: GeoJSON.FeatureCollection = {
        type: "FeatureCollection",
        features: predatorObservationLinesFeaturesList,
    };
    return predatorObservationLinesFeatureCollection;
};

const createOtherObservationLinesFeatureCollection = (tracking: Tracking) => {
    const {others} = tracking;
    if(!others.length) return undefined;

    const otherObservationLinesFeaturesList: LineFeature[] = [];

    // lines for when observation first created
    for (let i = 0; i < others.length; i++) {
        const other = others[i];
        const line: LineFeature = {
            type: "Feature" as const,
            properties: {
                edit: false
            },
            geometry: {
                type: "LineString",
                coordinates: [other.location, other.upsertLocations[0]]
            },
        };
        otherObservationLinesFeaturesList.push(line);
    }

    // lines for any subsequent edits on observation
    for (let i = 0; i < others.length; i++) { 
        const other = others[i];
        for (let j = 0; j < other.upsertLocations.length; j++) {
            const currentEditLocation = other.upsertLocations[j];
            if(j===0) {
                continue;
            } else {
                const editedLine: LineFeature = {
                    type: "Feature" as const,
                    properties: {
                        edit: true
                    },
                    geometry: {
                        type: "LineString",
                        coordinates: [other.location, currentEditLocation]
                    },
                };
                otherObservationLinesFeaturesList.push(editedLine);
            }
        }
    }

    const otherObservationLinesFeatureCollection: GeoJSON.FeatureCollection = {
        type: "FeatureCollection",
        features: otherObservationLinesFeaturesList,
    };
    return otherObservationLinesFeatureCollection;
};

const createDeadSheepObservationLinesFeatureCollection = (tracking: Tracking) => {
    const {deadSheeps} = tracking;
    if(!deadSheeps.length) return undefined;

    const deadSheepObservationLinesFeaturesList: LineFeature[] = [];

    // lines for when observation first created
    for (let i = 0; i < deadSheeps.length; i++) {
        const deadSheep = deadSheeps[i];
        const line: LineFeature = {
            type: "Feature" as const,
            properties: {
                edit: false
            },
            geometry: {
                type: "LineString",
                coordinates: [deadSheep.location, deadSheep.upsertLocations[0]]
            },
        };
        deadSheepObservationLinesFeaturesList.push(line);
    };

    // lines for any subsequent edits on observation
    for (let i = 0; i < deadSheeps.length; i++) { 
        const deadSheep = deadSheeps[i];
        for (let j = 0; j < deadSheep.upsertLocations.length; j++) {
            const currentEditLocation = deadSheep.upsertLocations[j];
            if(j===0) {
                continue;
            } else {
                const editedLine: LineFeature = {
                    type: "Feature" as const,
                    properties: {
                        edit: true
                    },
                    geometry: {
                        type: "LineString",
                        coordinates: [deadSheep.location, currentEditLocation]
                    },
                };
                deadSheepObservationLinesFeaturesList.push(editedLine);
            }
        }
    };

    const deadSheepObservationLinesFeatureCollection: GeoJSON.FeatureCollection = {
        type: "FeatureCollection",
        features: deadSheepObservationLinesFeaturesList,
    };
    return deadSheepObservationLinesFeatureCollection;
};


// sheepherd feature collection
export const createSheepHerdPointsFeatureCollection = (tracking: Tracking, userEmail: string | null | undefined) => {
    const sheepHerds = tracking.sheepHerds;
    if(sheepHerds.length===0) return;

    const listOfPoints: PointFeature[] = [];
    for (let i = 0; i < sheepHerds.length; i++) {
        const sheepHerd = sheepHerds[i];
        const adultCount = Object.values(sheepHerd.sheeps).reduce((prev, cur) => prev + cur, 0);
        const expectedLambs = Object.entries(sheepHerd.sheeps).reduce((prev, [tie, count]) => prev + getExpectedLambCountFromTie(tie as Tie) * count , 0);
        const _point: PointFeature = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: sheepHerd.location
            },
            properties: {
                adultCount: adultCount,
                id: sheepHerd.id,
                woundedCount: sheepHerd.woundedAnimals,
                lambsCount: sheepHerd.lambs,
                expectedLambsCount: expectedLambs,
                hasMyFarm: userEmail ? (sheepHerd.farms.filter((farm) => farm.owner.email == userEmail).length > 0) : false,
                hasImages: (sheepHerd.images.length > 0)
            }
        };
        listOfPoints.push(_point);
    }
    const geoJSON = {
        type: "FeatureCollection" as const,
        features: listOfPoints
    };
    return geoJSON;
};

// predator features
export const createPredatorPointsFeatureCollection = (tracking: Tracking) => {
    const predators = tracking.predators;
    if(predators.length===0) return;

    const listOfPoints: PointFeature[] = [];
    for (let i = 0; i < predators.length; i++) {
        const predator = predators[i];
        const _point: PointFeature = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: predator.location
            },
            properties: {
                type: predator.type,
                description: predator.description ?? ''
            }
        };
        listOfPoints.push(_point);
    }
    const geoJSON = {
        type: "FeatureCollection" as const,
        features: listOfPoints
    };
    return geoJSON;
};

// other features
export const createOtherPointsFeatureCollection = (tracking: Tracking) => {
    const others = tracking.others;
    if(others.length===0) return;

    const listOfPoints: PointFeature[] = [];
    for (let i = 0; i < others.length; i++) {
        const other = others[i];
        const _point: PointFeature = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: other.location
            },
            properties: {
                type: other.type,
                description: other.description
            }
        };
        listOfPoints.push(_point);
    }
    const geoJSON = {
        type: "FeatureCollection" as const,
        features: listOfPoints
    };
    return geoJSON;
};

// other features
export const createDeadSheepPointsFeatureCollection = (tracking: Tracking) => {
    const deadSheeps = tracking.deadSheeps;
    if(deadSheeps.length===0) return;

    const listOfPoints: PointFeature[] = [];
    for (let i = 0; i < deadSheeps.length; i++) {
        const deadSheep = deadSheeps[i];
        const _point: PointFeature = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: deadSheep.location
            },
            properties: {
                description: deadSheep.description,
                id: deadSheep.id,
                hasImages: (deadSheep.images.length > 0)
            }
        };
        listOfPoints.push(_point);
    }
    const geoJSON = {
        type: "FeatureCollection" as const,
        features: listOfPoints
    };
    return geoJSON;
};

export const drawUserLocationPoints = (map: React.RefObject<mapboxgl.Map>, tracking: Tracking) => {
    if(createUserLocationPointsFeatureCollection(tracking) === undefined) return;

    // user location points source
    map.current?.addSource(`source-${tracking.id}-${LayerTypes.UserLocationPoints}`, {
        type: 'geojson',
        data: createUserLocationPointsFeatureCollection(tracking),
    });

    // user location lines source
    map.current?.addSource(`source-${tracking.id}-${LayerTypes.UserLocationLines}`, {
        type: 'geojson',
        data: createUserLocationLinesFeature(tracking),
    });

    // user location lines
    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.UserLocationLines}`,
        type: 'line',
        source: `source-${tracking.id}-${LayerTypes.UserLocationLines}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "line-width": 2,
            "line-color": '#FF0000' // red color
        }
    });

    // user location point stroke (must be rendered before main points)
    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.UserLocationPointsStroke}`,
        type: 'circle',
        source: `source-${tracking.id}-${LayerTypes.UserLocationPoints}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "circle-radius": 7,
            "circle-color": '#ffffff' // red color
        }
    });

    // user location point
    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.UserLocationPoints}`,
        type: 'circle',
        source: `source-${tracking.id}-${LayerTypes.UserLocationPoints}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "circle-radius": 5,
            "circle-color": '#21609A' // red color
        }
    });
    
};

export const drawSheepHerdSymbols = (map: React.RefObject<mapboxgl.Map>, tracking: Tracking, userEmail: string | undefined | null) => {
    if(createSheepHerdPointsFeatureCollection(tracking, userEmail) === undefined) return;

    map.current?.addSource(`source-${tracking.id}-${ClickableLayerTypes.SheepHerdPoints}`, {
        type: 'geojson',
        data: createSheepHerdPointsFeatureCollection(tracking, userEmail)
    });   
    
    map.current?.addLayer({
        id: `layer-${tracking.id}-${ClickableLayerTypes.SheepHerdPoints}`,
        type: 'symbol',
        source: `source-${tracking.id}-${ClickableLayerTypes.SheepHerdPoints}`,
        layout: {
            visibility: 'visible',
            "icon-image": "sheep",
            "icon-size": 1,
            "text-field": "Saueflokk",
            "text-anchor": 'top',
            "text-offset": [0, 1]
        },
        paint: {
            "icon-opacity": 1,
            "text-opacity": 1
        },
        filter: ["==", ["get", "hasMyFarm"], true]
    });

    map.current?.addLayer({
        id: `layer-${tracking.id}-${ClickableLayerTypes.SheepHerdPoints}-myFarmNotRepresented`,
        type: 'symbol',
        source: `source-${tracking.id}-${ClickableLayerTypes.SheepHerdPoints}`,
        layout: {
            visibility: 'visible',
            "icon-image": "sheep",
            "icon-size": 1,
            "text-field": "Saueflokk",
            "text-anchor": 'top',
            "text-offset": [0, 1]
        },
        paint: {
            "icon-opacity": 0.5,
            "text-opacity": 0.5
        },
        filter: ["==", ["get", "hasMyFarm"], false]
    });
};

export const drawPredatorSymbols = (map: React.RefObject<mapboxgl.Map>, tracking: Tracking) => {
    if(createPredatorPointsFeatureCollection(tracking) === undefined) return;

    map.current?.addSource(`source-${tracking.id}-${ClickableLayerTypes.PredatorPoints}`, {
        type: 'geojson',
        data: createPredatorPointsFeatureCollection(tracking)
    });   
    
    map.current?.addLayer({
        id: `layer-${tracking.id}-${ClickableLayerTypes.PredatorPoints}`,
        type: 'symbol',
        source: `source-${tracking.id}-${ClickableLayerTypes.PredatorPoints}`,
        layout: {
            visibility: 'visible',
            "icon-image": "paw",
            "icon-size": 1,
            "text-field": ["get", "type"],
            "text-anchor": 'top',
            "text-offset": [0, 1]
        },
    });
};

export const drawOtherSymbols = (map: React.RefObject<mapboxgl.Map>, tracking: Tracking) => {
    if(createOtherPointsFeatureCollection(tracking) === undefined) return;

    map.current?.addSource(`source-${tracking.id}-${ClickableLayerTypes.OtherPoints}`, {
        type: 'geojson',
        data: createOtherPointsFeatureCollection(tracking)
    });   

    map.current?.addLayer({
        id: `layer-${tracking.id}-${ClickableLayerTypes.OtherPoints}`,
        type: 'symbol',
        source: `source-${tracking.id}-${ClickableLayerTypes.OtherPoints}`,
        layout: {
            visibility: 'visible',
            "icon-image": "other",
            "icon-size": 1,
            "text-field": ["get", "type"],
            "text-anchor": 'top',
            "text-offset": [0, 1]
        },
    });
};

export const drawDeadSheepSymbols = (map: React.RefObject<mapboxgl.Map>, tracking: Tracking) => {
    if(createDeadSheepPointsFeatureCollection(tracking) === undefined) return;

    map.current?.addSource(`source-${tracking.id}-${ClickableLayerTypes.DeadSheepPoints}`, {
        type: 'geojson',
        data: createDeadSheepPointsFeatureCollection(tracking)
    });   
    
    map.current?.addLayer({
        id: `layer-${tracking.id}-${ClickableLayerTypes.DeadSheepPoints}`,
        type: 'symbol',
        source: `source-${tracking.id}-${ClickableLayerTypes.DeadSheepPoints}`,
        layout: {
            visibility: 'visible',
            "icon-image": "skullCrossbones",
            "icon-size": 1,
            "text-field": "Død sau",
            "text-anchor": 'top',
            "text-offset": [0, 1]
        },
    });
};

export const drawSheepHerdObservationLines = (map: React.RefObject<mapboxgl.Map>, tracking: Tracking) => {
    if(createSheepherdObservationLinesFeatureCollection(tracking) === undefined) return;

    map.current?.addSource(`source-${tracking.id}-${LayerTypes.SheepHerdObservationLines}`, {
        type: 'geojson',
        data: createSheepherdObservationLinesFeatureCollection(tracking),
    });

    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.SheepHerdObservationLines}`,
        type: 'line',
        source: `source-${tracking.id}-${LayerTypes.SheepHerdObservationLines}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "line-width": 2,
            "line-color": EDITED_OBSERVATION_LINE_COLOR, // observation line color
            "line-dasharray": [3,2],
        },
        filter: ["==", ["get", "edit"], true]
    });

    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.SheepHerdObservationLines}-edited`,
        type: 'line',
        source: `source-${tracking.id}-${LayerTypes.SheepHerdObservationLines}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "line-width": 2,
            "line-color": ORIGINAL_OBSERVATION_LINE_COLOR , // edited observation line color
            "line-dasharray": [2,2],
        },
        filter: ["==", ["get", "edit"], false]
    });

};

export const drawPredatorObservationLines = (map: React.RefObject<mapboxgl.Map>, tracking: Tracking) => {
    if(createPredatorObservationLinesFeatureCollection(tracking) ===  undefined) return;

    map.current?.addSource(`source-${tracking.id}-${LayerTypes.PredatorObservationLines}`, {
        type: 'geojson',
        data: createPredatorObservationLinesFeatureCollection(tracking),
    });

    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.PredatorObservationLines}`,
        type: 'line',
        source: `source-${tracking.id}-${LayerTypes.PredatorObservationLines}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "line-width": 2,
            "line-color": EDITED_OBSERVATION_LINE_COLOR, // observation line color
            "line-dasharray": [3,2],
        },
        filter: ["==", ["get", "edit"], true]
    });

    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.PredatorObservationLines}-edited`,
        type: 'line',
        source: `source-${tracking.id}-${LayerTypes.PredatorObservationLines}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "line-width": 2,
            "line-color": ORIGINAL_OBSERVATION_LINE_COLOR , // edited observation line color
            "line-dasharray": [2,2],
        },
        filter: ["==", ["get", "edit"], false]
    });

};

export const drawOtherObservationLines = (map: React.RefObject<mapboxgl.Map>, tracking: Tracking) => {
    if(createOtherObservationLinesFeatureCollection(tracking) === undefined) return;

    map.current?.addSource(`source-${tracking.id}-${LayerTypes.OtherObservationLines}`, {
        type: 'geojson',
        data: createOtherObservationLinesFeatureCollection(tracking),
    });

    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.OtherObservationLines}`,
        type: 'line',
        source: `source-${tracking.id}-${LayerTypes.OtherObservationLines}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "line-width": 2,
            "line-color": EDITED_OBSERVATION_LINE_COLOR, // observation line color
            "line-dasharray": [3,2],
        },
        filter: ["==", ["get", "edit"], true]
    });

    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.OtherObservationLines}-edited`,
        type: 'line',
        source: `source-${tracking.id}-${LayerTypes.OtherObservationLines}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "line-width": 2,
            "line-color": ORIGINAL_OBSERVATION_LINE_COLOR, // observation line color
            "line-dasharray": [2,2],
        },
        filter: ["==", ["get", "edit"], false]
    });

};

export const drawDeadSheepObservationLines = (map: React.RefObject<mapboxgl.Map>, tracking: Tracking) => {
    if(createDeadSheepObservationLinesFeatureCollection(tracking) === undefined) return;

    map.current?.addSource(`source-${tracking.id}-${LayerTypes.DeadSheepObservationLines}`, {
        type: 'geojson',
        data: createDeadSheepObservationLinesFeatureCollection(tracking),
    });

    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.DeadSheepObservationLines}`,
        type: 'line',
        source: `source-${tracking.id}-${LayerTypes.DeadSheepObservationLines}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "line-width": 2,
            "line-color": EDITED_OBSERVATION_LINE_COLOR, // observation line color
            "line-dasharray": [3,2],
        },
        filter: ["==", ["get", "edit"], true]
    });

    map.current?.addLayer({
        id: `layer-${tracking.id}-${LayerTypes.DeadSheepObservationLines}-edited`,
        type: 'line',
        source: `source-${tracking.id}-${LayerTypes.DeadSheepObservationLines}`,
        layout: {
            visibility: 'visible'
        },
        paint: {
            "line-width": 2,
            "line-color": ORIGINAL_OBSERVATION_LINE_COLOR, // observation line color
            "line-dasharray": [2,2],
        },
        filter: ["==", ["get", "edit"], false]
    });

};

/**
 * Toggles visibility of a specific layer. If the layer is an observation line layer, it turns off the associated edited observation line layer also.
 * @param show whether to turn the layer on or off
 * @param map map object to perform the layer toggling on
 * @param trackingID id of current tracking object, to target the correct layer
 * @param postfix - enum of layer type
 * @returns void
 */
 export const toggleLayerVisibility = (show: boolean, map: React.RefObject<mapboxgl.Map>,  trackingID: string, postfix: LayerTypes | ClickableLayerTypes) => {
    if(!map.current) return;
    const layerName = `layer-${trackingID}-${postfix}`;
    if(map.current.getLayer(layerName)===undefined) return;
    if(map.current?.getLayoutProperty(layerName, "visibility") === undefined) return;
    
    const isObservationLineLayer = layerName.includes("ObservationLines");
    if(show) {
        map.current?.setLayoutProperty(layerName, 'visibility', 'visible');
        if(isObservationLineLayer) {
            map.current?.setLayoutProperty(`${layerName}-edited`, 'visibility', 'visible');
        }
        // toggle the sheepherd symbols for sheepherds that don't have the user's farms
        else if(layerName.includes("sheepHerdPoints")) {
            map.current?.setLayoutProperty(`${layerName}-myFarmNotRepresented`, 'visibility', 'visible');
        };
    } else {
        map.current?.setLayoutProperty(layerName, 'visibility', 'none');
        if(isObservationLineLayer) {
            map.current?.setLayoutProperty(`${layerName}-edited`, 'visibility', 'none');
        }
        // toggle the sheepherd symbols for sheepherds that don't have the user's farms
        else if(layerName.includes("sheepHerdPoints")) {
            map.current?.setLayoutProperty(`${layerName}-myFarmNotRepresented`, 'visibility', 'none');
        };
    }
};