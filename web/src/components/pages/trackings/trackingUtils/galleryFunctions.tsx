import { DeadSheep, SheepHerd } from "../../../../types";

const getAllImageStringsFromObsList = (observationWithImageList: DeadSheep[] | SheepHerd[]): string[] => {
    const listOfImageStrings: string[] = [];
    for (let i = 0; i < observationWithImageList.length; i++) {
        const images = observationWithImageList[i].images;
        for (let j = 0; j < images.length; j++) {
            const image = images[j];
            const listOfPartsSorted = image.parts.sort((a, b) => Number(a.id) - Number(b.id));
            let resultString = "";
            listOfPartsSorted.forEach((part) => {
                resultString += part.data;
            });
            listOfImageStrings.push(resultString);

        }
    }
    return listOfImageStrings;
};

/**
 * 
 * @param observationWithImageList 
 * @returns ImageString[][]
 */
const getListOfImageStringsList = (observationWithImageList: DeadSheep[] | SheepHerd[]): string[][] => {
    const listOfImageStrings: string[][] = [[]];
    for (let i = 0; i < observationWithImageList.length; i++) {
        const currentObs = observationWithImageList[i];
        const imageStringsFromCurObs = getAllImageStringsFromObs(currentObs);
        listOfImageStrings.push(imageStringsFromCurObs);
    }
    return listOfImageStrings;
};


const getAllImageStringsFromObs = (observationWithImage: DeadSheep | SheepHerd ): string[] => {
    if(observationWithImage.images===undefined) return [];
    const listOfImageStrings: string[] = [];
    const images = observationWithImage.images;
    if(images.length===0) return [];
    for (let j = 0; j < images.length; j++) {
        const image = images[j];
        const listOfPartsSorted = image.parts.sort((a, b) => Number(a.id) - Number(b.id));
        let resultString = "";
        listOfPartsSorted.forEach((part) => {
            resultString += part.data;
        });
        listOfImageStrings.push(resultString);
    }
    return listOfImageStrings;
};


export {
    getAllImageStringsFromObs,
    getAllImageStringsFromObsList,
    getListOfImageStringsList
};