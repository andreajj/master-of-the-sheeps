import React from 'react';
import { Button, Box, Card } from '@mui/material';
import ArrowBack from '@mui/icons-material/ArrowBack';
import {Tracking} from '../../../../types';
import { useNavigate } from 'react-router-dom';

interface BackButtonProps {
    setSelectedTracking: React.Dispatch<React.SetStateAction<Tracking | undefined>>,
    id: string,
}

const BackButton: React.FC<BackButtonProps> = ({setSelectedTracking, id}) => {

    const navigate = useNavigate();

    const handleClick = () => {
        setSelectedTracking(undefined);
        navigate("/trackings");
    };

    return (
    <Card elevation={5} >
            <Box display={"flex"} justifyContent={"center"} alignItems={"center"} >
                <Button onClick={handleClick} sx={{width: "100%"}} variant="text" startIcon={<ArrowBack/>} >Velg en annen sporing</Button>
            </Box>

    </Card>
    );
};

export default BackButton;