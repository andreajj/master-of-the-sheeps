import React from 'react';
import { Typography, Box, Switch, Tooltip } from '@mui/material';
import { InfoContainer } from '.';
import { Tracking } from '../../../../types';

interface FiltersProps {
    selectedTracking: Tracking,
    showUserLocation: boolean,
    setShowUserLocation: React.Dispatch<React.SetStateAction<boolean>>,
    showSheepHerds: boolean,
    setShowSheepHerds: React.Dispatch<React.SetStateAction<boolean>>,
    showPredators: boolean,
    showOthers: boolean,
    showDeadSheep: boolean,
    setShowPredators: React.Dispatch<React.SetStateAction<boolean>>,
    setShowOthers: React.Dispatch<React.SetStateAction<boolean>>,
    setshowDeadSheep: React.Dispatch<React.SetStateAction<boolean>>,
}

const textFromBool = (value: boolean): string => {
    if(!value) {
        return "på";
    } else {
        return "av";
    }
};

const Filters: React.FC<FiltersProps> = ({
    selectedTracking,
    showUserLocation, 
    setShowUserLocation, 
    showSheepHerds, 
    setShowSheepHerds,
    showPredators,
    showOthers,
    showDeadSheep,
    setShowPredators,
    setShowOthers,
    setshowDeadSheep
}) => {

    const hasSheepHerds = !!selectedTracking.sheepHerds.length;
    const hasPredators = !!selectedTracking.predators.length;
    const hasOthers = !!selectedTracking.others.length;
    const hasDeadSheep = !!selectedTracking.deadSheeps.length;

  return (
    <Box>
        <Typography sx={{marginTop: 3}} variant="h5" >Filtre</Typography>
        {hasSheepHerds && <InfoContainer>
            <Typography variant="subtitle1">Saueflokker</Typography>
            <Tooltip placement="right" title={`Skru ${textFromBool(showSheepHerds)} visning av saueflokker`}>
                <Switch defaultChecked={true} value={showSheepHerds} onChange={(event) => setShowSheepHerds(event.target.checked)} />
            </Tooltip>
        </InfoContainer>}
        {hasPredators && <InfoContainer>
            <Typography variant="subtitle1">Rovdyr</Typography>
            <Tooltip placement="right" title={`Skru ${textFromBool(showPredators)} visning av rovdyr`}>
                <Switch defaultChecked={true} value={showPredators} onChange={(event) => setShowPredators(event.target.checked)} />
            </Tooltip>
        </InfoContainer>}
        {hasOthers && <InfoContainer>
            <Typography variant="subtitle1">Annet</Typography>
            <Tooltip placement="right" title={`Skru ${textFromBool(showOthers)} visning av annet`}>
                <Switch defaultChecked={true} value={showOthers} onChange={(event) => setShowOthers(event.target.checked)} />
            </Tooltip>
        </InfoContainer>}
        {hasDeadSheep && <InfoContainer>
            <Typography variant="subtitle1">Død sau</Typography>
            <Tooltip placement="right" title={`Skru ${textFromBool(showDeadSheep)} visning av døde sauer`}>
                <Switch defaultChecked={true} value={showDeadSheep} onChange={(event) => setshowDeadSheep(event.target.checked)} />
            </Tooltip>
        </InfoContainer>}
        <InfoContainer>
            <Typography variant="subtitle1">Brukerlokasjoner</Typography>
            <Tooltip placement="right" title={`Skru ${textFromBool(showUserLocation)} visning av brukerlokasjoner`}>
                <Switch defaultChecked={true} value={showUserLocation} onChange={(event) => setShowUserLocation(event.target.checked)} />
            </Tooltip>
        </InfoContainer>
    </Box>
  );
};

export default Filters;