import React, {useRef, forwardRef, useEffect } from 'react';
import { Paper, Box, Divider } from '@mui/material';
import BackButton from './BackButton';
import { Tracking, SheepHerd, DeadSheep } from '../../../../types';
import {styled} from '@mui/system';
import Filters from './Filters';
import TrackingStats from './TrackingStats';
import Gallery from './images/Gallery';
import ImagesList from './images/ImagesList';

export const InfoContainer = styled(Box, {})({
    display: "flex",
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
});

interface TrackingInfoProps {
    selectedTracking: Tracking,
    setSelectedTracking: React.Dispatch<React.SetStateAction<Tracking | undefined>>,
    showUserLocation: boolean,
    setShowUserLocation: React.Dispatch<React.SetStateAction<boolean>>,
    showSheepHerds: boolean,
    setShowSheepHerds: React.Dispatch<React.SetStateAction<boolean>>,
    showPredators: boolean,
    showOthers: boolean,
    showDeadSheep: boolean,
    setShowPredators: React.Dispatch<React.SetStateAction<boolean>>,
    setShowOthers: React.Dispatch<React.SetStateAction<boolean>>,
    setshowDeadSheep: React.Dispatch<React.SetStateAction<boolean>>,
    selectedObservation: SheepHerd | DeadSheep | undefined,
    setSelectedObservation: React.Dispatch<React.SetStateAction<SheepHerd | DeadSheep | undefined>>
    clickedId: string,
}

const TrackingInfo = forwardRef<any, TrackingInfoProps>(({
    selectedTracking, 
    setSelectedTracking, 
    showUserLocation, 
    setShowUserLocation, 
    showSheepHerds, 
    setShowSheepHerds,
    showPredators,
    showOthers,
    showDeadSheep,
    setShowPredators,
    setShowOthers,
    setshowDeadSheep,
    selectedObservation,
    setSelectedObservation,
    clickedId
    }, mapRef) => {

        // get a list of all observations (sheepHerd, deadSheep) that contain an image
        const dsWithImages = selectedTracking.deadSheeps.filter((deadSheep) => deadSheep.images.length>0);
        const shWithImages = selectedTracking.sheepHerds.filter((sheepHerd) => sheepHerd.images.length>0);
        const showImagesSection = (dsWithImages.length>0 || shWithImages.length > 0);
        const dateJSON = selectedTracking.startTimestamp.toDate().toJSON().slice(0,10);

        const findObsFromId = (id: string) => {
            if(id==="") return;
            const result = dsWithImages.find((obs) => obs.id===id) ?? shWithImages.find((obs) => obs.id===id);
            return result;
        };

        const lgRef = useRef<any>(null);

        useEffect(() => {
          if(clickedId==="") return;
          const obs = findObsFromId(clickedId);
          setSelectedObservation(obs);
        }, [clickedId]);
        

        return (
        <>
        <Box>
            <BackButton id={selectedTracking.id} setSelectedTracking={setSelectedTracking} /> 
        </Box>
            <Paper sx={{height: "80vh", overflow: 'auto', padding: 2, marginTop: 2}} elevation={2}>
                <TrackingStats selectedTracking={selectedTracking}/>
                <Divider />
                {showImagesSection && <ImagesList date={dateJSON} sheepObsList={shWithImages} deadObsList={dsWithImages} ref={mapRef} 
                    setSelectedObservation={setSelectedObservation} />}
                {selectedObservation && showImagesSection && <Gallery selectedObservation={selectedObservation} ref={lgRef} 
                    setSelectedObservation={setSelectedObservation} /> }
                <Filters
                selectedTracking={selectedTracking}
                showUserLocation={showUserLocation}
                setShowUserLocation={setShowUserLocation}
                showSheepHerds={showSheepHerds}
                setShowSheepHerds={setShowSheepHerds}
                showPredators={showPredators}
                showOthers={showOthers}
                showDeadSheep={showDeadSheep}
                setShowPredators={setShowPredators}
                setShowOthers={setShowOthers}
                setshowDeadSheep ={setshowDeadSheep}/>
            </Paper>
        </>

        );
});

TrackingInfo.displayName = "TrackingInfo";

export default TrackingInfo;