import React, { useCallback, useEffect, forwardRef} from 'react';
import LightGallery from 'lightgallery/react';
import { GalleryItem } from 'lightgallery/lg-utils';
import { getAllImageStringsFromObs } from '../../trackingUtils/galleryFunctions';

import 'lightgallery/css/lightgallery.css';
import 'lightgallery/css/lg-zoom.css';
import 'lightgallery/css/lg-thumbnail.css';

import lgThumbnail from 'lightgallery/plugins/thumbnail';
import lgZoom from 'lightgallery/plugins/zoom';
import { DeadSheep, SheepHerd } from '../../../../../types';

interface GalleryProps {
    selectedObservation: SheepHerd | DeadSheep,
    setSelectedObservation:  React.Dispatch<React.SetStateAction<SheepHerd | DeadSheep | undefined>>
}

const Gallery = forwardRef<any, GalleryProps>(({selectedObservation, setSelectedObservation}, ref) => {

  const imageStrings = getAllImageStringsFromObs(selectedObservation);
  const isSheepHerd = ("lambs" in selectedObservation) ? true : false;

  const onInit = useCallback((detail) => {
    if(!ref) return;
    if (detail) {
      (ref as any).current = detail.instance;
    }
  }, []);

  useEffect(() => {
    if(!ref) return;
    (ref as any).current.openGallery();
    return () => {
      handleClose();
    };
  }, []);

  const handleClose = () => {
    setSelectedObservation(undefined);
  };


  const setDynamicImages = () => {
    const list: GalleryItem[] = [];
    if(imageStrings.length===0) return;
    imageStrings.map((image, i) => (
      list.push({
        src: `data:image/jpeg;base64, ${image}`,
        thumb: `data:image/jpeg;base64, ${image}`,
        subHtml: `<p>${isSheepHerd ? 'Saueflokk' : 'Død sau'}. Bilde ${i+1} av ${imageStrings.length}</p>`
      })
    ));
    return list;
  };

  return (
    <div className="App">
        <LightGallery
                speed={500}
                plugins={[lgThumbnail, lgZoom]}
                thumbnail={true}
                onInit={onInit}
                dynamic={true}
                dynamicEl={setDynamicImages()}
                onAfterClose={handleClose}
            />
    </div>
  );
});

Gallery.displayName = 'Gallery';

export default Gallery;