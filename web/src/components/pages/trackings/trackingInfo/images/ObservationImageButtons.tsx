import React, { forwardRef } from 'react';
import {  Box, IconButton, Tooltip, Typography} from '@mui/material';
import { DeadSheep, SheepHerd } from '../../../../../types';
import MapIcon from '@mui/icons-material/Map';
import PhotoIcon from './PhotoIcon';

interface ObservationImageButtonsProps {
    observation: DeadSheep | SheepHerd,
    index: number,
    setSelectedObservation:  React.Dispatch<React.SetStateAction<SheepHerd | DeadSheep | undefined>>
}

const ObservationImageButtons = forwardRef<any, ObservationImageButtonsProps>(({observation, index, setSelectedObservation}, ref) => {

    const handleImageClick = (observation: DeadSheep | SheepHerd) => {
        setSelectedObservation(observation);
    };

    const handleMapClick = () => {
        if(!ref || !('current' in ref)) return;
        ref.current.flyTo({
            center: observation.location,
            zoom: 19,
            speed: 1,
        });
    };

    const numberOfImages = observation.images.length;

    return (
        <Box 
        padding={1} 
        height={30} 
        alignItems={"center"} 
        display={'flex'} 
        justifyContent={"flex-start"} 
        sx={{backgroundColor: 'lightgrey', border: "0.5px solid black", borderRadius: 3}}>
            <Typography variant='body1' >#{index+1}</Typography>
            <Tooltip title={"Vis bilder"}>
                <IconButton onClick={() => handleImageClick(observation)} >
                    <PhotoIcon numberOfImages={numberOfImages} />
                </IconButton>
            </Tooltip>
            <Tooltip title={"Vis på kart"}>
                <IconButton onClick={handleMapClick} >
                        <MapIcon/>
                </IconButton>
                </Tooltip>
        </Box>
    );
});

ObservationImageButtons.displayName = 'ObservationImageButtons';

export default ObservationImageButtons;