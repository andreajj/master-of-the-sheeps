import React, { ReactElement } from 'react';
import { Filter1, Filter2, Filter3, Filter4, Filter5, Filter6, Filter7, Filter8, Filter9, Filter9Plus } from '@mui/icons-material/';


interface PhotoIconProps {
    numberOfImages: number,
}

const getIcon = (numberofImages: number): ReactElement => {
    switch (numberofImages) {
        case 1:
            return <Filter1/>;
        case 2:
            return <Filter2/>;
        case 3:
            return <Filter3/>;
        case 4:
            return <Filter4/>;
        case 5:
            return <Filter5/>;
        case 6:
            return <Filter6/>;
        case 7:
            return <Filter7/>;
        case 8:
            return <Filter8/>;
        case 9:
            return <Filter9/>;
        default:
            return <Filter9Plus/>;
    }
};

const PhotoIcon: React.FC<PhotoIconProps> = ({numberOfImages}) => {
  return (
    getIcon(numberOfImages)
  );
};

export default PhotoIcon;