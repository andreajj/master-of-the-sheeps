import React, { forwardRef } from 'react';
import { Box, Typography } from '@mui/material';
import { DeadSheep, SheepHerd } from '../../../../../types';
import ObservationImageButtons from "./ObservationImageButtons";
import DownloadImagesButton from './DownloadImagesButton';

interface ImagesListProps {
    sheepObsList: SheepHerd[],
    deadObsList: DeadSheep[],
    setSelectedObservation:  React.Dispatch<React.SetStateAction<SheepHerd | DeadSheep | undefined>>,
    date: string,
}

const ImagesList = forwardRef<any, ImagesListProps>(({sheepObsList, deadObsList, setSelectedObservation, date}, ref) => {
    
  return (
    <Box >
        <Typography sx={{marginTop: 3}} variant="h5" >Observasjoner med bilder</Typography>
        {sheepObsList.length>0 && 
        (<>
            <Typography variant="body1" >Saueflokker</Typography>
            <Box display={"flex"} flexDirection={"row"} justifyContent={"flex-start"} gap={1} >
                {sheepObsList.map((observation, i) => (
                    <ObservationImageButtons key={i} observation={observation} index={i} ref={ref} setSelectedObservation={setSelectedObservation} />
                    ))}
            </Box>
        </>)}
        {deadObsList.length>0 && 
        (<>
            <Typography pt={1} variant="body1" >Døde sauer</Typography>
            <Box display={"flex"} flexDirection={"row"} justifyContent={"flex-start"} gap={1} >
                {deadObsList.map((observation, i) => (
                    <ObservationImageButtons key={i} observation={observation} index={i} ref={ref} setSelectedObservation={setSelectedObservation} />
                    ))}
            </Box>
        </>)}
        <DownloadImagesButton deadObsList={deadObsList} sheepObsList={sheepObsList} date={date} />
    </Box>
  );
});

ImagesList.displayName = 'ImagesList';

export default ImagesList;