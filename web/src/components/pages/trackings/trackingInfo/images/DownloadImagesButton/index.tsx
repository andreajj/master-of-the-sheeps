import React from 'react';
import { Button, Box } from '@mui/material';
import { DeadSheep, SheepHerd } from '../../../../../../types';
import { getAllImageStringsFromObsList, getAllImageStringsFromObs, getListOfImageStringsList } from '../../../trackingUtils/galleryFunctions';
import { downloadZip } from 'client-zip';
import { Buffer } from 'buffer';
import DownloadIcon from '@mui/icons-material/Download';

interface DownloadImagesButtonProps {
    sheepObsList: SheepHerd[],
    deadObsList: DeadSheep[],
    date: string,
}

const DownloadImagesButton: React.FC<DownloadImagesButtonProps> = ({sheepObsList, deadObsList, date}) => {
    const handleDownload = async () => {
        const files = getImageFilesList();
        if(!files.length) return;
        const blob = await downloadZip(files).blob();

        const link = document.createElement("a");
        const blobUrl = URL.createObjectURL(blob);
        link.href = blobUrl;
        link.download = "tilsyn_bilder.zip";
        link.click();
        link.remove();
        URL.revokeObjectURL(blobUrl);
    };

    const getImageStringAsFile2 = (obsNumber: number, noOfImages: number, imageString: string, type: 'herd' | 'dead', index: number) => {
        const imageData = Buffer.from(imageString, 'base64');
        const imageFile = new File([imageData], `${date}_${type==='herd' ? "saueflokk" : 'dodsau'}_${obsNumber}_bilde_${index+1}_av_${noOfImages}.jpeg`, {type: 'image/jpeg'});
        return imageFile;
    };

    const getImageFilesList = (): File[] => {
        const herdList = getListOfImageStringsList(sheepObsList);
        const deadSheepList = getListOfImageStringsList(deadObsList);

        const listOfImageFiles: File[] = [];
        herdList.forEach((herdImages, herdNumber) => {
            const numberOfImagesInObs = herdImages.length;
            herdImages.forEach((herdImage, imageNumber) => {
                const _file = getImageStringAsFile2(herdNumber, numberOfImagesInObs, herdImage, 'herd', imageNumber);
                listOfImageFiles.push(_file);
            });
        });
        deadSheepList.forEach((deadSheepImages, herdNumber) => {
            const numberOfImagesInObs = deadSheepImages.length;
            deadSheepImages.forEach((deadSheepImage, imageNumber) => {
                const _file = getImageStringAsFile2(herdNumber, numberOfImagesInObs, deadSheepImage, 'dead', imageNumber);
                listOfImageFiles.push(_file);
            });
        });
        return listOfImageFiles;
    };

    return (
        <Box pt={1.5}>
            <Button endIcon={<DownloadIcon/>} variant="contained" onClick={handleDownload}>Last ned alle bilder</Button>
        </Box>
    );
};

export default DownloadImagesButton;