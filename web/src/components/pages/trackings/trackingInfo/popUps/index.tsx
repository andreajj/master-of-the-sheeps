import DeadSheepPopup from "./DeadSheepPopup";
import OtherPopup from "./OtherPopUp";
import PredatorPopup from "./PredatorPopup";
import SheepHerdPopup from "./SheepHerdPopup";

export {
    DeadSheepPopup, 
    PredatorPopup, 
    OtherPopup, 
    SheepHerdPopup
};