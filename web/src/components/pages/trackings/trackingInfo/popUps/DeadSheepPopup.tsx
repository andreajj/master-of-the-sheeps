import React from 'react';
import { Box, Typography, Divider, Button } from '@mui/material';
import { InfoContainer } from "../";

interface DeadSheepPopupProps {
    description: string,
    hasImages: boolean,
    id: string,
    setClickedId: React.Dispatch<React.SetStateAction<string>>,
}

const DeadSheepPopup: React.FC<DeadSheepPopupProps> = ({description, hasImages, id, setClickedId}) => {

    const handleShowPictures = () => {
        setClickedId(id);
    };

    return (
    <Box sx={{minWidth: "200px"}} >
        <Typography variant="h6">Død sau</Typography>
        <InfoContainer>
            <Typography fontWeight='bold' variant="body1">Beskrivelse:</Typography>
            <Typography variant="body1">{description!=='' ? description : "(Ingen)" }</Typography>
        </InfoContainer>
        <InfoContainer>
            <Typography fontWeight='bold' variant="body1">{!hasImages ? 'Har bilder?' : 'Bilder:'}</Typography>
            <Typography variant="body1">{hasImages 
                ? (<Button sx={{lineHeight: 'normal'}} onClick={handleShowPictures} variant="contained" >Vis</Button>)
                 : "Nei"}
            </Typography>
        </InfoContainer>
        <Divider/>
    </Box>

    );
};

export default DeadSheepPopup;