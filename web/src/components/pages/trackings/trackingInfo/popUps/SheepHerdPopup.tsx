import React from 'react';
import { Box, Typography, Divider, Button } from '@mui/material';
import { InfoContainer } from "../";

interface SheepherdPopupProps {
    lambs: number,
    wounded: number,
    adults: number,
    expectedLambs: number,
    hasMyFarm: boolean,
    hasImages: boolean,
    id: string,
    setClickedId: React.Dispatch<React.SetStateAction<string>>,
}

const SheepHerdPopup: React.FC<SheepherdPopupProps> = ({lambs: actualLambs, wounded, adults, expectedLambs, hasMyFarm, hasImages, id, setClickedId}) => {

    const lambsDiff = expectedLambs - actualLambs;

    const handleShowPictures = () => {
        setClickedId(id);
    };


    return (
    <Box sx={{minWidth: "200px"}} >
        <Typography variant="h6">Saueflokk</Typography>
        <InfoContainer>
            <Typography fontWeight='bold' variant="body1">Antall sauer:</Typography>
            <Typography variant="body1">{adults}</Typography>
        </InfoContainer>
        <InfoContainer>
            <Typography fontWeight='bold' variant="body1">Antall lam:</Typography>
            <Typography color={lambsDiff ? 'red ' : undefined}  variant="body1">{actualLambs}</Typography>
        </InfoContainer>
        {!!lambsDiff && (<InfoContainer>
            <Typography fontWeight='bold' variant="body1">Antall forventede lam:</Typography>
            <Typography color={"red"} variant="body1">{expectedLambs}</Typography>
        </InfoContainer>)}
        <InfoContainer>
            <Typography fontWeight='bold' variant="body1">Antall skadde dyr:</Typography>
            <Typography variant="body1">{wounded}</Typography>
        </InfoContainer>
        <InfoContainer>
            <Typography fontWeight='bold' variant="body1">{!hasImages? "Har bilder?" : "Bilder:"}</Typography>
            <Typography variant="body1">{hasImages 
                ? (<Button sx={{lineHeight: 'normal'}} onClick={handleShowPictures} variant="contained" >Vis</Button>) 
                : "Nei"}
            </Typography>
        </InfoContainer>
        <Divider/>
        <InfoContainer>
            <Typography color={hasMyFarm ? undefined : '#c95b00'} variant="body2" >Dyr fra din gård er {hasMyFarm ? '' : 'ikke'} del av flokken.</Typography>
        </InfoContainer>
        <InfoContainer>
            {lambsDiff ? 
            <Typography color={'red'} variant="body1">{Math.abs(lambsDiff)} lam {lambsDiff>0 ? "mangler" : lambsDiff < 0 ? 'for mye' : ''}</Typography>
            : <Typography color={'green'} variant="body1">Ingen lam mangler</Typography>}
        </InfoContainer>
    </Box>

    );
};

export default SheepHerdPopup;