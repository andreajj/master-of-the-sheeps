import React from 'react';
import { Box, Typography, Divider } from '@mui/material';
import { InfoContainer } from "../";
import { PredatorType } from "../../../../../types";

interface PredatorPopupProps {
    type: PredatorType,
    description: string,
}

const PredatorPopup: React.FC<PredatorPopupProps> = ({type, description}) => {

    return (
    <Box sx={{minWidth: "200px"}} >
        <Typography variant="h6">Rovdyr</Typography>
        <InfoContainer>
            <Typography fontWeight='bold' variant="body1">Type:</Typography>
            <Typography variant="body1">{type}</Typography>
        </InfoContainer>
        {!!description && (<InfoContainer>
            <Typography fontWeight='bold' variant="body1">Beskrivelse:</Typography>
            <Typography variant="body1">{description}</Typography>
        </InfoContainer>)}
        <Divider/>
    </Box>

    );
};

export default PredatorPopup;