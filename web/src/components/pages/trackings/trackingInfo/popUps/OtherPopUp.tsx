import React from 'react';
import { Box, Typography, Divider } from '@mui/material';
import { InfoContainer } from "../";
import { OtherType } from "../../../../../types";

interface PredatorPopupProps {
    type: OtherType,
    description: string,
}

const OtherPopup: React.FC<PredatorPopupProps> = ({type, description}) => {

    return (
    <Box sx={{minWidth: "200px"}} >
        <Typography variant="h6">Annet</Typography>
        <InfoContainer>
            <Typography fontWeight='bold' variant="body1">Type:</Typography>
            <Typography variant="body1">{type}</Typography>
        </InfoContainer>
        {description!=='' && (<InfoContainer>
            <Typography fontWeight='bold' variant="body1">Beskrivelse:</Typography>
            <Typography variant="body1">{description}</Typography>
        </InfoContainer>)}
        <Divider/>
    </Box>

    );
};

export default OtherPopup;