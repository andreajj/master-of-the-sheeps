import React from 'react';
import { Typography, Box, Divider } from '@mui/material';
import { InfoContainer } from '.';
import { Tracking, Tie } from '../../../../types';
import {length, lineString} from '@turf/turf';
import { getExpectedLambCountFromTie } from "../trackingUtils/calculationFunctions";
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { useFirebase } from "../../../../firebase";

interface TrackingStatsProps {
    selectedTracking: Tracking,
}

const TrackingStats: React.FC<TrackingStatsProps> = ({selectedTracking}) => {
    const firebase = useFirebase();
    const curUserEmail = firebase.auth.currentUser?.email;

    const routeDescription = selectedTracking.routeDescription ?? 'Tilsyn uten beskrivelse';
    const dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' } as const;
    const startDate = selectedTracking.startTimestamp.toDate().toLocaleDateString('nb-NO', dateOptions);
    const endDate = selectedTracking.endTimestamp.toDate().toLocaleDateString('nb-NO', dateOptions);
    const trackingOverMultipleDates = startDate!==endDate;
    
    const herds = selectedTracking.sheepHerds;
    const startTime = selectedTracking.startTimestamp.toDate().toLocaleTimeString('nb-NO');
    const endTime = selectedTracking.endTimestamp.toDate().toLocaleTimeString('nb-NO');
    const numberOfSheepHerds = herds.length;
    const numberOfDeadAnimals = selectedTracking.deadSheeps.length;
    const predatorsCount = selectedTracking.predators.length;
    const othersCount = selectedTracking.others.length;

    const totalAdultSheep= herds.map((herd) => Object.values(herd.sheeps).reduce((prev, cur) => prev + cur, 0)).reduce((prev, cur) => prev + cur, 0);
    const totalLambs = herds.map((herd) => herd.lambs).reduce((prev, cur) => prev + cur, 0);
    const totalAllSheep = totalAdultSheep + totalLambs;
    const totalExpectedLambs = (herds.map((herd) => Object.entries(herd.sheeps).reduce((prev, [tie, count]) => prev + getExpectedLambCountFromTie(tie as Tie) * count , 0))
                                .reduce((p, s) => p + s, 0));
    const numberOfSheepHerdsWithUsersFarm = curUserEmail ? herds.reduce((prev, cur) => prev + (cur.farms.findIndex((f) => f.owner.email === curUserEmail))!==-1 ? 1 : 0, 0) : 0;

    const line = lineString(selectedTracking.locations);
    const lengthInKm = length(line, {units: 'kilometers'}).toFixed(2);
    const sumWounded = herds.reduce((partialSum, s) => partialSum + s.woundedAnimals, 0);

    return (
    <Box>
        <Typography variant="h6">{routeDescription}</Typography>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">{trackingOverMultipleDates ? 'Startdato' : "Dato"}:</Typography>
                <Typography variant="subtitle1">{startDate}</Typography>
            </InfoContainer>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">Starttidspunkt:</Typography>
                <Typography variant="subtitle1">{startTime}</Typography>
            </InfoContainer>
            {trackingOverMultipleDates && (<InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">Sluttdato:</Typography>
                <Typography variant="subtitle1">{endDate}</Typography>
            </InfoContainer>)}
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">Sluttidspunkt:</Typography>
                <Typography variant="subtitle1">{endTime}</Typography>
            </InfoContainer>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">Avstand gått:</Typography>
                <Typography variant="subtitle1">{lengthInKm}km</Typography>
            </InfoContainer>
            <Divider variant="middle" />
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">Antall saueflokker med dyr fra mine gårder:</Typography>
                <Typography variant="subtitle1">{numberOfSheepHerdsWithUsersFarm}</Typography>
            </InfoContainer>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">Antall saueflokker totalt:</Typography>
                <Typography variant="subtitle1">{numberOfSheepHerds}</Typography>
            </InfoContainer>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">&ensp; Antall sau observert:</Typography>
                <Typography variant="subtitle1">{totalAllSheep}</Typography>
            </InfoContainer>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">&emsp;&emsp;&emsp; Hvorav voksne sau:</Typography>
                <Typography variant="subtitle1">{totalAdultSheep}</Typography>
            </InfoContainer>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">&emsp;&emsp;&emsp; Hvorav lam:</Typography>
                <Typography variant="subtitle1">{totalLambs}</Typography>
            </InfoContainer>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">&emsp;Antall lam telt/forventede lam:</Typography>
                <Box display={'flex'} justifyContent={'space-between'}  >
                    <Typography mr={1} color={totalExpectedLambs!==totalLambs ? 'red' : undefined} variant="subtitle1">{totalLambs}/{totalExpectedLambs}</Typography>
                    {totalExpectedLambs===totalLambs ? (<CheckCircleOutlineIcon color='success'/>) : <ErrorOutlineIcon color='error'/> }
                </Box>
            </InfoContainer>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">&emsp;Antall skadde sau/sau totalt:</Typography>
                <Box display={'flex'} justifyContent={'space-between'}  >
                    <Typography mr={1} color={sumWounded>0 ? 'red' : undefined} variant="subtitle1">{sumWounded}/{totalAllSheep}</Typography>
                    {!(sumWounded>0) ? (<CheckCircleOutlineIcon color='success'/>) : <ErrorOutlineIcon color='error'/> }
                </Box>
            </InfoContainer>
            <Divider variant="middle" />
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">Antall døde sau:</Typography>
                <Box display={'flex'} justifyContent={'space-between'}  >
                    <Typography mr={1} color={numberOfDeadAnimals>0 ? 'red' : undefined} variant="subtitle1">{numberOfDeadAnimals}</Typography>
                    {!(numberOfDeadAnimals>0) ? (<CheckCircleOutlineIcon color='success'/>) : <ErrorOutlineIcon color='error'/> }
                </Box>
            </InfoContainer>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">Antall rovdyr eller rovdyrtegn:</Typography>
                <Box display={'flex'} justifyContent={'space-between'}  >
                    <Typography mr={1} color={predatorsCount>0 ? 'red' : undefined} variant="subtitle1">{predatorsCount}</Typography>
                    {!(predatorsCount>0) ? (<CheckCircleOutlineIcon color='success'/>) : <ErrorOutlineIcon color='error'/> }
                </Box>
            </InfoContainer>
            <InfoContainer>
                <Typography fontWeight="bold" variant="subtitle1">Antall andre observasjoner:</Typography>
                <Typography variant="subtitle1">{othersCount}</Typography>
            </InfoContainer>
    </Box>
    );
};

export default TrackingStats;