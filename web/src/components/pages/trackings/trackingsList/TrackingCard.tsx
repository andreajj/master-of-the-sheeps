import React from 'react';
import { CardContent, Card, Typography, CardActionArea } from '@mui/material';
import { Tracking } from '../../../../types';
import { useNavigate } from 'react-router-dom';

interface TrackingCardProps {
    tracking: Tracking,
    index: number;
    setSelectedTracking: React.Dispatch<React.SetStateAction<Tracking | undefined>>
}

const TrackingCard: React.FC<TrackingCardProps> = ({ tracking, index, setSelectedTracking }) => {

    const {id} = tracking;
    const navigate = useNavigate();

    const handleClick = () => {
        navigate(`/trackings/${id}`);
    };

    const startTimestampString = tracking.startTimestamp.toDate().toLocaleString('nb-NO');
    const endTimestampString = tracking.endTimestamp.toDate().toLocaleString('nb-NO');
    
    return (
        <Card elevation={3} sx={{marginTop: 2, backgroundColor: index % 2 ? 'white' : '#ECECEC'}} >
            <CardActionArea onClick={handleClick} >
                <CardContent>
                    <Typography variant="subtitle1">Rutebeskrivelse: &quot;{tracking.routeDescription ?? '(Ingen)'}&quot;</Typography>
                    <Typography variant="subtitle2">Start: {startTimestampString}</Typography>
                    <Typography variant="subtitle2">Slutt: {endTimestampString}</Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};

export default TrackingCard;