import React from 'react';
import { Grid, Card, Box, Paper, Typography, Skeleton } from '@mui/material';
import TrackingCard from './TrackingCard';
import { Tracking } from '../../../../types';

interface TrackingsListProps {
    trackings: Array<Tracking> | undefined;
    setSelectedTracking: React.Dispatch<React.SetStateAction<Tracking | undefined>>,
}

const TrackingsList: React.FC<TrackingsListProps> = ({trackings, setSelectedTracking}) => {

  return (
    <Grid sx={{height: "85vh"}} item xs={3} padding={1}>
      <Box>
        <Typography variant="h5">Tidligere tilsyn</Typography>
      </Box>
        <Paper sx={{maxHeight: '85vh', overflow: 'auto'}} elevation={2}>
          {!trackings ? <Card elevation={3} sx={{marginTop: 2}} >
                <Skeleton animation="wave" variant="rectangular" width={"100%"} height={135} />
            </Card>
            : (trackings && trackings.length > 0) ? trackings.map((tracking, i) => (
                <TrackingCard key={tracking.id} tracking={tracking} index={i} setSelectedTracking={setSelectedTracking}  />
            )) 
            : <Typography mt={2} >Ingen sporinger å vise</Typography>
            }
      </Paper>
    </Grid>
  );
};

export default TrackingsList;