import React, { useState, useEffect } from 'react';
import { Grid, Typography } from '@mui/material';
import { useFirebase } from '../../../../firebase';
import { Tracking } from '../../../../types';
import TrackingsList from '../trackingsList';
import MapPlaceholder from '../MapPlaceholder';
import { toast } from 'react-toastify';

const TrackingsView: React.FC = () => {
    const firebase = useFirebase();
    const [trackings, setTrackings] = useState<Array<Tracking> | undefined>(undefined);
    const [selectedTracking, setSelectedTracking] = useState<Tracking | undefined>(undefined);

    useEffect(() => {
        const getTrackings = async () => {
            try {
                const tracking = await firebase.getTrackings();
                setTrackings(tracking);
            } catch (error) {
                console.log('Fetching trackings failed:', error);
                toast.error("En feil skjedde ved innhenting av sporinger");
                setTrackings([]);
            }
        };
            
        getTrackings();
    }, []);
    
    return (
        <Grid container spacing={0} direction="row" alignItems="flex-start" justifyContent="flex-start" flex={1}>
            <TrackingsList 
                trackings={trackings}
                setSelectedTracking={setSelectedTracking} />
            <Grid item xs={9}>
                <MapPlaceholder text={"Velg et tilsyn fra listen"}/> 
            </Grid>
        </Grid>
    );
};

export default TrackingsView;
