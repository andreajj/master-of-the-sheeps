import React, {MutableRefObject, useRef, useEffect, forwardRef} from 'react';
import { Box } from '@mui/material';
import { Tracking } from '../../../types';
import mapboxgl, {MapLayerMouseEvent, ErrorEvent} from 'mapbox-gl';
import { 
    drawUserLocationPoints, 
    drawSheepHerdSymbols, 
    toggleLayerVisibility, 
    drawPredatorSymbols, 
    drawDeadSheepSymbols, 
    drawOtherSymbols, 
    drawSheepHerdObservationLines,
    drawPredatorObservationLines,
    drawOtherObservationLines,
    drawDeadSheepObservationLines
 } from './trackingUtils/drawingFunctions';
import {LayerTypes, ClickableLayerTypes} from "./trackingUtils/drawingTypes";
import Sheep from "../../../assets/icons/sheep.png";
import Paw  from "../../../assets/icons/paw.png";
import Skullcrossbones from "../../../assets/icons/skullcrossbones.png";
import Other from "../../../assets/icons/other.png";
import { SheepHerdPopup, PredatorPopup, OtherPopup, DeadSheepPopup } from "./trackingInfo/popUps";
import {render} from 'react-dom';
import { useFirebase } from "../../../firebase";

interface MapProps {
    selectedTracking: Tracking,
    showUserLocation: boolean,
    showSheepHerds: boolean,
    showPredators: boolean,
    showOthers: boolean,
    showDeadSheep: boolean,
    height?: string,
    width?: string,
    setClickedId?: React.Dispatch<React.SetStateAction<string>>
}

const Mapbox = forwardRef<any, MapProps>(({
    selectedTracking, 
    showUserLocation, 
    showSheepHerds,
    showPredators,
    showOthers,
    showDeadSheep,
    height,
    width,
    setClickedId
    }, ref) => {
        const mapContainer = useRef(null);
        
        const id = selectedTracking.id;
        const firebase = useFirebase();
        const curUserEmail = firebase.auth.currentUser?.email;

        const map = ref;

        mapboxgl.accessToken = "pk.eyJ1IjoibGFyc2V2YSIsImEiOiJja3RjeGhtYmQyYWRqMnBuOTVwZTFjcmd2In0.yl_GUShVvT3hyW-0F0cj2w";

        const adjustMapBoundsToOfflineMap = () => {
            const bounds = selectedTracking.offlineMap.bounds;
            const cameraBounds = new mapboxgl.LngLatBounds([bounds.swLng, bounds.swLat], [bounds.neLng, bounds.neLat]);
            (map as MutableRefObject<mapboxgl.Map>).current.setMaxBounds(cameraBounds);
        };

        
        useEffect(() => {
            if(map && 'current ' in map) return;
            const m = (map as MutableRefObject<mapboxgl.Map>);
            m.current = new mapboxgl.Map({
                container: mapContainer.current || '',
                style: 'mapbox://styles/mapbox/outdoors-v11',
                center: [10.404385, 63.417352],
                zoom: 14,
            });
            m.current.addControl(new mapboxgl.NavigationControl());
            adjustMapBoundsToOfflineMap();
        }, []);

        const drawAllObservationLines = (map: React.RefObject<mapboxgl.Map>, tracking: Tracking) => {
            drawSheepHerdObservationLines(map, tracking);
            drawPredatorObservationLines(map, tracking);
            drawOtherObservationLines(map, tracking);
            drawDeadSheepObservationLines(map, tracking);
        };

        useEffect(() => {
        if(!map || !('current' in map)) return;
            // load assets and draw symbols
            map.current.on('load', () => {
                drawAllObservationLines(map, selectedTracking);
                drawUserLocationPoints(map, selectedTracking);
                map.current.loadImage(Sheep, (error: ErrorEvent, image: HTMLImageElement | ImageBitmap) => {
                    if(error) throw error;
                    if(image) {
                        map.current.addImage('sheep', image);
                        drawSheepHerdSymbols(map, selectedTracking, curUserEmail);
                    }});
                    map.current.loadImage(Paw, (error:ErrorEvent, image: HTMLImageElement | ImageBitmap) => {
                        if(error) throw error;
                        if(image) {
                            map.current.addImage('paw', image);
                            drawPredatorSymbols(map, selectedTracking);
                    }});
                    map.current?.loadImage(Other, (error:ErrorEvent, image: HTMLImageElement | ImageBitmap) => {
                        if(error) throw error;
                        if(image) {
                            map.current.addImage('other', image);
                        drawOtherSymbols(map, selectedTracking);
                    }});
                    map.current?.loadImage(Skullcrossbones, (error:ErrorEvent, image: HTMLImageElement | ImageBitmap) => {
                        if(error) throw error;
                        if(image) {
                            map.current.addImage('skullCrossbones', image);
                            drawDeadSheepSymbols(map, selectedTracking);
                        }
                });
            });
            // event listener to open popups for sheepHerd when clicked
            map.current.on('click', `layer-${id}-${ClickableLayerTypes.SheepHerdPoints}`, (e: MapLayerMouseEvent) => {
                if(!map.current) return;
                if(!e.features) return;
                if(!setClickedId) return;

                const coords = (e.features[0].geometry as any).coordinates.slice();
                const numberOfLambs = (e.features[0].properties as any).lambsCount;
                const numberOfWounded = (e.features[0].properties as any).woundedCount;
                const numberOfAdults = (e.features[0].properties as any).adultCount;
                const expectedLambs = (e.features[0].properties as any).expectedLambsCount;
                const hasMyFarm = (e.features[0].properties as any).hasMyFarm;
                const hasImages = (e.features[0].properties as any).hasImages;
                const id = (e.features[0].properties as any).id;
                const popupNode = document.createElement('div');
                render(<SheepHerdPopup lambs={numberOfLambs} wounded={numberOfWounded} adults={numberOfAdults} expectedLambs={expectedLambs} 
                                        hasMyFarm={hasMyFarm} hasImages={hasImages} id={id} setClickedId={setClickedId} />, popupNode);
                new mapboxgl.Popup().setLngLat(coords).setDOMContent(popupNode).setMaxWidth("400px").addTo(map.current);
            });

            map.current.on('click', `layer-${id}-${ClickableLayerTypes.SheepHerdPoints}-myFarmNotRepresented`, (e:MapLayerMouseEvent) => {
                if(!map.current) return;
                if(!e.features) return;
                if(!setClickedId) return;

                const coords = (e.features[0].geometry as any).coordinates.slice();
                const numberOfLambs = (e.features[0].properties as any).lambsCount;
                const numberOfWounded = (e.features[0].properties as any).woundedCount;
                const numberOfAdults = (e.features[0].properties as any).adultCount;
                const expectedLambs = (e.features[0].properties as any).expectedLambsCount;
                const hasMyFarm = (e.features[0].properties as any).hasMyFarm;
                const hasImages = (e.features[0].properties as any).hasImages;
                const id = (e.features[0].properties as any).id;
                const popupNode = document.createElement('div');
                render(<SheepHerdPopup lambs={numberOfLambs} wounded={numberOfWounded} adults={numberOfAdults} expectedLambs={expectedLambs} 
                                        hasMyFarm={hasMyFarm} hasImages={hasImages} id={id} setClickedId={setClickedId} />, popupNode);
                new mapboxgl.Popup().setLngLat(coords).setDOMContent(popupNode).setMaxWidth("400px").addTo(map.current);
            });

            // event listener to open popups for predator when clicked
            map.current.on('click', `layer-${id}-${ClickableLayerTypes.PredatorPoints}`, (e: MapLayerMouseEvent) => {
                if(!map.current) return;
                if(!e.features) return;

                const coords = (e.features[0].geometry as any).coordinates.slice();
                const type = (e.features[0].properties as any).type;
                const description = (e.features[0].properties as any).description;
                const popupNode = document.createElement('div');
                render(<PredatorPopup type={type} description={description} />, popupNode);
                new mapboxgl.Popup().setLngLat(coords).setDOMContent(popupNode).setMaxWidth("400px").addTo(map.current);
            });

            // event listener to open popups for other when clicked
            map.current.on('click', `layer-${id}-${ClickableLayerTypes.OtherPoints}`, (e: MapLayerMouseEvent) => {
                if(!map.current) return;
                if(!e.features) return;

                const coords = (e.features[0].geometry as any).coordinates.slice();
                const type = (e.features[0].properties as any).type;
                const description = (e.features[0].properties as any).description;
                const popupNode = document.createElement('div');
                render(<OtherPopup type={type} description={description} />, popupNode);
                new mapboxgl.Popup().setLngLat(coords).setDOMContent(popupNode).setMaxWidth("400px").addTo(map.current);
            });

            // event listener to open popups for deadsheep when clicked
            map.current.on('click', `layer-${id}-${ClickableLayerTypes.DeadSheepPoints}`, (e: MapLayerMouseEvent) => {
                if(!map.current) return;
                if(!e.features) return;
                if(!setClickedId) return;

                const coords = (e.features[0].geometry as any).coordinates.slice();
                const description = (e.features[0].properties as any).description;
                const hasImages = (e.features[0].properties as any).hasImages;
                const id = (e.features[0].properties as any).id;
                const popupNode = document.createElement('div');
                render(<DeadSheepPopup hasImages={hasImages} description={description} id={id} setClickedId={setClickedId} />, popupNode);
                new mapboxgl.Popup().setLngLat(coords).setDOMContent(popupNode).setMaxWidth("400px").addTo(map.current);
            });

            // change cursor when overing over observation symbols
            for (const layerName of Object.values(ClickableLayerTypes)) {
                map.current.on('mouseenter', `layer-${id}-${layerName}`, () => {
                    if(!map.current) return;
                    map.current.getCanvas().style.cursor = 'pointer';
                });
                map.current.on('mouseleave', `layer-${id}-${layerName}`, () => {
                    if(!map.current) return;
                    map.current.getCanvas().style.cursor  = '';
                });
            }
            map.current.on('mouseenter', `layer-${id}-${ClickableLayerTypes.SheepHerdPoints}-myFarmNotRepresented`, () => {
                if(!map.current) return;
                map.current.getCanvas().style.cursor = 'pointer';
            });
            map.current.on('mouseleave', `layer-${id}-${ClickableLayerTypes.SheepHerdPoints}-myFarmNotRepresented`, () => {
                if(!map.current) return;
                map.current.getCanvas().style.cursor  = '';
            });
        }, []);

        const toggleAllObservationLines = (show: boolean, map: React.RefObject<mapboxgl.Map>,  trackingID: string) => {
            toggleLayerVisibility(show, map, trackingID, LayerTypes.SheepHerdObservationLines);
            toggleLayerVisibility(show, map, trackingID, LayerTypes.PredatorObservationLines);
            toggleLayerVisibility(show, map, trackingID, LayerTypes.OtherObservationLines);
            toggleLayerVisibility(show, map, trackingID, LayerTypes.DeadSheepObservationLines);
        };

        useEffect(() => {
            // toggle visibility for userLocation points, their Strokes, and the lines connecting them
            if(!map || !('current' in map) || !map.current.loaded()) return;
            toggleLayerVisibility(showUserLocation, map, id, LayerTypes.UserLocationPoints);
            toggleLayerVisibility(showUserLocation, map, id, LayerTypes.UserLocationPointsStroke);
            toggleLayerVisibility(showUserLocation, map, id, LayerTypes.UserLocationLines);
            toggleAllObservationLines(showUserLocation, map, id);
        }, [showUserLocation]);

        useEffect(() => {
            if(!map || !('current' in map) || !map.current.loaded()) return;
            toggleLayerVisibility(showSheepHerds, map, id, ClickableLayerTypes.SheepHerdPoints);
            if(!showSheepHerds || !showUserLocation) {
                toggleLayerVisibility(false, map, id, LayerTypes.SheepHerdObservationLines);
            } else {
                toggleLayerVisibility(true, map, id, LayerTypes.SheepHerdObservationLines);
            }
        }, [showSheepHerds, showUserLocation]);

        useEffect(() => {
            if(!map || !('current' in map) || !map.current.loaded()) return;
            toggleLayerVisibility(showPredators, map, id, ClickableLayerTypes.PredatorPoints);
            if(!showPredators || !showUserLocation) {
                toggleLayerVisibility(false, map, id, LayerTypes.PredatorObservationLines);
            } else {
                toggleLayerVisibility(true, map, id, LayerTypes.PredatorObservationLines);
            }
        }, [showPredators, showUserLocation]);

        useEffect(() => {
            if(!map || !('current' in map) || !map.current.loaded()) return;
            toggleLayerVisibility(showOthers, map, id, ClickableLayerTypes.OtherPoints);
            if(!showOthers || !showUserLocation) {
                toggleLayerVisibility(false, map, id, LayerTypes.OtherObservationLines);
            } else {
                toggleLayerVisibility(true, map, id, LayerTypes.OtherObservationLines);
            }
        }, [showOthers, showUserLocation]);

        useEffect(() => {
            if(!map || !('current' in map) || !map.current.loaded()) return;
            toggleLayerVisibility(showDeadSheep, map, id, ClickableLayerTypes.DeadSheepPoints);
            if(!showDeadSheep || !showUserLocation) {
                toggleLayerVisibility(false, map, id, LayerTypes.DeadSheepObservationLines);
            } else {
                toggleLayerVisibility(true, map, id, LayerTypes.DeadSheepObservationLines);
            }
        }, [showDeadSheep, showUserLocation]);
        
        return (
            <Box sx={{height: height ?? "85vh", width: width ?? null}} ref={mapContainer} className="map-container" />
        );
});

Mapbox.displayName = 'Mapbox';

export default Mapbox;
