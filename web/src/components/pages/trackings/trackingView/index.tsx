import React, { useState, useEffect, useRef } from 'react';
import { Grid, Skeleton } from '@mui/material';
import { useFirebase } from '../../../../firebase';
import { Tracking, SheepHerd, DeadSheep } from '../../../../types';
import TrackingInfo from '../trackingInfo';
import Mapbox from "../Map";
import { useParams } from 'react-router-dom';
import MapPlaceholder from '../MapPlaceholder';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';

const TrackingView: React.FC = () => {
    const firebase = useFirebase();
    const [selectedTracking, setSelectedTracking] = useState<Tracking | undefined>(undefined);
    const [showUserLocation, setShowUserLocation] = useState<boolean>(true);
    const [showSheepHerds, setShowSheepHerds] = useState(true);
    const [showPredators, setShowPredators] = useState(true);
    const [showOthers, setShowOthers] = useState(true);
    const [showDeadSheep, setshowDeadSheep] = useState(true);
    const mapRef = useRef<mapboxgl.Map>(null);

    const { id } = useParams();
    const navigate = useNavigate();

    const [selectedObservation, setSelectedObservation] = useState<SheepHerd | DeadSheep | undefined>(undefined);
    const [clickedId, setClickedId] = useState("");

    useEffect(() => {
        if(clickedId!=='' && !selectedObservation) {
            setClickedId('');
        };
      }, [clickedId, selectedObservation]);

    useEffect(() => {
         const getTracking = async () => {
            try {
                if(!id) return; 
                const tracking = await firebase.doGetSingleTrackingOnce(id);
                setSelectedTracking(tracking);
            } catch (error) {
                console.log('Fetching trackings failed:', error);
                setSelectedTracking(undefined);
                toast.error("Sporingen ble ikke funnnet");
                navigate("/trackings");
            }
        };
        getTracking();
    }, []);
    
    return (

        <Grid container spacing={0} direction="row" alignItems="flex-start" justifyContent="flex-start" flex={1}>
            <Grid item xs={3} padding={1}>
            {selectedTracking ? 
                (<TrackingInfo 
                showUserLocation={showUserLocation} 
                setShowUserLocation={setShowUserLocation} 
                selectedTracking={selectedTracking} 
                setSelectedTracking={setSelectedTracking}
                showSheepHerds={showSheepHerds}
                setShowSheepHerds={setShowSheepHerds}
                showPredators={showPredators}
                setShowPredators={setShowPredators}
                showOthers={showOthers}
                setShowOthers={setShowOthers}
                showDeadSheep={showDeadSheep}
                setshowDeadSheep={setshowDeadSheep}
                selectedObservation={selectedObservation}
                setSelectedObservation={setSelectedObservation}
                clickedId={clickedId}
                ref={mapRef}
                />)
                : <Skeleton animation="wave" variant="rectangular" width={"100%"} height={"85vh"} />}
            </Grid>
            <Grid item xs={9}>
                {selectedTracking ? 
                 (<Mapbox 
                showUserLocation={showUserLocation} 
                showSheepHerds={showSheepHerds} 
                selectedTracking={selectedTracking}
                showPredators={showPredators}
                showOthers={showOthers}
                showDeadSheep={showDeadSheep}
                ref={mapRef}
                setClickedId={setClickedId}
                />) 
                : <MapPlaceholder text={"Laster inn..."}/>}
            </Grid>
            </Grid>
    );
};

export default TrackingView;
