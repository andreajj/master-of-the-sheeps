import React, { useEffect } from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { Grid, Paper } from '@mui/material';
import { Information } from './information';
import { DangerZone } from './dangerZone';
import { useSearchParams } from 'react-router-dom';

export const Profile: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [value, setValue] = React.useState('1');

  useEffect(() => {
    const tab = searchParams.get('tab');
    if (tab) {
      setValue(tab);
    }
  }, []);

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue);
    setSearchParams({tab: newValue});
  };

  return (
    <Grid
      container
      spacing={0}
      direction="row"
      sx={{
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Paper
        sx={{
          width: "50%",
          minWidth: "400px",
          maxWidth: "800px",
          height: "50%",
          minHeight: "300px",
          maxHeight: "500px"
        }}
      >
        <Box sx={{ width: '100%', typography: 'body1' }}>
          <TabContext value={value}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
              <TabList 
                onChange={handleChange} 
                variant="fullWidth"
              >
                <Tab label="Profil" value="1" />
                <Tab label="Faresone" value="2" />
              </TabList>
            </Box>
            <TabPanel value="1">
              <Information />
            </TabPanel>
            <TabPanel value="2">
              <DangerZone />
            </TabPanel>
          </TabContext>
        </Box>
      </Paper>
    </Grid>
  );
};