import React, { useState } from 'react';
import Box from '@mui/material/Box';
import { Email } from './Email';
import { Password } from './Password';
import { ReauthenticateModal } from '../../../ReauthenticateModal';


export const Information: React.FC = () => {
  const [showReauthenticationModal, setShowReauthenticationModal] = useState(false);

  const reauthenticate = () => {
    setShowReauthenticationModal(true);
  };

  return (
    <Box>
      <ReauthenticateModal open={showReauthenticationModal} setOpen={setShowReauthenticationModal} />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: 1,
        }}
      >
        <Email reauthenticate={reauthenticate} />
        <Password reauthenticate={reauthenticate} />
      </Box>
    </Box>
  );
};