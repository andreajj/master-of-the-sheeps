import React, { useState } from 'react';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { FormProvider, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { toast } from 'react-toastify';
import { useFirebase } from '../../../../firebase';
import PasswordInput from '../../entry/PasswordInput';
import { FirebaseError } from 'firebase/app';

type FormData = {
  newPassword: string;
};

const validationSchema = yup.object().shape({
  newPassword: yup
      .string()
      .min(8, 'Passordet må være minst 8 tegn langt')
      .matches(/^(?=.*[a-å])(?=.*[A-Å])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/, 'Passordet kan bare inneholde bokstaver fra a-å og må også inneholde en liten og stor bokstav, tall og et av tegnene "@$!%*?&"')
      .required('Nytt passord er påkrevd'),
});

interface Props {
  reauthenticate: () => void
}

export const Password: React.FC<Props> = ({ reauthenticate }) => {
  const firebase = useFirebase();
  
  const [editing, setEditing] = useState(false);
  const [loading, setLoding] = useState(false);

  const formMethods = useForm<FormData>({
    resolver: yupResolver(validationSchema),
    defaultValues: {
      newPassword: '',
    },
  });

  const onEditClick = () => {
    setEditing((editing) => !editing);
  };

  const onSubmit = formMethods.handleSubmit(async (data) => {
    updatePassword(data.newPassword);
  });

  const updatePassword = async (newPassword: string) => {
    setLoding(true);
    try {
      await firebase.doPasswordUpdate(newPassword);
      formMethods.reset();
      setEditing(false);
      toast.success("Endring av passord var vellykket");
    } catch(error) {
      let code;
      if (error instanceof FirebaseError) {
        code = error.code;
      } else if (typeof error === "string") {
        code = error;
      }

      if (code === 'auth/requires-recent-login') {
        reauthenticate();
      } else {
        toast.error("Noe gikk galt! Passordet ble ikke endret.");
      }
    }
    setLoding(false);
  };

  return (
    <FormProvider {...formMethods}>
      <Box
        component="form"
        sx={{
          display: "flex",
          flexDirection: "column",
        }}
        onSubmit={onSubmit}
      >
        <Typography>
          Endre passord:
        </Typography>
        <Box
          sx={{
            display: "flex"
          }}
        >
          <PasswordInput name={'newPassword'} helperText={'Nytt passord'} marginBottom={false} fullWidth={true} disabled={!editing} />
          <LoadingButton 
            variant="contained" 
            color={editing ? "warning" : "info"}
            size="large"
            onClick={onEditClick}
            sx={{
              marginLeft: 1,
            }}
          >
            {editing ? "Avbryt" : "Endre"}
          </LoadingButton>
          <LoadingButton 
            variant="contained" 
            color="success"
            size="large"
            type="submit"
            loading={loading}
            disabled={!editing}
            sx={{
              marginLeft: 1,
            }}
          >
            Lagre
          </LoadingButton>
        </Box>
      </Box>
    </FormProvider>
  );
};