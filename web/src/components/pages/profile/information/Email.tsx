import React, { useState } from 'react';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';
import TextInput from '../../entry/TextInput';
import { LoadingButton } from '@mui/lab';
import { FormProvider, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { toast } from 'react-toastify';
import { useFirebase } from '../../../../firebase';
import { FirebaseError } from 'firebase/app';

type FormData = {
  email: string;
};

const validationSchema = yup.object().shape({
  email: yup.string().email().required('Epost er påkrevd'),
});

interface Props {
  reauthenticate: () => void
}

export const Email: React.FC<Props> = ({ reauthenticate }) => {
  const firebase = useFirebase();
  
  const [editing, setEditing] = useState(false);
  const [loading, setLoding] = useState(false);

  const formMethods = useForm<FormData>({
    resolver: yupResolver(validationSchema),
    defaultValues: {
        email: '',
    },
  });

  const onEditClick = () => {
    setEditing((editing) => !editing);
  };

  const onSubmit = formMethods.handleSubmit(async (data) => {
    updateEmail(data.email);
  });

  const updateEmail = async (newEmail: string) => {
    setLoding(true);
    try {
      await firebase.doUpdateEmail(newEmail);
      formMethods.reset();
      setEditing(false);
      toast.success("Endring av epost var vellykket");
    } catch(error) {
      let code;
      if (error instanceof FirebaseError) {
        code = error.code;
      } else if (typeof error === "string") {
        code = error;
      }

      if (code === 'auth/requires-recent-login') {
        reauthenticate();
      } else {
        toast.error("Noe gikk galt! Epost ble ikke endret.");
      }
    }
    setLoding(false);
  };

  return (
    <FormProvider {...formMethods}>
      <Box
        component="form"
        sx={{
          display: "flex",
          flexDirection: "column",
        }}
        onSubmit={onSubmit}
      >
        <Typography>
          Endre epost:
        </Typography>
        <Box
          sx={{
            display: "flex"
          }}
        >
          <TextInput name={'email'} label={'Epost'} marginBottom={false} fullWidth={true} disabled={!editing} />
          <LoadingButton 
            variant="contained" 
            color={editing ? "warning" : "info"}
            size="large"
            onClick={onEditClick}
            sx={{
              marginLeft: 1,
            }}
          >
            {editing ? "Avbryt" : "Endre"}
          </LoadingButton>
          <LoadingButton 
            variant="contained" 
            color="success"
            size="large"
            type="submit"
            loading={loading}
            disabled={!editing}
            sx={{
              marginLeft: 1,
            }}
          >
            Lagre
          </LoadingButton>
        </Box>
      </Box>
    </FormProvider>
  );
};