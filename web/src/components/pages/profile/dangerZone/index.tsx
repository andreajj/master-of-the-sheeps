import React, { useState } from 'react';
import Box from '@mui/material/Box';
import { ReauthenticateModal } from '../../../ReauthenticateModal';
import { DeleteData } from './DeleteData';
import { DeleteAccount } from './DeleteAccount';
import { DownloadData } from './DownloadData';

export const DangerZone: React.FC = () => {
  const [showReauthenticationModal, setShowReauthenticationModal] = useState(false);

  const reauthenticate = () => {
    setShowReauthenticationModal(true);
  };

  return (
    <Box>
      <ReauthenticateModal open={showReauthenticationModal} setOpen={setShowReauthenticationModal} />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: 1,
        }}
      >
        <DownloadData />
        <DeleteData />
        <DeleteAccount reauthenticate={reauthenticate} />
      </Box>
    </Box>
  );
};