import React, { useState } from 'react';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { toast } from 'react-toastify';
import { useFirebase } from '../../../../firebase';
import { FirebaseError } from 'firebase/app';
import { Lock } from '@styled-icons/material/Lock';
import { LockOpen } from '@styled-icons/material/LockOpen';

export const DownloadData: React.FC = () => {  
  const firebase = useFirebase();
  const [editing, setEditing] = useState(false);
  const [loading, setLoding] = useState(false);

  const onEditClick = () => {
    setEditing((editing) => !editing);
  };

  const download: (data: Record<string, unknown>) => void = async (data) => {
    const jsonFile = new Blob([JSON.stringify(data)], {
        type: 'application/json'
    });

    const url = URL.createObjectURL(jsonFile);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute(
      'download',
      `data.json`,
    );

    // Append to html link element page
    document.body.appendChild(link);

    // Start download
    link.click();

    // Clean up and remove the link
    const linkParent = link.parentNode;
    if (linkParent) {
      linkParent.removeChild(link);
    }
    URL.revokeObjectURL(url);
  };

  const downloadData = async () => {
    setLoding(true);
    try {
      const data = await firebase.doGetMyData();
      await download(data);
      setEditing(false);
      toast.success("Nedlastingen av dataen var vellykket!");
    } catch(error) {
      let code;
      if (error instanceof FirebaseError) {
        code = error.code;
      } else if (typeof error === "string") {
        code = error;
      }
      console.log(code);

      toast.error("Noe gikk galt! Nedlastingen av dataen var ikke vellykket.");
    }
    setLoding(false);
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Typography>
        Last ned data:
      </Typography>
      <Box
        sx={{
          display: "flex"
        }}
      >
        <LoadingButton 
          variant="contained" 
          color={editing ? "warning" : "info"}
          size="large"
          endIcon={editing ? <LockOpen size="1.5rem" /> : <Lock size="1.5rem" />}
          onClick={onEditClick}
        >
          {editing ? "Avbryt" : "Lås opp"}
        </LoadingButton>
        <LoadingButton 
          variant="contained" 
          color="success"
          size="large"
          type="submit"
          onClick={downloadData}
          loading={loading}
          disabled={!editing}
          sx={{
            marginLeft: 1,
          }}
        >
          Last ned data
        </LoadingButton>
      </Box>
    </Box>
  );
};