import React, { useState } from 'react';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { toast } from 'react-toastify';
import { useFirebase } from '../../../../firebase';
import { FirebaseError } from 'firebase/app';
import { Lock } from '@styled-icons/material/Lock';
import { LockOpen } from '@styled-icons/material/LockOpen';

export const DeleteData: React.FC = () => {  
  const firebase = useFirebase();
  const [editing, setEditing] = useState(false);
  const [loading, setLoding] = useState(false);

  const onEditClick = () => {
    setEditing((editing) => !editing);
  };

  const deleteData = async () => {
    setLoding(true);
    try {
      await firebase.doDeleteAllData();
      setEditing(false);
      toast.success("Sletting av data var vellykket!");
    } catch(error) {
      let code;
      if (error instanceof FirebaseError) {
        code = error.code;
      } else if (typeof error === "string") {
        code = error;
      }
      console.log(code);

      toast.error("Noe gikk galt! Dataen ble ikke slettet.");
    }
    setLoding(false);
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Typography>
        Slett data:
      </Typography>
      <Box
        sx={{
          display: "flex"
        }}
      >
        <LoadingButton 
          variant="contained" 
          color={editing ? "warning" : "info"}
          size="large"
          endIcon={editing ? <LockOpen size="1.5rem" /> : <Lock size="1.5rem" />}
          onClick={onEditClick}
        >
          {editing ? "Avbryt" : "Lås opp"}
        </LoadingButton>
        <LoadingButton 
          variant="contained" 
          color="error"
          size="large"
          type="submit"
          onClick={deleteData}
          loading={loading}
          disabled={!editing}
          sx={{
            marginLeft: 1,
          }}
        >
          Slett data
        </LoadingButton>
      </Box>
    </Box>
  );
};