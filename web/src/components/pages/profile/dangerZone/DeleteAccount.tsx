import React, { useState } from 'react';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { toast } from 'react-toastify';
import { useFirebase } from '../../../../firebase';
import { FirebaseError } from 'firebase/app';
import { Lock } from '@styled-icons/material/Lock';
import { LockOpen } from '@styled-icons/material/LockOpen';

interface Props {
  reauthenticate: () => void
}

export const DeleteAccount: React.FC<Props> = ({ reauthenticate }) => {  
  const firebase = useFirebase();
  const [editing, setEditing] = useState(false);
  const [loading, setLoding] = useState(false);

  const onEditClick = () => {
    setEditing((editing) => !editing);
  };

  const deleteAccount = async () => {
    setLoding(true);
    try {
      await firebase.doDeleteAccount();
      setEditing(false);
      toast.success("Sletting av kontoen var vellykket!");
    } catch(error) {
      let code;
      if (error instanceof FirebaseError) {
        code = error.code;
      } else if (typeof error === "string") {
        code = error;
      }

      if (code === 'auth/requires-recent-login') {
        reauthenticate();
      } else {
        toast.error("Noe gikk galt! Kontoen ble ikke slettet.");
      }
    }
    setLoding(false);
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Typography>
        Slett Konto (og tilhørende data):
      </Typography>
      <Box
        sx={{
          display: "flex"
        }}
      >
        <LoadingButton 
          variant="contained" 
          color={editing ? "warning" : "info"}
          size="large"
          endIcon={editing ? <LockOpen size="1.5rem" /> : <Lock size="1.5rem" />}
          onClick={onEditClick}
        >
          {editing ? "Avbryt" : "Lås opp"}
        </LoadingButton>
        <LoadingButton 
          variant="contained" 
          color="error"
          size="large"
          type="submit"
          onClick={deleteAccount}
          loading={loading}
          disabled={!editing}
          sx={{
            marginLeft: 1,
          }}
        >
          Slett konto
        </LoadingButton>
      </Box>
    </Box>
  );
};