import { Paper, Box, Button, Typography, colors } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import React, { useState } from 'react';
import { ArrowLeftShort } from '@styled-icons/bootstrap/ArrowLeftShort';
import { useNavigate } from 'react-router-dom';
import { useFirebase } from '../../../../../firebase';
import { Report, ReportType, Tracking } from '../../../../../types';
import { toast } from 'react-toastify';
import { DeleteModal } from './DeleteModal';

interface Props {
  numberOfPages?: number,
  pdf?: Blob,
  reportType: ReportType,
  trackings: Array<Tracking>,
  generateOrShow: boolean,
  report?: Report,
}

export const Menu: React.FC<Props> = ({ numberOfPages, pdf, reportType, trackings, generateOrShow, report }) => {
  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const navigate = useNavigate();
  const firebase = useFirebase();
  const [loadingArchive, setLoadingArchive] = useState(false);
  const [loadingDownload, setLoadingDownload] = useState(false);
  const [loadingDelete, setLoadingDelete] = useState(false);

  const goToOverview = () => {
    navigate("/reports");
  };

  const downloadPdf = async () => {
    setLoadingDownload(true);
    try {
      await download();
      setLoadingDownload(false);
    } catch(e) {
      setLoadingDownload(false);
      toast.error("Noe gikk galt! Rapporten ble ikke lastet ned.");
    }
  };

  const downloadAndArchivePdf = async () => {
    setLoadingDownload(true);
    setLoadingArchive(true);
    try {
      download();
      await archive();
      setLoadingDownload(false);
      setLoadingArchive(false);
      navigate("/reports");
    } catch(e) {
      setLoadingDownload(false);
      setLoadingArchive(false);
      toast.error("Noe gikk galt! Rapport ble ikke arkivert.");
    }
  };

  const archivePdf = async () => {
    setLoadingArchive(true);
    try {
      await archive();
      setLoadingArchive(false);
      navigate("/reports");
    } catch(e) {
      setLoadingArchive(false);
      toast.error("Noe gikk galt! Rapport ble ikke arkivert.");
    }
  };

  const deleteReport = async () => {
    setLoadingDelete(true);
    try {
      if (report) {
        await firebase.doDeleteReport(report.storageRef, report.id);
        setLoadingDelete(false);
        toast.success("Rapport ble slettet!");
        navigate("/reports");
      } else {
        throw new Error("Report not found!");
      }
    } catch {
      setLoadingDelete(false);
      toast.error("Noe gikk galt! Rapport ble ikke arkivert.");
    }
  };

  const archive = async () => {
    if (pdf) {
      const numberOfTrackings = trackings.length;
      const seasonStart = trackings[0].startTimestamp.toDate();
      const seasonEnd = trackings[trackings.length-1].startTimestamp.toDate();
      await firebase.doSaveReport(pdf, reportType, seasonStart, seasonEnd, numberOfTrackings);
      toast.success("Rapport ble arkivert!");
    }
  };

  const download = () => {
    if (pdf) {
      const url = URL.createObjectURL(pdf);
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute(
        'download',
        `Rapport.pdf`,
      );
  
      // Append to html link element page
      document.body.appendChild(link);
  
      // Start download
      link.click();
  
      // Clean up and remove the link
      const linkParent = link.parentNode;
      if (linkParent) {
        linkParent.removeChild(link);
      }
      URL.revokeObjectURL(url);
    }
  };

  const onClickDeleteReport = () => {
    setShowDeleteModal(true);
  };
  
    return (
      <>
        <DeleteModal open={showDeleteModal} setOpen={setShowDeleteModal} deleteReport={deleteReport} />
        <Paper 
          elevation={2}
          sx={{
            padding: 1,
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Button 
            variant="contained" 
            size="large"
            type="submit"
            startIcon={<ArrowLeftShort size="2rem" />}
            sx={{
              backgroundColor: "#ffffff",
              color: "black",
              ':hover': {
                backgroundColor: "#f5f5f5",
                color: "black",
              }
            }}
            onClick={goToOverview}
            disabled={loadingArchive || loadingDownload}
          >
            Velg en annen rapport
          </Button>
          {generateOrShow ? (
            <>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  padding: 1,
                  borderBottom: "1px solid black"
                }}
              >
                <Typography>
                  Antall sider:
                </Typography>
                <Typography>
                  {numberOfPages ?? ""}
                </Typography>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  padding: 1,
                  borderBottom: "1px solid black"
                }}
              >
                <Typography>
                  Antall tilsynsrunder inkludert:
                </Typography>
                <Typography>
                  {trackings.length}
                </Typography>
              </Box>
              <LoadingButton 
                variant="contained" 
                color="info"
                size="large"
                type="submit"
                sx={{
                  marginTop: 5,
                }}
                loading={loadingArchive && !loadingDownload}
                disabled={loadingArchive || loadingDownload}
                onClick={archivePdf}
              >
                Arkiver rapport
              </LoadingButton>
              <LoadingButton 
                variant="contained" 
                color="success"
                size="large"
                type="submit"
                sx={{
                  marginTop: 1,
                }}
                loading={loadingArchive && loadingDownload}
                disabled={loadingArchive || loadingDownload}
                onClick={downloadAndArchivePdf}
              >
                Last ned og arkiver rapport
              </LoadingButton>
            </>
          ) : (
              <>
                <LoadingButton 
                  variant="contained" 
                  color="warning"
                  size="large"
                  type="submit"
                  sx={{
                    marginTop: 1,
                  }}
                  loading={loadingDelete}
                  disabled={loadingDelete || loadingDownload}
                  onClick={onClickDeleteReport}
                >
                  Slett rapport
                </LoadingButton>
                <LoadingButton 
                  variant="contained" 
                  color="success"
                  size="large"
                  type="submit"
                  sx={{
                    marginTop: 1,
                  }}
                  loading={loadingDownload}
                  disabled={loadingDownload || loadingDelete}
                  onClick={downloadPdf}
                >
                  Last ned rapport
                </LoadingButton>
            </>
          )}
        </Paper>
      </>
    );
};
