import * as React from 'react';
import Button from '@mui/material/Button';
import Slide from '@mui/material/Slide';
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";
import {TransitionProps} from "@mui/material/transitions";

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

interface Props {
    open: boolean,
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    deleteReport: () => Promise<void>
}

export const DeleteModal: React.FC<Props> = ({ open, setOpen , deleteReport }) => {
    const handleClose = () => setOpen(false);

    const handleDelete = async () => {
        deleteReport();
        handleClose();
    };

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle>{`Slett rapport?`}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                    Er du sikker på at du vil slette rapporten?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Nei</Button>
                <Button onClick={handleDelete}>Ja</Button>
            </DialogActions>
        </Dialog>
    );
};