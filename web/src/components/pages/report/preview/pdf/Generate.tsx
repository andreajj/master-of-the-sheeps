import React, { useState, useEffect } from 'react';
import { styled, Box } from '@mui/material';
import { jsPDF } from 'jspdf';
import autoTable, {CellInput, RowInput} from 'jspdf-autotable';
import { ReportType, Tracking } from '../../../../../types';
import { useStore } from '../../../../../state/store';
import { Pdf } from './Pdf';
import { useFirebase } from '../../../../../firebase';

interface Props {
  trackings: Array<Tracking>,
  setNumberOfPages: React.Dispatch<React.SetStateAction<number | undefined>>,
  reportType: ReportType,
  pdf?: Blob,
  setPdf: React.Dispatch<React.SetStateAction<Blob | undefined>>,
}

export const Generate: React.FC<Props> = ({ trackings, setNumberOfPages, reportType, pdf, setPdf }) => {
  const firebase = useFirebase();
  const [state] = useStore();

    const doc = new jsPDF({
      unit: "mm",
    });

    //const lastAutoTable = (doc as any).lastAutoTable;

    const getCenterX = (text: string) => {
      const fontSize = doc.getFontSize();

      const pageWidth = doc.internal.pageSize.width;

      const txtWidth = doc.getStringUnitWidth(text)*fontSize/doc.internal.scaleFactor;

      return ( pageWidth - txtWidth ) / 2;
    };

    const generateTable = (table: RowInput[], head?: RowInput[], linkColumn?: number) => {
      const headerText = "Rapport";
      const headerUserInfoText = `Eksportert av ${
        state.user?.email
      } den ${
        (new Date()).toLocaleDateString('nb-NO', {year: 'numeric', month: '2-digit', day: '2-digit'})
      }.`;
      const headerReportTypeText = `Type rapport: ${reportType}`;
      const footerText = `Side ${(doc.internal.pages.length - 1)}`;
      const headerFontSize = 36;
      const headerInfoFontSize = 10;
      const bodyFontSize = 12;
      const startMargin = 20;

      let y = startMargin;      
      y += doc.getTextDimensions(headerText, {fontSize: headerFontSize}).h;
      y += doc.getTextDimensions(headerUserInfoText, {fontSize: headerInfoFontSize}).h;
      y += 5;
      y += doc.getTextDimensions(headerReportTypeText, {fontSize: headerInfoFontSize}).h;
      y += 5;
      y += 1;
      y += 5;

      const pageMargin = {
        top: y,
        horizontal: 15,
        bottom: 25.4,
      };

      autoTable(doc, {
        didDrawPage: function (data) {
          const pageSize = doc.internal.pageSize;
          const pageHeight = pageSize.height
            ? pageSize.height
            : pageSize.getHeight();
          const pageWidth = pageSize.width
            ? pageSize.width
            : pageSize.getWidth();

          let y = startMargin;

          // Header - Title
          doc.setFontSize(headerFontSize);
          doc.setTextColor(40);
          doc.text(headerText, getCenterX(headerText), y);
          
          y += doc.getTextDimensions(headerText, {fontSize: headerFontSize}).h;
          //y += 10;

          // Header - User info
          doc.setFontSize(headerInfoFontSize);
          doc.setTextColor(0, 0, 0, 0.5);
          doc.text(headerUserInfoText, data.settings.margin.left, y);

          y += doc.getTextDimensions(headerUserInfoText, {fontSize: headerInfoFontSize}).h;
          y += 5;

          // Header - Report info
          doc.text(headerReportTypeText, data.settings.margin.left, y);
          y += doc.getTextDimensions(headerReportTypeText, {fontSize: headerInfoFontSize}).h;
          y += 5;

          // Header end
          doc.setDrawColor(40);
          doc.line(data.settings.margin.left, y, pageWidth - data.settings.margin.left, y);
          y += 1;
          y += 5;
      
          // Footer
          doc.setFontSize(bodyFontSize);
          doc.setTextColor(40);
          
          doc.text(footerText, pageWidth - data.settings.margin.right, pageHeight - 10);
        },
        didDrawCell: linkColumn ? ((data) => {
          if (data.column.index === linkColumn && data.cell.section === 'body') {
             const textPos = data.cell.getTextPos();
             const link = data.cell.text[0] + data.cell.text[1];
             const x = textPos.x;
             const y = textPos.y + (parseInt(data.cell.styles.cellPadding.toString())*3);
             doc.textWithLink(data.cell.text[0], x, y, {url: link});
             const diff = doc.getTextDimensions(headerReportTypeText, {fontSize: data.cell.styles.fontSize}).h + 0.5;
             doc.textWithLink(data.cell.text[1], x, y + diff, {url: link});
          }
        }) : undefined,
        margin: pageMargin,
        head: head,
        body: table,
        columnStyles: linkColumn ? {
          1: {
            cellWidth: 'auto'
          },
          2: {
            cellWidth: 34
          },
          8: {
            cellWidth: 10
          }
        } : undefined,
      });
    };

    const generateSummary = () => {
      const herds = trackings.flatMap((tracking) => tracking.sheepHerds);
      const numberOfTrackings = trackings.length;
      const numberOfsheep = herds.map((herd) => Object.values(herd.sheeps).reduce((prev, cur) => prev + cur, 0)).reduce((prev, cur) => prev + cur, 0);
      const numberOfLambs = herds.map((herd) => herd.lambs).reduce((prev, cur) => prev + cur, 0);
      const trackingsWithWoundedSheep = trackings.filter((tracking) => tracking.sheepHerds.map((herd) => herd.woundedAnimals).reduce((prev, cur) => prev + cur, 0) > 0);
      const numberOfTrackingsWithWoundedSheep = trackingsWithWoundedSheep.length;
      const numberOfWoundedSheep = trackingsWithWoundedSheep.flatMap((tracking) => tracking.sheepHerds.map((herd) => herd.woundedAnimals)).reduce((prev, cur) => prev + cur, 0);
      const trackingsWithDeadsheep = trackings.filter((tracking) => tracking.deadSheeps.length > 0);
      const numberOfTrackingsWithDeadSheep = trackingsWithDeadsheep.length;
      const numberOfDeadSheep = trackingsWithDeadsheep.map((tracking) => tracking.deadSheeps.length).reduce((prev, cur) => prev + cur, 0);
      const representedFarms = trackings.flatMap((tracking) => tracking.farms);
      const uniqueRepresentedFarms = [...new Set(representedFarms)]; 
      const numberOfUniqueRepresentedFarms = uniqueRepresentedFarms.length;
      const predators = trackings.flatMap((tracking) => tracking.predators);
      const numberOfPredators = predators.length;
      const others = trackings.flatMap((tracking) => tracking.others);
      const numberOfOthers = others.length;
      
      const table: Array<[CellInput, CellInput]> = [
        ['Antall tilsyn utført:', numberOfTrackings],
        ['Antall sau observert:', numberOfsheep + numberOfLambs],
        [{
          content: 'Derav voksne sau:',
          styles: {
            cellPadding: {
              top: 2,
              right: 0,
              left: 10,
              bottom: 2,
            }
          }
        }, numberOfsheep],
        [{
          content: 'Derav lam:',
          styles: {
            cellPadding: {
              top: 2,
              right: 0,
              left: 10,
              bottom: 2,
            }
          }
        }, numberOfLambs],
        ['Antall tilsynsrunder med skadet sau:', numberOfTrackingsWithWoundedSheep],
        ['Antall tilsynsrunder med død sau:', numberOfTrackingsWithDeadSheep],
        ['Antall døde sau:', numberOfDeadSheep],
        ['Antall skadet sau:', numberOfWoundedSheep],
        ['Antall unike gårder representert:', numberOfUniqueRepresentedFarms],
      ];

      for (let i = 0; i < uniqueRepresentedFarms.length; i++) {
        const farm = uniqueRepresentedFarms[i];
        table.push([{
          content: `Gård ${i + 1}:`,
          styles: {
            cellPadding: {
              top: 2,
              right: 0,
              left: 10,
              bottom: 2,
            }
          }
        }, farm]);
      }

      const endOfTable: Array<[string, number]> = [
        ['Antall rovdyr eller rovdyrtegn observert:', numberOfPredators],
        ['Antall andre typer observasjoner:', numberOfOthers]
      ];
      table.push(...endOfTable);

      generateTable(table);
    };

    const generateOverviewOfTrackings = async () => {
      const head = [['Dato', 'Beskrivelse av ruta', 'Bilde av ruta','Antall sau', 'Antall lam', 'Døde sau', 'Skadde sau', 'Rovdyr', 'Annet']];
      const table = [];
      for (const tracking of trackings) {
        const herds = tracking.sheepHerds;
        const deadSheep = tracking.deadSheeps;
        const predators = tracking.predators;
        const others = tracking.others;

        const date = tracking.startTimestamp.toDate().toLocaleDateString('nb-NO', {year: 'numeric', month: '2-digit', day: '2-digit'});
        const routeDescription = tracking.routeDescription ?? "";
        const numberOfSheeps = herds.map((herd) => Object.values(herd.sheeps).reduce((prev, cur) => prev + cur, 0)).reduce((prev, cur) => prev + cur, 0);
        const numberOfLambs = herds.map((herd) => herd.lambs).reduce((prev, cur) => prev + cur, 0);
        const numberOfDeadSheeps = deadSheep.length;
        const numberOfWoundedSheeps = herds.map((herd) => herd.woundedAnimals).reduce((prev, cur) => prev + cur, 0);
        const numberOfpredators = predators.length;
        const numberOfOthers = others.length;

        const url =`https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${firebase.getApiKey()}`;
        const response = await fetch(url, {
          method: 'POST',
          mode: 'cors',
          cache: 'no-cache',
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json'
          },
          redirect: 'follow',
          referrerPolicy: 'no-referrer',
          body: JSON.stringify({
            dynamicLinkInfo: {
              domainUriPrefix: "https://sporing.page.link",
              link: `https://sheep-tracker-master.web.app/view/${tracking.id}`,
            },
            suffix: {
              option: "SHORT"
            }
          })
        });
        const shortUrl: {
          previewLink: string,
          shortLink: string,
          warning: Array<{
            warningCode: string,
            warningMessage: string,
          }>
        } = await response.json();

        const row = [
          date, 
          routeDescription,
          shortUrl.shortLink,
          numberOfSheeps, 
          numberOfLambs, 
          numberOfDeadSheeps, 
          numberOfWoundedSheeps, 
          numberOfpredators,
          numberOfOthers,
        ];
        table.push(row);
      }

      generateTable(table, head, 2);
    };

    useEffect(() => {
      const init = async () => {
        generateSummary();
        doc.addPage();
        await generateOverviewOfTrackings();
        
        setNumberOfPages(doc.getNumberOfPages());

        const file = doc.output("blob");
        setPdf(file);
      };
      init();
    }, [trackings]);

    return (
      <Pdf pdf={pdf}  />
    );
};