import { Grid, Box } from '@mui/material';
import React, { useState, useEffect } from 'react';
import { Report, ReportType, Tracking } from '../../../../../types';
import { Menu } from './Menu';
import { Generate } from './Generate';
import { useFirebase } from '../../../../../firebase';
import { Pdf } from './Pdf';

interface Props {
  trackings?: Array<Tracking>,
  reportType: ReportType,
  report?: Report,
  generateOrShow: boolean,
}

export const Preview: React.FC<Props> = ({ trackings = [], reportType, report, generateOrShow }) => {
  const firebase = useFirebase();
  const [numberOfPages, setNumberOfPages] = useState<number>();
  const [pdf, setPdf] = useState<Blob>();

  useEffect(() => {
    const init = async () => {
      if (!generateOrShow && report) {
        const reportBlob = await firebase.doDownloadReport(report.storageRef);
        setPdf(reportBlob);
      }
    };
    init();
  }, [firebase, generateOrShow, report]);

    return (
      <Grid
        container
        spacing={0}
        direction="row"
        style={{ minHeight: '100vh' }}
      >
        <Box
          sx={{
            alignSelf: "flex-start"
          }}
        >
          <Menu 
            numberOfPages={numberOfPages}
            pdf={pdf}
            reportType={reportType}
            trackings={trackings}
            generateOrShow={generateOrShow}
            report={report}
          />
        </Box>
        <Box
          sx={{
            flex: 1,
            display: "flex",
          }}
        >
          {generateOrShow ? (
            <Generate trackings={trackings} setNumberOfPages={setNumberOfPages} reportType={reportType} pdf={pdf} setPdf={setPdf} />
          ) : (
            <Pdf pdf={pdf} />
          )}
        </Box>
      </Grid>
    );
};
