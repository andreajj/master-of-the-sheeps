import React, { useState, useEffect } from 'react';
import { styled, Box } from '@mui/material';
import { jsPDF } from 'jspdf';
import autoTable, {CellInput, RowInput} from 'jspdf-autotable';
import { ReportType, Tracking } from '../../../../../types';
import { useStore } from '../../../../../state/store';

const Wrapper = styled(Box)`
    flex: 1;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    padding: 10mm;
`;

const Page = styled(Box)`
  width: 100%;
  height: 100%;
  min-width: 210mm;
  max-height: 297mm;
  border: 1px #d3d3d3 solid;
  border-radius: 5px;
  overflow: hidden;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
`;

const Iframe = styled('iframe')`
  width: inherit;
  min-height: inherit;
  height: inherit;
  background: white;
  display: flex;
`;

interface Props {
  pdf?: Blob,
}

export const Pdf: React.FC<Props> = ({ pdf }) => {
  const [pdfUrl, setPdfUrl] = useState<string>();

  useEffect(() => {
    if (pdf) {
      const url = URL.createObjectURL(pdf);
      setPdfUrl(url);
      return () => {
        URL.revokeObjectURL(url);
      };
    }
  }, [pdf]);

    return (
      <Wrapper>
        <Page>
          {pdfUrl && (<Iframe title="pdf" src={`${pdfUrl}#toolbar=0`}></Iframe>)}
        </Page>
      </Wrapper>
    );
};