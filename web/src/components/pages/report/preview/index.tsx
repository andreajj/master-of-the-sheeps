import React, { useEffect, useState } from 'react';
import { useParams, useNavigate, Navigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useFirebase } from '../../../../firebase';
import { Report, ReportType, Tracking } from '../../../../types';
import { LoadingScreen } from './LoadingScreen';
import { Preview } from './pdf/Preview';

export const ReportPreview: React.FC = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const firebase = useFirebase();
  const [generatingPdf, setGeneratingPdf] = useState(true);
  const [dataLoadOver, setDataLoadLover] = useState(false);
  const [loadTimeProgress, setLoadTimeProgress] = useState(0);
  const loadingTimeSeconds = 10;
  const generateId = "generate";
  const generateOrShow = id == generateId;
  const [report, setReport] = useState<Report>();
  const [trackings, setTrackings] = useState<Array<Tracking>>();

  useEffect(() => {
    if (generateOrShow && !generatingPdf && trackings && trackings.length === 0) {
      toast.error("Du har ingen sporinger å generere rapport ut ifra.");
      navigate("/reports");
    }
  }, [generateOrShow, generatingPdf, trackings]);

  useEffect(() => {
    if (dataLoadOver && loadTimeProgress === 100) {
      setGeneratingPdf(false);
    } 
  }, [dataLoadOver, loadTimeProgress]);

  useEffect(() => {
    if (report || trackings) {
      setDataLoadLover(true);
    }
  }, [report, trackings]);

  useEffect(() => {
    if (dataLoadOver && loadTimeProgress === 99) {
      setLoadTimeProgress(100);
    } else if (!dataLoadOver && loadTimeProgress === 99) {
      setLoadTimeProgress(66);
      const loadingInterval = setInterval(() => {
        setLoadTimeProgress((prev) => {
          if (prev === 98) {
            clearInterval(loadingInterval);
            return 99;
          }
          return prev + 1;
        });
      }, loadingTimeSeconds / 100 * 1000);
    }
  }, [dataLoadOver, loadTimeProgress]);

  useEffect(() => {
    setLoadTimeProgress(0);
    const initGenerate = async () => {
      const date = new Date();
      const from = new Date(date.getFullYear(), 0, 1, 0, 0, 0);
      const to = new Date();
      const trackings = await firebase.doGetTrackingsOnce(from, to);
      setTrackings(trackings);
    };
    const initReport = async (id: string) => {
      const report = await firebase.doGetReport(id);
      if (report) {
        setReport(report);
      } else {
        navigate("/reports");
      }
    };

    const loadingInterval = setInterval(() => {
      setLoadTimeProgress((prev) => {
        if (prev === 98) {
          clearInterval(loadingInterval);
          return 99;
        }
        return prev + 1;
      });
    }, loadingTimeSeconds / 100 * 1000);

    if (id === generateId) {
      initGenerate();
    } else if(id) {
      initReport(id);
    } else {
      navigate("/reports");
    }

    return () => {
      clearInterval(loadingInterval);
    };
  }, [id]);

    return (
      <>
        {generatingPdf && (
          <LoadingScreen generateOrShow={generateOrShow} progress={loadTimeProgress} />
        )}
        {!generatingPdf && (!generateOrShow || (trackings && trackings.length > 0)) && (
            <Preview 
              trackings={trackings} 
              reportType={ReportType.sessongRapport} 
              generateOrShow={generateOrShow} 
              report={report}
            />
        )}
      </>
    );
};
