import { Typography, Grid, LinearProgress, Box } from '@mui/material';
import React, { useEffect } from 'react';

interface Props {
  generateOrShow: boolean,
  progress: number,
}

export const LoadingScreen: React.FC<Props> = ({ generateOrShow, progress }) => {
    return (
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: '100vh' }}
      >
        <Box>
          <Typography 
              variant="h2" 
              component="h2" 
              align="center"
            >
              {`${generateOrShow ? "Generer" : "Laster inn"} rapport... Vennligst ikke forlat siden.`}
          </Typography>
          <LinearProgress variant="determinate" value={progress} sx={{width: "100%"}} />
        </Box>
      </Grid>
    );
};
