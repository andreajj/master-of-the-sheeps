import { Typography, Grid, Paper, Box, colors } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useFirebase } from '../../../../firebase';
import { Report } from '../../../../types';
import { List } from './List';

export const ReportOverview: React.FC = () => {
  const navigate = useNavigate();
  const firebase = useFirebase();
  const [reports, setReports] = useState<Array<Report>>();

  const generateReport = () => {
    navigate("/reports/generate");
  };

  useEffect(() => {
    const init = async () => {
      const reports = await firebase.doGetReports();
      setReports(reports);
    };
    init();
  }, []);

    return (
      <Grid
        container
        spacing={0}
        direction="row"
        sx={(theme) => ({

        })}
      >
        <Box
          sx={{
            display: "flex",
            maxHeight: "-webkit-fill-available",
          }}
        >
          <List reports={reports} />
        </Box>
        <Box
          sx={{
            flex: 1,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Paper elevation={2} sx={{ display: "flex", flexDirection: "column", width: 400, height: 400, cursor: "pointer" }}>
            <Typography 
              variant="h3" 
              component="h2" 
              align="center"
              sx={{
                flex: 1,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                padding: 1,
                backgroundColor: colors.green[300]
              }}
              onClick={generateReport}
            >
              Generer ny rapport
            </Typography>
            <Box sx={{
              flex: 2,
              display: "flex",
              alignItems: "flex-end",
              padding: 1,
            }}>
              <Typography variant="body1" component="p">
                Rapport som oppsummerer hele nåværende beitesesongen from til i dag. Alle tilsynsrunder i løpet av sesongen kommer med.
              </Typography>
            </Box>
          </Paper>
        </Box>
      </Grid>
    );
};
