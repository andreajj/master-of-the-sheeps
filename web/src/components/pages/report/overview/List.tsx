import { Typography, Grid, Paper, Box, styled } from '@mui/material';
import React, { useEffect } from 'react';
import { Report } from '../../../../types';
import { useNavigate } from 'react-router-dom';

interface Props {
  reports?: Array<Report>
}

const TextRow = styled(Box)`
  flex-wrap: nowrap;
  display: flex;
  justify-content: space-between;
  margin-top: 5px;
  margin-bottom: 5px;
`;

export const List: React.FC<Props> = ({ reports }) => {
  const navigate = useNavigate();

  const goToReport = (id: string) => {
    navigate(`/reports/${id}`);
  };

    return (
      <Paper
        elevation={2}
        sx={{
          flex: 1,
          display: "flex",
          flexDirection: "column",
          width: "350px"
        }}
      >
        <Typography 
          component="h2" 
          variant="h4"
          sx={{
            textAlign: "center",
            marginBottom: 1,
          }}
        >
            Tidligere rapporter
        </Typography>
        <Box
          sx={{
            flex: 1,
            display: "flex",
            flexDirection: "column",
            overflow: "auto",
          }}
        >
          {!reports && (
            <Typography
            sx={{
              textAlign: "center",
            }}
          >
            Laster inn...
          </Typography>
          )}
          {reports && reports.length === 0 && (
            <Typography
              sx={{
                textAlign: "center",
              }}
            >
              Ingen rapporter å vise
            </Typography>
          )}
          {reports && reports.map((report, index) => (
            <Box 
              key={report.id}
              sx={{
                padding: 1,
                backgroundColor: (index % 2 === 0 ? "#f5f5f5" : "#ffffff"),
                cursor: "pointer",
              }}
              onClick={() => goToReport(report.id)}
            >
              <TextRow>
                <Typography>
                  Dato generert:
                </Typography>
                <Typography>
                  {report.timestamp.toDate().toLocaleDateString('nb-NO', {year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit'})}
                </Typography>
              </TextRow>
              <TextRow>
                <Typography>
                  Sesong:
                </Typography>
                <Typography>
                  {report.seasonStart.toDate().getFullYear() !== report.seasonEnd.toDate().getFullYear()
                  ? (
                    `${report.seasonStart.toDate().getFullYear()}/${report.seasonEnd.toDate().getFullYear()}`
                  ): (
                    `${report.seasonStart.toDate().getFullYear()}`
                  )}
                </Typography>
              </TextRow>
              <TextRow>
                <Typography>
                  Type:
                </Typography>
                <Typography>
                    {report.type}
                </Typography>
              </TextRow>
              <TextRow>
                <Typography>
                  Antall tilsynsrunder:
                </Typography>
                <Typography>
                  {report.numberOfTrackings}
                </Typography>
              </TextRow>
            </Box>
          ))}
        </Box>
      </Paper>
    );
};
