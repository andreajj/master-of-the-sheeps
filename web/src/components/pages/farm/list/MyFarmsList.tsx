/*
 * @created 04/02/2022 - 15:42
 * @project web
 * @author andreasjj
 */
import {
    Grid,
    Paper,
    Typography
} from '@mui/material';
import React, {useEffect, useState} from 'react';
import { Divider } from '@mui/material';
import {FarmsList} from "./FarmsList";
import {useFirebase} from "../../../../firebase";
import {DocumentData, QuerySnapshot} from "firebase/firestore";
import { Farm } from '../../../../types';

export const MyFarmsList: React.FC = () => {
    const firebase = useFirebase();
    const [farms, setFarms] = useState<Array<Farm>>([]);

    useEffect(() => {
        const onNext = (snapshot: QuerySnapshot<DocumentData>) => {
            const _farms: Array<Farm> = [];
            for(const doc of snapshot.docs) {
                const data = doc.data();
                const newFarm: Farm = {
                    name: doc.id,
                    color1: data.color1,
                    owner: {
                        uuid: data.owner.uuid,
                        email: data.owner.email
                    }
                };
                if (data.color2) {
                    newFarm.color2 = data.color2;
                }
                _farms.push(newFarm);
            }
            setFarms(_farms);
        };

        const onError = () => {
            setFarms([]);
        };

        const listener = firebase.doGetMyFarms(onNext, onError);

        return () => {
            listener();
        };
    }, []);

    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            flex={1}
            sx={{paddingBottom: 1, paddingTop: 1}}
        >
            <Paper
                elevation={2}
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    width: 3/4,
                    maxWidth: 600,
                    padding: 5,
                    flex: 'auto'
                }}
            >
                <Typography
                    variant="h4"
                    component="h2"
                    align="left"
                    sx={{
                        marginBottom: 1,
                    }}
                >
                    Mine Gårder
                </Typography>
                <Divider sx={{ marginBottom: 1 }} />
                <FarmsList farms={farms} editable={true} />
            </Paper>
        </Grid>
    );
};
