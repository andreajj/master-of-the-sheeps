/*
 * @created 05/02/2022 - 00:08
 * @project web
 * @author andreasjj
 */
import * as React from 'react';
import Button from '@mui/material/Button';
import Slide from '@mui/material/Slide';
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";
import {TransitionProps} from "@mui/material/transitions";
import {useFirebase} from "../../../../firebase";
import { Farm } from '../../../../types';

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

interface Props {
    open: boolean,
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    farm: Farm
}

export const DeleteModal: React.FC<Props> = ({ open, setOpen , farm }) => {
    const firebase = useFirebase();

    const handleClose = () => setOpen(false);

    const deleteFarm = async () => {
        await firebase.doDeleteFarm(farm.name);
        handleClose();
    };

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle>{`Slett '${farm.name}'?`}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                    Er du sikker på at du vil slette gården?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Nei</Button>
                <Button onClick={deleteFarm}>Ja</Button>
            </DialogActions>
        </Dialog>
    );
};