/*
 * @created 05/02/2022 - 00:08
 * @project web
 * @author andreasjj
 */
/*
 * @created 05/02/2022 - 00:08
 * @project web
 * @author andreasjj
 */
import * as React from 'react';
import Button from '@mui/material/Button';
import Slide from '@mui/material/Slide';
import {
    Box,
    colors,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    FormControlLabel,
    Switch,
    TextField,
    Typography
} from "@mui/material";
import {TransitionProps} from "@mui/material/transitions";
import {useState} from "react";
import {ColorSelector} from "../registration/ColorSelector";
import {CurrentColor} from "../registration/CurrentColor";
import {useFirebase} from "../../../../firebase";
import { Farm } from '../../../../types';
import { LoadingButton } from '@mui/lab';
import TextInput from '../../entry/TextInput';

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

interface Props {
    open: boolean,
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    farm: Farm
}

export const EditModal: React.FC<Props> = ({ open, setOpen , farm }) => {
    const firebase = useFirebase();

    const [color1, setColor1] = useState(farm.color1);
    const [color2, setColor2] = useState(farm.color2 ?? "#ffffff");
    const [twoColors, setTwoColors] = useState(!!farm.color2);
    const [newFarmName, setNewFarmName] = useState(farm.name);
    const [editing, setEditing] = useState(false);

    const handleClose = () => setOpen(false);

    const editFarm = async () => {
        console.log(color1, color2, twoColors);
        await firebase.doEditFarm(farm, color1, color2, twoColors, newFarmName);
        handleClose();
    };

    const handleSwitchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setTwoColors(event.target.checked);
    };

    const onEditClick = () => {
        setEditing((cur) => {
            if (cur) {
                setNewFarmName(farm.name);
                return false;
            } else {
                return true;
            }
        });
    };

    const onFarmNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setNewFarmName(event.target.value);
    };

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted={false}
            onClose={handleClose}
        >
            <DialogTitle>{`Endre '${farm.name}'?`}</DialogTitle>
            <DialogContent 
                sx={{ 
                    display: 'flex', 
                    flexDirection: "column", 
                    alignItems: 'flex-start',
                    width: "500px"
                }}
            >
                <FormControlLabel control={<Switch checked={twoColors} onChange={handleSwitchChange} />} label="2 Farger" />
                <Box sx={{ display: 'flex', width: "100%", justifyContent: 'space-between', alignItems: 'center'}}>
                    <Box sx={{ marginRight: twoColors ? 1 : 0 }}>
                        <ColorSelector color={color1} setColor={setColor1} />
                        <CurrentColor color={color1} label="Valgt farge 1:" />
                    </Box>
                    <Box sx={{ marginLeft: 1 }}>
                        {twoColors && (
                            <>
                                <ColorSelector color={color2} setColor={setColor2} />
                                <CurrentColor color={color2} label="Valgt farge 2:" />
                            </>
                        )}
                    </Box>
                </Box>
                <Box
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                        width: "100%",
                    }}
                >
                    <Box
                        sx={{
                            display: "flex",
                            flex: 1,
                        }}
                    >
                        <TextField
                            fullWidth={true}
                            label="Gårdsnavn"
                            name="farm"
                            variant="filled"
                            onChange={onFarmNameChange}
                            value={newFarmName}
                            disabled={!editing}
                        />
                        <LoadingButton 
                            variant="contained" 
                            color={editing ? "warning" : "info"}
                            size="large"
                            onClick={onEditClick}
                            sx={{
                            marginLeft: 1,
                            }}
                        >
                            {editing ? "Avbryt" : "Endre"}
                        </LoadingButton>
                    </Box>
                    {editing && (
                        <Typography
                            sx={{
                                flex: 1,
                                color: colors.red[500],
                                fontSize: "0.8rem"
                            }}
                        >
                            <b>ADVARSEL: </b> Å endre navn på gården vil føre til at du mister tilgang til sporinger assosisert med det tidligere navnet med mindre sporingene blir oppdatert med det nye navnet.
                        </Typography>
                    )}
                </Box>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Avbryt</Button>
                <Button onClick={editFarm}>Lagre</Button>
            </DialogActions>
        </Dialog>
    );
};