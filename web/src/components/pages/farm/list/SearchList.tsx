/*
 * @created 04/02/2022 - 15:43
 * @project web
 * @author andreasjj
 */
import {Box, Grid, IconButton, InputAdornment, Paper, TextField, Typography} from '@mui/material';
import React, {useEffect, useState} from 'react';
import { Divider } from '@mui/material';
import {FarmsList} from "./FarmsList";
import { Search } from "styled-icons/material";
import {useFirebase} from "../../../../firebase";
import {useStore} from "../../../../state/store";
import {DocumentData, QuerySnapshot} from "firebase/firestore";
import { Farm } from '../../../../types';

export const SearchList: React.FC = () => {
    const firebase = useFirebase();
    const [state] = useStore();
    const [searchText, setSearchText] = useState("");
    const [farms, setFarms] = useState<Array<Farm>>([]);

    useEffect(() => {
        if (searchText === "") {
            return setFarms([]);
        }

        const onNext = (snapshot: QuerySnapshot<DocumentData>) => {
            const _farms: Array<Farm> = [];
            for(const doc of snapshot.docs) {
                const data = doc.data();
                const newFarm: Farm = {
                    name: doc.id,
                    color1: data.color1,
                    owner: {
                        uuid: data.owner.uuid,
                        email: data.owner.email
                    }
                };
                if (data.color2) {
                    newFarm.color2 = data.color2;
                }
                _farms.push(newFarm);
            }
            setFarms(_farms.filter((farm) => farm.owner.uuid !== state.user?.uid));
        };

        const onError = () => {
            setFarms([]);
        };

        const listener  = firebase.doSearchFarms(searchText, onNext, onError);

        return () => {
            listener();
        };
    }, [searchText]);

    const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchText(event.target.value);
    };

    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            flex={1}
            sx={{paddingBottom: 1, paddingTop: 1}}
        >
            <Paper
                elevation={2}
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    width: 3/4,
                    maxWidth: 600,
                    padding: 5,
                    flex: 'auto'
                }}
            >
                <Box
                    sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-between",
                        marginBottom: 1,
                    }}
                >
                    <Typography
                        variant="h4"
                        component="h2"
                        align="left"
                        sx={{
                            marginRight: 5,
                        }}
                    >
                        Søkeresultat
                    </Typography>
                    <TextField
                        id="farms-list-search-textinput"
                        label="Søk..."
                        type="text"
                        variant="standard"
                        value={searchText}
                        onChange={handleSearchChange}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <Search size="1em" />
                                </InputAdornment>
                            ),
                        }}
                    />
                </Box>
                <Divider sx={{ marginBottom: 1 }} />
                <FarmsList farms={farms} editable={false} />
            </Paper>
        </Grid>
    );
};
