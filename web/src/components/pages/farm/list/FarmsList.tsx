/*
 * @created 04/02/2022 - 18:12
 * @project web
 * @author andreasjj
 */

import {FixedSizeList, ListChildComponentProps} from "react-window";
import {Box, Divider, Grid, IconButton, ListItem, ListItemText, Paper, Typography} from "@mui/material";
import {Delete} from "@styled-icons/material/Delete";
import {Edit} from "@styled-icons/boxicons-regular/Edit";
import React, {useEffect, useState} from "react";
import AutoSize from "react-virtualized-auto-sizer";
import {DeleteModal} from "./DeleteModal";
import {EditModal} from "./EditModal";
import { Farm } from "../../../../types";

function renderRow(props: ListChildComponentProps<[Array<Farm>, React.Dispatch<React.SetStateAction<boolean>>, React.Dispatch<React.SetStateAction<boolean>>, React.Dispatch<React.SetStateAction<Farm | undefined>>, boolean]>) {
    const { index, style, data } = props;
    const rowData = data[0][index];
    const setShowDeleteModal = data[1];
    const setShowEditModal = data[2];
    const setSelectedFarm = data[3];
    const editable = data[4];

    const onDeleteClick = () => {
        setShowDeleteModal(true);
        setSelectedFarm(rowData);
    };

    const onEditClick = () => {
        setShowEditModal(true);
        setSelectedFarm(rowData);
    };

    return (
        <ListItem
            sx={{
                background: rowData.color2 ? `linear-gradient(90deg, ${rowData.color1} 0%, ${rowData.color1} 50%, ${rowData.color2} 50%, ${rowData.color2} 100%);` : rowData.color1,
                paddingRight: '96px',
                paddingLeft: 1
            }}
            style={style} key={index}
            component="div"
            disablePadding
            secondaryAction={editable ? (
                <>
                    <IconButton edge="end" aria-label="delete" onClick={onDeleteClick}>
                        <Delete size="1rem" />
                    </IconButton>
                    <IconButton edge="end" aria-label="edit" onClick={onEditClick}>
                        <Edit size="1rem" />
                    </IconButton>
                </>
            ) : undefined}
        >
            <ListItemText primary={`${rowData.name} — ${rowData.owner.email}`} />
        </ListItem>
    );
}

interface Props {
    farms: Array<Farm>,
    editable?: boolean
}

export const FarmsList: React.FC<Props> = ({ farms, editable }) => {
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    const [selectedFarm, setSelectedFarm] = useState<Farm>();

    useEffect(() => {
        if (!(showDeleteModal || showEditModal)) {
            setSelectedFarm(undefined);
        }
    }, [showDeleteModal, showEditModal]);

    return (
        <Box sx={{minHeight: 50, flex: 1}}>
            {selectedFarm && (
                <DeleteModal open={showDeleteModal} setOpen={setShowDeleteModal} farm={selectedFarm}  />
            )}
            {selectedFarm && (
                <EditModal open={showEditModal} setOpen={setShowEditModal} farm={selectedFarm}  />
            )}
            <AutoSize>
                {({ height, width }) => (
                    <FixedSizeList
                        height={height}
                        width={width}
                        itemSize={46}
                        itemCount={farms.length}
                        itemData={[farms, setShowDeleteModal, setShowEditModal, setSelectedFarm, !!editable]}
                        overscanCount={5}
                    >
                        {renderRow}
                    </FixedSizeList>
                )}
            </AutoSize>
        </Box>
    );
};