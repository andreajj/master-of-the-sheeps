/*
 * @created 03/02/2022 - 21:14
 * @project master-of-the-sheeps
 * @author andreasjj
 */

import {Box, Grid, Paper, Typography} from '@mui/material';
import React, {useState} from 'react';
import { Divider } from '@mui/material';
import {MyFarmsList} from "./MyFarmsList";
import {SearchList} from "./SearchList";

export const FarmList: React.FC = () => {

    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            flex={1}
        >
            <MyFarmsList />
            <SearchList />
        </Grid>
    );
};
