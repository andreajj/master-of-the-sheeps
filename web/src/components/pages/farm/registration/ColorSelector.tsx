/*
 * @created 05/02/2022 - 16:48
 * @project web
 * @author andreasjj
 */
import React, {useState} from 'react';
import styled from "styled-components";
import {HexColorPicker} from "react-colorful";

const StyledColorSelector = styled(HexColorPicker)`
    flex: 1;
    
    &:first-child {
        margin-bottom: 8px;
    }
`;

interface Props {
    color: string,
    setColor: React.Dispatch<React.SetStateAction<string>>,
}

export const ColorSelector: React.FC<Props> = ({ color, setColor}) => {
    return (
        <StyledColorSelector color={color} onChange={setColor} />
    );
};
