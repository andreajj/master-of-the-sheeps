/*
 * @created 03/02/2022 - 21:14
 * @project master-of-the-sheeps
 * @author andreasjj
 */

import {Box, Button, Divider, FormControlLabel, Grid, Paper, Switch, Typography} from '@mui/material';
import React, {useState} from 'react';
import TextInput from "../../entry/TextInput";
import {FormProvider, useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import * as yup from "yup";
import {FirebaseError} from "firebase/app";
import {toast} from "react-toastify";
import {ColorSelector} from "./ColorSelector";
import {CurrentColor} from "./CurrentColor";
import { useFirebase } from '../../../../firebase';

type FormData = {
    farm: string;
};

const validationSchema = yup.object().shape({
    farm: yup.string().min(2, 'Navnet må være minst 2 tegn langt').required('Navn er påkrevd'),
});

export const FarmRegistration: React.FC = () => {
    const firebase = useFirebase();

    const [color1, setColor1] = useState("#b32aa9");
    const [color2, setColor2] = useState("#b32aa9");
    const [twoColors, setTwoColors] = useState(false);

    const formMethods = useForm<FormData>({
        resolver: yupResolver(validationSchema),
        defaultValues: {
            farm: '',
        },
    });

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setTwoColors(event.target.checked);
    };

    const resetState = () => {
        setColor1("#b32aa9");
        setColor2("#b32aa9");
        setTwoColors(false);
        formMethods.reset();
    };

    const onSubmit = formMethods.handleSubmit(async (data) => {
        try {
            await firebase.doRegisterFarm(data.farm, color1, color2, twoColors);
            toast.success(`Gården ${formMethods.getValues().farm} ble lagt til!`);
            resetState();

        } catch(error) {
            let code;
            if (error instanceof FirebaseError) {
                code = error.code;
            } else if (typeof error === "string") {
                code = error;
            }

            if (code === 'permission-denied') {
                toast.error("En gård med det navnet finnes allerede.");
            } else {
                toast.error("Noe gikk galt. Prøv igjen senere eller kontakt en administrator.");
            }
        }
    });

    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            flex={1}
        >
            <Paper elevation={2} sx={{ width: 3/4, maxWidth: 600, padding: 5 }}>
                <Typography
                    variant="h4"
                    component="h2"
                    align="left"
                    sx={{
                        marginBottom: 1,
                    }}
                >
                    Gårdsregistrering
                </Typography>
                <Divider sx={{marginBottom: 3}} />
                <FormControlLabel control={<Switch checked={twoColors} onChange={handleChange} />} label="2 Farger" />
                <Grid
                    container
                    spacing={0}
                    direction="row"
                    alignItems='stretch'
                    flexWrap="nowrap"
                >
                    <Box>
                        <ColorSelector color={color1} setColor={setColor1} />
                        {twoColors && (
                        <ColorSelector color={color2} setColor={setColor2} />
                        )}
                    </Box>
                    <Box sx={{marginLeft: 5, flex: 1, display: 'flex', flexDirection: 'column', justifyContent: "space-between" }}>
                        <Box sx={{marginBottom: 1}}>
                            <CurrentColor color={color1} label="Valgt farge 1:" />
                            {twoColors && (
                                <CurrentColor color={color2} label="Valgt farge 2:" />
                            )}
                        </Box>
                        <FormProvider {...formMethods}>
                            <Box component="form" onSubmit={onSubmit}>
                                <TextInput name="farm" label="Gårdsnavn" />
                                <Button
                                    variant="contained"
                                    fullWidth
                                    color="success"
                                    size="large"
                                    type="submit"
                                >
                                    Registrer Gård
                                </Button>
                            </Box>
                        </FormProvider>
                    </Box>
                </Grid>
            </Paper>
        </Grid>
    );
};
