/*
 * @created 05/02/2022 - 16:54
 * @project web
 * @author andreasjj
 */
import styled from "styled-components";
import React from "react";
import {Box, Typography} from "@mui/material";

const SelectedColorBox = styled.div.attrs<{backgroundColor: string}>(props => ({
    style: {
        backgroundColor: props.backgroundColor,
    },
}))<{backgroundColor: string}>`
    width: 100%;
    height: 30px;
    marginBottom: 8px;
`;

export const CurrentColor: React.FC<{color: string, label: string}> = ({ color, label }) => (
    <Box sx={{ display: 'flex', flex: 1, marginBottom: 1 }}>
        <Typography variant="body1" component="span" sx={{ whiteSpace: 'nowrap', marginRight: 1 }}>
            {label}
        </Typography>
        <SelectedColorBox backgroundColor={color} />
    </Box>
);