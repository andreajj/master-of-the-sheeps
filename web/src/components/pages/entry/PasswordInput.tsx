import { Box, TextField, InputAdornment, IconButton } from '@mui/material';
import React, { useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { Visibility, VisibilityOff } from 'styled-icons/material';
import { ErrorMessage } from '../../ErrorMessage';

const PasswordInput: React.FC<{name: string, helperText: string, marginBottom?: boolean, fullWidth?: boolean, disabled?: boolean}> = ({ name, helperText, marginBottom = true, fullWidth = false, disabled = false }) => {
  const [showPassword, setShowPassword] = useState(false);

  const { control } = useFormContext();

  const handleClickShowPassword = () => {
    setShowPassword((_) => !_);
  };

  const handleMouseDownPassword = (event: React.SyntheticEvent) => {
    event.preventDefault();
  };

  return (
    <Box 
      sx={{
        marginBottom: marginBottom ? 2 : 0,
        display: "flex",
        flexDirection: "column",
        flex: fullWidth ? 1 : null,
      }}
    >
      <Controller
        render={({ field, formState }) => (
          <TextField
            label={helperText} 
            type={showPassword ? "text" : "password"}
            variant="filled"
            {...field}
            error={!!formState.errors[name]}
            disabled={disabled}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showPassword ? <VisibilityOff size="1em" /> : <Visibility size="1em" />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        )}
        name={name}
        control={control}
      />
      <ErrorMessage name={name} />
  </Box>
  );
};

export default PasswordInput;