import { Grid, Paper } from '@mui/material';
import React from 'react';

import Image from '../../../assets/background.png';

export const Container: React.FC = ({ children }) => {

    return (
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: '100vh' }}
        sx={{
          background: `url(${Image}) no-repeat center center fixed`,
          backgroundSize: 'cover',
        }}
      >
        <Paper elevation={2} sx={{ width: 3/4, maxWidth: 600 }}>
          {children}
        </Paper>
      </Grid>
    );
};
