import React from 'react';
import { RegisterForm } from './RegisterForm';
import { Container } from '../Container';

export const Register: React.FC = () => {

    return (
      <Container>
        <RegisterForm />
      </Container>
    );
};
