import { Box, Button, Typography } from '@mui/material';
import React from 'react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, FormProvider } from 'react-hook-form';
import PasswordInput from '../PasswordInput';
import TextInput from '../TextInput';
import { Link } from 'react-router-dom';
import { useFirebase } from '../../../../firebase';
import { toast } from 'react-toastify';
import { FirebaseError } from 'firebase/app';

type FormData = {
    email: string;
    password: string;
    passwordConfirmation: string;
};

const validationSchema = yup.object().shape({
    email: yup.string().email().required('Epost er påkrevd'),
    password: yup
        .string()
        .min(8, 'Passordet må være minst 8 tegn langt')
        .matches(/^(?=.*[a-å])(?=.*[A-Å])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/, 'Passordet kan bare inneholde bokstaver fra a-å og må også inneholde en liten og stor bokstav, tall og et av tegnene @$!%*?&')
        .required('Passord er påkrevd'),
    passwordConfirmation: yup.string()
        .oneOf([yup.ref('password'), null], 'Passordene må matche')
});

export const RegisterForm: React.FC = () => {
    const firebase = useFirebase();

    const formMethods = useForm<FormData>({
        resolver: yupResolver(validationSchema),
        defaultValues: {
            email: '',
            password: '',
            passwordConfirmation: '',
        },
    });

    const onSubmit = formMethods.handleSubmit(async (data) => {
        try {
          await firebase.doCreateUserWithEmailAndPassword(data.email, data.password);
        } catch(error) {
          let code;
          if (error instanceof FirebaseError) {
            code = error.code;
          } else if (typeof error === "string") {
            code = error;
          }

          if (code === 'auth/email-already-in-use') {
            toast.error("Epost adressen du brukte er allerede i bruk");
          } else if (code === 'auth/invalid-email') {
            toast.error("Epost adressen du oppga er ikke gyldig");
          } else if (code === 'auth/weak-password') {
            toast.error("Passordet du oppga er for svakt. Minimum antall bokstaver og tegn er 6.");
          } else {
            toast.error("Noe gikk galt. Vennligst prøv igjen senere");
          }
        }
    });

    return (
        <FormProvider {...formMethods}>
            <Box 
              component="form" 
              onSubmit={onSubmit}
              sx={{
                display: 'flex',
                flexDirection: 'column',
                paddingTop: 15,
                paddingBottom: 15,
                paddingRight: 8,
                paddingLeft: 8,
              }}
            >
                <Typography 
                  variant="h3" 
                  component="h2" 
                  align="center"
                  sx={{
                    marginBottom: 4,
                  }}
                >
                    Registrer ny bruker
                </Typography>
                <TextInput name="email" label="Epost" />
                <PasswordInput name="password" helperText="Passord" />
                <PasswordInput name="passwordConfirmation" helperText="Bekreft Passord" />
                <Button 
                  variant="contained" 
                  color="success"
                  size="large"
                  type="submit"
                >
                  Registrer
                </Button>
                <Typography 
                  variant="caption" 
                  component="span"
                  align="left"
                >
                  <Link to="/login">Har du bruker? Klikk her for å logge inn</Link>
                </Typography>
            </Box>
        </FormProvider>
    );
};

