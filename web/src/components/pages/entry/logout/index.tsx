import { Typography, Grid } from '@mui/material';
import React, { useEffect } from 'react';
import { useFirebase } from '../../../../firebase';

export const Logout: React.FC = () => {
    const firebase = useFirebase();

    useEffect(() => {
      const logoutTimer = setTimeout(() => {
        firebase.doSignOut();
      }, 2000);

      return () => clearTimeout(logoutTimer);
    });

    return (
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: '100vh' }}
      >
        <Typography 
          variant="h3" 
          component="h2" 
          align="center"
          sx={{
            margin: 4,
          }}
        >
          Logger deg ut...
        </Typography>
      </Grid>
    );
};
