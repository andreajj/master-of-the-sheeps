import React, { useEffect } from 'react';
import { Container } from '../Container';
import { Box, Button, Typography } from '@mui/material';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, FormProvider } from 'react-hook-form';
import TextInput from '../TextInput';
import { Link, useNavigate, useParams, useSearchParams } from 'react-router-dom';
import { useFirebase } from '../../../../firebase';
import { FirebaseError } from 'firebase/app';
import { toast } from 'react-toastify';
import PasswordInput from '../PasswordInput';

type FormData = {
  password: string;
  passwordConfirmation: string;
};

const validationSchema = yup.object().shape({
  password: yup
        .string()
        .min(8, 'Passordet må være minst 8 tegn langt')
        .matches(/^(?=.*[a-å])(?=.*[A-Å])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/, 'Passordet kan bare inneholde bokstaver fra a-å og må også inneholde en liten og stor bokstav, tall og et av tegnene @$!%*?&')
        .required('Passord er påkrevd'),
  passwordConfirmation: yup.string()
      .oneOf([yup.ref('password'), null], 'Passordene må matche')
});

export const ResetPassword: React.FC = () => {
  const [searchParams] = useSearchParams();
  const passwordResetCode = searchParams.get("oobCode");
  const navigate = useNavigate();
  const firebase = useFirebase();

  useEffect(() => {
    const init = async () => {
      try {
        if (!passwordResetCode) {
          throw new FirebaseError("auth/invalid-action-code", "Code undefined");
        }
        await firebase.doVerifyPasswordResetCode(passwordResetCode);
      } catch (error) {
        let code;
        if (error instanceof FirebaseError) {
          code = error.code;
        } else if (typeof error === "string") {
          code = error;
        }
  
        if (code === 'auth/expired-action-code') {
          toast.error("Tilbakestillingskoden har utløpt");
        } else if (code === 'auth/invalid-action-code') {
          toast.error("Tilbakestillingskoden er ugyldig");
        } else if (code === 'auth/user-disabled') {
          toast.error("Brukeren er deaktivert");
        } else if (code === 'auth/user-not-found') {
          toast.error("Brukeren finnes ikke");
        } else {
          toast.error("Noe gikk galt. Prøv igjen senere.");
        }
        navigate(`/forgot`);
      }
    };
    init();
  }, [passwordResetCode]);

  const formMethods = useForm<FormData>({
      resolver: yupResolver(validationSchema),
      defaultValues: {
        password: '',
        passwordConfirmation: '',
      },
  });

  const onSubmit = formMethods.handleSubmit(async (data) => {
      try {
        if (!passwordResetCode) {
          throw new FirebaseError("auth/invalid-action-code", "Code undefined");
        }

        await firebase.doConfirmPasswordReset(passwordResetCode, data.password);
        formMethods.reset();
        toast.success("Tilbakestilling av passord var vellykket");
        navigate(`/login`);
      } catch(error) {
        let code;
        if (error instanceof FirebaseError) {
          code = error.code;
        } else if (typeof error === "string") {
          code = error;
        }

        if (code === 'auth/expired-action-code') {
          toast.error("Tilbakestillingskoden har utløpt");
        } else if (code === 'auth/invalid-action-code') {
          toast.error("Tilbakestillingskoden er ugyldig");
        } else if (code === 'auth/user-disabled') {
          toast.error("Brukeren er deaktivert");
        } else if (code === 'auth/user-not-found') {
          toast.error("Brukeren finnes ikke");
        } else if (code === 'auth/weak-password') {
          toast.error("Passordet ditt er for svakt");
        } else {
          toast.error("Noe gikk galt. Prøv igjen senere.");
        }
      }
  });

    return (
      <Container>
        <FormProvider {...formMethods}>
            <Box 
              component="form" 
              onSubmit={onSubmit}
              sx={{
                display: 'flex',
                flexDirection: 'column',
                paddingTop: 15,
                paddingBottom: 15,
                paddingRight: 8,
                paddingLeft: 8,
              }}
            >
                <Typography 
                  variant="h3" 
                  component="h2" 
                  align="center"
                  sx={{
                    marginBottom: 4,
                  }}
                >
                    Tilbakestill passordet ditt
                </Typography>
                <PasswordInput name="password" helperText="Passord" />
                <PasswordInput name="passwordConfirmation" helperText="Bekreft Passord" />
                <Button 
                  variant="contained" 
                  color="success"
                  size="large"
                  type="submit"
                >
                  Tilbakestill passord
                </Button>
                <Typography 
                  variant="caption" 
                  component="span"
                  align="left"
                >
                  <Link to="/login">Tilbake til login</Link>
                </Typography>
            </Box>
        </FormProvider>
      </Container>
    );
};