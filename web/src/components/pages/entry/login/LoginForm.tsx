import { Box, Button, Typography } from '@mui/material';
import React from 'react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, FormProvider } from 'react-hook-form';
import PasswordInput from '../PasswordInput';
import TextInput from '../TextInput';
import { Link } from 'react-router-dom';
import { useFirebase } from '../../../../firebase';
import { FirebaseError } from 'firebase/app';
import { toast } from 'react-toastify';

type FormData = {
    email: string;
    password: string;
};

const validationSchema = yup.object().shape({
    email: yup.string().email().required('Epost er påkrevd'),
    password: yup
        .string()
        .required('Passord er påkrevd'),
});

export const LoginForm: React.FC = () => {
    const firebase = useFirebase();

    const formMethods = useForm<FormData>({
        resolver: yupResolver(validationSchema),
        defaultValues: {
            email: '',
            password: '',
        },
    });

    const onSubmit = formMethods.handleSubmit(async (data) => {
        try {
          await firebase.doSignInWithEmailAndPassword(data.email, data.password);
        } catch(error) {
          let code;
          if (error instanceof FirebaseError) {
            code = error.code;
          } else if (typeof error === "string") {
            code = error;
          }

          if (code === 'auth/user-disabled') {
            toast.error("Brukeren er deaktivert");
          } else if (code === 'auth/invalid-email') {
            toast.error("Epost adressen du oppga er ikke gyldig");
          } else if (code === 'auth/user-not-found') {
            toast.error("Brukeren ble ikke funnet");
          } else if (code === 'auth/wrong-password') {
            toast.error("Feil passord");
          } else {
            toast.error("Noe gikk galt. Vennligst prøv igjen senere");
          }
        }
    });

    return (
        <FormProvider {...formMethods}>
            <Box 
              component="form" 
              onSubmit={onSubmit}
              sx={{
                display: 'flex',
                flexDirection: 'column',
                paddingTop: 15,
                paddingBottom: 15,
                paddingRight: 8,
                paddingLeft: 8,
              }}
            >
                <Typography 
                  variant="h3" 
                  component="h2" 
                  align="center"
                  sx={{
                    marginBottom: 4,
                  }}
                >
                    Logg inn
                </Typography>
                <TextInput name="email" label="Epost" />
                <PasswordInput name="password" helperText="Passord" />
                <Button 
                  variant="contained" 
                  color="success"
                  size="large"
                  type="submit"
                >
                  Logg inn
                </Button>
                <Typography 
                  variant="caption" 
                  component="span"
                  align="left"
                >
                  <Link to="/register">Ingen bruker? Klikk her for å registrere ny bruker</Link>
                </Typography>
                <Typography 
                  variant="caption" 
                  component="span"
                  align="left"
                >
                  <Link to="/forgot">Glemt passordet ditt? Klikk her for å tilbakestille passordet</Link>
                </Typography>
            </Box>
        </FormProvider>
    );
};

