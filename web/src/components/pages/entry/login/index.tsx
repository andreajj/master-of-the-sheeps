import React from 'react';
import { LoginForm } from './LoginForm';
import { Container } from '../Container';

export const Login: React.FC = () => {

    return (
      <Container>
        <LoginForm />
      </Container>
    );
};
