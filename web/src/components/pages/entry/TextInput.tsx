import { Box, TextField } from '@mui/material';
import React from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { ErrorMessage } from '../../ErrorMessage';

const TextInput: React.FC<{name: string, label: string, marginBottom?: boolean, fullWidth?: boolean, disabled?: boolean}> = ({ name, label, marginBottom = true, fullWidth = false, disabled = false }) => {
  const { control } = useFormContext();

  return (
    <Box 
      sx={{
        marginBottom: marginBottom ? 2 : 0,
        display: "flex",
        flexDirection: "column",
        flex: fullWidth ? 1 : null,
      }}
    >
      <Controller
      render={({ field, formState }) => (
        <TextField
          fullWidth={fullWidth}
          label={label}
          variant="filled"
          {...field}
          error={!!formState.errors[name]}
          disabled={disabled}
        />
      )}
      name={name}
      control={control}
    />
    <ErrorMessage name={name} />
  </Box>
  );
};

export default TextInput;