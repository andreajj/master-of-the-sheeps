import React from 'react';
import { Container } from '../Container';
import { Box, Button, Typography } from '@mui/material';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, FormProvider } from 'react-hook-form';
import TextInput from '../TextInput';
import { Link, useNavigate } from 'react-router-dom';
import { useFirebase } from '../../../../firebase';
import { FirebaseError } from 'firebase/app';
import { toast } from 'react-toastify';

type FormData = {
  email: string;
};

const validationSchema = yup.object().shape({
  email: yup.string().email().required('Epost er påkrevd'),
});

export const ForgotPassword: React.FC = () => {
  const navigate = useNavigate();
  const firebase = useFirebase();

  const formMethods = useForm<FormData>({
      resolver: yupResolver(validationSchema),
      defaultValues: {
          email: '',
      },
  });

  const onSubmit = formMethods.handleSubmit(async (data) => {
      try {
        await firebase.doPasswordReset(data.email);
        formMethods.reset();
        toast.success("Hvis eposten er assosiert med en konto vil du mota en epost med videre instruksjoner.");
        navigate(`/login`);
      } catch(error) {
        let code;
        if (error instanceof FirebaseError) {
          code = error.code;
        } else if (typeof error === "string") {
          code = error;
        }

        if (code === 'auth/invalid-email') {
          toast.error("Eposten du oppga er ugyldig formatert.");
        } else if (code === 'auth/too-many-requests') {
          toast.error("Vi har blokkert alle forespørsler fra denne enheten på grunn av uvanlig aktivitet. Prøv igjen senere.");
        } else {
          toast.error("Noe gikk galt. Prøv igjen senere.");
        }
      }
  });

    return (
      <Container>
        <FormProvider {...formMethods}>
            <Box 
              component="form" 
              onSubmit={onSubmit}
              sx={{
                display: 'flex',
                flexDirection: 'column',
                paddingTop: 15,
                paddingBottom: 15,
                paddingRight: 8,
                paddingLeft: 8,
              }}
            >
                <Typography 
                  variant="h3" 
                  component="h2" 
                  align="center"
                  sx={{
                    marginBottom: 4,
                  }}
                >
                    Glemt passordet ditt?
                </Typography>
                <TextInput name="email" label="Epost" />
                <Button 
                  variant="contained" 
                  color="success"
                  size="large"
                  type="submit"
                >
                  Tilbakestill passord
                </Button>
                <Typography 
                  variant="caption" 
                  component="span"
                  align="left"
                >
                  <Link to="/login">Tilbake til login</Link>
                </Typography>
            </Box>
        </FormProvider>
      </Container>
    );
};