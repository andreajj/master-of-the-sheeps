import React from 'react';
import { Link, AppBar, Box, Toolbar, IconButton, Typography, Menu, Container, Avatar, Tooltip, MenuItem, styled} from '@mui/material';
import { Menu as MenuIcon } from '@styled-icons/material/Menu';
import { Link as RouterLink } from 'react-router-dom';

const pages = [
  {name: 'Sporinger', link: '/trackings'},
  {name: 'Rapport', link: '/reports'},
  {name: 'Gårdsregistrering', link: '/farm/registration'},
  {name: 'Gårder', link: '/farm/list'},
];
const settings = [{name: 'Profil', link: '/profile'}, {name: 'Logg ut', link: '/logout'}];

const StyledMenuItem = styled(MenuItem)({
  position: "relative",
  padding: 0,
});

const StyledLink = styled(RouterLink)({
  width: "100%",
  height: "100%",
  top: 0,
  left: 0,
  display: "flex",
  alignItems: "center",
  justifyContent: "normal",
  color: 'black',
  padding: "6px 16px",
});

const Header: React.FC = () => {
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Link 
            to="/" 
            component={RouterLink} 
            variant="h6" 
            noWrap 
            sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
            underline="none"
            color="white"
          >
            LOGO
          </Link>

          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon size="1em" />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              {pages.map((page) => (
                <StyledMenuItem key={page.name} onClick={handleCloseNavMenu}>
                  <Link component={StyledLink} to={page.link} sx={{ color: 'black', display: 'block' }}>
                    <Typography textAlign="center">{page.name}</Typography>
                  </Link>
                </StyledMenuItem>
              ))}
            </Menu>
          </Box>
          <Link 
            to="/" 
            component={RouterLink} 
            variant="h6" 
            noWrap 
            sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}
            underline="none"
            color="white"
          >
            LOGO
          </Link>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            {pages.map((page) => (
              <Link
                key={page.name}
                component={RouterLink}
                to={page.link}
                onClick={handleCloseNavMenu}
                sx={{ color: 'white', display: 'block', margin: 2 }}
              >
                {page.name}
              </Link>
            ))}
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar alt="profile picture" />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: '45px' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              {settings.map((setting) => (
                <StyledMenuItem key={setting.name} onClick={handleCloseUserMenu}>
                  <Link component={StyledLink} to={setting.link}>
                    <Typography textAlign="center">{setting.name}</Typography>
                  </Link>
                </StyledMenuItem>
              ))}
            </Menu>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default Header;