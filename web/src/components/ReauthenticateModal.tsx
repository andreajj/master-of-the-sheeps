import * as React from 'react';
import {Dialog, DialogActions, DialogContent, DialogTitle} from "@mui/material";
import {TransitionProps} from "@mui/material/transitions";
import { useFirebase } from '../firebase';
import { Box, Slide, Button } from '@mui/material';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, FormProvider } from 'react-hook-form';
import TextInput from './pages/entry/TextInput';
import PasswordInput from './pages/entry/PasswordInput';
import { FirebaseError } from 'firebase/app';
import { toast } from 'react-toastify';

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

type FormData = {
  email: string;
  password: string;
};

const validationSchema = yup.object().shape({
  email: yup.string().email().required('Epost er påkrevd'),
  password: yup
      .string()
      .min(8, 'Passordet må være minst 8 tegn langt')
      .matches(/^(?=.*[a-å])(?=.*[A-Å])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/, 'Passordet kan bare inneholde bokstaver fra a-å og må også inneholde en liten og stor bokstav, tall og et av tegnene "@$!%*?&"')
      .required('Passord er påkrevd'),
});

interface Props {
    open: boolean,
    setOpen: React.Dispatch<React.SetStateAction<boolean>>,
}

export const ReauthenticateModal: React.FC<Props> = ({ open, setOpen }) => {
    const firebase = useFirebase();

    const formMethods = useForm<FormData>({
      resolver: yupResolver(validationSchema),
      defaultValues: {
          email: '',
          password: '',
      },
  });

    const handleClose = () => setOpen(false);

    const onSubmit = formMethods.handleSubmit(async (data) => {
      try {
        const credentials = firebase.doCreateEmailAuthCredentials(data.email, data.password);
        await firebase.doReauthenticateWithCredential(credentials);
        formMethods.reset();
        toast.info("Verifiseringen var vellykket");
        handleClose();
      } catch(error) {
        let code;
        if (error instanceof FirebaseError) {
          code = error.code;
        } else if (typeof error === "string") {
          code = error;
        }

        if (code === 'auth/user-disabled') {
          toast.error("Brukeren er deaktivert");
        } else if (code === 'auth/invalid-email') {
          toast.error("Epost adressen du oppga er ikke gyldig");
        } else if (code === 'auth/user-not-found') {
          toast.error("Brukeren ble ikke funnet");
        } else if (code === 'auth/wrong-password') {
          toast.error("Feil passord");
        } else {
          toast.error("Noe gikk galt. Vennligst prøv igjen senere");
        }
      }
    });

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle>{`Logg inn på nytt for å verifisere deg`}</DialogTitle>
            <DialogContent>
              <FormProvider {...formMethods}>
                <Box 
                  component="form" 
                  onSubmit={onSubmit}
                  sx={{
                    display: 'flex',
                    flexDirection: 'column',
                  }}
                >
                    <TextInput name="email" label="Nåværende epost" />
                    <PasswordInput name="password" helperText="Nåværende passord" marginBottom={false} />
                </Box>
              </FormProvider>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Avbryt</Button>
                <Button onClick={onSubmit}>Verifiser</Button>
            </DialogActions>
        </Dialog>
    );
};