import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render, RenderOptions, RenderResult } from '@testing-library/react';
import { StyledEngineProvider } from '@mui/material/styles';

const GlobalProviders: React.FC = ({ children }) => {
    return (
        <StyledEngineProvider injectFirst>
            {children}
        </StyledEngineProvider>
    );
};

const customRender = (ui: React.ReactElement, options?: Omit<RenderOptions, 'queries'>): RenderResult =>
    render(ui, { wrapper: GlobalProviders, ...options });

export * from '@testing-library/react';
export { customRender as render };
