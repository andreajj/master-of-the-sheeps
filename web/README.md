# Master - Sheep Tracking

## Getting it up

### Local Development

Local development requires [Node.js](https://nodejs.org/).

_**notice**: copy the file `.env-sample` and rename it to `.env`. It provides the
environment variables required, such as the base url for API-calls. See ["Building and environment variables"](#building-and-environment-variables) below._

* To start the development server simply execute `yarn start`.

### Build and Deployment

#### Producation building and deployment

Build and deployment is done automatically through the GitLab CI/CD pipeline. However, to manually build and deploy follow these steps:
1. cd web
2. npm install
3. npm run build:production
4. cp -R public/* ../firebase/public
5. cd ../firebase
6. firebase deploy --only hosting --token FIREBASE_TOKEN

#### Building and Environment Variables

The building process uses a set of environment variables (f.ex. the API base-url).
Webpack uses the [`dotenv-webpack`](https://www.npmjs.com/package/dotenv-webpack) plugin, and the process works as follows:

1. Variables defined in an optional `.env` file in root is loaded into the global `process.env` object.
   _`.env` files are intended for running and building locally._
2. Environment variables from the executing system/CLI session is loaded into the same global object. 
   _Names from the executing system/CLI session are prioritzed over the `.env` file._
3. References to `process.env.{NAME}` in the source code is substituted with the environment variable values at build time.