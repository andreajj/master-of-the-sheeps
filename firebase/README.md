# Firebase configuration

## Firestore, Hosting, Storage

Prerequisites:
1. Installed Firebase CLI

To read how to use and deploy please refer to the Firebase CLI documentation at https://firebase.google.com/docs/cli

### Storage Cors
https://firebase.google.com/docs/storage/web/download-files#cors_configuration

To download data directly in the browser, you must configure your Cloud Storage bucket for cross-origin access (CORS). This can be done with the gsutil command line tool.

If you don't want any domain-based restrictions (the most common scenario), copy this JSON to a file named cors.json:
```json
[
  {
    "origin": [
      "https://sheep-tracker-master.web.app/", 
      "https://sheep-tracker-master.firebaseapp.com/", 
      "http://0.0.0.0:3000", 
      "http://localhost:3000"
    ],
    "method": ["GET"],
    "maxAgeSeconds": 3600
  }
]
```

Our gs link: gs://sheep-tracker-master.appspot.com
our cors file: cors.json
command: gsutil cors set cors.json gs://sheep-tracker-master.appspot.com

more info at https://stackoverflow.com/questions/37760695/firebase-storage-and-access-control-allow-origin