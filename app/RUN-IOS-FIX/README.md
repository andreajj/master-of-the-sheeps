# Fix react native <0.64 cli building on ios

There's two files in this directory named as the filepath where they should be placed inside the node_modules folder. copy them and rename them:

`node_modules/@react-native-community/cli-platform-ios/build/commands/runIOS/index.js`

`node_modules/@react-native-community/cli-platform-ios/build/commands/runIOS/parseXtraceIOSDevicesList.js`

Then run command:
`yarn react-native run-ios --configuration Release --device "[device name]"`

Possibly this one too:
`pod install --repo-update`