import React from 'react';
import { View, Text } from 'react-native';
import { DrawerContentScrollView, DrawerItem, DrawerContentComponentProps } from '@react-navigation/drawer';
import { useTheme } from 'react-native-paper';
import { Colors } from 'react-native-paper';

const Sidebar = (props: DrawerContentComponentProps) => {
  const { state, descriptors, navigation } = props;
  const theme = useTheme();

    return (
        <DrawerContentScrollView {...props} style={{backgroundColor: theme.colors.primary700}}>
            {state.routes.map((route) => {
              const {
                drawerLabel,
              } = descriptors[route.key].options;

              return (
                <DrawerItem
                    key={route.key}
                    label={
                      ({ color }) =>
                        <Text style={{ color: Colors.white }}>
                          {drawerLabel}
                        </Text>
                    }
                    focused={
                      state.routes.findIndex(
                        (e) => e.name === route.name
                      ) === state.index
                    }
                    /*activeTintColor={activeTintColor}*/
                    onPress={() => navigation.navigate(route.name)}
                  />
              );
            })}
        </DrawerContentScrollView>
    );
};

export default Sidebar;