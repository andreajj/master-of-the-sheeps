import { Tie } from "../pages/Tracking/types"

const getLambFromTie = (tie: Tie) => {
  switch (tie) {
    case Tie.None: {
      return 0
    }
    case Tie.Blue: {
      return 1
    }
    case Tie.Green: {
      return 3
    }
    case Tie.Red: {
      return 0
    }
    case Tie.Yellow: {
      return 2
    }
  }
}

export default getLambFromTie;