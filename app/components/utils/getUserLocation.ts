import {
  getCurrentPositionAsync,
  LocationAccuracy,
} from "expo-location";
import { Location } from '../pages/Tracking/types';

const getUserLocation = async () => {
  const { coords } = await getCurrentPositionAsync({
    accuracy: LocationAccuracy.Balanced,
  });

  const userLocation: Location = [coords.longitude, coords.latitude];
  return userLocation;
}

export default getUserLocation;