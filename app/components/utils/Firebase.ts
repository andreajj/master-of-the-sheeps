
import firestore, { FirebaseFirestoreTypes } from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { DeadSheepType, FirebasOfflineMap, Image, OfflineMap, OtherType, PredatorType, SheepHerdType, TrackingState } from '../atoms';
import { Location, Predator } from '../pages/Tracking/types';

const getTracking = async (trackingUuid: string) => {
    const trackingRef = firestore().collection('trackings').doc(trackingUuid);

    const tracking = await trackingRef.get();
    const trackingData = tracking.data();
    if (!trackingData) {
      throw new Error("failed to get tracking data")
    }

    const sheepHerdsRef = trackingRef.collection('sheepHerds');
    const sheepHerdDocuments = await sheepHerdsRef.get();
    const predatorDocuments = await trackingRef.collection('predators').get();
    const otherDocuments = await trackingRef.collection('others').get();
    const deadSheepsRef = trackingRef.collection('deadSheep')
    const deadSheepDocuments = await deadSheepsRef.get();

    const sheepHerds: Array<SheepHerdType> = []
    
    const _ = sheepHerdDocuments.docs.map(async (sheepHerdDocument) => {
      const data = sheepHerdDocument.data();
      const sheepHerdRef = sheepHerdsRef.doc(sheepHerdDocument.id);
      const sheepHerdimagesRef = sheepHerdRef.collection('images');
      const sheepHerdImagesDocuments = await sheepHerdimagesRef.get();

      const images: Array<Image> = [];
      for(const imagesDocument of sheepHerdImagesDocuments.docs) {
        const data = imagesDocument.data();

        const partsDocuments = await sheepHerdimagesRef.doc(imagesDocument.id).collection('parts').get();

        let base64 = "";

        // The documents are by default ordered by the document ids
        // meaning the parts should be in the correct order of 0-x
        for(const partsDocument of partsDocuments.docs) {
          const data = partsDocument.data()
          base64 += data.data;
        }

        if (base64 !== "") {
          images.push({
            base64,
            firebaseUUID: imagesDocument.id,
            numberOfParts: data.numberOfParts,
          })
        }
      }

      // GeoPoint(lat, long), location(long, lat)
      const upsertLocations: Array<Location> = data.upsertLocations.map((geoPoint: FirebaseFirestoreTypes.GeoPoint) => [geoPoint.longitude, geoPoint.latitude])

      const newSheepHerd: SheepHerdType = {
        sheeps: data.sheeps,
        lambs: data.lambs,
        location: [data.location.longitude, data.location.latitude],
        upsertLocations: upsertLocations,
        farms: data.farms,
        uuid: sheepHerdDocument.id,
        firebaseUUID: sheepHerdDocument.id,
        woundedAnimals: data.woundedAnimals,
        images: images,
      }
      return newSheepHerd;
    })

    for (const sheepdHerd of _ ) {
      sheepHerds.push(await sheepdHerd)
    }
    
    const predators: Array<PredatorType> = predatorDocuments.docs.map((predatorDocument) => {
      const data = predatorDocument.data()

      const locations: Array<Location> = data.upsertLocations.map((geoPoint: FirebaseFirestoreTypes.GeoPoint) => [geoPoint.longitude, geoPoint.latitude])

      const newPredator: PredatorType = {
        type: data.type,
        description: data.description,
        location: [data.location.longitude, data.location.latitude],
        upsertLocations: locations,
        uuid: predatorDocument.id,
        firebaseUUID: predatorDocument.id,
      }
      return newPredator
    })

    const others: Array<OtherType> = otherDocuments.docs.map((otherDocument) => {
      const data = otherDocument.data()

      const locations: Array<Location> = data.upsertLocations.map((geoPoint: FirebaseFirestoreTypes.GeoPoint) => [geoPoint.longitude, geoPoint.latitude])

      const newOther: OtherType = {
        type: data.type,
        description: data.description,
        location: [data.location.longitude, data.location.latitude],
        upsertLocations: locations,
        uuid: otherDocument.id,
        firebaseUUID: otherDocument.id,
      }
      return newOther
    })

    const deadSheep: Array<DeadSheepType> = [];
    
    for(const deadSheepDocument of deadSheepDocuments.docs) {
      const deadSheepRef = deadSheepsRef.doc(deadSheepDocument.id);
      const deadSheepImagesRef = deadSheepRef.collection('images');
      const deadSheepImagesDocuments = await deadSheepImagesRef.get();

      const data = deadSheepDocument.data()

      const locations: Array<Location> = data.upsertLocations.map((geoPoint: FirebaseFirestoreTypes.GeoPoint) => [geoPoint.longitude, geoPoint.latitude])

      const images: Array<Image> = [];
      for(const deadSheepImagesDocument of deadSheepImagesDocuments.docs) {
        const data = deadSheepImagesDocument.data();

        const deadSheepImageRef = deadSheepImagesRef.doc(deadSheepImagesDocument.id);
        const partsRef = deadSheepImageRef.collection('parts');
        const partsDocuments = await partsRef.get();

        let base64 = "";

        // The documents are by default ordered by the document ids
        // meaning the parts should be in the correct order of 0-x
        for(const partsDocument of partsDocuments.docs) {
          const data = partsDocument.data()
          base64 += data.data;
        }
        
        if (base64 !== "") {
          images.push({
            base64,
            firebaseUUID: deadSheepImagesDocument.id,
            numberOfParts: data.numberOfParts,
          })
        }
      }

      const newDeadSheep: DeadSheepType = {
        description: data.description,
        location: [data.location.longitude, data.location.latitude],
        upsertLocations: locations,
        uuid: deadSheepDocument.id,
        firebaseUUID: deadSheepDocument.id,
        images: images,
      }
      
      deadSheep.push(newDeadSheep);
    }

    const offlineMap: OfflineMap = {
      uuid: trackingData.offlineMap.uuid,
      name: trackingData.offlineMap.name,
      styleURL: trackingData.offlineMap.styleURL,
      bounds: [[trackingData.offlineMap.bounds.neLng, trackingData.offlineMap.bounds.neLat], [trackingData.offlineMap.bounds.swLng, trackingData.offlineMap.bounds.swLat]],
      minZoom: trackingData.offlineMap.minZoom,
      maxZoom: trackingData.offlineMap.maxZoom
    }

    const newState = {
      firebaseUUID: tracking.id,
      isTracking: true,
      offlineMap: offlineMap,
      startTimestamp: trackingData.startTimestamp.toDate() as Date,
      endTimestamp: trackingData.endTimestamp.toDate() as Date,
      locations: trackingData.locations.map((geoPoint: FirebaseFirestoreTypes.GeoPoint) => [geoPoint.longitude, geoPoint.latitude]) as Array<[number, number]>,
      sheepHerds: sheepHerds,
      predators: predators,
      others: others,
      deadSheep: deadSheep,
      routeDescription: trackingData.routeDescription,
      farms: trackingData.farms,
      owner: trackingData.owner,
    }

    return newState
}

const getFarms = async () => {
  const trackingRef = firestore().collection('farms');
  const tracking = await trackingRef.get();
  const trackingData = tracking.docs;

  return trackingData;
}

const batchWrite = async (state: TrackingState) => {
  const trackingsRef = firestore().collection('trackings');
  const newTrackingRef = trackingsRef.doc();
  const batch = firestore().batch();

  // Create the new tracking document
  const locations: Array<FirebaseFirestoreTypes.GeoPoint> = []
  for (const location of state.locations) {
    locations.push(new firestore.GeoPoint(location[1], location[0]))
  }

  const offlineMap: FirebasOfflineMap = {
    uuid: state.offlineMap?.uuid,
    name: state.offlineMap?.name,
    styleURL: state.offlineMap?.styleURL,
    bounds: {
      neLng: state.offlineMap?.bounds[0][0],
      neLat: state.offlineMap?.bounds[0][1],
      swLng: state.offlineMap?.bounds[1][0],
      swLat: state.offlineMap?.bounds[1][1],
    }
  }

  if (state.offlineMap?.minZoom) {
    offlineMap.minZoom = state.offlineMap?.minZoom
  }
  if (state.offlineMap?.maxZoom) {
    offlineMap.maxZoom = state.offlineMap?.maxZoom
  }

  const farms: Array<string> = [];

  for(const herd of state.sheepHerds) {
    const farmNames = herd.farms.map((farm) => farm.name);
    farms.push(...farmNames)
  }

  batch.set(newTrackingRef, {
    locations: locations,
    startTimestamp: state.startTimestamp,
    endTimestamp: new Date(),
    offlineMap: offlineMap,
    routeDescription: state.routeDescription,
    farms: [...new Set(farms)],
    owner: auth().currentUser?.uid,
  })

  // Add all the sheep herds to the tracking documents sub-collection
  for(const herd of state.sheepHerds) {
    const newTrackingSheepHerdsRef  = newTrackingRef.collection('sheepHerds').doc()
    // GeoPoint(lat, long), location(long, lat)
    const upsertLocations = herd.upsertLocations.map((location) => new firestore.GeoPoint(location[1], location[0]))
    batch.set(newTrackingSheepHerdsRef, {
      location: new firestore.GeoPoint(herd.location[1], herd.location[0]),
      upsertLocations: upsertLocations,
      farms: herd.farms,
      woundedAnimals: herd.woundedAnimals,
      sheeps: herd.sheeps,
      lambs: herd.lambs,
    })
    // Add all the images to the images documents sub-collection
    for (const image of herd.images) {
      // firebase document size limit = 1,048,576 bytes = 1 MiB
      // firebasse field value limit = 1 MiB - 89 bytes (1,048,487 bytes)
      const size =  1048487;
      const base64 = image.base64;
      const blob = new Blob([base64]);
      const blobSize = blob.size;
      let numberOfParts = Math.ceil(blobSize / size);
      const parts: Array<string> = new Array(numberOfParts);
      // we can assume the parts will be of uniform size as base64 encoding
      // only uses 1 byte unicode characters
      const stringLength = Math.ceil(base64.length / numberOfParts);
      for (let i = 0, acc = 0; i < numberOfParts; ++i, acc += stringLength) {
        parts[i] = base64.substring(acc, acc + stringLength);
      }
      
      // Add a document for the image
      const newTrackingImagesRef  = newTrackingSheepHerdsRef.collection('images').doc();
      batch.set(newTrackingImagesRef, {
        size: blobSize,
        numberOfParts: numberOfParts,
      })

      // add documents for all the base64 parts of the image
      for(let i = 0; i < parts.length; i++) {
        const part = parts[i];
        const newTrackingImagePartRef  = newTrackingImagesRef.collection('parts').doc(i.toString());
        batch.set(newTrackingImagePartRef, {
          data: part
        })
      }
    }
  }

  // Add all the predators to the tracking documents sub-collection
  for(const predator of state.predators) {
    // GeoPoint(lat, long), location(long, lat)
    const upsertLocations = predator.upsertLocations.map((location) => new firestore.GeoPoint(location[1], location[0]))
    const newPredatorRef = newTrackingRef.collection('predators').doc()
    const value: {
      type: Predator,
      location: FirebaseFirestoreTypes.GeoPoint,
      upsertLocations: FirebaseFirestoreTypes.GeoPoint[],
      description?: string
    } = {
      type: predator.type,
      location: new firestore.GeoPoint(predator.location[1], predator.location[0]),
      upsertLocations: upsertLocations,
    }
    if (predator.description) {
      value.description = predator.description
    }
    batch.set(newPredatorRef, value)
  }

  // Add all the others to the tracking documents sub-collection
  for(const other of state.others) {
    // GeoPoint(lat, long), location(long, lat)
    const upsertLocations = other.upsertLocations.map((location) => new firestore.GeoPoint(location[1], location[0]))
    const newOtherRef = newTrackingRef.collection('others').doc()
    batch.set(newOtherRef, {
      type: other.type,
      description: other.description,
      location: new firestore.GeoPoint(other.location[1], other.location[0]),
      upsertLocations: upsertLocations,
    })
  }

  // Add all the dead sheep to the tracking documents sub-collection
  for(const deadSheep of state.deadSheep) {
    // GeoPoint(lat, long), location(long, lat)
    const upsertLocations = deadSheep.upsertLocations.map((location) => new firestore.GeoPoint(location[1], location[0]))
    const newDeadSheepRef = newTrackingRef.collection('deadSheep').doc()

    batch.set(newDeadSheepRef, {
      description: deadSheep.description,
      location: new firestore.GeoPoint(deadSheep.location[1], deadSheep.location[0]),
      upsertLocations: upsertLocations,
    })

    // Add all the images to the images documents sub-collection
    for (const image of deadSheep.images) {
      // firebase document size limit = 1,048,576 bytes = 1 MiB
      // firebasse field value limit = 1 MiB - 89 bytes (1,048,487 bytes)
      const size =  1048487;
      const base64 = image.base64;
      const blob = new Blob([base64]);
      const blobSize = blob.size;
      let numberOfParts = Math.ceil(blobSize / size);
      const parts: Array<string> = new Array(numberOfParts);
      // we can assume the parts will be of uniform size as base64 encoding
      // only uses 1 byte unicode characters
      const stringLength = Math.ceil(base64.length / numberOfParts);
      for (let i = 0, acc = 0; i < numberOfParts; ++i, acc += stringLength) {
        parts[i] = base64.substring(acc, acc + stringLength);
      }
      
      // Add a document for the image
      const newDeadSheepImagesRef  = newDeadSheepRef.collection('images').doc();
      batch.set(newDeadSheepImagesRef, {
        size: blobSize,
        numberOfParts: numberOfParts,
      })

      // add documents for all the base64 parts of the image
      for(let i = 0; i < parts.length; i++) {
        const part = parts[i];
        const newTrackingImagePartRef  = newDeadSheepImagesRef.collection('parts').doc(i.toString());
        batch.set(newTrackingImagePartRef, {
          data: part
        })
      }
    }
  }

  for (const write of batch._writes) {
    console.log(write.data)
  }

  // Perform batch write
  return batch.commit()
}

const batchUpdateAndWrite = async (state: TrackingState, trackingUuid: string) => {
  // Get the ref of the tracking document and start a batch write
  const trackingsRef = firestore().collection('trackings');
  const trackingRef = trackingsRef.doc(trackingUuid);
  const batch = firestore().batch();

  // Add all the locations to the updated tracking document
  const locations: Array<FirebaseFirestoreTypes.GeoPoint> = []
  for (const location of state.locations) {
    locations.push(new firestore.GeoPoint(location[1], location[0]))
  }

  const offlineMap: FirebasOfflineMap = {
    uuid: state.offlineMap?.uuid,
    name: state.offlineMap?.name,
    styleURL: state.offlineMap?.styleURL,
    bounds: {
      neLng: state.offlineMap?.bounds[0][0],
      neLat: state.offlineMap?.bounds[0][1],
      swLng: state.offlineMap?.bounds[1][0],
      swLat: state.offlineMap?.bounds[1][1],
    }
  }

  if (state.offlineMap?.minZoom) {
    offlineMap.minZoom = state.offlineMap?.minZoom
  }
  if (state.offlineMap?.maxZoom) {
    offlineMap.maxZoom = state.offlineMap?.maxZoom
  }

  const farms: Array<string> = [];

  for(const herd of state.sheepHerds) {
    const farmNames = herd.farms.map((farm) => farm.name);
    farms.push(...farmNames)
  }

  // update the tracking document with the previous values, the new locations and an updated update timestamp
  batch.update(trackingRef, {
    locations: locations,
    startTimestamp: state.startTimestamp,
    endTimestamp: state.endTimestamp,
    lastUpdateTimestamp: new Date(),
    offlineMap: offlineMap,
    routeDescription: state.routeDescription,
    farms: [...new Set(farms)],
    owner: auth().currentUser?.uid,
  })

  // Add all the sheep herds to the tracking documents sub-collection
  for(const herd of state.sheepHerds) {
    // GeoPoint(lat, long), location(long, lat)
    const upsertLocations = herd.upsertLocations.map((location) => new firestore.GeoPoint(location[1], location[0]))
    // If the herd (firebaseUUID exists) exists then we want to update it instead of inserting a new one
    if (herd.firebaseUUID) {
      const trackingSheepHerdRef  = trackingRef.collection('sheepHerds').doc(herd.uuid)
      batch.update(trackingSheepHerdRef, {
        location: new firestore.GeoPoint(herd.location[1], herd.location[0]),
        upsertLocations: upsertLocations,
        farms: herd.farms,
        woundedAnimals: herd.woundedAnimals,
        sheeps: herd.sheeps,
        lambs: herd.lambs
      })
      // Add all the images to the images documents sub-collection
      for (const image of herd.images) {
        if (image.firebaseUUID) {
          // Do nothing as the image can't change
        } else {
          // firebase document size limit = 1,048,576 bytes = 1 MiB
          // firebasse field value limit = 1 MiB - 89 bytes (1,048,487 bytes)
          const size =  1048487;
          const base64 = image.base64;
          const blob = new Blob([base64]);
          const blobSize = blob.size;
          let numberOfParts = Math.ceil(blobSize / size);
          const parts: Array<string> = new Array(numberOfParts);
          // we can assume the parts will be of uniform size as base64 encoding
          // only uses 1 byte unicode characters
          const stringLength = Math.ceil(base64.length / numberOfParts);
          for (let i = 0, acc = 0; i < numberOfParts; ++i, acc += stringLength) {
            parts[i] = base64.substring(acc, acc + stringLength);
          }
          
          // Add a document for the image
          const newTrackingImagesRef  = trackingSheepHerdRef.collection('images').doc();
          batch.set(newTrackingImagesRef, {
            size: blobSize,
            numberOfParts: numberOfParts,
          })

          // add documents for all the base64 parts of the image
          for(let i = 0; i < parts.length; i++) {
            const part = parts[i];
            const newTrackingImagePartRef  = newTrackingImagesRef.collection('parts').doc(i.toString());
            batch.set(newTrackingImagePartRef, {
              data: part
            })
          }
        }
      }
    } else {
      const trackingSheepHerdRef  = trackingRef.collection('sheepHerds').doc()
      batch.set(trackingSheepHerdRef, {
        location: new firestore.GeoPoint(herd.location[1], herd.location[0]),
        upsertLocations: upsertLocations,
        farms: herd.farms,
        woundedAnimals: herd.woundedAnimals,
        sheeps: herd.sheeps,
        lambs: herd.lambs
      })
      // Add all the images to the images documents sub-collection
      for (const image of herd.images) {
        // firebase document size limit = 1,048,576 bytes = 1 MiB
        // firebasse field value limit = 1 MiB - 89 bytes (1,048,487 bytes)
        const size =  1048487;
        const base64 = image.base64;
        const blob = new Blob([base64]);
        const blobSize = blob.size;
        let numberOfParts = Math.ceil(blobSize / size);
        const parts: Array<string> = new Array(numberOfParts);
        // we can assume the parts will be of uniform size as base64 encoding
        // only uses 1 byte unicode characters
        const stringLength = Math.ceil(base64.length / numberOfParts);
        for (let i = 0, acc = 0; i < numberOfParts; ++i, acc += stringLength) {
          parts[i] = base64.substring(acc, acc + stringLength);
        }
        
        // Add a document for the image
        const newTrackingImagesRef  = trackingSheepHerdRef.collection('images').doc();
        batch.set(newTrackingImagesRef, {
          size: blobSize,
          numberOfParts: numberOfParts,
        })

        // add documents for all the base64 parts of the image
        for(let i = 0; i < parts.length; i++) {
          const part = parts[i];
          const newTrackingImagePartRef  = newTrackingImagesRef.collection('parts').doc(i.toString());
          batch.set(newTrackingImagePartRef, {
            data: part
          })
        }
      }
    }
  }

  // Add all the predators to the tracking documents sub-collection
  for(const predator of state.predators) {
    // GeoPoint(lat, long), location(long, lat)
    const upsertLocations = predator.upsertLocations.map((location) => new firestore.GeoPoint(location[1], location[0]))
    if (predator.firebaseUUID) {
      const predatorRef = trackingRef.collection('predators').doc(predator.uuid)
      const value: {
        type: Predator,
        location: FirebaseFirestoreTypes.GeoPoint,
        upsertLocations: FirebaseFirestoreTypes.GeoPoint[],
        description?: string
      } = {
        type: predator.type,
        location: new firestore.GeoPoint(predator.location[1], predator.location[0]),
        upsertLocations: upsertLocations
      }
      if (predator.description) {
        value.description = predator.description
      }
      batch.update(predatorRef, value)
    } else {
      const predatorRef = trackingRef.collection('predators').doc()
      const value: {
        type: Predator,
        location: FirebaseFirestoreTypes.GeoPoint,
        upsertLocations: FirebaseFirestoreTypes.GeoPoint[],
        description?: string
      } = {
        type: predator.type,
        location: new firestore.GeoPoint(predator.location[1], predator.location[0]),
        upsertLocations: upsertLocations
      }
      if (predator.description) {
        value.description = predator.description
      }
      batch.set(predatorRef, value)
    }
  }

  // Add all the others to the tracking documents sub-collection
  for(const other of state.others) {
    // GeoPoint(lat, long), location(long, lat)
    const upsertLocations = other.upsertLocations.map((location) => new firestore.GeoPoint(location[1], location[0]))
    if (other.firebaseUUID) {
      const otherRef = trackingRef.collection('others').doc(other.uuid)
      batch.update(otherRef, {
        type: other.type,
        description: other.description,
        location: new firestore.GeoPoint(other.location[1], other.location[0]),
        upsertLocations: upsertLocations
      })
    } else {
      const otherRef = trackingRef.collection('others').doc()
      batch.set(otherRef, {
        type: other.type,
        description: other.description,
        location: new firestore.GeoPoint(other.location[1], other.location[0]),
        upsertLocations: upsertLocations
      })
    }
  }

  // Add all the dead sheep to the tracking documents sub-collection
  for(const deadSheep of state.deadSheep) {
    // GeoPoint(lat, long), location(long, lat)
    const upsertLocations = deadSheep.upsertLocations.map((location) => new firestore.GeoPoint(location[1], location[0]))
    if (deadSheep.firebaseUUID) {
      const deadSheepRef = trackingRef.collection('deadSheep').doc(deadSheep.uuid)
      batch.update(deadSheepRef, {
        description: deadSheep.description,
        location: new firestore.GeoPoint(deadSheep.location[1], deadSheep.location[0]),
        upsertLocations: upsertLocations
      })
      // Add all the images to the images documents sub-collection
      for (const image of deadSheep.images) {
        if (image.firebaseUUID) {
          // Do nothing as the image can't change
        } else {
          // firebase document size limit = 1,048,576 bytes = 1 MiB
          // firebasse field value limit = 1 MiB - 89 bytes (1,048,487 bytes)
          const size =  1048487;
          const base64 = image.base64;
          const blob = new Blob([base64]);
          const blobSize = blob.size;
          let numberOfParts = Math.ceil(blobSize / size);
          const parts: Array<string> = new Array(numberOfParts);
          // we can assume the parts will be of uniform size as base64 encoding
          // only uses 1 byte unicode characters
          const stringLength = Math.ceil(base64.length / numberOfParts);
          for (let i = 0, acc = 0; i < numberOfParts; ++i, acc += stringLength) {
            parts[i] = base64.substring(acc, acc + stringLength);
          }
          
          // Add a document for the image
          const newTrackingImagesRef  = deadSheepRef.collection('images').doc();
          batch.set(newTrackingImagesRef, {
            size: blobSize,
            numberOfParts: numberOfParts,
          })

          // add documents for all the base64 parts of the image
          for(let i = 0; i < parts.length; i++) {
            const part = parts[i];
            const newTrackingImagePartRef  = newTrackingImagesRef.collection('parts').doc(i.toString());
            batch.set(newTrackingImagePartRef, {
              data: part
            })
          }
        }
      }
    } else {
      const deadSheepRef = trackingRef.collection('deadSheep').doc()
      batch.set(deadSheepRef, {
        description: deadSheep.description,
        location: new firestore.GeoPoint(deadSheep.location[1], deadSheep.location[0]),
        upsertLocations: upsertLocations
      })
      // Add all the images to the images documents sub-collection
      for (const image of deadSheep.images) {
        // firebase document size limit = 1,048,576 bytes = 1 MiB
        // firebasse field value limit = 1 MiB - 89 bytes (1,048,487 bytes)
        const size =  1048487;
        const base64 = image.base64;
        const blob = new Blob([base64]);
        const blobSize = blob.size;
        let numberOfParts = Math.ceil(blobSize / size);
        const parts: Array<string> = new Array(numberOfParts);
        // we can assume the parts will be of uniform size as base64 encoding
        // only uses 1 byte unicode characters
        const stringLength = Math.ceil(base64.length / numberOfParts);
        for (let i = 0, acc = 0; i < numberOfParts; ++i, acc += stringLength) {
          parts[i] = base64.substring(acc, acc + stringLength);
        }
        
        // Add a document for the image
        const newTrackingImagesRef  = deadSheepRef.collection('images').doc();
        batch.set(newTrackingImagesRef, {
          size: blobSize,
          numberOfParts: numberOfParts,
        })

        // add documents for all the base64 parts of the image
        for(let i = 0; i < parts.length; i++) {
          const part = parts[i];
          const newTrackingImagePartRef  = newTrackingImagesRef.collection('parts').doc(i.toString());
          batch.set(newTrackingImagePartRef, {
            data: part
          })
        }
      }
    }
  }

  // Delete the deleted stuff
  await _deleteToFirebase(trackingUuid, state, batch);

  // Perform batch write
  await batch.commit()
}

const _deleteToFirebase = async (trackingUuid: string, oldState: TrackingState, batch: FirebaseFirestoreTypes.WriteBatch) => {
  // Read the exisiting state so that we can diff what to delete
  // It shouldnt be a problem with document reads as they should be cached locally
  const exisitingState = await getTracking(trackingUuid);

  const trackingRef = firestore().collection('trackings').doc(trackingUuid);
  const predatorsRef = trackingRef.collection('predators');
  const othersRef = trackingRef.collection('others');
  const deadSheepsRef = trackingRef.collection('deadSheep');
  const sheepHerdsRef = trackingRef.collection('sheepHerds');

  // Delete predators
  const toBeKeptPredatorUUIDs = oldState.predators.map((p) => p.firebaseUUID).filter((uuid) => uuid !== undefined)
  const existingPredatorUUIDs = exisitingState.predators.map((p) => p.firebaseUUID).filter((uuid) => uuid !== undefined)
  const toBeDeletedPredatorUUIDs = existingPredatorUUIDs.filter((s) => !toBeKeptPredatorUUIDs.includes(s))
  for (const toBeDeletedPredatorUUID of toBeDeletedPredatorUUIDs) {
    batch.delete(predatorsRef.doc(toBeDeletedPredatorUUID))
  }
  
  // Delete others
  const toBeKeptOtherUUIDs = oldState.others.map((o) => o.firebaseUUID).filter((uuid) => uuid !== undefined)
  const existingOtherUUIDs = exisitingState.others.map((o) => o.firebaseUUID).filter((uuid) => uuid !== undefined)
  const toBeDeletedOtherUUIDs = existingOtherUUIDs.filter((s) => !toBeKeptOtherUUIDs.includes(s))
  for (const toBeDeletedOtherUUID of toBeDeletedOtherUUIDs) {
    batch.delete(othersRef.doc(toBeDeletedOtherUUID))
  }

  // Delete dead sheep
  const toBeKeptDeadSheepUUIDs = oldState.deadSheep.map((ds) => ds.firebaseUUID).filter((uuid) => uuid !== undefined)
  const existingDeadSheepUUIDs = exisitingState.deadSheep.map((ds) => ds.firebaseUUID).filter((uuid) => uuid !== undefined)
  const toBeDeletedDeadSheepUUIDs = existingDeadSheepUUIDs.filter((s) => !toBeKeptDeadSheepUUIDs.includes(s))
  for (const toBeDeletedDeadSheepUUID of toBeDeletedDeadSheepUUIDs) {
    batch.delete(deadSheepsRef.doc(toBeDeletedDeadSheepUUID))
    // Delete images
    const deadSheep = exisitingState.deadSheep.find((d) => d.firebaseUUID == toBeDeletedDeadSheepUUID);
    if (deadSheep) {
      const deadSheepRef = deadSheepsRef.doc(deadSheep.firebaseUUID);
      const deadSheepImagesRef = deadSheepRef.collection('images');
      for (const image of deadSheep.images) {
        batch.delete(deadSheepImagesRef.doc(image.firebaseUUID))
        if (image.numberOfParts) {
          const partsRef = deadSheepImagesRef.doc(image.firebaseUUID).collection('parts');
          for (let i = 0; i < image.numberOfParts; i++) {
            batch.delete(partsRef.doc(i.toString()))
          }
        }
      }
    }
    batch.delete(deadSheepsRef.doc(toBeDeletedDeadSheepUUID))
  }

  // Delete images from toBeKeptDeadSheeps
  for (const toBeKeptDeadSheepUUID of toBeKeptDeadSheepUUIDs) {
    const deadSheep = exisitingState.deadSheep.find((d) => d.firebaseUUID == toBeKeptDeadSheepUUID);
    if (deadSheep) {
      const deadSheepRef = deadSheepsRef.doc(deadSheep.firebaseUUID);
      const deadSheepImagesRef = deadSheepRef.collection('images');
      for (const image of deadSheep.images) {
        batch.delete(deadSheepImagesRef.doc(image.firebaseUUID))
        if (image.numberOfParts) {
          const partsRef = deadSheepImagesRef.doc(image.firebaseUUID).collection('parts');
          for (let i = 0; i < image.numberOfParts; i++) {
            batch.delete(partsRef.doc(i.toString()))
          }
        }
      }
    }
  }

  // Delete herds
  const toBeKeptHerdUUIDs = oldState.sheepHerds.map((h) => h.firebaseUUID).filter((uuid) => uuid !== undefined)
  const existingHerdUUIDs = exisitingState.sheepHerds.map((h) => h.firebaseUUID).filter((uuid) => uuid !== undefined)
  const toBeDeletedHerdUUIDs = existingHerdUUIDs.filter((s) => !toBeKeptHerdUUIDs.includes(s));
  // Delete sub-collections as they arent deleted when the parent document is deleted
  for (const toBeDeletedHerdUUID of toBeDeletedHerdUUIDs) {
    const herd = exisitingState.sheepHerds.find((h) => h.firebaseUUID == toBeDeletedHerdUUID)
    const sheepHerdRef = sheepHerdsRef.doc(toBeDeletedHerdUUID);
    if (herd) {
      for (const image of herd.images) {
        if (image.firebaseUUID) {
          const sheepHerdImagesRef = sheepHerdRef.collection('images');
          const sheepHerdImageRef = sheepHerdImagesRef.doc(image.firebaseUUID);
          batch.delete(sheepHerdImageRef)
          if (image.numberOfParts) {
            const partsRef = sheepHerdImageRef.collection('parts');
            for (let i = 0; i < image.numberOfParts; i++) {
              batch.delete(partsRef.doc(i.toString()))
            }
          }
        }
      }
    }
    batch.delete(sheepHerdsRef.doc(toBeDeletedHerdUUID))
  }

  // Delete sheeps and lambs from toBeKeptHerd
  for (const toBeKeptHerdUUID of toBeKeptHerdUUIDs) {
    // Delete images
    const toBeKeptImages = oldState.sheepHerds.find((h) => h.firebaseUUID === toBeKeptHerdUUID)?.images.map((i) => i.firebaseUUID)
    const exisitingImages = exisitingState.sheepHerds.find((h) => h.firebaseUUID === toBeKeptHerdUUID)?.images.map((i) => i.firebaseUUID)
    console.log("1", exisitingImages)
    if (exisitingImages) {
      const toBeDeletedImages = exisitingImages.filter((uuid) => !toBeKeptImages?.includes(uuid))
      console.log("2", toBeDeletedImages)
      for (const toBeDeletedImage of toBeDeletedImages) {
        console.log("3", toBeDeletedImage)
        if (toBeDeletedImage) {
          const sheepHeardRef = sheepHerdsRef.doc(toBeKeptHerdUUID);
          const sheepHeardImagesRef = sheepHeardRef.collection('images');
          const sheepHeardImageRef = sheepHeardImagesRef.doc(toBeDeletedImage);
          batch.delete(sheepHeardImageRef)
          const herd = exisitingState.sheepHerds.find((h) => h.firebaseUUID == toBeKeptHerdUUID);
          console.log("3", herd?.firebaseUUID)
          if (herd) {
            const image = herd.images.find((i) => i.firebaseUUID == toBeDeletedImage);
            console.log("4", image?.firebaseUUID)
            if (image && image.numberOfParts) {
              const partsRef = sheepHeardImageRef.collection('parts');
              for (let i = 0; i < image.numberOfParts; i++) {
                batch.delete(partsRef.doc(i.toString()))
              }
            }
          }
        }
      }
    }
  }
}

export {
  getTracking,
  batchWrite,
  batchUpdateAndWrite,
  getFarms
}