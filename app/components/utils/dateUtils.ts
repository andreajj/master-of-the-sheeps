export const NORWEGIAN_DATE_NAMES = {
    monthNames: ["januar", "februar", "mars", "april", "mai", "juni", 
      "juli", "august", "september", "oktober", "november", "desember"],
    dayNames: ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", 
      "fredag", "lørdag",],
    dayNamesShort: ['søn.','man.','tir.','ons.','tor.','fre.','lør.']
}