import AsyncStorage from '@react-native-async-storage/async-storage';

const storeData = async (key: string, value: string) => {
  try {
    await AsyncStorage.setItem(key, value)
    return true
  } catch (e) {
    return false
  }
}

const getData = async (key: string) => {
  try {
    const value = await AsyncStorage.getItem(key)
    return value
  } catch(e) {
    return null
  }
}

const deleteData = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key)
    return true
  } catch(e) {
    return false
  }
}

export {
  storeData,
  getData,
  deleteData
}