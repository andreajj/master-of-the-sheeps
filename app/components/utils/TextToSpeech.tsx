import { speak } from 'expo-speech';

/**
 * Plays the given message through text-to-speech in Norwegian (bokmål).
 * @param message 
 */
export const sayMessage = (message: string) => {
    speak(message, {
        language: "nb-NO",
        rate: 1
    })
}
