import React from 'react';
import { Appbar } from "react-native-paper";
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTheme } from 'react-native-paper';

interface Props {
    navigation: any,
    title: string
}

const TopBar: React.FC<Props> = ({navigation, title}) => {
    const theme = useTheme();
    return (
        <SafeAreaView edges={['top']} style={{backgroundColor: theme.colors.primary}}>
            <Appbar theme={theme} style={{elevation: 0}}>
                <Appbar.Action
                    icon="hamburger"
                    onPress={() => navigation.openDrawer()}
                />
                <Appbar.Content title={title} />
            </Appbar>
        </SafeAreaView>    
    );
};

export default TopBar;
