import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";
import MapboxGL from "@react-native-mapbox-gl/maps";
import BackgroundTracker from "./pages/Tracking/BackgroundTracker";

MapboxGL.setAccessToken(
  "pk.eyJ1IjoibGFyc2V2YSIsImEiOiJja3RjeGhtYmQyYWRqMnBuOTVwZTFjcmd2In0.yl_GUShVvT3hyW-0F0cj2w"
);

// array with two numbers gets interpreted as number[]
// instead of [number, number], </3 microsoft
type maxBounds = {
  ne: [number, number];
  sw: [number, number];
};

interface Props {
  maxBounds: {
    sw: [number, number],
    ne: [number, number]
  },
  centerCoordinate: [number, number],
  zoomLevel: number,
  children: React.ReactNode
}

type RefType = {
  mapRef: MapboxGL.MapView | null;
  cameraRef: MapboxGL.Camera | null;
}

const Map = React.forwardRef<RefType, Props>(({ maxBounds, centerCoordinate, zoomLevel, children }, ref) => {
  // typescript complains when you pass multiple refs, doesn't matter though
  const {mapRef, cameraRef} = ref
  useEffect(() => {
    MapboxGL.setTelemetryEnabled(false);
  }, []);

  const key = JSON.stringify(maxBounds) + JSON.stringify(centerCoordinate) + JSON.stringify(zoomLevel);

  return (
    <View style={styles.container}>
      <MapboxGL.MapView style={styles.map} ref={mapRef}>
        <MapboxGL.Camera
          zoomLevel={zoomLevel}
          maxBounds={maxBounds}
          key={key}
          centerCoordinate={centerCoordinate}
          ref={cameraRef}
        />
        {children}
      </MapboxGL.MapView>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    backgroundColor: "white",
  },
  map: {
    flex: 1,
  },
});

export default Map;
