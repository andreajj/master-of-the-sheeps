import React from "react";
import MapboxGL, { OnPressEvent } from "@react-native-mapbox-gl/maps";
import { SheepHerdType } from "../atoms";
import { useNavigation } from '@react-navigation/native';
import { RouteNames } from "../Navigation";
import {GenericIconFeatureType} from "../atoms/utils/types";

interface Props {
  sheepHerds: Array<SheepHerdType>;
}

const DrawSheepHerdPoints: React.FC<Props> = ({ sheepHerds }) => {
  if (sheepHerds.length === 0) return null;

  const navigation = useNavigation();

  const sheepHerdFeaturesList: GenericIconFeatureType[] = [];
  for (let i = 0; i < sheepHerds.length; i++) {
    const currSheepHerd = sheepHerds[i];

    const sheepHerdFeature: GenericIconFeatureType = {
      type: "Feature" as const,
      properties: {
        id: `Herd ${currSheepHerd.uuid}`,
        uuid: currSheepHerd.uuid
      },
      geometry: {
        type: "Point" as const,
        coordinates: currSheepHerd.location,
      },
    };
    sheepHerdFeaturesList.push(sheepHerdFeature);
  }

  const sheepHerdFeatureCollection = {
    type: "FeatureCollection" as const,
    features: sheepHerdFeaturesList,
  };

  const onPress = async (event: OnPressEvent) => {
    const sheepHerd = event.features[0]
    const uuid: string = sheepHerd.properties.uuid
    navigation.navigate(RouteNames.TrackingSheepHerdScreen, {
      sheepHerdUUID: uuid
    })
  }

  return (
    <MapboxGL.ShapeSource
      key="ShapeSource: sheepHerds"
      id="sheepHerdSource"
      shape={sheepHerdFeatureCollection}
      onPress={onPress}
    >
      <MapboxGL.SymbolLayer
        id="SymbolLayer: sheepHerds"
        sourceID="sheepHerdSource"
        style={{
          iconSize: 1,
          iconAllowOverlap: true,
          textField: "Saueflokk",
          textAnchor: "top",
          textOffset: [0, 0.5],
          iconImage: "sheep",
        }}
      />
    </MapboxGL.ShapeSource>
  );
};

export default DrawSheepHerdPoints;
