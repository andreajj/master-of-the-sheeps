import React from "react";
import MapboxGL from "@react-native-mapbox-gl/maps";
import { DrawLocationHistoryProps } from "./DrawLocationHistoryLines";

type pointFeaturesType = {
  type: "Feature";
  properties: {};
  geometry: {
    type: "Point";
    coordinates: number[];
  };
};

/**
 * Draws the points (circles) that represent previously
 * registered locations where the user has been.
 * @param props nested list of coordinates
 * @returns Circles that are overlaid on the map
 */
const DrawLocationHistoryPoints = (props: DrawLocationHistoryProps) => {
  const coordinates = props.coordinates;
  if (coordinates === undefined) return null;
  if (coordinates.length < 2) return null;
  if (coordinates[0].length === 0) return null;

  // loop through list of coordinates and create a "Point" feature for each of them
  const pointCoordinatesList: pointFeaturesType[] = [];
  for (let i = 0; i < coordinates.length; i++) {
    const pointFeature = {
      type: "Feature" as const,
      properties: {},
      geometry: {
        type: "Point" as const,
        coordinates: coordinates[i],
      },
    };
    pointCoordinatesList.push(pointFeature);
  }

  const circleGeoJSONSource = {
    type: "FeatureCollection" as const,
    features: pointCoordinatesList,
  };

  let circleShapes = (
    <MapboxGL.ShapeSource id="2" shape={circleGeoJSONSource}>
      <MapboxGL.CircleLayer
        id="2"
        style={{
          circleColor: "#19e32a",
          circleRadius: 5,
          circleStrokeColor: "#ffffff",
          circleStrokeWidth: 1,
        }}
      ></MapboxGL.CircleLayer>
    </MapboxGL.ShapeSource>
  );

  return circleShapes;
};

export default DrawLocationHistoryPoints;
