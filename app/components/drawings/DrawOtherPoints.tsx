import React from "react";
import MapboxGL, { OnPressEvent } from "@react-native-mapbox-gl/maps";
import { OtherType } from "../atoms";
import { Location, Others } from "../pages/Tracking/types";
import { useNavigation } from '@react-navigation/native';
import { RouteNames } from "../Navigation";

interface Props {
  others: Array<OtherType>;
}

type OtherFeatureType = {
  type: "Feature";
  properties: {
    id: string;
    uuid: string;
    otherType: Others;
  };
  geometry: {
    type: "Point";
    coordinates: Location;
  };
};

const DrawOtherPoints: React.FC<Props> = ({ others }) => {
  if (others.length === 0) return null;

  const navigation = useNavigation();

  const otherFeaturesList: OtherFeatureType[] = [];
  for (let i = 0; i < others.length; i++) {
    const otherFeature: OtherFeatureType = {
      type: "Feature" as const,
      properties: {
        id: `Other ${others[i].uuid}`,
        uuid: others[i].uuid,
        otherType: others[i].type,
      },
      geometry: {
        type: "Point" as const,
        coordinates: others[i].location,
      },
    };
    otherFeaturesList.push(otherFeature);
  }

  const otherFeatureCollection = {
    type: "FeatureCollection" as const,
    features: otherFeaturesList,
  };

  const onPress = async (event: OnPressEvent) => {
    const other = event.features[0]
    const uuid: string = other.properties.uuid
    navigation.navigate(RouteNames.TrackingOtherScreen, {
      otherUUID: uuid
    })
  }

  return (
    <MapboxGL.ShapeSource id="otherSource" shape={otherFeatureCollection} onPress={onPress}>
      {/* Each 'other' may have its own symbol in the future, this requires unique Symbollayer per other */}

      {Object.values(Others).map((other) => (
        <MapboxGL.SymbolLayer
          key={other}
          id={`otherDrawing-${other}`}
          sourceID="otherSource"
          style={{
            iconSize: 1,
            iconAllowOverlap: true,
            textField: other,
            textAnchor: "top",
            textOffset: [0, 0.5],
            iconImage: "other",
          }}
          filter={["==", `${other}`, ["get", "otherType"]]}
        />
      ))}
    </MapboxGL.ShapeSource>
  );
};

export default DrawOtherPoints;
