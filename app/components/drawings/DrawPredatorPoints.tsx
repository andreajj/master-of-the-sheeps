import React from "react";
import MapboxGL, { OnPressEvent } from "@react-native-mapbox-gl/maps";
import { PredatorType } from "../atoms";
import { Location, Predator } from "../pages/Tracking/types";
import { useNavigation } from '@react-navigation/native';
import { RouteNames } from "../Navigation";

interface Props {
  predators: Array<PredatorType>;
}

type PredatorFeatureType = {
  type: "Feature";
  properties: {
    id: string;
    uuid: string,
    animalType: Predator;
  };
  geometry: {
    type: "Point";
    coordinates: Location;
  };
};

const DrawPredatorPoints: React.FC<Props> = ({ predators }) => {
  if (predators.length === 0) return null;

  const navigation = useNavigation();

  const predatorFeaturesList: PredatorFeatureType[] = [];
  for (let i = 0; i < predators.length; i++) {
    const predatorFeature: PredatorFeatureType = {
      type: "Feature" as const,
      properties: {
        id: `Predator ${predators[i].uuid}`,
        uuid: predators[i].uuid,
        animalType: predators[i].type,
      },
      geometry: {
        type: "Point" as const,
        coordinates: predators[i].location,
      },
    };
    predatorFeaturesList.push(predatorFeature);
  }

  const predatorFeatureCollection = {
    type: "FeatureCollection" as const,
    features: predatorFeaturesList,
  };

  const onPress = async (event: OnPressEvent) => {
    const predator = event.features[0]
    const uuid: string = predator.properties.uuid
    navigation.navigate(RouteNames.TrackingPredatorScreen, {
      predatorUUID: uuid
    })
  }

  return (
    <MapboxGL.ShapeSource id="predatorSource" shape={predatorFeatureCollection} onPress={onPress}>
      {/* Each predator has its own symbol, this requires unique Symbollayer per animal */}

      {Object.values(Predator).map((predator) => (
        <MapboxGL.SymbolLayer
          key={predator}
          id={`predatorDrawing-${predator}`}
          sourceID="predatorSource"
          style={{
            iconSize: 1,
            iconAllowOverlap: true,
            textField: predator,
            textAnchor: "top",
            textOffset: [0, 0.5],
            iconImage: "paw"
          }}
          filter={["==", `${predator}`, ["get", "animalType"]]}
        />
      ))}
    </MapboxGL.ShapeSource>
  );
};

export default DrawPredatorPoints;
