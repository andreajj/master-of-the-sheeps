import React from "react";
import MapboxGL from "@react-native-mapbox-gl/maps";
import { TrackingState } from '../atoms/utils/types'

type Props = {
  tracking: TrackingState;
};

type lineFeaturesType = {
  type: "Feature";
  properties: {};
  geometry: {
    type: "LineString";
    coordinates: number[][];
  };
};

/**
 * Similar to DrawObservation lines, but only apply to edits.
 * Meaning these lines are drawn from the user's location at time of editing,
 * to the relevant observation marker.
 * @returns Dashed lines that are overlaid on the map
 */
const DrawEditedObservationLines: React.FC<Props> = ({tracking}) => {
  // DRAWING FOR SHEEPHERDS HAS BEEN DISABLED DUE TO STATE BEING INCORRECT.
  // RE-ENABLE WHEN FIXED
  const {sheepHerds, predators, others, deadSheep} = tracking;
  
  if (!sheepHerds.length && !predators.length && !others.length) return null;

  const observationLinesEditedFeatureList: lineFeaturesType[] = [];

  const addEditedSheepHerdFeatures = () => {
    for (let i = 0; i < sheepHerds.length; i++) {
      for (let j = 0; j < sheepHerds[i].upsertLocations.length; j++) {
        const currentEditLocation = sheepHerds[i].upsertLocations[j];
          if(j===0) {
            continue
          } else {
            const lineFeature: lineFeaturesType = {
              type: "Feature" as const,
              properties: {},
              geometry: {
                type: "LineString" as const,
                coordinates: [sheepHerds[i].location, currentEditLocation],
              },
            };
            observationLinesEditedFeatureList.push(lineFeature);
          }
        }
    }
  }

  const addEditedDeadSheepFeatures = () => {
    for (let i = 0; i < deadSheep.length; i++) {
      for (let j = 0; j < deadSheep[i].upsertLocations.length; j++) {
        const currentEditLocation = deadSheep[i].upsertLocations[j];
          if(j===0) {
            continue
          } else {
            const lineFeature: lineFeaturesType = {
              type: "Feature" as const,
              properties: {},
              geometry: {
                type: "LineString" as const,
                coordinates: [deadSheep[i].location, currentEditLocation],
              },
            };
            observationLinesEditedFeatureList.push(lineFeature);
          }
        }
    }
  }

  const addEditedPredatorFeatures = () => {
    for (let i = 0; i < predators.length; i++) {
      for (let j = 0; j < predators[i].upsertLocations.length; j++) {
        const currentEditLocation = predators[i].upsertLocations[j];
          if(j===0) {
            continue
          } else {
            const lineFeature: lineFeaturesType = {
              type: "Feature" as const,
              properties: {},
              geometry: {
                type: "LineString" as const,
                coordinates: [predators[i].location, currentEditLocation],
              },
            };
            observationLinesEditedFeatureList.push(lineFeature);
          }
        }
    }
  }

  const addEditedOtherFeatures = () => {
    for (let i = 0; i < others.length; i++) {
      for (let j = 0; j < others[i].upsertLocations.length; j++) {
        const currentEditLocation = others[i].upsertLocations[j];
          if(j===0) {
            continue
          } else {
            const lineFeature: lineFeaturesType = {
              type: "Feature" as const,
              properties: {},
              geometry: {
                type: "LineString" as const,
                coordinates: [others[i].location, currentEditLocation],
              },
            };
            observationLinesEditedFeatureList.push(lineFeature);
          }
        }
    }
  }

  addEditedSheepHerdFeatures();
  addEditedPredatorFeatures();
  addEditedOtherFeatures();
  addEditedDeadSheepFeatures();

  const observationLinesEditedGeoJSONSource: GeoJSON.FeatureCollection = {
    type: "FeatureCollection" as const,
    features: observationLinesEditedFeatureList,
  };

  let linesToEditedObservationsSource = (
      <MapboxGL.ShapeSource id="linesToEditedObservationsSource" shape={observationLinesEditedGeoJSONSource}>
        <MapboxGL.LineLayer
            id="linesToEditedObservationsLayer"
            // original dasharray was [3,2]
            style={{ lineColor: "#4b51fa", lineWidth: 2, lineDasharray: [2,2] }}
        />
      </MapboxGL.ShapeSource>
  );

  return linesToEditedObservationsSource;
};

export default DrawEditedObservationLines;
