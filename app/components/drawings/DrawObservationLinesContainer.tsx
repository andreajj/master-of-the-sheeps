import React from 'react'
import DrawObservationLines from "../drawings/DrawObservationLines";
import DrawEditedObservationLines from "../drawings/DrawEditedObservationLines"
import { TrackingState } from '../atoms/utils/types'

type Props = {
  tracking: TrackingState;
};

/**
 * Groups observation lines and lines for *edited* observations
 * @param tracking - TrackingState object 
 * @returns The two observationLines components
 */
const DrawObservationLinesContainer: React.FC<Props> = ({tracking}) => {
    return (
        <>
            <DrawObservationLines tracking={tracking}/>
            <DrawEditedObservationLines tracking={tracking}/>
        </>
    )
}

export default DrawObservationLinesContainer
