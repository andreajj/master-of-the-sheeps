import React from "react";
import MapboxGL, { OnPressEvent } from "@react-native-mapbox-gl/maps";
import { DeadSheepType } from "../atoms/utils/types";
import { useNavigation } from '@react-navigation/native';
import { RouteNames } from "../Navigation";
import {GenericIconFeatureType} from "../atoms/utils/types";

interface Props {
  deadSheep: Array<DeadSheepType>;
}

const DrawDeadSheepPoints: React.FC<Props> = ({ deadSheep }) => {
  if (deadSheep.length === 0) return null;

  const navigation = useNavigation();

  const deadSheepFeaturesList: GenericIconFeatureType[] = [];
  for (let i = 0; i < deadSheep.length; i++) {
    const currDeadSheep = deadSheep[i];

    const deadSheepFeature: GenericIconFeatureType = {
      type: "Feature" as const,
      properties: {
        id: `DeadSheep ${currDeadSheep.uuid}`,
        uuid: currDeadSheep.uuid
      },
      geometry: {
        type: "Point" as const,
        coordinates: currDeadSheep.location,
      },
    };
    deadSheepFeaturesList.push(deadSheepFeature);
  }

  const deadSheepFeatureCollection = {
    type: "FeatureCollection" as const,
    features: deadSheepFeaturesList,
  };

  const onPress = async (event: OnPressEvent) => {
    const deadSheep = event.features[0]
    const uuid: string = deadSheep.properties.uuid
    navigation.navigate(RouteNames.TrackingDeadSheepScreen, {
        deadSheepUUID: uuid
    })
  }

  return (
    <MapboxGL.ShapeSource
      key="ShapeSource: deadSheep"
      id="deadSheepSource"
      shape={deadSheepFeatureCollection}
      onPress={onPress}
    >
      <MapboxGL.SymbolLayer
        id="SymbolLayer: deadSheep"
        sourceID="deadSheepSource"
        style={{
          iconSize: 1,
          iconAllowOverlap: true,
          textField: "Død sau",
          textAnchor: "top",
          textOffset: [0, 0.5],
          iconImage: "deadSheep",
        }}
      />
    </MapboxGL.ShapeSource>
  );
};

export default DrawDeadSheepPoints;
