import React from "react";
import MapboxGL from "@react-native-mapbox-gl/maps";
import { TrackingState } from '../atoms/utils/types'

type Props = {
  tracking: TrackingState;
};

type lineFeaturesType = {
  type: "Feature";
  properties: {};
  geometry: {
    type: "LineString";
    coordinates: number[][];
  };
};

/**
 * Draws lines from the user's location (at time of observation creation) to the observation location.
 * @returns Dashed lines that are overlaid on the map
 */
const DrawObservationLines: React.FC<Props> = ({tracking}) => {
  // DRAWING FOR SHEEPHERDS HAS BEEN DISABLED DUE TO STATE BEING INCORRECT.
  // RE-ENABLE WHEN FIXED
  const {sheepHerds, predators, others, deadSheep} = tracking;
  
  if (!sheepHerds.length && !predators.length && !others.length) return null;
  
  const observationLinesFeatureList: lineFeaturesType[] = [];

  const addSheepHerdFeatures = () => {
    for (let i = 0; i < sheepHerds.length; i++) {
    const lineFeature: lineFeaturesType = {
      type: "Feature" as const,
      properties: {},
      geometry: {
        type: "LineString" as const,
        coordinates: [sheepHerds[i].location, sheepHerds[i].upsertLocations[0]],
      },
    };
    observationLinesFeatureList.push(lineFeature);
    }
  }

  const addDeadSheepFeatures = () => {
    for (let i = 0; i < deadSheep.length; i++) {
    const lineFeature: lineFeaturesType = {
      type: "Feature" as const,
      properties: {},
      geometry: {
        type: "LineString" as const,
        coordinates: [deadSheep[i].location, deadSheep[i].upsertLocations[0]],
      },
    };
    observationLinesFeatureList.push(lineFeature);
    }
  }

  const addPredatorFeatures = () => {
    for (let i = 0; i < predators.length; i++) {
    const lineFeature: lineFeaturesType = {
      type: "Feature" as const,
      properties: {},
      geometry: {
        type: "LineString" as const,
        coordinates: [predators[i].location, predators[i].upsertLocations[0]],
      },
    };
    observationLinesFeatureList.push(lineFeature);
    }
  }

  const addOtherFeatures = () => {
    for (let i = 0; i < others.length; i++) {
    const lineFeature: lineFeaturesType = {
      type: "Feature" as const,
      properties: {},
      geometry: {
        type: "LineString" as const,
        coordinates: [others[i].location, others[i].upsertLocations[0]],
      },
    };
    observationLinesFeatureList.push(lineFeature);
    }
  }
  
  addSheepHerdFeatures();
  addPredatorFeatures();
  addOtherFeatures();
  addDeadSheepFeatures();

  const observationLinesGeoJSONSource: GeoJSON.FeatureCollection = {
    type: "FeatureCollection" as const,
    features: observationLinesFeatureList,
  };

  let linesToObservationsSource = (
    <MapboxGL.ShapeSource id="linesToObservationsSource" shape={observationLinesGeoJSONSource}>
      <MapboxGL.LineLayer
        id="linesToObservationsLayer"
        style={{ lineColor: "#0b646d", lineWidth: 2, lineDasharray: [3,2] }}
      />
    </MapboxGL.ShapeSource>
  );

  return linesToObservationsSource;
};

export default DrawObservationLines;
