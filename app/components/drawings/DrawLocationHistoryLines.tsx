import React from "react";
import MapboxGL from "@react-native-mapbox-gl/maps";

export type DrawLocationHistoryProps = {
  coordinates?: number[][];
};

/**
 * Draws the lines connecting points that represent previously
 * registered locations where the user has been.
 * @param props nested list of coordinates
 * @returns Lines that are overlaid on the map
 */
const DrawLocationHistoryLines = (props: DrawLocationHistoryProps) => {
  const coordinates = props.coordinates;
  if (coordinates === undefined) return null;
  if (coordinates.length < 2) return null;
  if (coordinates[0].length === 0) return null;

  const lineGeoJSONSource = {
    type: "Feature" as const,
    properties: {},
    geometry: {
      type: "LineString" as const,
      coordinates: coordinates,
    },
  };

  let lineShapes = (
    <MapboxGL.ShapeSource id="1" shape={lineGeoJSONSource}>
      <MapboxGL.LineLayer
        id="1"
        style={{ lineColor: "#c91c1c", lineWidth: 3, lineOpacity: 0.5 }}
      />
    </MapboxGL.ShapeSource>
  );

  return lineShapes;
};

export default DrawLocationHistoryLines;
