import {  atom } from 'recoil';
import { OfflineMap } from '.';
import { persistenceEffect } from './utils/persistence';

interface User {
  hasSeenOfflineMapTutorial: boolean,
  isOnline: boolean,
  offlineMap?: OfflineMap
}

const userState = atom({
  key: 'userState',
  default: {
    hasSeenOfflineMapTutorial: true,
    isOnline: true
  } as User,
  effects_UNSTABLE: [persistenceEffect("userState")],
});

export {
  User,
  userState
}