import {  atom } from 'recoil';
import { persistenceEffect } from './utils/persistence';
import { FarmType } from './utils/types';

interface FarmsType {
  farmsList: Array<FarmType>,
}

const farmsState = atom({
  key: 'farmsState',
  default: {
    farmsList: [],
  } as FarmsType,
  effects_UNSTABLE: [persistenceEffect("farmsState")],
});

export {
    FarmsType,
    farmsState
}