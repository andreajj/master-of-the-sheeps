import {  atom } from 'recoil';
import { backgroundTracking } from './utils/backgroundTracking';
import { TrackingState } from './utils/types';

const defaultCurrentTrackingState = {
  isTracking: false,
  locations: [],
  sheepHerds: [],
  predators: [],
  others: [],
  deadSheep: [],
  farms: [],
} as TrackingState

const currentTrackingState = atom({
  key: 'currentTracking',
  default: {...defaultCurrentTrackingState},
  effects_UNSTABLE: [backgroundTracking],
});

export {
  defaultCurrentTrackingState,
  currentTrackingState,
}