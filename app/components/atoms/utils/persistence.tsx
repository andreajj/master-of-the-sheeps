import { getData, storeData, deleteData } from '../../utils/Storage';

const persistenceEffect = (key: string) => ({setSelf, onSet}) => {
  const loadPersisted = async () => {
    const savedValue = await getData(key)
    if (savedValue != null) {
      setSelf(JSON.parse(savedValue));
    }
  }

  loadPersisted();

  onSet(newValue => {
    storeData(key, JSON.stringify(newValue));
  });
};

export {
  persistenceEffect
}