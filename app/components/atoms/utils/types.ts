import { Location } from "../../pages/Tracking/types";
import { Others } from '../../pages/Tracking/types';
import { Predator } from '../../pages/Tracking/types';
import { EarTag, Tie } from '../../pages/Tracking/types';

interface TrackingState {
  firebaseUUID?: string,
  startTimestamp?: Date,
  endTimestamp?: Date,
  lastUpdateTimestamp?: Date,
  isTracking: boolean,
  isPlacingPOI?: POI,
  locations: Array<Location>,
  sheepHerds: Array<SheepHerdType>,
  predators: Array<PredatorType>,
  others: Array<OtherType>,
  deadSheep: Array<DeadSheepType>,
  farms: Array<string>
  intermediateSheepHerd?: SheepHerdType,
  offlineMap?: OfflineMap,
  routeDescription?: string,
  owner?: string,
}

interface FarmType {
  name: string,
  color1: string,
  color2?: string,
  owner: {
      uuid: string,
      email: string
  }
}

interface SheepHerdType {
  sheeps: {
    [Tie.Blue]: number,
    [Tie.Green]: number,
    [Tie.None]: number,
    [Tie.Red]: number,
    [Tie.Yellow]: number,
  }
  lambs: number,
  woundedAnimals: number,
  location: Location,
  upsertLocations: Array<Location>
  earTags?: Array<string>, // TODO: Remove entirely
  farms: Array<FarmType>,
  images: Array<Image>,
  uuid: string,
  firebaseUUID?: string,
}

interface PredatorType {
  type: Predator,
  description?: string
  location: Location,
  upsertLocations: Array<Location>
  uuid: string,
  firebaseUUID?: string,
}

interface OtherType {
  type: Others,
  description: string,
  location: Location,
  upsertLocations: Array<Location>
  uuid: string,
  firebaseUUID?: string,
}

interface DeadSheepType {
  description: string,
  images: Array<Image>
  location: Location,
  upsertLocations: Array<Location>
  uuid: string,
  firebaseUUID?: string,
}

interface Image {
  base64: string,
  firebaseUUID?: string,
  numberOfParts?: number,
}

enum POI {
  SheepHerd,
  Predator,
  Other,
  DeadSheep
}

interface OfflineMap {
  uuid: string,
  name: string,
  styleURL: string,
  minZoom?: number,
  maxZoom?: number,
  // [[neLng, neLat], [swLng, swLat]]
  bounds: [[number, number], [number, number]]
}

interface FirebasOfflineMap {
  uuid: string,
  name: string,
  styleURL: string,
  bounds: {
    neLng: number,
    neLat: number,
    swLng: number,
    swLat: number,
  },
  minZoom?: number,
  maxZoom?: number,
}

type GenericIconFeatureType = {
  type: "Feature";
  properties: {
    id: string;
    uuid: string;
  };
  geometry: {
    type: "Point";
    coordinates: Location;
  };
};

export {
  TrackingState,
  SheepHerdType,
  PredatorType,
  OtherType,
  DeadSheepType,
  POI,
  OfflineMap,
  FirebasOfflineMap,
  GenericIconFeatureType,
  FarmType,
  Image
}