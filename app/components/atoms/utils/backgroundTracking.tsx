import { TrackingState } from './types';
import { beginBackgroundTracking, isBackgroundTrackingActive, stopBackgroundTracking } from '../../pages/Tracking/BackgroundTracker';

const backgroundTracking = ({setSelf, onSet}) => {
  const init = async () => {
    console.log("STATE INITIALIZING")
    const isActive = await isBackgroundTrackingActive();
    if (isActive) {
      stopBackgroundTracking();
      console.log("STOPPED TRACKING POSITION");
    }
  }
  
  init();
  
  onSet(async (newValue: TrackingState) => {
    const isActive = await isBackgroundTrackingActive();
    if (!newValue.isTracking && isActive) {
      stopBackgroundTracking();
      console.log("STOPPED TRACKING POSITION");
    } else if (newValue.isTracking && !isActive) {
      beginBackgroundTracking();
      console.log("CURRENTLY TRACKING POSITION");
    }
  });
};

export {
  backgroundTracking
}