import {  atom } from 'recoil';
import { FirebasOfflineMap, OfflineMap } from './utils/types';
import { getData, storeData } from '../utils/Storage';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import MapboxGL from '@react-native-mapbox-gl/maps';

const firebaseAndLocalPersistence = (key: string) => ({setSelf, onSet}) => {
  const loadPersisted = async () => {
    const savedValue = await getData(key)
    const storedLocalMaps: Array<OfflineMap> = savedValue ? JSON.parse(savedValue) : [];

    const firebaseMaps: Array<OfflineMap> = [];
    const firebaseRef = firestore().collection('users').doc(auth().currentUser?.uid).collection('offlineMaps')
    const firebaseCollectionSnap = await firebaseRef.get()
    firebaseCollectionSnap.forEach((mapDocument) => {
      const mapData = mapDocument.data()
      firebaseMaps.push({
        uuid: mapDocument.id,
        name: mapData.name,
        styleURL: mapData.styleURL,
        bounds: [[mapData.bounds.neLng, mapData.bounds.neLat], [mapData.bounds.swLng, mapData.bounds.swLat]],
        minZoom: mapData.minZoom,
        maxZoom: mapData.maxZoom
      })
    })

    const mapsNotInStoredLocal = firebaseMaps.filter((map) => !storedLocalMaps.map((_) => _.uuid).includes(map.uuid));
    const mapsNotInFirebase = storedLocalMaps.filter((map) => !firebaseMaps.map((_) => _.uuid).includes(map.uuid));

    console.log("NOT IN LOCAL", mapsNotInStoredLocal)
    console.log("NOT IN FIREBASE", mapsNotInFirebase)

    for (const map of mapsNotInFirebase) {
      const firebaseRef = firestore().collection('users').doc(auth().currentUser?.uid).collection('offlineMaps').doc(map.uuid)
      const newFirebaseMap: FirebasOfflineMap = {
        uuid: map.uuid,
        name: map.name,
        styleURL: map.styleURL,
        bounds: {
          neLng: map.bounds[0][0],
          neLat: map.bounds[0][1],
          swLng: map.bounds[1][0],
          swLat: map.bounds[1][1],
        }
      }
      if (map.minZoom) {
        newFirebaseMap.minZoom = map.minZoom
      }
      if (map.maxZoom) {
        newFirebaseMap.maxZoom = map.maxZoom
      }
      await firebaseRef.set(newFirebaseMap)
    }

    for (const map of mapsNotInStoredLocal) {
      await MapboxGL.offlineManager.createPack({
        name: map.uuid,
        styleURL: map.styleURL,
        bounds: map.bounds
      }) 
    }

    const maps = storedLocalMaps.concat(mapsNotInStoredLocal);
    storeData(key, JSON.stringify(maps));

    setSelf(maps);
  }

  loadPersisted();

  onSet((newValue: Array<OfflineMap>, oldValue: Array<OfflineMap>) => {
    const firebaseUpdate = async () => {
      // Find the newly added map(s) and add them to firebase
      const added = newValue.filter((map) => !oldValue.map((_) => _.uuid).includes(map.uuid));
      if (added.length > 0) {
        for (const newMap of added) {
          const firebaseRef = firestore().collection('users').doc(auth().currentUser?.uid).collection('offlineMaps').doc(newMap.uuid)
          const newFirebaseMap: FirebaseMap = {
            name: newMap.name,
            styleURL: newMap.styleURL,
            bounds: {
              neLng: newMap.bounds[0][0],
              neLat: newMap.bounds[0][1],
              swLng: newMap.bounds[1][0],
              swLat: newMap.bounds[1][1],
            }
          }
          if (newMap.minZoom) {
            newFirebaseMap.minZoom = newMap.minZoom
          }
          if (newMap.maxZoom) {
            newFirebaseMap.maxZoom = newMap.maxZoom
          }
          await firebaseRef.set(newFirebaseMap)
        }
      }
      // Find newly removed map(s) and remove them from firebase
      const removed = oldValue.filter((map) => !newValue.map((_) => _.uuid).includes(map.uuid));
      if (removed.length > 0) {
        for (const removedMap of removed) {
          await firestore().collection('users').doc(auth().currentUser?.uid).collection('offlineMaps').doc(removedMap.uuid).delete()
        }
      }

      // Store the map(s) in local storage
      storeData(key, JSON.stringify(newValue));
    }
    
    firebaseUpdate();
  });
};

const offlineMapsState = atom({
  key: 'offlineMaps',
  default: [] as Array<OfflineMap>,
  effects_UNSTABLE: [firebaseAndLocalPersistence("offlineMaps")],
});

export {
  OfflineMap,
  offlineMapsState
}