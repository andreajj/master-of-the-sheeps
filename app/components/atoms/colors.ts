import {  atom } from 'recoil';
import { persistenceEffect } from './utils/persistence';

interface ColorsType {
  colorsList: Array<ColorType>,
  pickerValue: string,
  currentlyEditing: string
}

interface ColorType {
    uuid: string,
    colorValue: string
}

const colorsState = atom({
  key: 'colorsState',
  default: {
    colorsList: [],
    pickerValue: '#ffffff',
    currentlyEditing: ''
  } as ColorsType,
  effects_UNSTABLE: [persistenceEffect("colorsState")],
});

export {
    ColorType,
    ColorsType,
    colorsState
}