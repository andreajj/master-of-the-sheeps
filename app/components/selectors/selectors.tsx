import { selector } from "recoil";
import { currentTrackingState } from "../atoms";

const locationHistoryLonLat = selector({
  key: "locationHistoryLonLat",
  get: ({ get }) => {
    const trackingState = get(currentTrackingState);
    const locations = trackingState.locations;

    return locations;
  },
});

export { locationHistoryLonLat };
