import React, {useState, useEffect} from 'react'
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native'
import { Colors, Text } from 'react-native-paper';
import { useRecoilState } from "recoil";
import { colorsState } from '../../atoms/colors';

export interface ColorRowProps {
  color: string,
  index: number,
  id: string,
  edit: (uuid: string) => void,
  item: any,
  drag: any,
  isActive: boolean,
  isEditing: boolean,
}


const ColorRow: React.FC<ColorRowProps> = ({color, index, edit, id, drag, isEditing}) => {
  const [stateColors, setStateColors] = useRecoilState(colorsState)
  const colorBeingEdited = stateColors.currentlyEditing

  
  const setEditing = () => {
    if(!isEditing && colorBeingEdited === '') {
      edit(id)
    } else if (isEditing && colorBeingEdited === id) {
      edit('')
    }
  }
  

   return (
    <View 
    style={[styles.row, isEditing ? styles.editedRow 
      : null, index % 2 
      ? { backgroundColor: Colors.grey400 } 
      : { backgroundColor: Colors.grey300 } ]}>
      <TouchableOpacity 
      disabled={colorBeingEdited !== '' && colorBeingEdited !== id} 
      onPress={() => setEditing()} 
      onLongPress={!isEditing ? drag : () => setEditing() } 
      style={styles.touchable} >
        <View 
        style={colorBeingEdited !== '' && colorBeingEdited !== id 
        ? [styles.box, {backgroundColor: color}, styles.disabled]
        : [styles.box, {backgroundColor: color}] } 
        >
          <Text style={[styles.rowText, {flex: 1, textAlign: "center"}]}>{isEditing && 'Redigerer '}{index===0 ? 'Hovedgård' : `Gård #${index+1}`}</Text>
          {isEditing 
            ? <Text style={styles.rowText}>Trykk her for å avbryte</Text> 
            : <Text style={{textAlign: "center", fontSize: 13.5}}>Trykk for å redigere. Hold og dra for å endre rekkefølgen</Text>
          }
        </View>
      </TouchableOpacity>
    </View>
   )
};

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    justifyContent: "center",
    flex: 1,
    padding: 5,
    alignItems: 'center',
    width: "100%"
  },
  editedRow: {
    borderStyle: 'dashed',
    borderColor: 'red',
    borderWidth: 2,
    borderRadius: 1,
  },
  box: {
    width: "100%",
    height: 60,
    padding: 5
  },
  rowText: {
    alignSelf: 'center',
    color: 'white',
    textShadowColor: 'black',
    textShadowRadius: 10,
    textShadowOffset: {height: 1, width: -1}
  },
  touchable: {
    width: "100%",
  },
  disabled: {
    opacity: 0.5,
  }
});

export default ColorRow;
