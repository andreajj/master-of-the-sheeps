import React, {useState, useEffect} from 'react';
import {Platform, StyleSheet} from 'react-native'
import { Avatar, Banner} from 'react-native-paper';
import { useNavigation } from "@react-navigation/native";
import { RouteNames } from '../../Navigation';

interface Props {
  visible: boolean,
}

const ColorBanner: React.FC<Props> = ({visible}) => {
    const navigation = useNavigation();
    const COLOR_MESSAGE = 'Du har ingen lagrede gårder.\nVelg hvilke gårder som er relevante for dine tilsyn.'

    return (
        <Banner
        visible={visible}
        style={styles.banner}
        actions={[{
            label: 'Ta meg til gårdsoversikten',
            onPress: () => navigation.navigate(RouteNames.DrawerFarmsOverview)
        }]}
        icon={() => (
            <Avatar.Icon
                icon="format-paint"
            />
        )}>
            {COLOR_MESSAGE}
        </Banner>
    )
};

export default ColorBanner

const styles = StyleSheet.create({
  banner: {
    zIndex: 5,
    elevation: 5
  }
});