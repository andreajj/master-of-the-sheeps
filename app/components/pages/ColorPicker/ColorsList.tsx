import React from "react";
import { View, StyleSheet} from "react-native";
import { Colors } from "react-native-paper";
import { useRecoilState } from "recoil";
import ColorRow from './ColorRow';
import { colorsState } from '../../atoms/colors';
import DraggableFlatList from 'react-native-draggable-flatlist'
interface Props {
  edit: (uuid: string) => void
}


const ColorsList: React.FC<Props> = ({edit}) => {
  const [stateColors, setStateColors] = useRecoilState(colorsState)

  const renderItem = ({item, drag, isActive, index})  => {
    return ( 
      <ColorRow 
        item={item} 
        drag={drag} 
        isActive={isActive} 
        edit={edit} 
        color={item.colorValue} 
        id={item.uuid} 
        index={index}
        isEditing={stateColors.currentlyEditing === item.uuid}
        />
      )
    }

  return (
    <View  style={styles.colorsListContainer}>
        <DraggableFlatList
          data={stateColors.colorsList}
          renderItem={renderItem}
          autoscrollThreshold={100}
          keyExtractor={(item) => item.uuid}
          showsVerticalScrollIndicator={false}
          onDragEnd={({data}) => setStateColors((cur) => {
            return {
              ...cur,
              colorsList: data
            }
        }) }
        
        />
      
     
    
    </View>
  );
};

const styles = StyleSheet.create({
  colorsListContainer: {
    width: '100%',
    flex: 1,
    borderRadius: 1,
    borderStyle: 'solid',
    borderColor: Colors.black,
    borderWidth: 1,
    backgroundColor: Colors.grey300,
  },
  helperText: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    padding: 5,
  },
});

export default ColorsList;
