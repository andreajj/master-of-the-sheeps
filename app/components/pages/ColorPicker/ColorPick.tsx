import React from 'react'
import { View, StyleSheet } from 'react-native'
import { colorsState, ColorType } from '../../atoms/colors';
import { useRecoilState } from "recoil";
import ColorPicker from 'react-native-wheel-color-picker'
import uuid from 'react-native-uuid';

interface Props {
	currentColor: string,
}

const ColorPick: React.FC<Props> = ({currentColor}) => {
	const [stateColors, setStateColors] = useRecoilState(colorsState)
    let pickRef: ColorPicker | null;

	const changePickerState = (color: string) => {
		setStateColors({
			...stateColors,
			pickerValue: color
		})
	}

    return (
		<View style={styles.pickerContainer}>
			<ColorPicker
				ref={r => { pickRef = r}}
				color={currentColor}
				swatchesOnly={false}
				onColorChange={() => {}}
				//@ts-ignore
				onColorChangeComplete={color => changePickerState(color)}
				thumbSize={50}
				autoResetSlider={false}
				shadeSliderThumb={false}
				shadeWheelThumb={false}
				sliderSize={0}
				noSnap={true}
				row={false}
				swatchesLast={false}
				swatches={false}
				discrete={false}
				/>
		</View>
    )
}

const styles = StyleSheet.create({
  pickerContainer: {
	flex: 1,
	overflow: 'hidden'
  }
});

export default ColorPick;
