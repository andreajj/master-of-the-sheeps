import React from "react";
import { View, StyleSheet, SafeAreaView } from "react-native";
import { Colors, Button, Text } from "react-native-paper";
import { useRecoilState } from "recoil";
import { colorsState, ColorType } from '../../atoms/colors';
import ColorsList from './ColorsList';
import ColorPick from './ColorPick'
import uuid from 'react-native-uuid';

const ColorsOverview = () => {
  const [stateColors, setStateColors] = useRecoilState(colorsState)

  // don't add colors that already exist
  const checkDuplicate = (color: string) => {
    const allColors = stateColors.colorsList
    for (let i = 0; i < allColors.length; i++) {
      const currentColor = allColors[i];
      if(currentColor.colorValue === color) {
        return true
      }
    }
    return false
  }

  const addColor = () => {
    if(checkDuplicate(stateColors.pickerValue)) return null;
    if(stateColors.colorsList.length >= 10) return null

    const UUID = uuid.v4().toString()
    const colorToAdd: ColorType = { uuid: UUID, colorValue: stateColors.pickerValue}
    setStateColors({
      ...stateColors,
      colorsList: [
        ...stateColors.colorsList,
        colorToAdd
      ],
    })
  }

  const deleteColor = (deletedUuid: string) => {
    if(deletedUuid === '') return;
    setStateColors((cur) => {
      const newColors = cur.colorsList.filter((s) => s.uuid !== deletedUuid)
      return {
        ...cur,
        colorsList: newColors,
        currentlyEditing: ''
      }
  })}

  const changeColor = (uuid: string, newColor: string) => {
    // don't allow duplicates
    if(checkDuplicate(newColor)) {
      return setStateColors((cur) => {
        return {
          ...cur,
          currentlyEditing: ''
        }
      })
    }

    const updatedColors = stateColors.colorsList.map(color => {
      if (color.uuid === uuid) {
        return {...color, colorValue: newColor}
      }
      return color
    })
    setStateColors((cur) => {
      return {
        ...cur,
        colorsList: updatedColors,
        currentlyEditing: ''
      }
    })
  }
    
  

  const setEditing = (uuid: string) => {
    setStateColors((cur) => {
    if(uuid === '') {
      return {
        ...cur,
        currentlyEditing: uuid,
      }
    } else {
      const pickerValueToSet = cur.colorsList?.filter((s) => s.uuid === uuid )[0].colorValue
      return {
        ...cur,
        currentlyEditing: uuid,
        pickerValue: pickerValueToSet
      }
    }
    })
  }



  return (
    <SafeAreaView style={styles.container}>
      <ColorPick currentColor={stateColors.pickerValue} />
      <View style={styles.buttonContainer}>
      <Button 
        onPress={stateColors.currentlyEditing === '' ? addColor : () => changeColor(stateColors.currentlyEditing, stateColors.pickerValue )} 
        style={styles.button} 
        labelStyle={styles.buttonLabel} 
        mode="contained" >
          {stateColors.currentlyEditing === '' ? "Legg til farge" : "Bekreft redigering"}
      </Button>

      {stateColors.currentlyEditing !== '' && <Button 
        onPress={() => deleteColor(stateColors.currentlyEditing)} 
        style={styles.deleteButton} 
        labelStyle={styles.buttonLabel} 
        mode="contained" >
          Slett
      </Button>}

      </View>
      <View style={styles.helperText}>
        <Text>Antall farger: {stateColors.colorsList.length}</Text>
      </View>
      <ColorsList edit={setEditing} />
    </SafeAreaView>
    )
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    padding: 5,
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  },
  button: {
    width: '80%',
    borderRadius: 0,
    marginBottom: 20,
  },
  deleteButton: {
    width: '80%',
    borderRadius: 0,
    marginBottom: 20,
    backgroundColor: Colors.red500,
    color: Colors.white,
  },
  buttonLabel: {
    color: Colors.white,
  },
  buttonContainer: {
    alignItems: 'center',
    width: "100%",
    height: 120
  },
  cancelButton: {
    width: '80%',
    borderRadius: 0,
    marginBottom: 20,
    backgroundColor: Colors.grey500,
    color: Colors.white,
  },
  colorsList: {
    width: "80%"
  },
  helperText: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    padding: 5,
  },
});

export default ColorsOverview;
