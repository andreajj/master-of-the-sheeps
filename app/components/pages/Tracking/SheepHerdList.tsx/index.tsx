import { useNavigation } from "@react-navigation/native";
import React from "react"
import { View, StyleSheet } from "react-native"
import { Button, Colors } from "react-native-paper";
import { useRecoilState } from "recoil";
import { currentTrackingState } from "../../../atoms";
import { Tie } from "../types";
import { SheepHerd } from "./SheepHerd"

const SheepHerdList: React.FC = () => {
  const navigation = useNavigation();
  const [currentTracking, setCurrentTracking] = useRecoilState(currentTrackingState);

  const onDeleteSheep = (tie: Tie) => {
    setCurrentTracking((cur) => {
      const newIntermediateSheepHerd = cur.intermediateSheepHerd ? {
        ...cur.intermediateSheepHerd,
        sheeps: {
          ...cur.intermediateSheepHerd.sheeps,
          [tie]: cur.intermediateSheepHerd.sheeps[tie] - 1
        },
      } : undefined;

      return {
        ...cur,
        intermediateSheepHerd: newIntermediateSheepHerd
      }
    })
  }

  const onDeleteLamb = (index: number) => {
    setCurrentTracking((cur) => {
      const newIntermediateSheepHerd = cur.intermediateSheepHerd ? {
        ...cur.intermediateSheepHerd,
        lambs: cur.intermediateSheepHerd?.lambs - 1,
      } : undefined;

      return {
        ...cur,
        intermediateSheepHerd: newIntermediateSheepHerd
      }
    })
  }

  const onBack = () => {
    navigation.goBack();
  }
  
  return (
    <View style={styles.container}>
      {currentTracking.intermediateSheepHerd && (
        <SheepHerd sheepHerd={currentTracking.intermediateSheepHerd} removeSheep={onDeleteSheep} removeLamb={onDeleteLamb} />
      )}
      <Button labelStyle={styles.buttonLabel} style={styles.button} mode="contained" onPress={onBack}>
        Tilbake
      </Button>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    padding: 15
  },
  button: {
    width: '100%',
    borderRadius: 0,
    marginTop: 10,
    backgroundColor: Colors.orange500,
    padding: 10,
  },
  buttonLabel: {
    color: Colors.white,
  },
})

export default SheepHerdList;