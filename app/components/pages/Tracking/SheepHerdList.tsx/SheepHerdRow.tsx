import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Colors } from 'react-native-paper';
import { EarTag, Tie } from '../types';

interface Props {
  tie?: Tie,
  index: number,
  startsEven?: boolean,
}

const getTieColor = (tie: Tie) => {
  switch(tie) {
    case Tie.Blue: {
      return tieColors.blue
    }
    case Tie.Red: {
      return tieColors.red
    }
    case Tie.None: {
      return tieColors.none
    }
    case Tie.Green: {
      return tieColors.green
    }
    case Tie.Yellow: {
      return tieColors.yellow
    }
  }
}

const SheepHerdRow: React.FC<Props> = ({ tie, index, startsEven = true }) => { 
  const color1 = startsEven ? Colors.grey400 : Colors.grey300;
  const color2 = startsEven ? Colors.grey300 : Colors.grey400;

  return (
    <View style={[styles.row, index % 2 ? { backgroundColor: color1 } : { backgroundColor: color2 } ]}>
      <Image style={styles.image} source={tie !== undefined ? require('../../../../assets/sheep.png') : require('../../../../assets/lamb.png')} />
      <View style={[styles.box, getTieColor(tie ?? Tie.None)]} />
    </View>
  )
}

const tieColors = StyleSheet.create({
  none: {
    backgroundColor: "transparent"
  },
  yellow: {
    backgroundColor: Colors.yellow500
  },
  blue: {
    backgroundColor: Colors.blue500
  },
  red: {
    backgroundColor: Colors.red500
  },
  green: {
    backgroundColor: Colors.green500
  }
})

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    padding: 5,
  },
  image: {
    height: 50,
    width: 50
  },
  circle: {
    height: 50,
    width: 50,
    borderRadius: 25
  },
  box: {
    width: 50,
    height: 50
  }
});

export default SheepHerdRow;