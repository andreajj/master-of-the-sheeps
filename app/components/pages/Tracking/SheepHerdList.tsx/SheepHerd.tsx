import React, { useEffect, useState } from 'react';
import { View, StyleSheet, ScrollView, Image, Text, Animated, TouchableOpacity } from 'react-native';
import { Colors } from 'react-native-paper';
import { SheepHerdType } from '../../../atoms';
import { EarTag, Tie } from '../types';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { RectButton } from 'react-native-gesture-handler';
import SheepHerdRow from './SheepHerdRow';
import uuid from 'react-native-uuid';

interface Props {
  sheepHerd: SheepHerdType,
  removeSheep: (tie: Tie) => void,
  removeLamb: () => void,
}

const SheepHerd: React.FC<Props> = ({ sheepHerd, removeSheep, removeLamb }) => {
  const sheepRefs: Array<Swipeable | null> = [];
  const lambRefs: Array<Swipeable | null> = [];

  const deleteSheepOrLamb = (isSheep: boolean, tie?: Tie) => {
    if (isSheep && tie) {
      removeSheep(tie)
    } else {
      removeLamb()
    }
  }


  const swipe = (progress: Animated.AnimatedInterpolation, dragX: Animated.AnimatedInterpolation, isSheep: boolean, tie?: Tie) => (
    <RectButton style={styles.rectButton} onPress={() => deleteSheepOrLamb(isSheep, tie)}>
      <Text
        style={[
          {
            color: Colors.white,
            textAlign: "center"
          }
        ]}>
        Slett
      </Text>
    </RectButton>
  );

  const lambRows = [];
  for (let i = 0; i < sheepHerd.lambs; i++) {
    /* 
      We set a new key each time with the uuid as each object has nothing unique value
      this is needed for the list to simply not just remove the last object in the list.
      This is however not very performant and makes open menues/swipeables close
      but it is the best option as of now.
    */
    lambRows.push(
      <Swipeable key={uuid.v4().toString()} ref={(ref) => lambRefs.push(ref)} containerStyle={{width: "100%"}} renderRightActions={(progress, dragX) => swipe(progress, dragX, i, false)}>
        <TouchableOpacity activeOpacity={1} onPress={() => lambRefs[i]?.openRight()}>
          <SheepHerdRow index={i} startsEven={Object.values(sheepHerd.sheeps).reduce((prev, next) => prev + next) % 2 == 0} />
        </TouchableOpacity>
      </Swipeable>
    )
  }
  const sheepRows = [];
  for (const [_tie, number] of Object.entries(sheepHerd.sheeps)){
    const tie = _tie as Tie;
    for (let i = 0; i < number; i++) {
      sheepRows.push(
        <Swipeable ref={(ref) => sheepRefs.push(ref)} containerStyle={{width: "100%"}} renderRightActions={(progress, dragX) => swipe(progress, dragX, i, true)}>
          <TouchableOpacity activeOpacity={1} onPress={() => sheepRefs[i]?.openRight()}>
            <SheepHerdRow tie={tie} index={i} />
          </TouchableOpacity>
        </Swipeable>
      )
    }
  }

  return (
    <View style={styles.herdView}>
      <View style={styles.helperText}>
        <Text>Sau/lam</Text>
        <Text>Slips</Text>
      </View>
      <ScrollView contentContainerStyle={styles.containerHerdView} style={styles.herdScrollView}>
        {sheepRows}
        {lambRows}
      </ScrollView>
      <View style={styles.counterText}>
        <Text>Sau: {Object.values(sheepHerd.sheeps).reduce((prev, next) => prev + next)}</Text>
        <Text>Lam: {sheepHerd.lambs}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  helperText: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    padding: 5,
  },
  counterText: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    padding: 5,
  },
  containerHerdView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  herdScrollView: {
    borderColor: Colors.black,
    borderWidth: 1,
    backgroundColor: Colors.grey300,
  },
  herdView: {
    width: '100%',
    flex: 1,
  },
  rectButton: {
    width: 100,
    paddingVertical: 10,
    paddingHorizontal: 20,
    alignContent: "center",
    justifyContent: "center",
    flexDirection: 'column',
    backgroundColor: Colors.red500,
  },
});

export {
  SheepHerd
}