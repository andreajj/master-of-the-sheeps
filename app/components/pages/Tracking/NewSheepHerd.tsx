import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, Alert } from 'react-native';
import { Colors, Button } from 'react-native-paper';
import { useRecoilState, useRecoilValue } from 'recoil';
import { currentTrackingState, POI } from '../../atoms';
import { useNavigation, RouteProp } from '@react-navigation/native';
import { Location, Tie } from './types';
import uuid from 'react-native-uuid';
import { sayMessage } from '../../utils/TextToSpeech';
import getLambFromTie from '../../utils/getLambFromTie';
import EarTagSelector from './EarTagSelector';
import getUserLocation from '../../utils/getUserLocation';
import { RouteNames } from '../../Navigation';
import InputSpinner from 'react-native-input-spinner';

interface Props {
  route: RouteProp<{params: { location: Location, sheepHerdUUID?: string } }, 'params'>
}

const NewSheepHerd: React.FC<Props> = ({ route: { params: { location, sheepHerdUUID }}}) => {
  const navigation = useNavigation();
  const [currentTracking, setCurrentTracking] = useRecoilState(currentTrackingState);
  const [sheepUuid, setSheepUuid] = useState(sheepHerdUUID ?? uuid.v4().toString());
  const currentSheepHerd = currentTracking.sheepHerds.find((s) => s.uuid == sheepHerdUUID);
  const [numberOfWoundedAnimals, setNumberOfWoundedAnimals] = useState(currentSheepHerd?.woundedAnimals ?? 0);

  useEffect(() => {
    const asyncInit = async () => {
      const userLocation: Location = await getUserLocation();

      // Create a new sheepheard at startup so that newLam and newSheep have something to update
      setCurrentTracking({
        ...currentTracking,
        locations: [...currentTracking.locations, userLocation],
        intermediateSheepHerd: currentSheepHerd ? {
          ...currentSheepHerd,
          upsertLocations: [...currentSheepHerd.upsertLocations, userLocation]
        } : {
          sheeps: {
            [Tie.Blue]: 0,
            [Tie.Green]: 0,
            [Tie.None]: 0,
            [Tie.Red]: 0,
            [Tie.Yellow]: 0
          },
          lambs: 0,
          woundedAnimals: 0,
          location: location,
          upsertLocations: [userLocation],
          farms: [],
          images: [],
          uuid: sheepUuid
        }
      })
    }

    asyncInit();

    // When the screen/modal closes, set isPlacingPoi to undefined to remove the crosshair as we are done placing a POI and 
    // clear intermediateSheepHerd for the next useage
    return () => {
      setCurrentTracking((cur) => {
        return {
          ...cur,
          isPlacingPOI: undefined,
          intermediateSheepHerd: undefined
        }
      })
    }
  }, [])

  const onSubmitContinue = () => {
    // Save the state by adding the intermediate sheep herd state to the sheepHerds state
    setCurrentTracking((cur) => {
      const filteredHerds = cur.sheepHerds.filter((s) => s.uuid !== sheepHerdUUID)
      const sheepHerds = cur.intermediateSheepHerd 
        ? [...filteredHerds, {
          ...cur.intermediateSheepHerd,
          woundedAnimals: numberOfWoundedAnimals
        }] 
        : filteredHerds
      return {
        ...cur,
        sheepHerds: sheepHerds,
      }
    })
    navigation.goBack()
  }

  const onSubmit = () => {
    let correctNumberOfLambs = 0;
    const actualNumberOflambs = currentTracking.intermediateSheepHerd?.lambs ?? 0;
    for (const color in Tie) {
      const tie = color as Tie;
      if (currentTracking.intermediateSheepHerd?.sheeps) {
        correctNumberOfLambs += (getLambFromTie(tie) * currentTracking.intermediateSheepHerd?.sheeps[tie]);
      }
    }

    if (correctNumberOfLambs !== actualNumberOflambs) {
      Alert.alert(
        "Feil antall lam",
        `Utifra fargen på slipsene til søyene skal det være ${correctNumberOfLambs} lam, men du registrerte bare ${actualNumberOflambs} lam`,
        [
          { text: "OK", onPress: onSubmitContinue },
          { text: "Avbryt" }
        ]
      );
    } else {
      onSubmitContinue()
    }
  }

  const onShowSheepsLambs = () => {
    navigation.navigate(RouteNames.TrackingSheepAndLambListScreen);
  }

  const onShowHerdImages = () => {
    navigation.navigate(RouteNames.TrackingSheepHerdImagesScreen);
  }

  const onDelete = () => {
    setCurrentTracking((cur) => {
      const filteredHerds = cur.sheepHerds.filter((s) => s.uuid !== sheepHerdUUID)
      return {
        ...cur,
        sheepHerds: filteredHerds,
      }
    })
    navigation.goBack()
  }

  const onNewSheep = () => {
    sayMessage("Sau");
    navigation.navigate(RouteNames.TrackingSheepScreen, { sheepHeardUuid: sheepUuid });
  }

  const onNewLamb = () => {
    sayMessage("Lam");

    if (currentTracking.intermediateSheepHerd === undefined) {
      Alert.alert(
        "Kritisk feil!",
        "Sauflokken finnes ikke, sauen ble ikke lagt til.",
        [
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
      return
    }

    const updatedHerd = {
      ...currentTracking.intermediateSheepHerd,
      lambs: currentTracking.intermediateSheepHerd?.lambs + 1,
    }

    setCurrentTracking((cur) => ({
      ...cur,
      intermediateSheepHerd: updatedHerd
    }))
  }
  
  const onNewWoundedAnimal: (number: number) => void = (number) => {
    setNumberOfWoundedAnimals(number)
  }

    return (
      <View style={styles.container}>
        <View style={styles.buttons}>
          <Button
              labelStyle={styles.buttonLabel} 
              style={[styles.button, styles.buttonLeft]} 
              contentStyle={styles.buttonContent}
              mode="contained" 
              onPress={onNewSheep}
            >
              Ny Sau
          </Button>
          <Button
              labelStyle={styles.buttonLabel} 
              style={[styles.button, styles.buttonRight]} 
              contentStyle={styles.buttonContent}
              mode="contained" 
              onPress={onNewLamb}
            >
              Nytt Lam
          </Button>
        </View>
        <EarTagSelector />
        <View style={styles.spinnerContainer}>
          <Text>Antall skadde dyr</Text>
          <InputSpinner
            value={numberOfWoundedAnimals}
            rounded={false}
            showBorder={true}
            style={styles.spinner}
            onChange={onNewWoundedAnimal}
          />
        </View>
        <View style={styles.twoButtonContainer}>
          <Button 
            labelStyle={styles.buttonLabel} 
            style={styles.imageButton} 
            mode="contained"
            onPress={onShowHerdImages}
          >
            Legg til bilder
          </Button>
          <Button 
            labelStyle={styles.buttonLabel} 
            style={styles.listButton} 
            mode="contained" 
            onPress={onShowSheepsLambs}
          >
            Vis sau og lam
          </Button>
        </View>
        <Button 
          labelStyle={styles.buttonLabel} 
          style={styles.finishButton} 
          mode="contained" 
          onPress={onSubmit}
        >
          Ferdig
        </Button>
        {sheepHerdUUID && (
          <Button 
            labelStyle={styles.buttonLabel} 
            style={styles.deleteButton} 
            mode="contained" 
            onPress={onDelete}
          >
            Slett
        </Button>
        )}
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    padding: 15,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttons: {
    width: "100%",
    flex: 1,
    flexDirection: "row",
    marginBottom: 20,
  },
  button: {
    borderRadius: 0,
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  buttonContent: {
    height: "100%",
    padding: 10,
  },
  buttonLeft: {
    marginRight: 10
  },
  buttonRight : {
    marginLeft: 10
  },
  buttonLabel: {
    color: Colors.white,
  },
  spinnerContainer: {
    width: "100%", 
    marginTop: 5,
  },
  spinner: {
		marginVertical: 10,
	},
  deleteButton: {
    marginTop: 10,
    backgroundColor: Colors.red500,
    width: '100%',
    borderRadius: 0,
  },
  twoButtonContainer: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
  },
  listButton: {
    width: '100%',
    borderRadius: 0,
    marginLeft: 2.5,
    flex: 1,
    backgroundColor: Colors.orange500
  },
  imageButton: {
    width: '100%',
    borderRadius: 0,
    marginRight: 2.5,
    flex: 1,
    backgroundColor: Colors.green500
  },
  finishButton: {
    width: '100%',
    borderRadius: 0,
    marginTop: 10,
  },
  containerHerdView: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  herdView: {
    borderColor: Colors.black,
    borderWidth: 1,
    backgroundColor: Colors.grey300,
    width: '100%',
  },
})

export default NewSheepHerd;