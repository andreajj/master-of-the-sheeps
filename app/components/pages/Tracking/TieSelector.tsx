import React, { useEffect } from 'react';
import { View, StyleSheet, Pressable, Text} from 'react-native';
import { Colors, Title } from 'react-native-paper';
import { Tie } from './types';
import { sayMessage } from '../../utils/TextToSpeech';

interface Props {
  tie: Tie,
  setTie: (tie: Tie) => void,
}

const TieSelector: React.FC<Props> = ({ tie, setTie }) => {
  const onPress = () => {
    switch(tie) {
      case Tie.None: {
        setTie(Tie.Yellow)
        return;
      }
      case Tie.Yellow: {
        setTie(Tie.Blue)
        return;
      }
      case Tie.Blue: {
        setTie(Tie.Green)
        return;
      }
      case Tie.Green: {
        setTie(Tie.Red)
        return;
      }
      case Tie.Red: {
        setTie(Tie.None)
        return;
      }
    }
  }

  useEffect(() => {
    switch(tie) {
      case Tie.None: {
        sayMessage("Ingen slips")
        return;
      }
      case Tie.Yellow: {
        sayMessage("Gul")
        return;
      }
      case Tie.Blue: {
        sayMessage("Blå")
        return;
      }
      case Tie.Green: {
        sayMessage("Grønn")
        return;
      }
      case Tie.Red: {
        sayMessage("Rød")
        return;
      }
    }
  }, [tie])
  
  return (
    <View style={styles.tie}>
      <Title>Slips</Title>
      <View style = {styles.lineStyle} />
      <View style={styles.selector}>
        {tie == Tie.None && (
          <View style={[styles.none, styles.box]}>
            <Pressable style={styles.pressable} onPress={onPress}>
              <Title style={styles.text} >Ikke noe slips</Title>
              <Text style={styles.text}>Tap for å bytte til Gul</Text>
            </Pressable>
          </View>
        )}
        {tie == Tie.Yellow && (
          <View style={[styles.yellow, styles.box]}>
            <Pressable style={styles.pressable} onPress={onPress}>
              <Title style={styles.text} >Gult</Title>
              <Text style={styles.text}>Tap for å bytte til Blå</Text>
            </Pressable>
          </View>
        )}
        {tie == Tie.Blue && (
          <View style={[styles.blue, styles.box]}>
            <Pressable style={styles.pressable} onPress={onPress}>
              <Title style={styles.text} >Blått</Title>
              <Text style={styles.text}>Tap for å bytte til Grønn</Text>
            </Pressable>
          </View>
        )}
        {tie == Tie.Green && (
          <View style={[styles.green, styles.box]}>
            <Pressable style={styles.pressable} onPress={onPress}>
              <Title style={styles.text} >Grønt</Title>
              <Text style={styles.text}>Tap for å bytte til Rød</Text>
            </Pressable>
          </View>
        )}
        {tie == Tie.Red && (
          <View style={[styles.red, styles.box]}>
            <Pressable style={styles.pressable} onPress={onPress}>
              <Title style={styles.text} >Rødt</Title>
              <Text style={styles.text}>Tap for å bytte til Ingen slips</Text>
            </Pressable>
          </View>
        )}
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  pressable: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  text: {
    textAlign: "center"
  },
  tie: {
    flex: 1
  },
  lineStyle:{
    borderWidth: 0.5,
    borderColor: Colors.grey400,
    marginBottom: 10,
  },
  selector: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  box: {
    height: "100%",
    width: "100%",
    borderWidth: 1
  },
  none: {
    backgroundColor: "transparent"
  },
  yellow: {
    backgroundColor: Colors.yellow500
  },
  red: {
    backgroundColor: Colors.red500
  },
  blue: {
    backgroundColor: Colors.blue500
  },
  green: {
    backgroundColor: Colors.green500
  }
})

export default TieSelector;