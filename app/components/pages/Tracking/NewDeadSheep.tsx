import { useNavigation, RouteProp } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Colors, Button, TextInput, Text, Paragraph, Portal, Dialog } from 'react-native-paper';
import { useRecoilState } from 'recoil';
import { currentTrackingState, Image } from '../../atoms';
import { Location } from './types';
import uuid from 'react-native-uuid';
import getUserLocation from '../../utils/getUserLocation';
import ImagePicker, { ImageType } from '../../ImagePicker';
import * as FileSystem from 'expo-file-system';

interface Props {
  route: RouteProp<{params: { location: Location, deadSheepUUID?: string, } }, 'params'>
}

const NewDeadSheep: React.FC<Props> = ({ route: { params: { location, deadSheepUUID }}}) => {
  const navigation = useNavigation();
  
  const [currentTracking, setCurrentTracking] = useRecoilState(currentTrackingState);
  const currentDeadSheep = currentTracking.deadSheep.find((ds) => ds.uuid === deadSheepUUID)
  const [details, setDetails] = useState(currentDeadSheep?.description ?? '');
  const [images, setImages] = useState<Array<ImageType>>([]);
  const [showWarning, setShowWarning] = useState(false)

  useEffect(() => {
    if (currentDeadSheep?.images) {
      setImages(currentDeadSheep.images.map((image) => ({...image})));
    } else {
      setImages([]);
    }
  }, [currentDeadSheep])

  useEffect(() => {
    return () => {
      setCurrentTracking((cur) => ({
        ...cur,
        isPlacingPOI: undefined,
      }))
    }
  }, [])

  const onDelete = () => {
    const newState = currentTracking.deadSheep.filter((p) => p.uuid !== currentDeadSheep?.uuid)
    setCurrentTracking({
      ...currentTracking,
      deadSheep: [...newState]
    })
    navigation.goBack();
  }

  const onSubmit = async () => {
    if (images.length===0 && !showWarning) {
      return setShowWarning(true)
    }

    const userLocation: Location = await getUserLocation();

    const newImages: Array<Image> = images.map((image) => ({base64: image.base64}));

    if (currentDeadSheep) {
      const newState = currentTracking.deadSheep.filter((o) => o.uuid !== currentDeadSheep.uuid);

      setCurrentTracking({
        ...currentTracking,
        locations: [...currentTracking.locations, userLocation],
        deadSheep: [
          ...newState,
          {
            description: details,
            images: newImages,
            location: currentDeadSheep.location,
            upsertLocations: [...currentDeadSheep.upsertLocations, userLocation],
            uuid: currentDeadSheep.uuid
          }
        ]
      })
    } else {
      setCurrentTracking({
        ...currentTracking,
        locations: [...currentTracking.locations, userLocation],
        deadSheep: [
          ...currentTracking.deadSheep,
          {
            description: details,
            images: newImages,
            location: location,
            upsertLocations: [userLocation],
            uuid: uuid.v4().toString()
          }
        ]
      })
    }
    setShowWarning(false);
    navigation.goBack()
  }
  
  const WarningDialog: React.FC = () => {
    return (
      <Portal>
      <Dialog visible={showWarning}>
        <Dialog.Title>Ingen bilder lagt til</Dialog.Title>
        <Dialog.Content>
          <Paragraph>Ved registrering av død sau burde bilder medfølge. Vil du legge til bilder og prøve på nytt?</Paragraph>
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={onSubmit}>Lagre uten bilder</Button>
          <Button onPress={() => setShowWarning(false)}>Ok</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
    );
  };

    return (
      <View style={styles.container}>
        <WarningDialog/>
        <View>
          <View style={styles.picturesPlaceHolder}>
              <ImagePicker images={images} setImages={setImages} />
          </View>
          <Text style={styles.descriptionLabel}>Beskrivelse</Text>
          <TextInput 
                value={details}
                onChangeText={setDetails}
                style={styles.textArea} 
                mode='outlined' 
                multiline={true} 
                numberOfLines={4} 
                maxLength={100}
                right={<TextInput.Affix text="/100" />}
          />
        </View>
        <View>
          <Button icon="plus" mode="contained" onPress={onSubmit}>
            Ferdig
          </Button>
          {deadSheepUUID && (
            <Button icon="minus" mode="contained" style={styles.deleteButton} onPress={onDelete}>
              Slett
            </Button>
          )}
        </View>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingVertical: 50,
    paddingHorizontal: 20,
    justifyContent: "space-between"
  },
  dropDown: {
    width: '100%',
    flex: 1,
    borderRadius: 0,
  },
  textArea: {
    width: '100%',
    maxHeight: 200,
    backgroundColor: Colors.grey300
  },
  deleteButton: {
    marginTop: 10,
    backgroundColor: Colors.red500,
  },
  descriptionLabel: {
  },
  picturesPlaceHolder: {
    height: 200,
      display: 'flex',
      flexDirection: "column",
      padding: 5,
      justifyContent: "center",
      width: "100%",
  }
})

export default NewDeadSheep;