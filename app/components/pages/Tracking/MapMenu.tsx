import React, { useEffect, useState } from "react";
import { View, StyleSheet, Alert } from "react-native";
import { Colors, FAB, Button, Text, Portal, Dialog, Paragraph, TextInput } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import {
  currentTrackingState,
  defaultCurrentTrackingState,
  POI,
  TrackingState,
} from "../../atoms";
import { useRecoilState, useRecoilValue } from "recoil";
import {
  stopBackgroundTracking,
} from "./BackgroundTracker";
import distance from '@turf/distance';
import { point } from '@turf/helpers';
import uuid from 'react-native-uuid';
import { Location, Tie } from '../Tracking/types';
import auth from '@react-native-firebase/auth';
import { batchWrite, batchUpdateAndWrite } from "../../utils/Firebase";
import getUserLocation from '../../utils/getUserLocation';
import { RouteNames } from "../../Navigation";
import { userState } from "../../atoms/userState";

interface Props {
  getCenter: () => Promise<GeoJSON.Position> | undefined;
}

enum SheepButtonStep {
  step1,
  step2
}

const MapMenu: React.FC<Props> = ({ getCenter }) => {
  const navigation = useNavigation();
  const [trackingState, setTrackingState] = useRecoilState(currentTrackingState);
  const [loading, setLoading] = useState(false);
  const user = useRecoilValue(userState);
  const [sheepButtonStep, setSheepButtonStep] = useState(SheepButtonStep.step1)
  const [endTrackingDialogVisible, setEndTrackingDialogVisible] = useState(false);
  const [trackingRouteDescription, setTrackingRouteDescription] = useState("");

  useEffect(() => {
    if (trackingState.routeDescription) {
      setTrackingRouteDescription(trackingState.routeDescription);
    } else {
      setTrackingRouteDescription("");
    }
  }, [trackingState.routeDescription])

  const startTracking = () => {
    navigation.navigate(RouteNames.MainLanding, { 
      screen: RouteNames.DrawerSheepTracking,
      params: {
        screen: RouteNames.TrackingOfflineMapSelector,
        params: {
          isForTracking: true,
        }
     }
    });
  };

  const showDatabaseErrorAlert = () => {
    Alert.alert(
      "Noe gikk galt!",
      "Noe gikk galt under lagring av sporingen",
      [
        { text: "OK" }
      ]
    );
  }

  const writeToFirebase = async (state: TrackingState) => {
    try {
      const writePromise = batchWrite(state);
      if (!user.isOnline) {
        return undefined;
      }
      await writePromise;
      return true;
    } catch(e: unknown) {
      console.log(e);
      showDatabaseErrorAlert();
      return false;
    }
  }

  const updateToFirebase = async (state: TrackingState, trackingUuid: string) => {
    try {
      const updatePromise = batchUpdateAndWrite(state, trackingUuid);
      if (!user.isOnline) {
        return undefined;
      }
      await updatePromise;
      return true;
    } catch(e: unknown) {
      console.log(e);
      showDatabaseErrorAlert();
      return false;
    }
  }

  const startEndTracking = async () => {
    setEndTrackingDialogVisible(true);
  }

  const cancelEndTracking = async () => {
    setLoading(false);
    setEndTrackingDialogVisible(false);
  }

  const finishEndTracking = async () => {
    setLoading(true);
    // Stop background tracking
    stopBackgroundTracking();

    // check if user is signed in before upserting firebase
    if (!auth().currentUser?.uid) {
      console.log("user is not logged in");
      return showDatabaseErrorAlert();
    }

    let successful: boolean | undefined = false;

    const state = {
      ...trackingState,
    }

    if (trackingRouteDescription !== "") {
      state.routeDescription = trackingRouteDescription;
    }

    if (state.firebaseUUID) {
      // If a firebase UUID exists then this is an exisiting tracking to be updated in firebase
      successful = await updateToFirebase(state, state.firebaseUUID);
    } else {
      // If no firebase UUID exists then this is a new tracking to be written to firebase
      successful = await writeToFirebase(state);
    }
    if (successful || successful === undefined) {
      if (successful === undefined) {
        Alert.alert(
          "Du er offline!",
          "Du er offline og sporingen blir dermed lagret lokalt inntil du får nettilkobling igjen. Når du kobler til nettet blir sporingen automatisk synkronisert til skyen.",
          [
            { text: "OK" }
          ]
        )
      }
      setTrackingState({ ...defaultCurrentTrackingState });
    }
    setLoading(false);
    setEndTrackingDialogVisible(false);
  };

  const setSheepHerdPOI = () => {
    setTrackingState({
      ...trackingState,
      isPlacingPOI: POI.SheepHerd,
    });
  };

  const setDeadSheepPOI = () => {
    setTrackingState({
      ...trackingState,
      isPlacingPOI: POI.DeadSheep,
    });
  };

  const setPredatorPOI = () => {
    setTrackingState({
      ...trackingState,
      isPlacingPOI: POI.Predator,
    });
  };

  const setOterPOI = () => {
    setTrackingState({
      ...trackingState,
      isPlacingPOI: POI.Other,
    });
  };

  const cancelPOIPlacement = () => {
    setTrackingState({
      ...trackingState,
      isPlacingPOI: undefined,
    });
  };

  const goToSheepHerdScreen = async () => {
    const possibleSheepLocation = await getCenter();
    const retrievedUserLocation: Location = await getUserLocation();
    let length: number | undefined = undefined
    if (possibleSheepLocation) {
      const sheepLocation = point(possibleSheepLocation);
      const userLocation = point(retrievedUserLocation);
      length = distance(sheepLocation, userLocation, { units: 'meters' });
    }

    if (length && length >= 30) {
      setTrackingState({
        ...trackingState,
        locations: [...trackingState.locations, retrievedUserLocation],
        sheepHerds: [
          ...trackingState.sheepHerds,
          {
            sheeps: {
              [Tie.Blue]: 0,
              [Tie.Green]: 0,
              [Tie.None]: 0,
              [Tie.Red]: 0,
              [Tie.Yellow]: 0
            },
            lambs: 0,
            location: (possibleSheepLocation as unknown) as Location,
            upsertLocations: [retrievedUserLocation],
            farms: [],
            uuid: uuid.v4().toString(),
            woundedAnimals: 0,
            images: [],
          }
        ],
        isPlacingPOI: undefined
      })
    } else {
      navigation.navigate(RouteNames.TrackingSheepHerdScreen, {
        location: possibleSheepLocation,
      })
    }
  }

  const goToPredatorScreen = async () => {
    navigation.navigate(RouteNames.TrackingPredatorScreen, {
      location: await getCenter(),
    })
  } 

  const goToOtherScreen = async () => {
    navigation.navigate(RouteNames.TrackingOtherScreen, {
      location: await getCenter(),
    })
  }

  const goToDeadSheepScreen = async () => {
    navigation.navigate(RouteNames.TrackingDeadSheepScreen, {
      location: await getCenter(),
    })
  }

  const renderPlacingButton = () => {
    switch (trackingState.isPlacingPOI) {
      case POI.SheepHerd: {
        return (
          <Button
            icon="plus"
            labelStyle={styles.placementButtonLabel}
            uppercase={false}
            mode="contained"
            onPress={goToSheepHerdScreen}
          >
            Plasser saueflokk
          </Button>
        );
      }
      case POI.Predator: {
        return (
          <Button
            icon="plus"
            labelStyle={styles.placementButtonLabel}
            uppercase={false}
            mode="contained"
            onPress={goToPredatorScreen}
          >
            Plasser rovdyr
          </Button>
        );
      }
      case POI.Other: {
        return (
          <Button
            icon="plus"
            labelStyle={styles.placementButtonLabel}
            uppercase={false}
            mode="contained"
            onPress={goToOtherScreen}
          >
            Plasser annet
          </Button>
        );
      }
      case POI.DeadSheep: {
        return (
          <Button
            icon="plus"
            labelStyle={styles.placementButtonLabel}
            uppercase={false}
            mode="contained"
            onPress={goToDeadSheepScreen}
          >
            Plasser død sau
          </Button>
        );
      }
      default:
        return null;
    }
  };


  const renderOpenMenu = () => (
    <>
      <View style={styles.firstRow}>
        <FAB
          icon="sheep"
          style={styles.sheepButton}
          onPress={() => setSheepButtonStep(SheepButtonStep.step2)}
          animated={false}
        />
      </View>
      <View style={styles.secondRow}>
        <FAB
          icon="paw"
          style={styles.predatorButton}
          onPress={setPredatorPOI}
          animated={false}
        />
        <FAB
          icon="dots-horizontal"
          style={styles.otherButton}
          onPress={setOterPOI}
          animated={false}
        />
      </View>
      <View style={styles.firstRow}>
        <FAB
          icon="stop"
          loading={loading}
          color={Colors.white}
          disabled={loading}
          style={styles.endButton}
          onPress={startEndTracking}
        />
      </View>
    </>
  );

  const renderSheepButtons = () => (
    <>
      <View style={styles.secondRow}>
        <FAB
          icon="sheep"
          // icon={require("../../../assets/sheepherd.png")} // custom icon - looks too small?
          style={styles.sheepHerdButton}
          onPress={setSheepHerdPOI}
          animated={false}
        />
        <FAB
          icon="skull-crossbones"
          style={styles.deadSheepButton}
          onPress={setDeadSheepPOI}
          animated={false}
        />
      </View>
      <View style={styles.firstRow}>
        <FAB
          icon="keyboard-backspace"
          color={Colors.black}
          style={styles.backButton}
          onPress={() => setSheepButtonStep(SheepButtonStep.step1)}
        />
      </View>
    </>
  );

  return (
    <View style={styles.fixedView}>
      <Portal>
        <Dialog visible={endTrackingDialogVisible} onDismiss={cancelEndTracking}>
          <Dialog.Title>Lagre sporing</Dialog.Title>
          <Dialog.Content>
            <Paragraph>Skriv en kort beskrivelse av ruten du har tatt under sporingen.</Paragraph>
            <TextInput
              label="Rutebeskrivelse" 
              value={trackingRouteDescription}
              onChangeText={(text) => setTrackingRouteDescription(text)} 
            />
          </Dialog.Content>
          <Dialog.Actions>
          <Button onPress={cancelEndTracking} disabled={loading}>Avbryt</Button>
            <Button onPress={finishEndTracking} disabled={loading} loading={loading}>Lagre</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      <View style={styles.buttons}>
        {trackingState.isTracking && trackingState.isPlacingPOI === undefined
          ? sheepButtonStep===SheepButtonStep.step1 ? renderOpenMenu() : renderSheepButtons()
          : null}
        {trackingState.isTracking &&
        trackingState.isPlacingPOI !== undefined ? (
          <>
            <View style={styles.firstRow}>{renderPlacingButton()}</View>
            <View style={styles.secondRow}>
              <Button
                icon="stop"
                uppercase={false}
                mode="contained"
                style={styles.cancelPlacementButton}
                labelStyle={styles.cancelPlacementButtonLabel}
                onPress={cancelPOIPlacement}
              >
                Avbryt plassering
              </Button>
            </View>
          </>
        ) : null}
        {!trackingState.isTracking ? (
          <FAB
            icon="play"
            style={styles.startButton}
            onPress={() => startTracking()}
          />
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  fixedView: {
    position: "absolute",
    alignSelf: "center",
    bottom: "5%",
    flexDirection: "row",
    justifyContent: "flex-end",
    zIndex: 2,
    elevation: 2,
  },
  buttons: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
  },
  firstRow: {
    flex: 1,
  },
  secondRow: {
    flex: 1,
    flexDirection: "row"
  },
  startButton: {
    backgroundColor: Colors.greenA400,
    marginBottom: 30,
  },
  endButton: {
    marginTop: 10,
    marginBottom: 30,
    backgroundColor: Colors.black,
  },
  sheepButton: {
    marginBottom: 10,
    backgroundColor: Colors.blue500,
  },
  predatorButton: {
    marginRight: 30,
    backgroundColor: Colors.red500,
  },
  sheepHerdButton: {
    marginRight: 30,
    backgroundColor: Colors.blue300,
  },
  debugStyle: {
    height: 100,
    width: 100,
    backgroundColor: Colors.green700
  },
  deadSheepButton: {
    marginLeft: 30,
    backgroundColor: Colors.red300
  },
  backButton: {
    marginTop: 10,
    marginBottom: 30,
    backgroundColor: Colors.grey200,
  },
  otherButton: {
    marginLeft: 30,
    backgroundColor: Colors.yellow500,
  },
  placementButtonLabel: {
    fontSize: 18,
  },
  cancelPlacementButton: {
    marginTop: 30,
  },
  cancelPlacementButtonLabel: {
    fontSize: 12,
  },
});

export default MapMenu;
