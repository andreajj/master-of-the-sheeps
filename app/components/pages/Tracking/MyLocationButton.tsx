import React from 'react'
import { Colors, FAB, Button } from "react-native-paper";
import { View, StyleSheet, Alert } from "react-native";

interface Props {
    jump: () => void,
    offlineShiftDown: boolean
}

const MyLocationButton: React.FC<Props> = ({jump, offlineShiftDown}) => {
    return (
        <View style={[offlineShiftDown ? styles.shiftLocation : styles.normalLocation, styles.fixedView]}>
            <FAB
                icon="target"
                style={styles.userLocationButton}
                onPress={jump}
              />
        </View>
    )
}

const styles = StyleSheet.create({
    fixedView: {
        position: "absolute",
        alignSelf: "flex-start",
        paddingRight: "5%",
        flexDirection: "row",
        justifyContent: "center",
        zIndex: 2,
        elevation: 2,
    },
    // when offline banner is visible, move button down a bit
    shiftLocation: {
        top: "25%"
    },
    normalLocation: {
        top: "1%"
    },
    userLocationButton: {
        backgroundColor: Colors.grey100
    }
})

export default MyLocationButton
