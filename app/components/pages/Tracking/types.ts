import { Colors } from "react-native-paper";

export interface FullLocation {
  coords: {
    accuracy: number;
    altitude: number;
    altitudeAccuracy: number;
    heading: number;
    latitude: number;
    longitude: number;
    speed: number;
  };
  timestamp: number;
}

export type Location = [number, number];

export enum Others {
  Moose = "Elg",
  Viper = "Hoggorm",
  Dog = "Hund",
  Reindeer = "Reinsdyr",
  Other = "Annet"
}

export enum Predator {
  Wolverine = "Jerv",
  Bear = "Bjørn",
  Wolf = "Ulv",
  Eagle = "Ørn",
  Other = "Annet"
}

export enum EarTag {
  Green,
  Blue,
  Red,
  Purple
}

export function getEarTag(tag: EarTag) {
  switch(tag) {
    case EarTag.Green: return Colors.green500;
    case EarTag.Blue: return Colors.blue500;
    case EarTag.Red: return Colors.red500;
    case EarTag.Purple: return Colors.purple500;
  }
}

export enum Tie {
  None = "None",
  Red = "Red",
  Blue = "Blue",
  Yellow = "Yellow",
  Green = "Green",
}

export function getTie(tie: Tie) {
  switch(tie) {
    case Tie.None: return "transparent";
    case Tie.Yellow: return Colors.yellow500;
    case Tie.Red: return Colors.red500;
    case Tie.Blue: return Colors.blue500;
    case Tie.Green: return Colors.green500;
  }
}