import {
  startLocationUpdatesAsync,
  Accuracy,
  stopLocationUpdatesAsync,
  hasStartedLocationUpdatesAsync,
} from "expo-location";
import { defineTask } from "expo-task-manager";
// State
import { setRecoil } from "recoil-nexus";
import { currentTrackingState } from "../../atoms";
// types
import { FullLocation, Location } from "./types";

const LOCATION_TASK_NAME = "background-location-task";
/**
 * Begin tracking background location.
 * Permission for foreground and background location must be granted before this can run.
 */
export const beginBackgroundTracking = async () => {
  try {
    await startLocationUpdatesAsync(LOCATION_TASK_NAME, {
      accuracy: Accuracy.BestForNavigation,
      timeInterval: 30000,
      // Makes background tracking as accurate as foreground tracking
      // NOTE: Shows an unremovable notification.
      foregroundService: {
        notificationTitle: "Saueappen",
        notificationBody: "Sporing av lokasjon i bakgrunnen er aktivt.",
      },
    });
    console.log("Started tracking locations");
  } catch (error) {
    console.log("Background location initialization task failed:", error);
  }
};

/**
 * Stop tracking of background location.
 */
export const stopBackgroundTracking = async () => {
  try {
    await stopLocationUpdatesAsync(LOCATION_TASK_NAME);
    console.log("Stopped tracking locations");
  } catch (error) {
    console.log("Background tracking shutdown task failed:", error);
  }
};

/**
 * Check whether tracking of background location is currently active.
 * @returns boolean
 */
export const isBackgroundTrackingActive = async () => {
  try {
    const isActive = await hasStartedLocationUpdatesAsync(LOCATION_TASK_NAME);
    return isActive;
  } catch (error) {
    console.log("Background tracking task status check failed:", error);
  }
};

/**
 * Logic-only component for tracking user location in the background
 * @returns null
 */
const BackgroundTracker = () => {
  return null;
};

export default BackgroundTracker;

interface DataLocation {
  locations: Array<FullLocation>;
}

// This must be outside of a component to function
defineTask(LOCATION_TASK_NAME, ({ data, error }) => {
  if (error) {
    // Error occurred - check `error.message` for more details.
    console.log("An error occured in defineTask:", error.message);
    return;
  }
  if (data) {
    const { locations } = data as DataLocation;
    console.log("I received update:", locations);
    setRecoil(currentTrackingState, (cur) => {
      const coords = locations[0].coords;
      const newLocation: Location = [coords.longitude, coords.latitude];
      return {
        ...cur,
        locations: [...cur.locations, newLocation],
      };
    });
  }
});
