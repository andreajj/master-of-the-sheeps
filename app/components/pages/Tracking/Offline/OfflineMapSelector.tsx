import MapboxGL from '@react-native-mapbox-gl/maps';
import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Alert, ScrollView } from 'react-native';
import { Button, Colors, Banner, Avatar } from 'react-native-paper';
import { useRecoilState } from 'recoil';
import { offlineMapsState, OfflineMap, currentTrackingState } from '../../../atoms';
import { RouteProp, useNavigation } from "@react-navigation/native";
import OfflineMapsList from '../../OfflineMaps/OfflineMapsList';
import { userState } from '../../../atoms/userState';

interface Props {
  route: RouteProp<{params: { isForTracking?: boolean } }, 'params'>
}

const OfflineMapSelector: React.FC<Props> = ({ route: { params: { isForTracking }}}) => {
  // Navigation
  const navigation = useNavigation();

  const [offlineMaps, setOfflineMaps] = useRecoilState(offlineMapsState);
  const [currentTracking, setCurrentTracking] = useRecoilState(currentTrackingState);
  const [user, setUser] = useRecoilState(userState);

  const onPress = (offlineMap: OfflineMap) => {
    if (isForTracking) {
      setCurrentTracking((cur) => ({
        ...cur,
        offlineMap: offlineMap,
        isTracking: true,
        startTimestamp: new Date(),
      }))
    } else {
      setUser((cur) => ({
          ...cur,
          offlineMap: offlineMap
        })
      );
    }
    
    navigation.goBack();
  }

  const goBack = () => {
    navigation.goBack();
  }

  return (
    <View style={styles.container}>
      {isForTracking && <Banner
        visible={true}
        style={styles.banner}
        actions={[]}
      icon={() => (
        <Avatar.Icon
            icon="map-outline"
          />
        )}>
            Velg et kart som dekker tilsynsområdet.
        </Banner>}
      <ScrollView>
        <OfflineMapsList offlineMaps={offlineMaps} onPress={(offlineMap: OfflineMap) => onPress(offlineMap)} />
      </ScrollView>
      <Button
        uppercase={false}
        mode="contained"
        style={{margin: 10}}
        onPress={goBack}
      >
        Tilbake
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  banner: {
    zIndex: 5,
    elevation: 5,
    backgroundColor: Colors.grey200,
    maxHeight: "18%"
  }
});

export default OfflineMapSelector