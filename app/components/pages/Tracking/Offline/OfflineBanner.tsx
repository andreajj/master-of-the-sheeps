import React, {useState, useEffect} from 'react';
import {Platform} from 'react-native'
import { Avatar, Banner} from 'react-native-paper';
import {openSettings} from 'expo-linking'
import { useNavigation } from '@react-navigation/native';
import { RouteNames } from '../../../Navigation';

interface Props {
  visible: boolean,
  isOfflineMapSelected: boolean,
  offlineMapName: string | undefined;
}

const OfflineBanner: React.FC<Props> = ({ visible, isOfflineMapSelected, offlineMapName }) => {
  // Navigation
  const navigation = useNavigation();
  
  const onPress = () => {
    navigation.navigate(RouteNames.MainLanding, { 
      screen: RouteNames.DrawerSheepTracking,
      params: {
        screen: RouteNames.TrackingOfflineMapSelector,
        // This needs to be here even if empty as OfflineMapSelector extracts the isForTracker param (which may be undefined) from the params
        // meaning if params is undefined we do undefined.isForTracking which will throw an error
        params: {}
     }
    });
  }

    return (
    <Banner
      visible={visible}
      actions={[{
        label: isOfflineMapSelected ? 'Velg et annet offline kart' : 'Velg offline kart',
        onPress: onPress
      }]}
      icon={() => (
        <Avatar.Icon
            icon="map"
          />
      )}>
        {isOfflineMapSelected ? (
          `Du har ikke nettilkobling og bruker offline-kartet "${offlineMapName}".`
        ) : (
          "Du har ikke nettilkobling, du må velge et offline kart for å kunne bruke appen."
        )}
    </Banner>
    )
}

export default OfflineBanner