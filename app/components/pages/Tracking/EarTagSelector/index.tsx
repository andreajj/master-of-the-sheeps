import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, Alert, Platform } from "react-native";
import DropDownPicker, {ValueType} from 'react-native-dropdown-picker';
import { useRecoilState, useRecoilValue } from "recoil";
import {farmsState } from '../../../atoms/farms'
import { FarmType } from '../../../atoms'
import Badge from './EarTagBadge';
import { currentTrackingState } from "../../../atoms";

DropDownPicker.addTranslation("NO", {
  PLACEHOLDER: "Velg en øremerke farge",
  SEARCH_PLACEHOLDER: "Søk etter en øremerke farge",
  SELECTED_ITEMS_COUNT_TEXT: "{count} øremerke farge(r) har blitt valgt",
  NOTHING_TO_SHOW: "Venligst legg til øremerke farger før du velger en."
});

// Set as default
DropDownPicker.setLanguage("NO")

DropDownPicker.setMode("BADGE");

interface DropDownIconProps {
  color1: string,
  color2?: string
}

const EarTagSelector: React.FC = () => {
  // Preset of colors selected by the user
  // const earTagColors = useRecoilValue(colorsState);
  const {farmsList} = useRecoilValue(farmsState);
  // The current tracking state
  const [currentTracking, setCurrentTracking] = useRecoilState(currentTrackingState);
  // Make a set (unique values) of the preset of colors
  const farms = new Set(farmsList)
  // If an earlier tracking already have farms selected, add them to the set
  const existingFarms = currentTracking.intermediateSheepHerd?.farms.filter((farm) => !farms.has(farm)) ?? []; // TODO: fix this

  const DropDownOpenIcons: React.FC<DropDownIconProps> =  ({color1, color2}) => {
    return ( 
      <View style={styles.iconContainer}>
        {color2 ? 
        <>
          <View style={[{ backgroundColor: color1}, styles.twoColorItem]} />
          <View style={[{ backgroundColor: color2}, styles.twoColorItem]} />
        </>
        : <View style={[{ backgroundColor: color1}, styles.singleColorItem]}/> }
      </View>
    )
  }

  // Create the values for the dropdown component
  let items = ([...farms]).map((farm) => {
    let {name, color1, color2} = farm
    return ({
      label: name, 
      value: JSON.stringify(farm),
      icon: () => <DropDownOpenIcons color1={color1} color2={color2} />
    })
  });

  // TODO: Fix for existing farms instead of existing colors

  // Create values of exisiting not duplicate farms for the dropdown component and add it to exisiting values
  for (let i = 0; i < existingFarms.length; i++) {
    const farm = existingFarms[i];
    let {name, color1, color2} = existingFarms[i];
    items.push({
      label: name, 
      value: JSON.stringify(farm),
      icon: () => <DropDownOpenIcons color1={color1} color2={color2} />
    })
  }

  const [value, setValue] = useState<Array<string> | null>(null);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if(currentTracking.intermediateSheepHerd?.farms) {
      if (value) {
        const parsedValues = value.map((stringifiedFarm) => JSON.parse(stringifiedFarm));
        const parsedValueNames = parsedValues.map((parsedValue) => parsedValue.name);

        const notInValue = currentTracking.intermediateSheepHerd.farms.filter((farm) => !parsedValueNames.includes(farm.name))

        if (notInValue.length === 0) {
          return
        }
        const allFarmsNotInValue = notInValue.map((farm) => JSON.stringify(farm))
        setValue([...value, ...allFarmsNotInValue])
      } else {
        const allFarms = currentTracking.intermediateSheepHerd.farms.map((farm) => JSON.stringify(farm))
        setValue(allFarms)
      }
    }
  }, [currentTracking.intermediateSheepHerd?.farms])

  useEffect(() => {
    if (value === null) {
      return;
    }
    
    const currentHerd = currentTracking.intermediateSheepHerd

    if (currentHerd === undefined) {
      Alert.alert(
        "Kritisk feil!",
        "Sauflokken finnes ikke, sauen ble ikke lagt til.",
        [
          { text: "OK" }
        ]
      );
      return
    }

    const parsedValues = value.map((stringifiedFarm) => JSON.parse(stringifiedFarm));
    const parsedValueNames = parsedValues.map((parsedValue) => parsedValue.name);

    const updatedHerd = {
      ...currentHerd,
      // get farms based on color1. Not really optimal... (value needs to be of type string | number | boolean)
      farms: [
        ...farmsList.filter((farm) => parsedValueNames.includes(farm.name))
      ]
    }

    setCurrentTracking((cur) => ({
      ...cur,
      intermediateSheepHerd: updatedHerd
    }))
  }, [value])

  console.log(value)

  return (
    <View style={[Platform.OS === 'android' && open && styles.androidHeightFixer, styles.container]}>
      <Text style={styles.text}>Velg øremerke farge(r)</Text>
      <DropDownPicker
          open={open}
          value={value}
          items={items}
          setOpen={setOpen}
          setValue={setValue}
          multiple={true}
          dropDownDirection={'TOP'}
          renderBadgeItem={(props) => <Badge {...props} />}
          maxHeight={Platform.OS==='android' ? 200 : undefined }
          arrowIconStyle={{marginLeft: 10}}
        />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    zIndex: 10,
    elevation: 10,
  },
  text: {
    marginBottom: 5,
  },
  androidHeightFixer: {
    justifyContent: 'flex-end',
    height: 240
  },
  iconContainer: {
    height: 20, 
    width: 100, 
    display: "flex", 
    flexDirection: 'row'
  },
  twoColorItem: {
    width: 50, 
    height: 20 
  },
  singleColorItem: {
    width: 100, 
    height: 20
  }
})

export default EarTagSelector;