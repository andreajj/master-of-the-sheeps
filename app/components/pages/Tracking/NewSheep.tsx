import React, { useEffect } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { Colors, Button } from 'react-native-paper';
import { useRecoilState } from 'recoil';
import { currentTrackingState } from '../../atoms';
import { EarTag, Tie } from './types';
import { useNavigation, RouteProp } from '@react-navigation/native';
import TieSelector from './TieSelector';

const NewSheep: React.FC = () => {
  const navigation = useNavigation();
  const [selectedTie, setSelectedTie] = React.useState(Tie.Yellow);
  const [currentTracking, setCurrentTracking] = useRecoilState(currentTrackingState);

  const onSubmit = () => {
    const currentHerd = currentTracking.intermediateSheepHerd

    if (currentHerd === undefined) {
      Alert.alert(
        "Kritisk feil!",
        "Sauflokken finnes ikke, sauen ble ikke lagt til.",
        [
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
      return
    }

    const updatedHerd = {
      ...currentHerd,
      sheeps: {
        ...currentHerd.sheeps,
        [selectedTie]: currentHerd.sheeps[selectedTie] + 1
      },
    }

    setCurrentTracking((cur) => ({
      ...cur,
      intermediateSheepHerd: updatedHerd
    }))
    navigation.goBack()
  }

    return (
      <View style={styles.container}>
        <View style={styles.selectors}>
          <TieSelector tie={selectedTie} setTie={(tie: Tie) => setSelectedTie(tie)} />
        </View>
        <Button
            labelStyle={styles.buttonLabel} 
            style={styles.button} 
            mode="contained" 
            onPress={onSubmit}
        >
          Ferdig
        </Button>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    padding: 30,
    justifyContent: "space-between",
  },
  selectors: {
    flex: 1,
  },
  button: {
    borderRadius: 0,
    marginTop: 10,
    marginBottom: 10,
    padding: 10,
  },
  buttonLabel: {
    color: Colors.white,
  }
})

export default NewSheep;