import { useNavigation, RouteProp } from '@react-navigation/native';
import React, { useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { Colors, Button, TextInput, Text } from 'react-native-paper';
import DropDown from "react-native-paper-dropdown";
import { useRecoilState } from 'recoil';
import { currentTrackingState, POI } from '../../atoms';
import { Location, Others } from './types';
import uuid from 'react-native-uuid';
import getUserLocation from '../../utils/getUserLocation';


interface Props {
  route: RouteProp<{params: { location: Location, otherUUID?: string, } }, 'params'>
}

const NewOther: React.FC<Props> = ({ route: { params: { location, otherUUID }}}) => {
  const navigation = useNavigation();
  
  const [currentTracking, setCurrentTracking] = useRecoilState(currentTrackingState);
  const currentOther = currentTracking.others.find((o) => o.uuid === otherUUID)
  const [visible, setVisible] = React.useState(currentOther?.type === Others.Other ? true : false);
  const [selected, setSelected] = React.useState(currentOther?.type ?? Others.Moose);
  const [details, setDetails] = React.useState(currentOther?.description ?? '');

  useEffect(() => {
    return () => {
      setCurrentTracking((cur) => ({
        ...cur,
        isPlacingPOI: undefined,
      }))
    }
  }, [])

  const otherList = Object.values(Others).map((o) => ({
    label: o,
    value: o
  }))

  const onDelete = () => {
    const newState = currentTracking.others.filter((p) => p.uuid !== currentOther?.uuid)
    setCurrentTracking({
      ...currentTracking,
      others: [...newState]
    })
    navigation.goBack();
  }

  const onSubmit = async () => {
    const userLocation: Location = await getUserLocation();

    if (currentOther) {
      const newState = currentTracking.others.filter((o) => o.uuid !== currentOther.uuid)
      setCurrentTracking({
        ...currentTracking,
        locations: [...currentTracking.locations, userLocation],
        others: [
          ...newState,
          {
            type: selected,
            description: details,
            location: currentOther.location,
            upsertLocations: [...currentOther.upsertLocations, userLocation],
            uuid: currentOther.uuid
          }
        ]
      })
    } else {
      setCurrentTracking({
        ...currentTracking,
        locations: [...currentTracking.locations, userLocation],
        others: [
          ...currentTracking.others,
          {
            type: selected,
            description: details,
            location: location,
            upsertLocations: [userLocation],
            uuid: uuid.v4().toString()
          }
        ]
      })
    }
    navigation.goBack()
  }

    return (
      <View style={styles.container}>
        <View>
          <DropDown
                label={"Annet"}
                mode={"outlined"}
                visible={visible}
                showDropDown={() => setVisible(true)}
                onDismiss={() => setVisible(false)}
                value={selected}
                setValue={setSelected}
                list={otherList}
                dropDownStyle={{width: '100%', flex: 1}}
              />
          <Text style={styles.descriptionLabel}>Beskrivelse</Text>
          <TextInput 
                value={details}
                onChangeText={setDetails}
                style={styles.textArea} 
                mode='outlined' 
                multiline={true} 
                numberOfLines={4} 
                maxLength={100}
                right={<TextInput.Affix text="/100" />}
          />
        </View>
        <View>
          <Button icon="plus" mode="contained" onPress={onSubmit}>
            Ferdig
          </Button>
          {otherUUID && (
            <Button icon="minus" mode="contained" style={styles.deleteButton} onPress={onDelete}>
              Slett
            </Button>
          )}
        </View>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingVertical: 50,
    paddingHorizontal: 20,
    justifyContent: "space-between"
  },
  dropDown: {
    width: '100%',
    flex: 1,
    borderRadius: 0,
  },
  textArea: {
    width: '100%',
    maxHeight: 200,
    marginTop: 30,
    backgroundColor: Colors.grey300
  },
  deleteButton: {
    marginTop: 10,
    backgroundColor: Colors.red500,
  },
  descriptionLabel: {
    marginBottom: -30,
    marginTop: 25,
    marginLeft: 10
  },
})

export default NewOther;