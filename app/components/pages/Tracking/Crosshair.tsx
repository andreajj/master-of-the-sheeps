import React from 'react';
import { View, StyleSheet } from 'react-native';

const Crosshair: React.FC = () => (
  <View style={styles.container} pointerEvents="none">
    <View style={styles.vertical} pointerEvents="none"></View>
    <View style={styles.horizontal} pointerEvents="none"></View>
    <View style={styles.circle} pointerEvents="none"></View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    position: 'absolute',
    alignSelf: 'center',
    alignItems: "center",
    justifyContent: "center",
    zIndex: 1,
    elevation: 1,
  },
  circle: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    position: 'absolute',
    borderWidth: 1
  },
  vertical: {
    borderWidth: 0.5,
    height: "100%",
    position: 'absolute',
  },
  horizontal: {
    borderWidth: 0.5,
    width: "100%",
    position: 'absolute',
  }
})

export default Crosshair;