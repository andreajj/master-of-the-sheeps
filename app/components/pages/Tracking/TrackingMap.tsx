import React, { useRef, useEffect, useState } from "react";
import { StyleSheet, View, AppState, AppStateStatus } from "react-native";
import MapboxGL from "@react-native-mapbox-gl/maps";
import MapMenu from "./MapMenu";
import BackgroundTracker from "./BackgroundTracker";
import DrawLocationHistoryLines from "../../drawings/DrawLocationHistoryLines";
import DrawLocationHistoryPoints from "../../drawings/DrawLocationHistoryPoints";
import { locationHistoryLonLat } from "./../../selectors/selectors";
import { useRecoilValue } from "recoil";
import Crosshair from "./Crosshair";
import { currentTrackingState, offlineMapsState, userState } from '../../atoms';
import DrawPredatorPoints from "../../drawings/DrawPredatorPoints";
import DrawOtherPoints from "../../drawings/DrawOtherPoints";
import DrawSheepHerdPoints from "../../drawings/DrawSheepHerdPoints";
import Map from "../../Map";
import OfflineBanner from "./Offline/OfflineBanner";
import {
  getBackgroundPermissionsAsync,
  getLastKnownPositionAsync,
  getCurrentPositionAsync,
  LocationAccuracy,
} from "expo-location";
import PermissionBanner from "./Permissions/PermissionBanner";
import MyLocationButton from "./MyLocationButton";
import FarmsBanner from "../FarmPicker/FarmsBanner";
import DrawObservationLinesContainer from "../../drawings/DrawObservationLinesContainer";
import { points } from '@turf/helpers';
import center from '@turf/center';
import DrawDeadSheepPoints from "../../drawings/DrawDeadSheepPoints";
import { farmsState } from "../../atoms/farms";

// array with two numbers gets interpreted as number[]
// instead of [number, number], </3 microsoft
type maxBounds = {
  ne: [number, number];
  sw: [number, number];
};

const TrackingMap = () => {
  // Map ref
  const map = useRef<MapboxGL.MapView | null>(null);
  // camera ref
  const camera = useRef<MapboxGL.Camera | null>(null);
  const refs = {
    mapRef: map,
    cameraRef: camera,
  };
  // Full current tracking state
  const currentTracking = useRecoilValue(currentTrackingState);
  // Offline maps state
  const offlineMaps = useRecoilValue(offlineMapsState);
  // user state
  const user = useRecoilValue(userState);
  // Maximum bounds for navigating, map cannot leave these bounds.
  // Should be set to contain all of Norway when a specific offline map is not selected
  const _maxBounds: maxBounds = {
    sw: [4.9920780, 58.078884],
    ne: [32.258960, 71.244356]
  }
  const [maxBounds, setMaxBounds] = useState<maxBounds>(_maxBounds);
  
  // point where map will initially start
  const [centerCoordinate, setCenterCoordinate] = useState<[number, number]>([
    10.402757159741896, 63.41773186344639,
  ]);

  const [zoomLevel, setZoomLevel] = useState(14);
  // permission banner
  const [isPermissionBannerVisible, setIsPermissionBannerVisible] =
    useState(false);
  const [isFarmsBannerVisible, setIsFarmsBannerVisible] = useState(false);
  const isOfflineBannerVisible = !isPermissionBannerVisible && !isFarmsBannerVisible && !currentTracking.isTracking && !user.isOnline;

  const locationHistory = useRecoilValue(locationHistoryLonLat);

  const { farmsList } = useRecoilValue(farmsState);

  const paw = require("../../../assets/paw.png");
  const sheep = require("../../../assets/sheep.png");
  const other = require("../../../assets/other.png");
  const sheepHerd = require("../../../assets/sheepherd.png");
  const deadSheep = require("../../../assets/skull-crossbones.png");

  const images = {
    paw,
    sheep,
    other,
    sheepHerd,
    deadSheep
  };

  const getCenter = () => {
    return map.current?.getCenter();
  };

  const userLocation: () => Promise<[number, number]> = async () => {
    const lastKnownPosition = await getLastKnownPositionAsync({
      requiredAccuracy: 500,
    });
    if (lastKnownPosition === null) {
      const { coords } = await getCurrentPositionAsync({
        accuracy: LocationAccuracy.Balanced,
      });
      return [coords.longitude, coords.latitude]
    } else {
      const { coords } = lastKnownPosition;
      return [coords.longitude, coords.latitude]
    }
  }

  const jumpToUserLocation = async () => {
    const location = await userLocation();
    camera.current?.moveTo(location, 1000);
  };

  // check location permissions, close banner if OK
  const checkPerms = async () => {
    const { status } = await getBackgroundPermissionsAsync();
    if (status !== "granted") {
      setIsPermissionBannerVisible(true);
    } else {
      setIsPermissionBannerVisible(false);
    }
  };

  // close banner when returning to app and sufficient permissions granted
  const handleChange = (newState: AppStateStatus) => {
    if (newState === "active") {
      checkPerms();
    }
  };

  // handle automatically open/closing banner based on permission state
  useEffect(() => {
    AppState.addEventListener("change", handleChange);
    checkPerms();
    return () => {
      AppState.removeEventListener("change", handleChange);
    };
  }, []);

  useEffect(() => {
    if (farmsList.length === 0) {
      setIsFarmsBannerVisible(true);
    } else {
      setIsFarmsBannerVisible(false);
    }
  }, [farmsList]);

  useEffect(() => {
    if (currentTracking.offlineMap?.uuid || user.offlineMap?.uuid) {
      const offlineMap = offlineMaps.find((offlineMap) => offlineMap.uuid == (currentTracking.offlineMap?.uuid || user.offlineMap?.uuid));
      if (offlineMap) {
        setMaxBounds({
          sw: offlineMap.bounds[1],
          ne: offlineMap.bounds[0],
        })
      } else if (user.isOnline && currentTracking.offlineMap) {
        setMaxBounds({
          sw: currentTracking.offlineMap?.bounds[1],
          ne: currentTracking.offlineMap?.bounds[0]
        })
      }
    } else {
      setMaxBounds(_maxBounds)
    }
  }, [currentTracking.offlineMap, user.offlineMap])

  useEffect(() => {
    const init = async () => {
      if (JSON.stringify(maxBounds) === JSON.stringify(_maxBounds)) {
        const location = await userLocation();
        return setCenterCoordinate(location);
      }
      const features = points([
          maxBounds.ne,
          maxBounds.sw
      ]);
      const centerCoord = center(features);
      setCenterCoordinate([centerCoord.geometry.coordinates[0], centerCoord.geometry.coordinates[1]]);
    }
    init();
  }, [maxBounds])

  return (
    <View style={styles.container}>
      <OfflineBanner visible={isOfflineBannerVisible} isOfflineMapSelected={user.offlineMap !== undefined} offlineMapName={user.offlineMap?.name} />
      <BackgroundTracker />
      <PermissionBanner visible={isPermissionBannerVisible} />
      {!isPermissionBannerVisible && (
        <FarmsBanner visible={isFarmsBannerVisible} />
      )}
      <Map
        maxBounds={maxBounds}
        zoomLevel={zoomLevel}
        centerCoordinate={centerCoordinate}
        ref={refs}
      >
        <DrawLocationHistoryLines coordinates={locationHistory} />
        <DrawLocationHistoryPoints coordinates={locationHistory} />
        <MapboxGL.UserLocation showsUserHeadingIndicator visible />
        {currentTracking.isTracking ? (
          <>
            <DrawOtherPoints others={currentTracking.others} />
            <DrawPredatorPoints predators={currentTracking.predators} />
            <DrawSheepHerdPoints sheepHerds={currentTracking.sheepHerds} />
            <DrawDeadSheepPoints deadSheep={currentTracking.deadSheep} />
            <DrawObservationLinesContainer tracking={currentTracking}/>
            <MapboxGL.Images images={images} />
          </>
        ) : null}
      </Map>
      <MyLocationButton offlineShiftDown={isOfflineBannerVisible} jump={jumpToUserLocation} />
      {!isPermissionBannerVisible && !isFarmsBannerVisible ? (
        <MapMenu getCenter={getCenter} />
      ) : null}
      {currentTracking.isTracking &&
      currentTracking.isPlacingPOI !== undefined ? (
        <Crosshair />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    backgroundColor: "white",
  },
  map: {
    flex: 1,
  },
});

export default TrackingMap;
