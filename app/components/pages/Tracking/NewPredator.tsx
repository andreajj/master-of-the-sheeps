import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Colors, Button, TextInput } from 'react-native-paper';
import { useRecoilState } from 'recoil';
import { currentTrackingState, POI } from '../../atoms';
import { useNavigation, RouteProp } from '@react-navigation/native';
import { Location, Predator } from './types';
import uuid from 'react-native-uuid';
import getUserLocation from '../../utils/getUserLocation';

interface Props {
  route: RouteProp<{params: { location: Location, predatorUUID?: string } }, 'params'>
}

const NewPredator: React.FC<Props> = ({ route: { params: { location, predatorUUID }}}) => {
  const navigation = useNavigation();

  const [currentTracking, setCurrentTracking] = useRecoilState(currentTrackingState);
  const currentPredator = currentTracking.predators.find((p) => p.uuid === predatorUUID)
  const [isOther, setIsOther] = useState(currentPredator?.type === Predator.Other ? true : false);
  const [selectedPredator, setSelectedPredator] = useState<Predator>(currentPredator?.type ?? Predator.Wolverine);
  const [otherDetails, setOtherDetails] = useState(currentPredator?.description ?? '');

  useEffect(() => {
    return () => {
      setCurrentTracking((cur) => ({
        ...cur,
        isPlacingPOI: undefined,
      }))
    }
  }, [])

  const onBearButtonPress = () => {
    setSelectedPredator(Predator.Bear)
    setIsOther(false)
  }

  const onWolfButtonPress = () => {
    setSelectedPredator(Predator.Wolf)
    setIsOther(false)
  }

  const onEagleButtonPress = () => {
    setSelectedPredator(Predator.Eagle)
    setIsOther(false)
  }

  const onWolverineButtonPress = () => {
    setSelectedPredator(Predator.Wolverine)
    setIsOther(false)
  }

  const onOtherButtonPress = () => {
    setSelectedPredator(Predator.Other)
    setIsOther(true)
  }

  const onDelete = () => {
    const newState = currentTracking.predators.filter((p) => p.uuid !== currentPredator?.uuid)
    setCurrentTracking({
      ...currentTracking,
      predators: [...newState]
    })
    navigation.goBack();
  }

  const onSubmit = async () => {
    const userLocation: Location = await getUserLocation();

    if(currentPredator) {
      const newState = currentTracking.predators.filter((p) => p.uuid !== currentPredator.uuid)
      setCurrentTracking({
        ...currentTracking,
        locations: [...currentTracking.locations, userLocation],
        predators: [
          ...newState,
          {
            type: selectedPredator,
            description: selectedPredator == Predator.Other ? otherDetails : undefined,
            location: currentPredator.location,
            upsertLocations: [...currentPredator.upsertLocations, userLocation],
            uuid: currentPredator.uuid
          }
        ]
      })
    } else {
      setCurrentTracking({
        ...currentTracking,
        locations: [...currentTracking.locations, userLocation],
        predators: [
          ...currentTracking.predators,
          {
            type: selectedPredator,
            description: selectedPredator == Predator.Other ? otherDetails : undefined,
            location: location,
            upsertLocations: [userLocation],
            uuid: uuid.v4().toString()
          }
        ]
      })
    }
    navigation.goBack()
  }

    return (
      <View style={styles.container}>
        <View style={styles.buttons}>
          {Object.values(Predator).map((p) => (
            <Button
              key={"Predator: " + p}
              labelStyle={styles.buttonLabel} 
              style={selectedPredator == p ? styles.disabledButton : styles.button} 
              mode="contained" 
              disabled={selectedPredator == p} 
              onPress={() => {
                switch(p) {
                  case Predator.Wolverine: return onWolverineButtonPress()
                  case Predator.Bear: return onBearButtonPress()
                  case Predator.Wolf: return onWolfButtonPress()
                  case Predator.Eagle: return onEagleButtonPress()
                  case Predator.Other: return onOtherButtonPress()
                }
              }}
            >
              {p}
            </Button>
          ))}
          {isOther && (
            <TextInput 
              value={otherDetails}
              onChangeText={setOtherDetails}
              style={styles.textArea} 
              mode='outlined' 
              multiline={true} 
              numberOfLines={4} 
              maxLength={100}
              right={<TextInput.Affix text="/100" />}
              />
          )}
        </View>
        <View>
          <Button icon="plus" mode="contained" onPress={onSubmit}>
            {currentPredator ? "Lagre" : "Ferdig"}
          </Button>
          {predatorUUID && (
            <Button icon="minus" mode="contained" style={styles.deleteButton} onPress={onDelete}>
              Slett
            </Button>
          )}
        </View>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    padding: 30,
    justifyContent: 'space-between',
  },
  buttons: {
    flex: 1,
    alignItems: 'center'
  },
  deleteButton: {
    marginTop: 10,
    backgroundColor: Colors.red500,
  },
  button: {
    width: '80%',
    borderRadius: 0,
    marginBottom: 20
  },
  disabledButton: {
    width: '80%',
    borderRadius: 0,
    marginBottom: 20,
    backgroundColor: Colors.deepPurple500,
    color: Colors.white,
  },
  buttonLabel: {
    color: Colors.white,
  },
  textArea: {
    width: '80%',
    maxHeight: 200,
    backgroundColor: Colors.grey300
  }
})

export default NewPredator;