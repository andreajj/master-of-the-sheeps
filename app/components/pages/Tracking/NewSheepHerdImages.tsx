import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { Colors, Button } from 'react-native-paper';
import { useRecoilState } from 'recoil';
import { currentTrackingState, Image } from '../../atoms';
import ImagePicker, { ImageType } from '../../ImagePicker';

const NewSheepHerdImages: React.FC = () => {
  const navigation = useNavigation();
  
  const [currentTracking, setCurrentTracking] = useRecoilState(currentTrackingState);
  const [images, setImages] = useState<Array<ImageType>>([]);

  useEffect(() => {
    if (currentTracking.intermediateSheepHerd?.images) {
      setImages(currentTracking.intermediateSheepHerd.images.map((image) => ({...image})));
    } else {
      setImages([]);
    }
  }, [currentTracking.intermediateSheepHerd])

  useEffect(() => {
    return () => {
      setCurrentTracking((cur) => ({
        ...cur,
        isPlacingPOI: undefined,
      }))
    }
  }, [])

  const onSubmit = async () => {
    const currentHerd = currentTracking.intermediateSheepHerd

    if (currentHerd === undefined) {
      Alert.alert(
        "Kritisk feil!",
        "Sauflokken finnes ikke, bildene ble ikke lagt til.",
        [
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
      return
    }

    const newImages: Array<Image> = images.map((image) => ({base64: image.base64}));

    const updatedHerd = {
      ...currentHerd,
      images: newImages,
    }

    setCurrentTracking((cur) => ({
      ...cur,
      intermediateSheepHerd: updatedHerd
    }))
    navigation.goBack()
  }

    return (
      <View style={styles.container}>
        <View style={styles.picturesPlaceHolder}>
          <ImagePicker images={images} setImages={setImages} />
        </View>
        <View>
          <Button icon="plus" mode="contained" onPress={onSubmit}>
            Ferdig
          </Button>
        </View>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingVertical: 50,
    paddingHorizontal: 20,
    justifyContent: "space-between"
  },
  picturesPlaceHolder: {
    height: 200,
      display: 'flex',
      flexDirection: "column",
      padding: 5,
      justifyContent: "center",
      width: "100%",
  }
})

export default NewSheepHerdImages;