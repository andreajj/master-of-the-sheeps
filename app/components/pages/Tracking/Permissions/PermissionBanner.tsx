import React, {useState, useEffect} from 'react';
import {Platform, StyleSheet} from 'react-native'
import { Avatar, Banner} from 'react-native-paper';
import {openSettings} from 'expo-linking'

interface Props {
  visible: boolean,
}

enum BannerPermissionMessage {
    ANDROID = `Gå til "Tillatelser" > "Lokasjon" og velg "Tillat hele tiden"`,
    IOS = `Gå til "Sted" og velg "Alltid"`,
}

const BASE_MESSAGE = 'Du må gi tillatelse til å spore lokasjonen din.\n'

const PermissionBanner: React.FC<Props> = ({visible}) => {
  const [message, setMessage] = useState<string>("")
  
  const settingsOpened = () => {
    openSettings()
  }

    useEffect(() => {
        if(Platform.OS === "ios") {
            setMessage(`${BASE_MESSAGE}${BannerPermissionMessage.IOS}`)
        } else {
            setMessage(`${BASE_MESSAGE}${BannerPermissionMessage.ANDROID}`)
        }
    }, [])
    return (
    <Banner
      visible={visible}
      style={styles.banner}
      actions={[{
        label: 'Åpne innstillinger',
        onPress: () => settingsOpened()
      }]}
      icon={() => (
        <Avatar.Icon
            icon="map-marker-radius"
          />
      )}>
        {message}
    </Banner>
    )
}

const styles = StyleSheet.create({
  banner: {
    zIndex: 5,
    elevation: 5
  }
})

export default PermissionBanner
