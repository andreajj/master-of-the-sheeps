import React, { useState } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { Button, Colors, Headline, TextInput } from 'react-native-paper';
import { useNavigation } from "@react-navigation/native";
import auth from '@react-native-firebase/auth';
import { useRecoilState } from 'recoil';
import { userState } from '../atoms/userState';
import { RouteNames } from '../Navigation';

const Login = () => {
  // Navigation
  const navigation = useNavigation();
  const [user, setUser] = useRecoilState(userState);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordVisible, setPasswordVisible] = useState(false);

  const toRegister = () => {
    navigation.navigate(RouteNames.MainRegister)
  }

  const login = async () => {
    try {
      const user = await auth().signInWithEmailAndPassword(email, password)
      setUser((cur) => ({
        ...cur,
        hasSeenOfflineMapTutorial: !user.additionalUserInfo.isNewUser
      }));
    } catch (error: any) {
      if (error.code === 'auth/user-disabled') {
        Alert.alert(
          "Noe gikk galt",
          "Brukeren er deaktivert",
          [
            { text: "OK" }
          ]
        );
      } else if (error.code === 'auth/invalid-email') {
        Alert.alert(
          "Noe gikk galt",
          "Epost adressen du oppga er ikke gyldig",
          [
            { text: "OK" }
          ]
        );
      } else if (error.code === 'auth/user-not-found') {
        Alert.alert(
          "Noe gikk galt",
          "Brukeren ble ikke funnet",
          [
            { text: "OK" }
          ]
        );
      } else if (error.code === 'auth/wrong-password') {
        Alert.alert(
          "Noe gikk galt",
          "Feil passord",
          [
            { text: "OK" }
          ]
        );
      } else {
        Alert.alert(
          "Noe gikk galt",
          "Vennligst prøv igjen senere",
          [
            { text: "OK" }
          ]
        );
      }
    }
  }

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Headline style={styles.header}>Login</Headline>
        </View>
        <View style={styles.content}>
          <TextInput style={styles.formElement} label="Email" value={email} onChangeText={(text) => setEmail(text)} />
          <TextInput 
            secureTextEntry={!passwordVisible} 
            style={styles.formElement} 
            label="Passord" 
            right={<TextInput.Icon name="eye" onPress={() => setPasswordVisible((cur) => !cur)} />} 
            value={password} 
            onChangeText={(text) => setPassword(text)} 
          />
          <Button 
            uppercase={false}
            mode="contained"
            style={styles.formElement}
            onPress={login}
          >
            Logg På
          </Button>
          <Button 
            uppercase={false}
            mode="contained"
            style={styles.formElement}
            onPress={toRegister}
          >
            Lag ny bruker
        </Button>
        </View>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.black,
    padding: 10,
  },
  header: {
    color: Colors.white,
    textAlign: "center"
  },
  headerContainer: {
    margin: 30,
  },
  content: {
    flex: 1,
    justifyContent: "center"
  },
  formElement: {
    marginBottom: 20,
  }
})

export default Login;