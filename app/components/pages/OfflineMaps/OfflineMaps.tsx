import MapboxGL from '@react-native-mapbox-gl/maps';
import React from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { Button } from 'react-native-paper';
import { useRecoilState } from 'recoil';
import { offlineMapsState, userState } from '../../atoms';
import { useNavigation } from "@react-navigation/native";
import { deleteData } from '../../utils/Storage';
import OfflineMapsList from './OfflineMapsList';
import { RouteNames } from '../../Navigation';

const OfflineMaps = () => {
  // Navigation
  const navigation = useNavigation();

  const [offlineMaps, setOfflineMaps] = useRecoilState(offlineMapsState);
  const [user, setUser] = useRecoilState(userState);

  const goToAreaSelectorMaps = () => {
    navigation.navigate(RouteNames.OfflineMapAreaSelector)
  }

  const deleteOfflineMap = async (uuid: string) => {
    await MapboxGL.offlineManager.deletePack(uuid);
    await deleteData(uuid);
    const newState = offlineMaps.filter((offlineMap) => offlineMap.uuid !== uuid);
    setOfflineMaps(newState);
    if(user.offlineMap?.uuid === uuid) {
      setUser((cur) => {
        return {
          ...cur,
          offlineMap: undefined
        }});
      }
  }

  const onDelete = (name: string, uuid: string) => {
    Alert.alert(
      "Slett offline kart",
      `Er du sikker på at du vil slette offline kartet "${name}"?`,
      [
        {
          text: "Nei",
          style: "cancel"
        },
        { text: "Ja", onPress: () => deleteOfflineMap(uuid) }
      ]
    );
  }

  return (
    <View style={styles.container}>
      <OfflineMapsList offlineMaps={offlineMaps} onLongPress={(offlineMap) => onDelete(offlineMap.name, offlineMap.uuid)} />
      <Button 
        icon="plus"
        uppercase={false}
        mode="contained"
        style={{margin: 10}}
        disabled={!user.isOnline}
        onPress={goToAreaSelectorMaps}
      >
        Last ned nytt kart
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default OfflineMaps