import React, { useEffect, useRef, useState } from "react";
import { StyleSheet, View } from "react-native";
import { OfflineMapTutorialDialog } from "./OfflineMapTutorialDialog";
import { useNavigation } from "@react-navigation/native";
import AreaSelectorMap from "../AreaSelectorMap";
import { useRecoilState } from 'recoil';
import { userState } from '../../../atoms/userState';
import { RouteNames } from "../../../Navigation";

enum TutorialStep {
  step1,
  step2
}

const OfflineMapTutorial = () => {
  // Navigation
  const navigation = useNavigation();
  const [user, setUser] = useRecoilState(userState);

  const [visible, setVisible] = useState(true);
  const [step, setStep] = useState(TutorialStep.step1);

  const getDialogText = (step: TutorialStep) => {
    switch(step) {
      case TutorialStep.step1: {
        return "For at appen skal fungere uten nettilkobling, må du laste ned en region av kartet."
      }
      case TutorialStep.step2: {
        return "Zoom og beveg kartet til området du ønsker er dekket innenfor rammen. Trykk “Last ned kart” når du er fornøyd."
      }
    }
  }

  const ok = () => {
    switch(step) {
      case TutorialStep.step1: {
        return setStep(TutorialStep.step2)
      }
      case TutorialStep.step2: {
        setVisible(false);
        return setUser((cur) => ({
          ...cur,
          hasSeenOfflineMapTutorial: true
        }));
      }
    }
  }

  const later = () => {
    setVisible(false);
    navigation.navigate(RouteNames.TrackingMainScreen)
  }

  return (
    <View>
      <AreaSelectorMap />
      <OfflineMapTutorialDialog text={getDialogText(step)} ok={ok} later={later} visible={visible} />
    </View>
  );
}

export default OfflineMapTutorial;