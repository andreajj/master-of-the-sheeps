import * as React from 'react';
import { Button, Paragraph, Dialog, Portal, Provider } from 'react-native-paper';

interface Props {
  text: string,
  visible: boolean,
  ok: () => void,
  later: () => void,
}

const OfflineMapTutorialDialog: React.FC<Props> = ({ text, visible, ok, later}) => {
  return (
    <Portal>
    <Dialog visible={visible}>
      <Dialog.Title>Last ned kart</Dialog.Title>
      <Dialog.Content>
        <Paragraph>{text}</Paragraph>
      </Dialog.Content>
      <Dialog.Actions>
        <Button onPress={later}>Senere</Button>
        <Button onPress={ok}>Ok</Button>
      </Dialog.Actions>
    </Dialog>
  </Portal>
  );
};

export {
  OfflineMapTutorialDialog
}