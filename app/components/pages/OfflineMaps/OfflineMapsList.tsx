import React, { useEffect, useState } from 'react';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { Image, View, Text, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { OfflineMap } from '../../atoms';
import { storeData, getData } from '../../utils/Storage';

interface Props {
  offlineMaps: OfflineMap[],
  onLongPress?: (offlineMap: OfflineMap) => void,
  onPress?: (offlineMap: OfflineMap) => void,
}

const OfflineMapsList: React.FC<Props> = ({ offlineMaps, onLongPress, onPress }) => {
  const [thumbnails, setThumbnail] = useState(new Map());
  const [loading, setLoading] = useState(true);
  
  const createSnapshotImage = async (offlineMap: OfflineMap) => {
    const uri: string = await MapboxGL.snapshotManager.takeSnap({
      bounds: offlineMap.bounds,
      width: 500,
      height: 500,
      styleURL: MapboxGL.StyleURL.Outdoors,
    });
    return uri;
  }

  useEffect(() => {
    setLoading(true);
    const init = async () => {
      const uris = new Map(thumbnails);
      for (const offlineMap of offlineMaps) {
        let uri = await getData(offlineMap.uuid);
        if (!uri) {
          uri = await createSnapshotImage(offlineMap);
          storeData(offlineMap.uuid, uri)
        }
        uris.set(offlineMap.uuid, uri)
      }
      setThumbnail(uris);
      setLoading(false);
    }
    init();
  }, [offlineMaps])

  return (
    <>
      {loading ? (
        <Text style={styles.loadingText}>Laster inn offline kart...</Text>
      ) : (
        <>
          {offlineMaps.length == 0 && <Text style={styles.noMapsText}>Du har ingen nedlastede offline kart</Text>}
          <ScrollView contentContainerStyle={styles.images}>
              {thumbnails.size > 0 && offlineMaps.map((offlineMap) => {
                const uri = thumbnails.get(offlineMap.uuid);
                return (
                  <TouchableOpacity 
                    key={offlineMap.uuid} 
                    style={styles.touchable} 
                    onLongPress={() => onLongPress && onLongPress(offlineMap)}
                    onPress={() => onPress && onPress(offlineMap)}
                  >
                    <View style={styles.imageContainer}>
                      <Image source={{uri: uri}} style={styles.image} />
                      <Text style={styles.imageText}>{offlineMap.name}</Text>
                    </View>
                  </TouchableOpacity>
                )
              })}
            </ScrollView >
        </>
      )}
    </>
  )
}

const styles = StyleSheet.create({
  noMapsText: {
    textAlign: "center",
    marginTop: 20,
  },
  loadingText: {
    textAlign: "center",
    marginTop: 20,
    flex: 1,
  },
  touchable: {
    width: "40%",
  },
  images: {
    flexGrow: 1,
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 20,
  },
  imageContainer: {
    width: "100%",
    height: 150,
  },
  image: {
    flex: 1,
  },
  imageText: {
    marginTop: 5,
    textAlign: "center"
  }
});

export default OfflineMapsList;