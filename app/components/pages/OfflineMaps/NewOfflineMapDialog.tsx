import React, { useState } from 'react';
import { Alert } from 'react-native';
import {
  Button,
  Dialog,
  Portal,
  TextInput,
  Text,
} from 'react-native-paper';

interface Props {
  isDialogVisible: boolean,
  setIsDialogVisible: (bool: boolean) => void,
  setName: (name: string) => void
}

const NewOfflineMapDialog: React.FC<Props> = ({ isDialogVisible, setIsDialogVisible, setName }) => {
  const [mapName, setMapName] = useState('');

  const onSubmit = () => {
    if (mapName !== '') {
      setName(mapName)
      setIsDialogVisible(false)
    } else {
      Alert.alert(
        "Feil navngiving",
        "Navnet kan ikke være tomt",
        [
          { text: "OK" }
        ]
      );
    }
  }

  return (
    <Portal>
      <Dialog
        visible={isDialogVisible}
        onDismiss={() => setIsDialogVisible(false)}>
        <Dialog.Title>Last ned offline kart</Dialog.Title>
        <Dialog.Content>
          <Text>Navngi offline kart</Text>
        </Dialog.Content>
        <Dialog.Content>
          <TextInput
            value={mapName}
            onChangeText={text => setMapName(text)}
          />
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={() => setIsDialogVisible(false)}>Cancel</Button>
          <Button onPress={onSubmit}>Done</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  )
}

export default NewOfflineMapDialog;