import React, { useEffect, useRef, useState } from "react";
import { StyleSheet, View } from "react-native";
import MapboxGL from "@react-native-mapbox-gl/maps";
import { useRecoilState } from "recoil";
import { OfflineMap, offlineMapsState } from '../../atoms';
import Map from '../../Map';
import { Button, Colors } from "react-native-paper";
import uuid from 'react-native-uuid';
import NewOfflineMapDialog from "./NewOfflineMapDialog";
import { useNavigation } from "@react-navigation/native";
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { RouteNames } from "../../Navigation";
import { getCurrentPositionAsync, getLastKnownPositionAsync, LocationAccuracy } from "expo-location";

// array with two numbers gets interpreted as number[]
// instead of [number, number], </3 microsoft
type maxBounds = {
  ne: [number, number];
  sw: [number, number];
};

const AreaSelectorMap = () => {
  // Navigation
  const navigation = useNavigation();
  // Map ref
  const map = useRef<MapboxGL.MapView | null>(null);
  const camera = useRef<MapboxGL.Camera | null>(null);
  const refs = {
    mapRef: map,
    cameraRef: camera,
  };
  // Full current tracking state
  const [offlineMaps, setOfflineMaps] = useRecoilState(offlineMapsState);
  // Offline map name
  const [offlineMapName, setOfflineMapName] = useState('');
  const [showSetNameDialog, setShowSetNameDialog] = useState(false);
  // Maximum bounds for navigating, map cannot leave these bounds.
  // Should be set to contain all of Norway when a specific offline map is not selected
  const _maxBounds: maxBounds = {
    sw: [4.9920780, 58.078884],
    ne: [32.258960, 71.244356]
  }
  const [maxBounds, setMaxBounds] = useState<maxBounds>(_maxBounds);

  // point where map will initially start
  const [centerCoordinate, setCenterCoordinate] = useState<[number, number]>([
    10.402757159741896, 63.41773186344639,
  ]);

  const [zoomLevel, setZoomLevel] = useState(14);

  const [loading, setLoading] = useState(false);

  const progressListener = (offlineRegion: MapboxGL.OfflinePack, status: MapboxGL.OfflineProgressStatus) => {
    console.log("progress", offlineRegion, status)
    if (status.percentage === 100) {
      setLoading(false);
      navigation.navigate(RouteNames.OfflineMapList)
    }
  };
  const errorListener = (offlineRegion: MapboxGL.OfflinePack, err: MapboxGL.OfflineProgressError) => {
    console.log("error", offlineRegion, err)
    setLoading(false);
  };

  const addOfflineMap = async () => {
    const visibleBounds = await map.current?.getVisibleBounds();

    if (visibleBounds) {
      setLoading(true);

      const neBound = visibleBounds[0]
      const swBound = visibleBounds[1]
      const bounds: [[number, number], [number, number]] = [[neBound[0], neBound[1]], [swBound[0], swBound[1]]]
      const UUID = firestore().collection('users').doc(auth().currentUser?.uid).collection('offlineMaps').doc().id;

      const newMap: OfflineMap = {
        uuid: UUID,
        styleURL: MapboxGL.StyleURL.Outdoors,
        name: offlineMapName,
        bounds: bounds,
      }

      // Add new map to the local state
      setOfflineMaps([...offlineMaps, newMap])

      // Add the map to mapbox
      await MapboxGL.offlineManager.createPack({
        name: UUID,
        styleURL: MapboxGL.StyleURL.Outdoors,
        bounds: [bounds[0], bounds[1]]
      }, progressListener, errorListener) 
    }
  }

  useEffect(() => {
    if (offlineMapName !== '') {
      addOfflineMap();
    }
  }, [offlineMapName])

  const userLocation: () => Promise<[number, number]> = async () => {
    const lastKnownPosition = await getLastKnownPositionAsync({
      requiredAccuracy: 500,
    });
    if (lastKnownPosition === null) {
      const { coords } = await getCurrentPositionAsync({
        accuracy: LocationAccuracy.Balanced,
      });
      return [coords.longitude, coords.latitude]
    } else {
      const { coords } = lastKnownPosition;
      return [coords.longitude, coords.latitude]
    }
  }

  useEffect(() => {
    const init = async () => {
      const location = await userLocation();
      return setCenterCoordinate(location);
    }
    init();
  }, [maxBounds])

  const onSubmit = () => {
    setShowSetNameDialog(true)
  }

  return (
    <View style={styles.container}>
      <View style={{flex: 1}}>
      <View style={styles.shadow} pointerEvents="none"></View>
        <Map maxBounds={maxBounds} zoomLevel={zoomLevel} centerCoordinate={centerCoordinate} ref={refs}>
            <MapboxGL.UserLocation showsUserHeadingIndicator visible />
        </Map>
      </View>
      <Button 
        icon="plus"
        uppercase={false}
        mode="contained"
        loading={loading}
        disabled={loading}
        style={{margin: 10}}
        onPress={onSubmit}
      >
        Last ned kart
      </Button>
      <NewOfflineMapDialog 
        isDialogVisible={showSetNameDialog} 
        setIsDialogVisible={(bool) => setShowSetNameDialog(bool)} 
        setName={(name) => setOfflineMapName(name)}  
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    backgroundColor: "white",
  },
  shadow: {
    borderWidth: 3,
    borderColor: Colors.blue500,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 5,
    position: 'absolute',
    overflow: 'visible',
    elevation: 5,
    margin: 5,
    shadowColor: Colors.blue500,
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 1,
      width: 1
    }
  },
});

export default AreaSelectorMap;
