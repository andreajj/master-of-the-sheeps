import React,  {useState} from 'react';
import { StyleSheet, View, SafeAreaView } from "react-native";
import { Colors, Button, Text } from "react-native-paper";
import { useRecoilState } from "recoil";
import { farmsState } from '../../atoms/farms';
import { FarmType } from '../../atoms';
import { getFarms } from '../../utils/Firebase';
import FarmsList from './FarmsList';
import { useNavigation } from "@react-navigation/native";
import { RouteNames } from "../../Navigation"

const FarmsOverview = () => {
    const navigation = useNavigation();
    const [farms, setFarms] = useRecoilState(farmsState);
    const [selectedFarm, setSelectedFarm] = useState('')

    const deleteFarm = (deleteName: string) => {
        if(deleteName==='') return;

        setFarms((cur) => {
            const newFarms = cur.farmsList.filter((f) => f.name !== deleteName)
            return {
                ...cur,
                farmsList: newFarms
            }
        })
    }    

    const loadFarms = async () => {
        const allFarms = await getFarms();
        const _farms: Array<FarmType> = [];
        allFarms.forEach((farm) => {
            const data = farm.data();
            const newFarm: FarmType = {
                name: farm.id,
                color1: data.color1,
                owner: {
                    uuid: data.owner.uuid,
                    email: data.owner.email
                }
            }
            if (data.color2) {
                newFarm.color2 = data.color2
            }
            _farms.push(newFarm);
        })
        setFarms((cur) => ({
            ...cur,
            farmsList: _farms
        }));
    }
      
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.buttonContainer}>
                <Button 
                    onPress={() => navigation.navigate(RouteNames.FarmsSearch)} 
                    style={styles.button} 
                    labelStyle={styles.buttonLabel} 
                    icon="magnify"
                    mode="contained" 
                >
                        Søk etter gårder
                </Button>
                
                    {selectedFarm!=='' && <Button 
                    onPress={() => {
                        deleteFarm(selectedFarm)
                        setSelectedFarm('')
                    }} 
                    style={styles.deleteButton} 
                    labelStyle={styles.buttonLabel} 
                    mode="contained" >
                    Fjern gården
                </Button>}
            </View>
            <View style={styles.listContainer}>
                <Text style={styles.helperText}>Lagrede gårder: {farms.farmsList.length}</Text>
                <FarmsList farms={farms.farmsList} selectedFarm={selectedFarm} setSelectedFarm={setSelectedFarm} />
            </View>
        </SafeAreaView>
    )
}

export default FarmsOverview;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        padding: 5,
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%"
    },
    buttonContainer: {
        alignItems: 'center',
        width: "100%",
        marginVertical: 10,
        height: 120
    },
      button: {
        width: '80%',
        borderRadius: 0,
        marginVertical: 10,
    },
    buttonLabel: {
        color: Colors.white,
    },
    deleteButton: {
        width: '80%',
        borderRadius: 0,
        backgroundColor: Colors.red500,
        color: Colors.white,
        marginVertical: 10,
    },
    helperText: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
        padding: 5,
    },
    listContainer: {
        marginBottom: 20,
        width: "100%",
        flex: 1,
    }
})