import React,  {useEffect, useState} from 'react';
import { StyleSheet, View, SafeAreaView } from "react-native";
import { Colors, Button, Searchbar, Text } from "react-native-paper";
import { useRecoilState } from "recoil";
import { farmsState } from '../../atoms/farms';
import { FarmType } from '../../atoms';
import { getFarms } from '../../utils/Firebase';
import FarmsList from './FarmsList';
import { useNavigation } from "@react-navigation/native";

const FarmsSearch = () => {
    const navigation = useNavigation();
    const [farms, setFarms] = useRecoilState(farmsState);
    const [selectedFarm, setSelectedFarm] = useState('');
    const [fetchedFarms, setFetchedFarms] = useState<FarmType[]>([]);
    const [searchValue, setSearchValue] = useState('');

    const searchByName = (name: string) => {
        if (name==='') return [];
        const results = fetchedFarms.filter((f) => {
            return f.name.toLowerCase().includes(name.toLowerCase());
        })
        return results;
    }

    useEffect(() => {
        loadAllFarms()
    }, [])
    

    const loadAllFarms = async () => {
        const allFarms = await getFarms();
        const _farms: Array<FarmType> = [];
        allFarms.forEach((farm) => {
            const data = farm.data();
            const newFarm: FarmType = {
                name: farm.id,
                color1: data.color1,
                owner: {
                    uuid: data.owner.uuid,
                    email: data.owner.email
                }
            }
            if (data.color2) {
                newFarm.color2 = data.color2
            }
            _farms.push(newFarm);
        })
        setFetchedFarms(_farms)
    }

    const selectedFarmExistsInFarmList = (): boolean => {
        const dupe = farms.farmsList.find((f) => {
            return f.name===selectedFarm;
        })
        if (dupe===undefined) {
            return false
        } else return true
    }

    const saveFarm = () => {
        if(selectedFarm==='') return;
        if(!selectedFarmExistsInFarmList()) {
            const farmToSave = fetchedFarms.filter((f) => {
                return f.name===selectedFarm;
            })
            setFarms({
                ...farms,
                farmsList: [...farms.farmsList, farmToSave[0]]
            })
        } else {
            return
        }
    }
    
    
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.buttonContainer}>
                <Searchbar
                    style={styles.searchBar}
                    placeholder="Søk etter gårdsnavn"
                    onChangeText={(text: string) => setSearchValue(text)}
                    value={searchValue}
                />
                {<Button 
                    onPress={() => saveFarm()} 
                    style={styles.button}
                    disabled={selectedFarm=='' || selectedFarmExistsInFarmList()}
                    labelStyle={styles.buttonLabel} 
                    mode="contained" >
                        {selectedFarm=='' ? 'Velg en gård' : !selectedFarmExistsInFarmList() ? "Lagre gården" : "Allerede lagret"}
                    </Button>}
            </View>
            <View style={styles.listContainer}>
                <Text style={styles.helperText}>Søkeresultater: {searchByName(searchValue).length}</Text>
                <FarmsList farms={searchByName(searchValue)} selectedFarm={selectedFarm} setSelectedFarm={setSelectedFarm} />
            </View>
            <Button style={styles.doneButton} icon="page-previous-outline" mode="contained" onPress={() => navigation.goBack()}>
                Ferdig
          </Button>
        </SafeAreaView>
    )
}

export default FarmsSearch;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        padding: 5,
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%",
        display: "flex",
    },
    buttonContainer: {
        alignItems: 'center',
        justifyContent: 'space-evenly',
        width: "100%",
        flex: 1,
    },
    listContainer: {
        width: "100%",
        flex: 2,
    },
      button: {
        width: '80%',
        borderRadius: 0,
    },
    buttonLabel: {
        color: Colors.white,
    },
    deleteButton: {
        width: '80%',
        borderRadius: 0,
        backgroundColor: Colors.red500,
        color: Colors.white,
    },
    helperText: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
        padding: 5,
    },
    searchBar: {
        width: "90%",
        alignSelf: 'center'
    },
    doneButton: {
        opacity: 1,
        marginVertical: 10,
    }
})