import React, {useEffect} from "react";
import { View, StyleSheet, FlatList} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Colors, Text } from "react-native-paper";
import { SafeAreaView } from "react-native-safe-area-context";
import { FarmType } from "../../atoms";

interface Props {
    farms: Array<FarmType>,
    setSelectedFarm: React.Dispatch<React.SetStateAction<string>>,
    selectedFarm: string
}

interface RowProps {
    color1: string,
    color2?: string,
    name: string,
    index: number,
}


const FarmsList: React.FC<Props> = ({farms, setSelectedFarm, selectedFarm}) => {

    const handleSelection = (name: string) => {
        if(selectedFarm!==name) {
            setSelectedFarm(name)
        } else {
            setSelectedFarm('')
        }
    }

    const SingleColorItem: React.FC<RowProps> = ({color1, name, index}) => {
        return (
            <View style={[styles.row, index % 2 ? {backgroundColor: Colors.grey400} : {backgroundColor: Colors.grey300}]}>
                <TouchableOpacity 
                onPress={() => handleSelection(name)}
                style={styles.touchable} >
                <View key={name} style={[styles.rowContent, selectedFarm===name ? styles.editedRow : null, {backgroundColor: color1}]}>
                    <Text style={[styles.rowText, styles.text]}>{name}</Text>
                </View>
                </TouchableOpacity>
            </View>
        )
    }

    const TwoColorItem: React.FC<RowProps> = ({color1, color2, name, index}) => {
        return (
            <View style={[styles.row, index % 2 ? {backgroundColor: Colors.grey400} : {backgroundColor: Colors.grey300}]}>
                <TouchableOpacity 
                onPress={() => handleSelection(name)}
                style={styles.touchable} >
                <View key={name} style={[styles.twoColorItemContainer, selectedFarm===name ? styles.editedRow : null]} > 
                    <View style={[styles.twoColorRowContent, {backgroundColor: color1}]}/>
                    <View style={[styles.twoColorRowContent, {backgroundColor: color2}]}/>
                    <Text style={[styles.twoColorRowText, styles.text]}>{name}</Text>
                </View>
                </TouchableOpacity>
            </View>
        )
    }

    const renderItem = ({item, index}) => {
        return (
            item.color2 ? 
                <TwoColorItem color1={item.color1} color2={item.color2} name={item.name} index={index}/>
                : <SingleColorItem color1={item.color1} name={item.name} index={index}/>
        )
    }
    
    return (
        <>
        {/* <View style={styles.helperText}>
            <Text>Lagrede gårder: {farms.length}</Text>
        </View> */}
        <SafeAreaView  style={styles.colorsListContainer}>
            <FlatList
            data={farms}
            renderItem={renderItem}
            keyExtractor={item => item.name}
            />
        </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({
  colorsListContainer: {
    zIndex: 9999,
    flex: 1,
    borderRadius: 1,
    borderStyle: 'solid',
    borderColor: Colors.black,
    borderWidth: 1,
    backgroundColor: Colors.grey300,
    minWidth: "100%"
  },
  helperText: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    padding: 5,
    marginBottom: 0
  },
  // row stuff
  row: {
    flexDirection: "row",
    justifyContent: "center",
    flex: 1,
    padding: 3,
    alignItems: 'center',
    width: "100%"
  },
  rowText: {
    alignSelf: 'center'
  },
  touchable: {
    width: "100%",
    height: "100%"
  },
  text: {
    fontSize: 18,
    color: Colors.white,
    textShadowColor: 'black',
    textShadowRadius: 10,
    textShadowOffset: {height: 1, width: -1}
  },
  twoColorItemContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: 'center',
    width: "100%",
    height: 60
  },
  rowContent: {
    minWidth: "100%",
    height: "100%",
    padding: 15,
  },
  twoColorRowContent: {
    width: "50%",
    padding: 5
  },
  twoColorRowText: {
    flexDirection: "row",
    alignSelf: 'center',
    textAlign:"center",
    marginLeft: "35%",
    position: "absolute",
  },
  editedRow: {
    borderStyle: 'dashed',
    borderColor: 'red',
    borderWidth: 3,
    borderRadius: 1,
  },
});

export default FarmsList;
