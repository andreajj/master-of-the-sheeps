import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Colors } from 'react-native-paper';
import { FirebaseFirestoreTypes } from '@react-native-firebase/firestore';
import XDate from 'xdate';
// XDate does not know norwegian date names, provide them
import {NORWEGIAN_DATE_NAMES} from '../../utils/dateUtils'
interface Props {
  index: number,
  documentData: FirebaseFirestoreTypes.DocumentData,
  documentId: string,
}

// formatting for XDate string
const FORMAT_STRING = "d MMMM yyyy, HH:mm:ss"

const handleDateTime = (date: Date) => {
  const d = new XDate(date)
  XDate.locales['no'] = {
    monthNames: NORWEGIAN_DATE_NAMES.monthNames,
    dayNames: NORWEGIAN_DATE_NAMES.dayNames,
    dayNamesShort: NORWEGIAN_DATE_NAMES.dayNamesShort
  };
  return (d.toString(FORMAT_STRING, 'no'))
}

const TrackingsRow: React.FC<Props> = ({ index, documentData, documentId }) => { 

  return (
    <View style={[styles.row, index % 2 ? { backgroundColor: Colors.grey400 } : { backgroundColor: Colors.grey300 } ]}>
      <Text>
        <Text style={styles.boldText}>
          startdato og tid:
        </Text>{` ${handleDateTime(documentData.startTimestamp.toDate())}`}
      </Text>
      <Text>
        <Text style={styles.boldText}>
          sluttdato og tid:
        </Text>
        {` ${handleDateTime(documentData.endTimestamp.toDate())}`}
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: "column",
    justifyContent: "space-between",
    flex: 1,
    padding: 10,
  },
  boldText: {
    fontWeight: "bold"
  }
});

export default TrackingsRow;