import React, { useEffect, useRef, useState } from 'react';
import { View, StyleSheet, ScrollView, Alert, Text, Animated, TouchableOpacity } from 'react-native';
import { Button, Colors } from 'react-native-paper';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { RectButton } from 'react-native-gesture-handler';
import TrackingsRow from './TrackingsRow';
import firestore, { firebase, FirebaseFirestoreTypes} from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { useRecoilState, useRecoilValue } from 'recoil';
import { currentTrackingState, offlineMapsState, OtherType, PredatorType, SheepHerdType } from '../../atoms';
import { useNavigation } from "@react-navigation/native";
import { getTracking } from '../../utils/Firebase';
import { userState } from '../../atoms/userState';
import { RouteNames } from '../../Navigation';

const TrackingsList: React.FC = () => {
  const navigation = useNavigation();
  const [trackingState, setTrackingState] = useRecoilState(currentTrackingState);
  const offlineMaps = useRecoilValue(offlineMapsState);
  const [user, setUser] = useRecoilState(userState);
  const [trackings, setTrackings] = useState<Array<FirebaseFirestoreTypes.QueryDocumentSnapshot<FirebaseFirestoreTypes.DocumentData>>>([]);
  const trackingsRefs: Array<Swipeable | null> = [];

  const Swipe: React.FC<{trackingUuid: string}> = ({ trackingUuid }) => {
    const [localLoadingEdit, setlocalLoadingEdit] = useState(false);
    const [localLoadingDelete, setlocalLoadingDelete] = useState(false);

    const deletion = async () => {
      setlocalLoadingDelete(true);
      await deleteTracking(trackingUuid);
      setlocalLoadingDelete(false);
    }

    const edit = async () => {
      setlocalLoadingEdit(true);
      await editTracking(trackingUuid);
      setlocalLoadingEdit(false);
      navigation.navigate(RouteNames.TrackingMainScreen);
    }

    return (
      <>
      <Button loading={localLoadingDelete} disabled={localLoadingEdit || localLoadingDelete} mode="text" style={styles.deleteButton} labelStyle={styles.labelStyle} onPress={deletion}>
        <Text
          style={[
            {
              color: Colors.white,
              textAlign: "center"
            }
          ]}>
          Slett
        </Text>
      </Button>
      <Button loading={localLoadingEdit} disabled={localLoadingEdit || localLoadingDelete} mode="text" style={styles.editButton} labelStyle={styles.labelStyle} onPress={edit}>
        <Text
          style={[
            {
              color: Colors.white,
              textAlign: "center"
            }
          ]}>
          Rediger
        </Text>
      </Button>
      </>
    )
  }

  const swipe = (progress: Animated.AnimatedInterpolation, dragX: Animated.AnimatedInterpolation, index: number, trackingUuid: string) => {
    return <Swipe trackingUuid={trackingUuid} />
  };

  useEffect(() => {
    const subscriber = firestore()
      .collection('trackings')
      .where('owner', '==', auth().currentUser?.uid)
      .orderBy('startTimestamp', 'desc')
      .onSnapshot(querySnapshot => {
        setTrackings(querySnapshot ? querySnapshot.docs : [])
      });
    return () => subscriber();
  }, [])

  const showDatabaseErrorAlert = () => {
    Alert.alert(
      "Noe gikk galt!",
      "Noe gikk galt under slettingen av sporingen",
      [
        { text: "OK" }
      ]
    );
  }

  const deleteTracking = async (trackingUuid: string) => {
    try {
      const res = await firestore()
        .collection('trackings')
        .doc(trackingUuid)
        .delete();
    } catch (e: unknown) {Text
      console.log(e);
      showDatabaseErrorAlert();
    }
  }

  const editTracking = async (trackingUuid: string) => {
    if (trackingState.isTracking) {
      return Alert.alert(
        "Sporing allerede aktiv",
        "Venligst stop den aktive sporingen før du redigerer en annen.",
        [
          { text: "OK" }
        ]
      );
    }
    
    try {
      const newState = await getTracking(trackingUuid);

      if (!user.isOnline && !offlineMaps.find((map) => map.uuid === newState.offlineMap.uuid)) {
        return Alert.alert(
          "Offline kart finnes ikke",
          "Offline-kartet assosiert med tilsynsrunden er slettet. For å redigere tilsynsrunden må du være tilkoblet nettet.",
          [
            { text: "OK" }
          ]
        );
      }

      setTrackingState(newState)
    } catch (e: unknown) {
      Alert.alert(
        "Noe gikk galt!",
        "Noe gikk galt under hentingen av sporingen.",
        [
          { text: "OK" }
        ]
      );
    }
  }

  return (
    <View style={styles.herdView}>
      {trackings.length == 0 && <Text style={styles.noTrackingsText}>Ingen sporinger å vise</Text>}
      <ScrollView contentContainerStyle={styles.containerHerdView} style={styles.herdScrollView}>
        {trackings.map((document, i) => (
          <Swipeable ref={ref => trackingsRefs.push(ref)} key={document.id} containerStyle={{width: "100%"}} renderRightActions={(progress, dragX) => swipe(progress, dragX, i, document.id)}>
            <TouchableOpacity activeOpacity={1} onPress={() => trackingsRefs[i]?.openRight()}>
              <TrackingsRow documentId={document.id} documentData={document.data()} index={i} />
            </TouchableOpacity>
          </Swipeable>
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  containerHerdView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  herdScrollView: {
    backgroundColor: Colors.white,
  },
  herdView: {
    width: '100%',
    flex: 1,
  },
  deleteButton: {
    width: 100,
    borderRadius: 0,
    alignContent: "center",
    justifyContent: "center",
    flexDirection: 'column',
    backgroundColor: Colors.red500,
  },
  editButton: {
    width: 100,
    borderRadius: 0,
    alignContent: "center",
    justifyContent: "center",
    flexDirection: 'column',
    backgroundColor: Colors.blue500,
  },
  labelStyle: {
    fontSize: 12,
  },
  noTrackingsText: {
    textAlign: "center",
    paddingTop: 20,
    backgroundColor: Colors.white,
  },
});

export default TrackingsList