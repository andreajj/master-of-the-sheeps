import React, { useState } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { Button, Headline, TextInput, Colors} from 'react-native-paper';
import { useNavigation } from "@react-navigation/native";
import auth from '@react-native-firebase/auth';
import { useRecoilState } from 'recoil';
import { userState } from '../atoms/userState';
import { RouteNames } from '../Navigation';

const Register = () => {
  // Navigation
  const navigation = useNavigation();
  const [user, setUser] = useRecoilState(userState);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [passwordVisible, setPasswordVisible] = useState(false);

  const toLogin = () => {
    navigation.navigate(RouteNames.MainLogin)
  }

  const register = async () => {
    if (password != password2) {
      return (
        Alert.alert(
          "Noe gikk galt",
          "Passordene dine er ikke like, sørg for at 'Passord' er lik 'Gjenta passord'",
          [
            { text: "OK" }
          ]
        )
      )
    }
    try {
      const res = await auth().createUserWithEmailAndPassword(email, password);
      setUser((cur) => ({
        ...cur,
        hasSeenOfflineMapTutorial: !res.additionalUserInfo.isNewUser
      }));
    } catch (error: any) {
      if (error.code === 'auth/email-already-in-use') {
        Alert.alert(
          "Noe gikk galt",
          "Epost adressen du brukte er allerede i bruk",
          [
            { text: "OK" }
          ]
        );
      } else if (error.code === 'auth/invalid-email') {
        Alert.alert(
          "Noe gikk galt",
          "Epost adressen du oppga er ikke gyldig",
          [
            { text: "OK" }
          ]
        );
      } else if (error.code === 'auth/weak-password') {
        Alert.alert(
          "Noe gikk galt",
          "Passordet du oppga er for svakt. Minimum antall bokstaver og tegn er 6.",
          [
            { text: "OK" }
          ]
        );
      } else {
        Alert.alert(
          "Noe gikk galt",
          "Vennligst prøv igjen senere",
          [
            { text: "OK" }
          ]
        );
      }
    }
  }

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Headline style={styles.header}>Lag ny bruker</Headline>
        </View>
        <View style={styles.content}>
          <TextInput style={styles.formElement} label="Email" value={email} onChangeText={(text) => setEmail(text)} />
          <TextInput 
            secureTextEntry={!passwordVisible} 
            style={styles.formElement} 
            label="Passord" 
            right={<TextInput.Icon name="eye" 
            onPress={() => setPasswordVisible((cur) => !cur)} />} 
            value={password} 
            onChangeText={(text) => setPassword(text)} 
          />
          <TextInput 
            secureTextEntry={!passwordVisible} 
            style={styles.formElement} 
            label="Gjenta passord" 
            right={<TextInput.Icon name="eye" 
            onPress={() => setPasswordVisible((cur) => !cur)} />} 
            value={password2}
            onChangeText={(text) => setPassword2(text)} 
          />
          <Button 
            uppercase={false}
            mode="contained"
            style={styles.formElement}
            onPress={register}
          >
            Registrer bruker
          </Button>
          <Button 
            uppercase={false}
            mode="contained"
            style={styles.formElement}
            onPress={toLogin}
          >
            Tilbake til login
        </Button>
        </View>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.black,
    padding: 10,
  },
  header: {
    color: Colors.white,
    textAlign: "center"
  },
  headerContainer: {
    margin: 30,
  },
  content: {
    flex: 1,
    justifyContent: "center"
  },
  formElement: {
    marginBottom: 20,
  }
})

export default Register;