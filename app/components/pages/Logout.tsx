import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, Headline, TextInput, Colors} from 'react-native-paper';
import { useNavigation } from "@react-navigation/native";
import auth from '@react-native-firebase/auth';

const Register = () => {

  const logout = () => {
    auth().signOut();
  }

  useEffect(() => {
    logout();
  }, [])

    return (
      <View style={styles.container}>
        <Headline style={styles.header}>Logger deg ut...</Headline>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignContent: "center",
    justifyContent: "center"
  },
  header: {
    color: Colors.black,
    textAlign: "center"
  },
})

export default Register;