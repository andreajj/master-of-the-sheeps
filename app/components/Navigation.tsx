import React, { useCallback, useEffect, useRef, useState } from 'react';
import { NavigationContainer, NavigationContainerRef } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth';
import Sidebar from './Sidebar';
import { useRecoilState} from 'recoil';
import NetInfo, { NetInfoState } from '@react-native-community/netinfo';
import { BackHandler } from 'react-native';

//Header
import TopBar from "./TopBar";
// Pages
import TrackingMap from './pages/Tracking/TrackingMap';
import TrackingsList from './pages/Trackings/TrackingsList';
import NewSheepHerd from './pages/Tracking/NewSheepHerd';
import NewPredator from './pages/Tracking/NewPredator';
import NewOther from './pages/Tracking/NewOther';
import NewSheep from './pages/Tracking/NewSheep';
import NewDeadSheep from "./pages/Tracking/NewDeadSheep";
//
import AreaSelectorMaps from './pages/OfflineMaps/AreaSelectorMap';
import OfflineMaps from './pages/OfflineMaps/OfflineMaps';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
// Color
import FarmsOverview from './pages/FarmPicker/FarmsOverview';
import FarmsSearch from './pages/FarmPicker/FarmsSearch';
import OfflineMapTutorial from './pages/OfflineMaps/tutorial/OfflineMapTutorial';
import { userState } from './atoms/userState';
import OfflineMapSelector from './pages/Tracking/Offline/OfflineMapSelector';
import SheepHerdList from './pages/Tracking/SheepHerdList.tsx';
import NewSheepHerdImages from './pages/Tracking/NewSheepHerdImages';

// Navigation system
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export enum RouteNames {
  TrackingMainScreen = "TrackingScreen",
  TrackingSheepHerdScreen = "TrackingSheepHerdScreen",
  TrackingSheepAndLambListScreen = "TrackingSheepAndLambListScreen",
  TrackingSheepHerdImagesScreen = "TrackingSheepHerdImagesScreen",
  TrackingSheepScreen = "TrackingSheepScreen",
  TrackingPredatorScreen = "TrackingPredatorScreen",
  TrackingOtherScreen = "TrackingOtherScreen",
  TrackingDeadSheepScreen = "TrackingDeadSheepScreen",
  TrackingOfflineMapSelector = "TrackingOfflineMapSelector",
  OfflineMapAreaSelector = "OfflineMapAreaSelector",
  OfflineMapList = "OfflineMapList",
  OfflineMapTutorial = "OfflineMapTutorial",
  DrawerSheepTracking = "SheepTracking",
  DrawerDownloadedMaps = "DownloadedMaps",
  DrawerTrackings = "Trackings",
  DrawerFarmsOverview = "FarmsOverviewScreen",
  FarmsSearch = "FarmsSearchScreen",
  DrawerLogout = "logout",
  MainLogin = "login",
  MainRegister = "register",
  MainLanding = "landing"
}

function TrackingStack() {
  return (
    <Stack.Navigator initialRouteName={RouteNames.TrackingMainScreen}>
      <Stack.Screen
        name={RouteNames.TrackingMainScreen}
        component={TrackingMap}
        options={{
          title: 'Tracking Screen',
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={RouteNames.TrackingSheepHerdScreen}
        component={NewSheepHerd}
        options={{
          title: 'Tracking Sheep herd Screen',
          headerShown: false,
          cardStyle: {
            backgroundColor: 'transparent',
          },
          presentation: 'modal'
        }}
      />
      <Stack.Screen
        name={RouteNames.TrackingSheepAndLambListScreen}
        component={SheepHerdList}
        options={{
          title: 'Tracking Sheep and lamb list Screen',
          headerShown: false,
          cardStyle: {
            backgroundColor: 'transparent',
          },
          presentation: 'modal'
        }}
      />
      <Stack.Screen
        name={RouteNames.TrackingSheepHerdImagesScreen}
        component={NewSheepHerdImages}
        options={{
          title: 'Tracking Sheep herd images Screen',
          headerShown: false,
          cardStyle: {
            backgroundColor: 'transparent',
          },
          presentation: 'modal'
        }}
      />
      <Stack.Screen
        name={RouteNames.TrackingSheepScreen}
        component={NewSheep}
        options={{
          title: 'Tracking Sheep Screen',
          headerShown: false,
          cardStyle: {
            backgroundColor: 'transparent',
          },
          presentation: 'modal'
        }}
      />
      <Stack.Screen
        name={RouteNames.TrackingPredatorScreen}
        component={NewPredator}
        options={{
          title: 'Tracking New Predator Screen',
          headerShown: false,
          cardStyle: {
            backgroundColor: 'transparent',
          },
          presentation: 'modal'
        }}
      />
      <Stack.Screen
        name={RouteNames.TrackingOtherScreen}
        component={NewOther}
        options={{
          title: 'Tracking New Other Screen',
          headerShown: false,
          cardStyle: {
            backgroundColor: 'transparent',
          },
          presentation: 'modal'
        }}
      />
      <Stack.Screen
        name={RouteNames.TrackingDeadSheepScreen}
        component={NewDeadSheep}
        options={{
          title: 'Tracking New Dead Sheep SCreen',
          headerShown: false,
          cardStyle: {
            backgroundColor: 'transparent',
          },
          presentation: 'modal'
        }}
      />
      <Stack.Screen
        name={RouteNames.TrackingOfflineMapSelector}
        component={OfflineMapSelector}
        options={{
          title: 'Offline map selector',
          headerShown: false,
          cardStyle: {
            backgroundColor: 'transparent',
          },
          gestureEnabled: false,
          presentation: 'modal'
        }}
      />
  </Stack.Navigator>
  )
}

function OfflineMapsStack() {
  return (
    <Stack.Navigator initialRouteName={RouteNames.OfflineMapList}>
      <Stack.Screen
        name={RouteNames.OfflineMapAreaSelector}
        component={AreaSelectorMaps}
        options={{
          title: 'Offline Maps Selector Screen',
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={RouteNames.OfflineMapList}
        component={OfflineMaps}
        options={{
          title: 'Offline Maps List Screen',
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={RouteNames.OfflineMapTutorial}
        component={OfflineMapTutorial}
        options={{
          title: 'Offline Maps List Screen',
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  )
}

function FarmsStack() {
  return (
    <Stack.Navigator initialRouteName={RouteNames.DrawerFarmsOverview}>
      <Stack.Screen
      name={RouteNames.DrawerFarmsOverview}
      component={FarmsOverview}
      options={{
        title: "Farms Overview Screen",
        headerShown: false
      }}
      />
      <Stack.Screen
      name={RouteNames.FarmsSearch}
      component={FarmsSearch}
      options={{
        title: "Farms Overview Screen",
        headerShown: false
      }}
      />
    </Stack.Navigator>
  )
}

function DrawerStack() {
  return (
    <Drawer.Navigator initialRouteName={RouteNames.DrawerSheepTracking} drawerContent={(props) => <Sidebar {...props} />}>
      <Drawer.Screen
        name={RouteNames.DrawerSheepTracking}
        options={{
          header: (props) => (<TopBar title={"Kart"} navigation={props.navigation} />),
          drawerLabel: 'Sporing av sau',
        }}
        component={TrackingStack}
      />
      <Drawer.Screen
        name={RouteNames.DrawerDownloadedMaps}
        options={{
          header: (props) => (<TopBar title={"Nedlastede kart"} navigation={props.navigation} />),
          drawerLabel: 'Nedlastede kart',
        }}
        component={OfflineMapsStack}
      />
      <Drawer.Screen
        name={RouteNames.DrawerTrackings}
        options={{
          header: (props) => (<TopBar title={"Mine sporinger"} navigation={props.navigation} />),
          drawerLabel: 'Mine sporinger',
        }}
        component={TrackingsList}
      />
        <Drawer.Screen
          name={RouteNames.DrawerFarmsOverview}
          options={{
            header: (props) => (<TopBar title={"Velg gårder (øremerker)"} navigation={props.navigation} />),
            drawerLabel: 'Velg gårder',
          }}
          component={FarmsStack}
        />
      <Drawer.Screen
        name={RouteNames.DrawerLogout}
        options={{
          header: (props) => (<TopBar title={"Logg ut"} navigation={props.navigation} />),
          drawerLabel: 'Logg ut',
        }}
        component={Logout}
      />
    </Drawer.Navigator>
  )
}

const Navigation = () => {
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState<FirebaseAuthTypes.User | null>(null);
  const navigationRef = useRef<NavigationContainerRef<ReactNavigation.RootParamList> | null>(null);
  // state
  const [userStateObject, setUserStateObject] = useRecoilState(userState);

  // Handle user state changes
  function onAuthStateChanged(user: FirebaseAuthTypes.User | null) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  const onNetworkStateChanged = (state: NetInfoState) => {
    if (state.isConnected !== null && state.isConnected !== undefined) {
      setUserStateObject((cur) => {
        return {
        ...cur,
        isOnline: state.isConnected
      }})
    }
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    const networkSubscriber = NetInfo.addEventListener(onNetworkStateChanged);

    return () => {
      subscriber()
      networkSubscriber()
    };
  }, []);

  // Function to extract the current route name from the navigation state
  const getCurrentRouteState = (state) => {
    const routes = state?.routes;
    if (routes) {
      const lowerState = routes[state.index];
      if (lowerState.state) {
        return getCurrentRouteState(lowerState.state)
      } else {
        return lowerState.name
      }
    } else {
      return state.name
    }
  }

  const callback = useCallback(() => {
    const navState = navigationRef.current?.getState();
    const currentRoute = getCurrentRouteState(navState)
    /* True means that you arent allowed to click the back button / gesture back
       False means that you are allowed to click the back button / gesture back
    */
    console.log("Current route name: ", currentRoute)
    switch(currentRoute) {
      case RouteNames.TrackingMainScreen: {
        return true;
      }
      case RouteNames.TrackingSheepHerdScreen: {
        return false;
      }
      case RouteNames.TrackingSheepAndLambListScreen: {
        return false;
      }
      case RouteNames.TrackingSheepHerdImagesScreen: {
        return true;
      }
      case RouteNames.TrackingSheepScreen: {
        return true;
      }
      case RouteNames.TrackingPredatorScreen: {
        return false;
      }
      case RouteNames.TrackingOtherScreen: {
        return false;
      }
      case RouteNames.TrackingDeadSheepScreen: {
        return false;
      }
      case RouteNames.OfflineMapAreaSelector: {
        return false;
      }
      case RouteNames.OfflineMapList: {
        return true;
      }
      case RouteNames.OfflineMapTutorial: {
        return false;
      }
      case RouteNames.DrawerSheepTracking: {
        return true;
      }
      case RouteNames.DrawerDownloadedMaps: {
        return true;
      }
      case RouteNames.DrawerTrackings: {
        return false;
      }
      case RouteNames.DrawerFarmsOverview: {
        return false;
      }
      case RouteNames.FarmsSearch: {
        return false;
      }
      case RouteNames.DrawerLogout: {
        return true;
      }
      case RouteNames.MainLogin: {
        return true;
      }
      case RouteNames.MainRegister: {
        return true;
      }
      case RouteNames.MainLanding: {
        return true;
      }
      default: {
        return true;
      }
    }
  }, [navigationRef.current, getCurrentRouteState]);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', callback);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', callback);
    }
  }, [navigationRef.current, getCurrentRouteState])

  useEffect(() => {
    if (!user) {
      navigationRef.current?.navigate(RouteNames.MainLogin)
    } else {
      if (userStateObject.hasSeenOfflineMapTutorial) {
        navigationRef.current?.navigate(RouteNames.MainLanding);
      } else {
        navigationRef.current?.navigate(RouteNames.MainLanding, { 
          screen: "DownloadedMaps",
          params: {
            screen: 'OfflineMapTutorial',
         }
        });
      }
    }
  }, [user, userStateObject]);

  if (initializing) return null;
  
    return (
        <NavigationContainer ref={navigationRef}>
          <Stack.Navigator initialRouteName={RouteNames.MainLanding}>
            <Stack.Screen name={RouteNames.MainLogin} component={Login} options={{headerShown: false, gestureEnabled: false}} />
            <Stack.Screen name={RouteNames.MainRegister} component={Register} options={{headerShown: false, gestureEnabled: false}} />
            <Stack.Screen name={RouteNames.MainLanding} component={DrawerStack} options={{headerShown: false, gestureEnabled: false}} />
          </Stack.Navigator>
        </NavigationContainer>
    );
};

export default Navigation;