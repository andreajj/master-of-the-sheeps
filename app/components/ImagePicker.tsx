import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import * as SystemImagePicker from 'expo-image-picker';
import { Button, Colors, IconButton, Text } from 'react-native-paper';
import Carousel from 'react-native-snap-carousel';
import * as FileSystem from 'expo-file-system';
import { manipulateAsync, FlipType, SaveFormat } from 'expo-image-manipulator';

export interface ImageType {
  base64: string,
  height?: number,
  width?: number,
}

interface Props {
  images: Array<ImageType>,
  setImages: React.Dispatch<React.SetStateAction<Array<ImageType>>>;
}

interface CaruselProps extends Props {
  item: ImageType, 
  index: number
}

const ImagePicker: React.FC<Props> = ({images, setImages}) => {
  let openImagePickerAsync = async () => {
    let permissionResult = await SystemImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("Permission to access camera roll is required!");
      return;
    }

    let pickerResult = await SystemImagePicker.launchImageLibraryAsync({
      mediaTypes: SystemImagePicker.MediaTypeOptions.Images,
      base64: true,
    });

    if (pickerResult.cancelled === true) {
      return;
    }

    const manipResult = await manipulateAsync(
      pickerResult.uri,
      [],
      { compress: 0, format: SaveFormat.JPEG, base64: true }
    );

    let base64 = manipResult.base64;

    if (!base64) {
      const res = await FileSystem.readAsStringAsync(manipResult.uri, { 
        encoding: FileSystem.EncodingType.Base64 
      });
      base64 = res;
    }

    const newImage = {
      base64: base64,
      height: manipResult.height,
      width: manipResult.width,
    }

    // on Android, uri's are dynamic each time a file is uploaded. Therefore compare base64 instead.
    if (images.findIndex((image) => image.base64 === newImage.base64) !== -1) {
      return;
    }

    setImages((images) => [...images, newImage]);
  }

  const openCameraAsync = async () => {
    const permissionResult = await SystemImagePicker.requestCameraPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("Permission to access camera is required!");
      return;
    }

    const pickerResult = await SystemImagePicker.launchCameraAsync({base64: true});

    if (pickerResult.cancelled === true) {
      return;
    }

    const manipResult = await manipulateAsync(
      pickerResult.uri,
      [],
      { compress: 0, format: SaveFormat.JPEG, base64: true }
    );

    let base64 = manipResult.base64;

    if (!base64) {
      const res = await FileSystem.readAsStringAsync(manipResult.uri, { 
        encoding: FileSystem.EncodingType.Base64 
      });
      base64 = res;
    }

    const newImage = {
      base64: base64,
      height: manipResult.height,
      width: manipResult.width,
    }

    setImages((images) => [...images, newImage]);
  }

  const RenderItem: React.FC<{item: ImageType, index: number}> = ({ item, index }) => {
    const deleteImage = () => {
      setImages((images) => images.filter((_, _index) => _index !== index));
    }
  
    return (
      <View style={{
          display: "flex",
          height: "100%",
       }}>
        <Image
          style={{
            flex: 1,
            width: "100%",
            height: "100%",
            resizeMode: 'contain',
          }}
          source={{uri: `data:image/png;base64, ${item.base64}`}} 
        />
         <IconButton
          icon="delete"
          style={{
            position: "absolute",
            top: 0,
            right: 0,
          }}
          color={Colors.red500}
          size={30}
          onPress={deleteImage}
        />
      </View>
  
    )
  }

    return (
      <View style={styles.container}>
        <View style={styles.buttonContainer}>
            <Button 
              labelStyle={styles.buttonLabel} 
              style={styles.button} 
              contentStyle={styles.buttonContent} 
              mode="contained" 
              onPress={openImagePickerAsync}
              uppercase={false}
            >
              Last opp bilde
            </Button>
            <Button 
              labelStyle={styles.buttonLabel} 
              style={styles.button} 
              contentStyle={styles.buttonContent} 
              mode="contained" 
              onPress={openCameraAsync}
              uppercase={false}
            >
              Ta bilde
            </Button>
        </View>
        <View style={styles.imagesContainer}>
          <Text>Valgte bilder:</Text>
          <Carousel
              layout='stack'
              layoutCardOffset={30}
              data={images}
              renderItem={RenderItem}
              sliderWidth={300}
              itemWidth={100}
            />
        </View>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    width: "100%",
  },
  imagesContainer: {
    flex: 2,
    marginHorizontal: 5,
  },
  buttonContainer: {
    paddingVertical: 10,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  buttonLabel: {
    color: Colors.white,
  },
  buttonContent: {
  },
  button: {
    marginHorizontal: 5,
    borderRadius: 0,
    flex: 1,
  }
})

export default ImagePicker;
