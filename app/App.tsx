import React from 'react';
import { LogBox } from 'react-native';
import { Provider as PaperProvider } from "react-native-paper";
import theme from "./components/themes/Theme";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { RecoilRoot } from 'recoil';
import RecoilNexus from "recoil-nexus";

// first entry: ignore unmitigatable warning on Android
// second entry: reactNative-draggable-flatlist has unmitigatable warning
LogBox.ignoreLogs(["Setting a timer", 'ReactNativeFiberHostComponent: Calling getNode() on the ref of an Animated component is'])


// Components
import Navigation from "./components/Navigation";


export default function App() {
  return (
    <RecoilRoot>
      <RecoilNexus />
      <SafeAreaProvider>
          <PaperProvider theme={theme}>
            <Navigation />
          </PaperProvider>
      </SafeAreaProvider>
    </RecoilRoot>
  );
}