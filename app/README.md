# This is the App

## Running the app (emulator or through Expo)

You need to `Npm install` for both iOS and Android

_for more documentation from expo https://docs.expo.dev/workflow/customizing/_

### iOS

Prerequists:
1. You need a device running MacOS

Installation:
1. expo prebuild
2. expo run:ios

### Android

Android API v30 (Android Version 11 aka R) is NOT supported by Mapbox due to using system calls that are deprecated in API v30. Android API v29 (Android version 10 aka Q) is recommended instead - you should choose that system image in Android Studio. However a workaround is in place to still enable running on API v30 due to difficulty of downgrading a real device.

If app crashes immediately when opening on real device, add inside the `dependencies` field in `app/android/app/build.gradle`:

`implementation "com.mapbox.mapboxsdk:mapbox-android-telemetry:6.1.0"`

This should fix the problem.

**NOTE:** This field may disappear from the gradle file after running `expo prebuild`. There is no easy way around this. Generally you don't need to use prebuild unless major dependencies have been added to the project, so use `expo prebuild` sparingly.

___
Prerequisites:
1. Have Android Studio with emulator setup
OR
2. Have an Android phone connected over USB with developer mode enabled

Installation
1. expo prebuild
2. expo run:android

## Building the app

### Android (building an APK)

Prerequisites:
1. Install Android Studio

**Build steps:**

First you must eject the project from Expo.
`cd app/`, then run `expo eject`

Then:
1. Open Android Studio.
1. Open the 'app/android' foilder through Android Studio.
1. Go to Build > Select Build Variant.
1. In the topmost row, named "app", use the dropdown to change the active build variant to "release". This will change all modules to "release".
1. Go to Build > Build bundle(s) / APK -> Build APK(s)
1. Android Studio will begin building the .apk for you.
1. When finished, locate the folder where the APK was built. Android studio will show you its location. Open that directory in the terminal.
1. Connect your android phone via USB (make sure it has developer meode enabled).
1. Run command 'adb devices' to verify that your phone is connected and authorized to interface with ADB. You may have to give provide permission on your phone.
1. Run 'adb install <app-filename>.apk', depending on what the .apk file is named.
1. The app will install on your phone.
1. If successful, you are finished.

### iOS

Prerequisites:
1. A computer running Mac OS
2. An iPhone
3. Install XCode

**Build steps:**
1. Move the file "index.js" from /RUN-IOS-FIX/ to /node_modules/@react-native-community/cli-platform-ios/build/commands/runIOS/
2. Move the file "parseXtraceIOSDevicesList.js" from /RUN-IOS-FIX/ to /node_modules/@react-native-community/cli-platform-ios/build/commands/runIOS/
3. run the command ```yarn react-native run-ios --configuration Release --device "[device name]"```
4. You might possibly have to run ```pod install --repo-update``` to updaate the pods
5. You might have to remove notifcations permission from the xcworkspace file if you dont have an iOS development profile.